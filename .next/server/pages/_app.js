"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 4548:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./components/MyContext/IdDetailsContext.js
var IdDetailsContext = __webpack_require__(6790);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./components/common/route/index.js
var route = __webpack_require__(1191);
// EXTERNAL MODULE: ./components/common/Modal/invite&earn.jsx
var invite_earn = __webpack_require__(6716);
// EXTERNAL MODULE: ./components/common/Modal/signin.jsx + 1 modules
var signin = __webpack_require__(8709);
// EXTERNAL MODULE: ./components/common/Modal/signup.jsx + 9 modules
var signup = __webpack_require__(8524);
// EXTERNAL MODULE: ./components/helpers/auth_services.jsx
var auth_services = __webpack_require__(9062);
// EXTERNAL MODULE: ./components/common/Modal/AddCarModal/index.jsx + 10 modules
var AddCarModal = __webpack_require__(4375);
// EXTERNAL MODULE: ./components/helpers/service.jsx
var service = __webpack_require__(421);
// EXTERNAL MODULE: ./components/common/Search/index.jsx
var Search = __webpack_require__(4877);
;// CONCATENATED MODULE: ./components/Hearder/allPageHeader.jsx














const Logout = ({ logout  })=>{
    return(/*#__PURE__*/ _jsx("li", {
        role: "menuitem",
        onClick: logout,
        children: /*#__PURE__*/ _jsxs("a", {
            className: ` ${styles.dropDownItem} dropdown-item`,
            children: [
                /*#__PURE__*/ _jsx("img", {
                    className: styles.menuImg,
                    src: "/images/others/icon/sidebar/sign_out.png",
                    alt: ""
                }),
                "Sign Out",
                /*#__PURE__*/ _jsx("span", {
                    className: styles.floatRight
                })
            ]
        })
    }));
};
const AuthenticationHeader = ()=>{
    return(/*#__PURE__*/ _jsxs("ul", {
        className: `dropdown-menu ${styles.dropdownMenu} `,
        "aria-labelledby": "exampleModal1",
        children: [
            /*#__PURE__*/ _jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ _jsx(SignUp, {
                    className: `${styles.dropDownItem} dropdown-item`,
                    text: "Sign up",
                    style: styles
                })
            }),
            /*#__PURE__*/ _jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ _jsx(Login, {
                    className: `${styles.dropDownItem} dropdown-item`,
                    style: styles,
                    text: "Log in"
                })
            }),
            /*#__PURE__*/ _jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ _jsx(Link, {
                    href: Routes.Help.href,
                    as: Routes.Help.href,
                    children: /*#__PURE__*/ _jsx("a", {
                        className: `${styles.dropDownItem}  dropdown-item`,
                        children: "Help"
                    })
                })
            })
        ]
    }));
};
const AllPageHeader = ()=>{
    let token =  false && 0;
    const { userData  } = useContext(IdContext);
    const { 0: count , 1: setCount  } = useState("");
    const { 0: currentBooking , 1: setCurrentBooking  } = useState("");
    useEffect(()=>{
        token && notificationCount();
    }, []);
    const notificationCount = async ()=>{
        const response = await notification_count_service();
        console.log(response);
        if (response.code == 200) {
            setCount(response.notificationCount);
            setCurrentBooking(response.bookingRequest);
        }
    };
    const handleLogout = ()=>{
        logout_service();
    };
    return(/*#__PURE__*/ _jsx("div", {
        className: styles.header,
        children: /*#__PURE__*/ _jsxs("nav", {
            className: `navbar navbar-expand-lg navbar-dark fixed-top ${styles.mainNav} ${styles.mainNavFixedTop} `,
            children: [
                /*#__PURE__*/ _jsx("a", {
                    className: `navbar-brand  ${styles.navbarBrand}`,
                    href: "",
                    children: /*#__PURE__*/ _jsx("img", {
                        src: "/images/others/ejaro_logo_text.png",
                        alt: "Ejaro"
                    })
                }),
                /*#__PURE__*/ _jsx("button", {
                    className: "navbar-toggler navbar-toggler-right",
                    type: "button",
                    "data-toggle": "collapse",
                    "data-target": "#navbarResponsive",
                    "aria-controls": "navbarResponsive",
                    "aria-expanded": "false",
                    "aria-label": "Toggle navigation",
                    children: /*#__PURE__*/ _jsx("span", {
                        className: "navbar-toggler-icon"
                    })
                }),
                /*#__PURE__*/ _jsxs("div", {
                    className: `collapse navbar-collapse ${styles.navCollapse}`,
                    id: "navbarResponsive",
                    children: [
                        /*#__PURE__*/ _jsx("div", {
                            className: style.SeachFix,
                            children: /*#__PURE__*/ _jsx(SearchSec, {
                                style: style
                            })
                        }),
                        /*#__PURE__*/ _jsx("ul", {
                            className: `navbar-nav ml-auto ${styles.navbarNav}`,
                            children: /*#__PURE__*/ _jsx("li", {
                                className: `nav-item ${styles.navItem} `,
                                children: token == undefined || token == '' ? /*#__PURE__*/ _jsx(Login, {
                                    className: styles,
                                    text: "Share Your Vehicle"
                                }) : /*#__PURE__*/ _jsx(AddNewCar, {
                                    text: "Share Your Vehicle",
                                    style: `nav-link scroll ${styles.navLink}`
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsxs("div", {
                            className: ` ${styles.HeaderDiv} mt-2 mt-md-0 form-inline`,
                            children: [
                                /*#__PURE__*/ _jsxs("div", {
                                    className: `${styles.navDropdown}`,
                                    children: [
                                        /*#__PURE__*/ _jsxs("a", {
                                            className: `nav-link dropdown-toggle ${styles.navLink}`,
                                            id: "messagesDropdown",
                                            href: "#",
                                            "data-toggle": "dropdown",
                                            "aria-haspopup": "true",
                                            "aria-expanded": "false",
                                            children: [
                                                /*#__PURE__*/ _jsx("img", {
                                                    src: "/images/others/network_icon.png",
                                                    alt: "",
                                                    style: {
                                                        width: "16px"
                                                    }
                                                }),
                                                /*#__PURE__*/ _jsx("img", {
                                                    src: "/images/others/icon/arrow_down.png",
                                                    alt: "",
                                                    style: {
                                                        width: "15px"
                                                    }
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: `dropdown-menu ${styles.dropdownMenu}`,
                                            "aria-labelledby": "messagesDropdown",
                                            children: [
                                                /*#__PURE__*/ _jsx("a", {
                                                    className: `dropdown-item ${styles.dropdownItem}`,
                                                    href: "#",
                                                    children: "English"
                                                }),
                                                /*#__PURE__*/ _jsx("a", {
                                                    className: `dropdown-item ${styles.dropdownItem}`,
                                                    href: "#",
                                                    children: "عربى"
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ _jsxs("div", {
                                    className: `${styles.navProfileDropdown}`,
                                    children: [
                                        /*#__PURE__*/ _jsxs("a", {
                                            className: `nav-link dropdown-toggle ${styles.navLink}`,
                                            id: token ? "exampleModal" : "exampleModal1",
                                            href: "#",
                                            "data-toggle": "dropdown",
                                            "aria-haspopup": "true",
                                            "aria-expanded": "false",
                                            children: [
                                                /*#__PURE__*/ _jsx("img", {
                                                    src: "/images/others/menu_icon.png",
                                                    style: {
                                                        width: "15px",
                                                        marginTop: "17px",
                                                        float: "left",
                                                        display: "inline-block"
                                                    },
                                                    alt: ""
                                                }),
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: styles.profileImg,
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        alt: "",
                                                        src: (userData === null || userData === void 0 ? void 0 : userData.image) ? userData === null || userData === void 0 ? void 0 : userData.image : "/images/others/icon/userProfile.png"
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: styles.count,
                                                    children: "1"
                                                })
                                            ]
                                        }),
                                        token ? /*#__PURE__*/ _jsxs("ul", {
                                            className: `dropdown-menu ${styles.dropdownMenu}`,
                                            "aria-labelledby": "exampleModal",
                                            children: [
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.myCar.SharedCar.href,
                                                        as: Routes.myCar.SharedCar.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `${styles.bold} ${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/my_cars.png",
                                                                    alt: ""
                                                                }),
                                                                "My Cars",
                                                                /*#__PURE__*/ _jsxs("span", {
                                                                    className: styles.floatRight,
                                                                    children: [
                                                                        " 1",
                                                                        /*#__PURE__*/ _jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ _jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.Booking.RequestedBooking.as,
                                                        as: Routes.Booking.RequestedBooking.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `${styles.bold} ${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/booking_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Bookings",
                                                                /*#__PURE__*/ _jsxs("span", {
                                                                    className: styles.floatRight,
                                                                    children: [
                                                                        " ",
                                                                        currentBooking,
                                                                        /*#__PURE__*/ _jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ _jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.Notification.href,
                                                        as: Routes.Notification.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `${styles.bold} ${styles.dropDownItem}  dropdown-item`,
                                                            style: {
                                                                fontFamily: "NeoSansBold !important"
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/my_cars.png",
                                                                    alt: ""
                                                                }),
                                                                "Message",
                                                                /*#__PURE__*/ _jsxs("span", {
                                                                    className: styles.floatRight,
                                                                    children: [
                                                                        " ",
                                                                        count,
                                                                        /*#__PURE__*/ _jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ _jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.TrasctionList.href,
                                                        as: Routes.TrasctionList.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/wallet_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Wallet",
                                                                /*#__PURE__*/ _jsxs("span", {
                                                                    className: styles.floatRight,
                                                                    children: [
                                                                        " 21 ",
                                                                        /*#__PURE__*/ _jsx("span", {
                                                                            children: "SAR"
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.FavouriteList.href,
                                                        as: Routes.FavouriteList.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: ` ${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/favourite_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Favorites",
                                                                /*#__PURE__*/ _jsx("span", {
                                                                    className: styles.floatRight
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(InviteAndEarn, {
                                                        className: `${styles.dropDownItem} dropdown-item`,
                                                        style: styles
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.FavouriteList.href,
                                                        as: Routes.FavouriteList.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/setting.png",
                                                                    alt: ""
                                                                }),
                                                                "Settings"
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ _jsx(Link, {
                                                        href: Routes.Help.href,
                                                        as: Routes.Help.as,
                                                        children: /*#__PURE__*/ _jsxs("a", {
                                                            className: `$ ${styles.dropDownItem}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ _jsx("img", {
                                                                    className: styles.menuImg,
                                                                    src: "/images/others/icon/sidebar/help.png",
                                                                    alt: ""
                                                                }),
                                                                "Help"
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx(Logout, {
                                                    logout: handleLogout
                                                })
                                            ]
                                        }) : /*#__PURE__*/ _jsx(AuthenticationHeader, {
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const allPageHeader = ((/* unused pure expression or super */ null && (AllPageHeader)));

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
;// CONCATENATED MODULE: ./pages/_app.js






function MyApp({ Component , pageProps  }) {
    let router = (0,router_.useRouter)().asPath;
    console.log(router == route/* default.Auth.landing_page.as */.Z.Auth.landing_page.as);
    return(/*#__PURE__*/ jsx_runtime_.jsx(IdDetailsContext/* IdProvider */.v, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(Component, {
            ...pageProps
        })
    }));
}
/* harmony default export */ const _app = (MyApp);


/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 8557:
/***/ ((module) => {

module.exports = require("google-map-react");

/***/ }),

/***/ 4585:
/***/ ((module) => {

module.exports = require("jstz");

/***/ }),

/***/ 2245:
/***/ ((module) => {

module.exports = require("moment");

/***/ }),

/***/ 1788:
/***/ ((module) => {

module.exports = require("moment-hijri");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 8743:
/***/ ((module) => {

module.exports = require("react-datepicker");

/***/ }),

/***/ 6141:
/***/ ((module) => {

module.exports = require("react-image-crop");

/***/ }),

/***/ 9931:
/***/ ((module) => {

module.exports = require("react-modal");

/***/ }),

/***/ 2250:
/***/ ((module) => {

module.exports = require("react-onclickoutside");

/***/ }),

/***/ 5452:
/***/ ((module) => {

module.exports = require("react-phone-input-2");

/***/ }),

/***/ 2932:
/***/ ((module) => {

module.exports = require("react-popper");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [730,664,603,671,191,310], () => (__webpack_exec__(4548)));
module.exports = __webpack_exports__;

})();