"use strict";
exports.id = 687;
exports.ids = [687];
exports.modules = {

/***/ 8687:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4375);
/* harmony import */ var _styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2760);
/* harmony import */ var _styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_common_route__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1191);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1664);







const LinkTag = ({ activeClass , as , href , iconPath , name , count  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_5__["default"], {
            as: as,
            href: href,
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                className: activeClass,
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                    href: "#tabs-1",
                    role: "tab",
                    "data-toggle": "pill",
                    children: [
                        name,
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            children: [
                                count,
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    src: iconPath,
                                    style: {
                                        marginLeft: " 8px"
                                    }
                                })
                            ]
                        })
                    ]
                })
            })
        })
    }));
};
const CardSideNav = ({ shareCount , parkedCount  })=>{
    let { pathname  } = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
    let shared_car = pathname === _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.SharedCar.as */ .Z.myCar.SharedCar.as;
    let parked_car = pathname === _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.ParkedCar.as */ .Z.myCar.ParkedCar.as;
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default().showStatus),
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default().leftMenu),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                        children: "My Cars"
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                        className: `nav nav-pills nav-stacked ${(_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default().NavSec)}`,
                        role: "tablist",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkTag, {
                                activeClass: shared_car ? (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default().active) : "",
                                as: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.SharedCar.as */ .Z.myCar.SharedCar.as,
                                href: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.SharedCar.href */ .Z.myCar.SharedCar.href,
                                iconPath: shared_car ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png",
                                iconAlt: "Shared",
                                count: parkedCount,
                                name: "Shared"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkTag, {
                                activeClass: parked_car ? (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_6___default().active) : "",
                                as: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.ParkedCar.as */ .Z.myCar.ParkedCar.as,
                                href: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].myCar.ParkedCar.href */ .Z.myCar.ParkedCar.href,
                                count: shareCount,
                                iconPath: parked_car ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png",
                                iconAlt: "Parked",
                                name: "Parked"
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                        text: "Add a new car"
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardSideNav);


/***/ })

};
;