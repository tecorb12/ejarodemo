"use strict";
exports.id = 883;
exports.ids = [883];
exports.modules = {

/***/ 3972:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9931);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6191);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_myCar_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7883);






const DeleteModal = ({ text , carId , car_list , set_car_list , setCount , style  })=>{
    const { 0: open , 1: setOpen  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isDisable , 1: setDisabled  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const openClick = ()=>{
        setOpen(true);
    };
    const closeclick = ()=>{
        setOpen(false);
    };
    const deleteCar = async (e)=>{
        e.preventDefault();
        const response = await (0,_helpers_myCar_services__WEBPACK_IMPORTED_MODULE_3__/* .mycar_action_service */ .fW)(carId, "D");
        if (response.code == 200) {
            let temp_array = car_list === null || car_list === void 0 ? void 0 : car_list.filter((item)=>item.id != carId
            );
            set_car_list([
                ...temp_array
            ]);
            setCount((previous)=>previous - 1
            );
            setOpen(false);
        }
    };
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                onClick: openClick,
                children: text
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().customeModal)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().customeSignUpModal)}`,
                isOpen: open,
                onRequestClose: closeclick,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().dialogBox),
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().registrationSec)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().onlyHeading)}`,
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-12",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                onClick: closeclick,
                                                className: `float-right close mr-0 mt-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().colseBtn)}`,
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    children: "x"
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-12 text-center",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                children: "Do you want to delete car?"
                                            })
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    class: "row",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            class: "col-6 text-right",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                style: {
                                                    float: "unset"
                                                },
                                                className: ` mt-0 mr-2 mb-3 `,
                                                onClick: (e)=>{
                                                    deleteCar(e);
                                                },
                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().sendBtn),
                                                children: "Yes"
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            class: "col-6 text-right",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                style: {
                                                    float: "unset"
                                                },
                                                onClick: closeclick,
                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().backBtn),
                                                children: "No"
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DeleteModal);


/***/ }),

/***/ 3214:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9931);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6191);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _helpers_myCar_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7883);






const SharedParkedAction = ({ text , setSuccessresponse , setMessage , carId , car_list , set_car_list , setCount , title , requestFor  })=>{
    const { 0: open , 1: setOpen  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isDisable , 1: setDisabled  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const openClick = ()=>{
        setOpen(true);
    };
    const closeclick = ()=>{
        setOpen(false);
    };
    const deleteCar = async (e)=>{
        e.preventDefault();
        const response = await (0,_helpers_myCar_services__WEBPACK_IMPORTED_MODULE_3__/* .mycar_action_service */ .fW)(carId, requestFor, `Unlisting Car ${carId}`);
        console.log(response.code == 200);
        if (response.code == 200) {
            // let temp_array = car_list?.filter(item => item.id != carId)
            // set_car_list([...temp_array])
            // setCount((previous) => previous - 1)
            setOpen(false);
            setSuccessresponse(true);
        } else if (response.code == 422) {
            setOpen(false);
            setSuccessresponse(true);
            setMessage(response.message);
        }
    };
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                onClick: openClick,
                children: text
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().customeModal)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().customeSignUpModal)}`,
                isOpen: open,
                onRequestClose: closeclick,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().dialogBox),
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().registrationSec)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().onlyHeading)}`,
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-12",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                onClick: closeclick,
                                                className: `float-right close mr-0 mt-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().colseBtn)}`,
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    children: "x"
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-12 text-center",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                children: title
                                            })
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    class: "row",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            class: "col-6 text-right",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                style: {
                                                    float: "unset"
                                                },
                                                className: ` mt-0 mr-2 mb-3 `,
                                                onClick: (e)=>{
                                                    deleteCar(e);
                                                },
                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().sendBtn),
                                                children: "Yes"
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            class: "col-6 text-right",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                style: {
                                                    float: "unset"
                                                },
                                                onClick: closeclick,
                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_4___default().backBtn),
                                                children: "No"
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SharedParkedAction);


/***/ })

};
;