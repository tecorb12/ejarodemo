"use strict";
exports.id = 597;
exports.ids = [597];
exports.modules = {

/***/ 7597:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4375);
/* harmony import */ var _components_common_Modal_noCarDataModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2037);
/* harmony import */ var _styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2760);
/* harmony import */ var _styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3__);




const MyCar = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: `col ${(_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default().smallCol)}`,
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default().NodataSec),
            children: [
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default().heading),
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                            children: "Welcome to your Garage"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            children: "Sharing your vehicle takes only a few minutes and you’ll be ready to start earning!"
                        })
                    ]
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default().imageSec),
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                        src: "/images/others/icon/help/image_one.png",
                        alt: ""
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_common_Modal_noCarDataModal__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                    styles: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default())
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_styles_carProfile_module_scss__WEBPACK_IMPORTED_MODULE_3___default().formGroup)
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyCar);


/***/ })

};
;