exports.id = 310;
exports.ids = [310];
exports.modules = {

/***/ 9020:
/***/ ((module) => {

// Exports
module.exports = {
	"DayNamesList": "calendar_DayNamesList__Ys_RU",
	"DayName": "calendar_DayName__aZCnV",
	"HijriCalender": "calendar_HijriCalender__3s8UP",
	"HijriCalenderControls": "calendar_HijriCalenderControls__d_wHJ",
	"ControlButton": "calendar_ControlButton__t5WgQ"
};


/***/ }),

/***/ 5836:
/***/ ((module) => {

// Exports
module.exports = {
	"searchSec": "search_searchSec__s1s57",
	"inputArea": "search_inputArea__L1A7L",
	"single": "search_single__rBGRy",
	"inputWithIcon": "search_inputWithIcon__6Q8kw",
	"searchBtn": "search_searchBtn__VmONW",
	"white": "search_white__VbsI6",
	"red": "search_red__am_5m",
	"SeachFix": "search_SeachFix__DGKS3",
	"searchSection": "search_searchSection__Q6Pkm"
};


/***/ }),

/***/ 6716:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9931);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6191);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3__);




const InviteAndEarn = ({ className , style  })=>{
    const { 0: open , 1: setOpen  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const openModal = ()=>{
        setOpen(true);
    };
    const closeModal = ()=>{
        setOpen(false);
    };
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                onClick: openModal,
                className: className,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                        src: "/images/others/icon/sidebar/invite_earn.png",
                        alt: "",
                        style: {
                            width: "14px",
                            marginRight: "15px"
                        }
                    }),
                    "Invite & Earn"
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeModal)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeMoneyModal)}  ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeAccountModal)}`,
                isOpen: open,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().genrateLink),
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().genrateLinkSec),
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeFormGroup)}`,
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                            children: "Template Type"
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                            class: "form-control",
                                            name: "maritalStatus",
                                            size: "0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                    value: "01",
                                                    children: "Mr."
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                    value: "02",
                                                    children: "Mrs."
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeFormGroup)}`,
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                            children: "Template Title"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                            type: "text",
                                            id: "contact",
                                            name: "contact",
                                            placeholder: "Plase enter Mobile Number",
                                            className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().stepbarInput)}`
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    class: "row",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        class: "col-12",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().sendBtn),
                                            children: "Save"
                                        })
                                    })
                                })
                            ]
                        })
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (InviteAndEarn);


/***/ }),

/***/ 6683:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


// keyCode constants
const BACKSPACE = 8;
const LEFT_ARROW = 37;
const RIGHT_ARROW = 39;
const DELETE = 46;
const SPACEBAR = 32;
// Doesn't really check if it's a style Object
// Basic implementation to check if it's not a string
// of classNames and is an Object
// TODO: Better implementation
const isStyleObject = (obj)=>typeof obj === 'object'
;
class SingleOtpInput extends react__WEBPACK_IMPORTED_MODULE_1__.PureComponent {
    // input: ?HTMLInputElement;
    // Focus on first render
    // Only when shouldAutoFocus is true
    componentDidMount() {
        const { input , props: { focus , shouldAutoFocus  } ,  } = this;
        if (input && focus && shouldAutoFocus) {
            input.focus();
        }
    }
    componentDidUpdate(prevProps) {
        const { input , props: { focus  } ,  } = this;
        // Check if focusedInput changed
        // Prevent calling function if input already in focus
        if (prevProps.focus !== focus && input && focus) {
            input.focus();
            input.select();
        }
    }
    getClasses = (...classes)=>classes.filter((c)=>!isStyleObject(c) && c !== false
        ).join(' ')
    ;
    getType = ()=>{
        if (this.props.isInputSecure) {
            return 'password';
        }
        if (this.props.isInputNum) {
            return 'tel';
        }
        return 'text';
    };
    render() {
        const { placeholder , separator , isLastChild , inputStyle , focus , isDisabled , hasErrored , errorStyle , focusStyle , disabledStyle , shouldAutoFocus , isInputNum , index , value , className , isInputSecure , ...rest } = this.props;
        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                    "aria-label": `${index === 0 ? 'Please enter verification code. ' : ''}${isInputNum ? 'Digit' : 'Character'} ${index + 1}`,
                    autoComplete: "off",
                    style: Object.assign({
                        textAlign: 'center'
                    }, isStyleObject(inputStyle) && inputStyle, focus && isStyleObject(focusStyle) && focusStyle, isDisabled && isStyleObject(disabledStyle) && disabledStyle, hasErrored && isStyleObject(errorStyle) && errorStyle),
                    placeholder: placeholder,
                    className: this.getClasses(inputStyle, focus && focusStyle, isDisabled && disabledStyle, hasErrored && errorStyle),
                    type: this.getType(),
                    maxLength: "1",
                    ref: (input)=>{
                        this.input = input;
                    },
                    disabled: isDisabled,
                    value: value ? value : '',
                    ...rest
                }),
                !isLastChild && separator
            ]
        }));
    }
}
class OtpInput extends react__WEBPACK_IMPORTED_MODULE_1__.Component {
    static defaultProps = {
        numInputs: 4,
        onChange: (otp)=>console.log(otp)
        ,
        isDisabled: false,
        shouldAutoFocus: false,
        value: '',
        isInputSecure: false
    };
    state = {
        activeInput: 0
    };
    getOtpValue = ()=>this.props.value ? this.props.value.toString().split('') : []
    ;
    getPlaceholderValue = ()=>{
        const { placeholder , numInputs  } = this.props;
        if (typeof placeholder === 'string') {
            if (placeholder.length === numInputs) {
                return placeholder;
            }
            if (placeholder.length > 0) {
                console.error('Length of the placeholder should be equal to the number of inputs.');
            }
        }
    };
    // Helper to return OTP from input
    handleOtpChange = (otp)=>{
        const { onChange  } = this.props;
        const otpValue = otp.join('');
        onChange(otpValue);
    };
    isInputValueValid = (value)=>{
        const isTypeValid = this.props.isInputNum ? !isNaN(parseInt(value, 10)) : typeof value === 'string';
        return isTypeValid && value.trim().length === 1;
    };
    // Focus on input by index
    focusInput = (input)=>{
        const { numInputs  } = this.props;
        const activeInput = Math.max(Math.min(numInputs - 1, input), 0);
        this.setState({
            activeInput
        });
    };
    // Focus on next input
    focusNextInput = ()=>{
        const { activeInput  } = this.state;
        this.focusInput(activeInput + 1);
    };
    // Focus on previous input
    focusPrevInput = ()=>{
        const { activeInput  } = this.state;
        this.focusInput(activeInput - 1);
    };
    // Change OTP value at focused input
    changeCodeAtFocus = (value)=>{
        const { activeInput  } = this.state;
        const otp = this.getOtpValue();
        otp[activeInput] = value[0];
        this.handleOtpChange(otp);
    };
    // Handle pasted OTP
    handleOnPaste = (e)=>{
        e.preventDefault();
        const { activeInput  } = this.state;
        const { numInputs , isDisabled  } = this.props;
        if (isDisabled) {
            return;
        }
        const otp = this.getOtpValue();
        let nextActiveInput = activeInput;
        // Get pastedData in an array of max size (num of inputs - current position)
        const pastedData = e.clipboardData.getData('text/plain').slice(0, numInputs - activeInput).split('');
        // Paste data from focused input onwards
        for(let pos = 0; pos < numInputs; ++pos){
            if (pos >= activeInput && pastedData.length > 0) {
                otp[pos] = pastedData.shift();
                nextActiveInput++;
            }
        }
        this.setState({
            activeInput: nextActiveInput
        }, ()=>{
            this.focusInput(nextActiveInput);
            this.handleOtpChange(otp);
        });
    };
    handleOnChange = (e)=>{
        const { value  } = e.target;
        if (this.isInputValueValid(value)) {
            this.changeCodeAtFocus(value);
        }
    };
    // Handle cases of backspace, delete, left arrow, right arrow, space
    handleOnKeyDown = (e)=>{
        if (e.keyCode === BACKSPACE || e.key === 'Backspace') {
            e.preventDefault();
            this.changeCodeAtFocus('');
            this.focusPrevInput();
        } else if (e.keyCode === DELETE || e.key === 'Delete') {
            e.preventDefault();
            this.changeCodeAtFocus('');
        } else if (e.keyCode === LEFT_ARROW || e.key === 'ArrowLeft') {
            e.preventDefault();
            this.focusPrevInput();
        } else if (e.keyCode === RIGHT_ARROW || e.key === 'ArrowRight') {
            e.preventDefault();
            this.focusNextInput();
        } else if (e.keyCode === SPACEBAR || e.key === ' ' || e.key === 'Spacebar' || e.key === 'Space') {
            e.preventDefault();
        }
    };
    // The content may not have changed, but some input took place hence change the focus
    handleOnInput = (e)=>{
        if (this.isInputValueValid(e.target.value)) {
            this.focusNextInput();
        } else {
            // This is a workaround for dealing with keyCode "229 Unidentified" on Android.
            if (!this.props.isInputNum) {
                const { nativeEvent  } = e;
                if (nativeEvent.data === null && nativeEvent.inputType === 'deleteContentBackward') {
                    e.preventDefault();
                    this.changeCodeAtFocus('');
                    this.focusPrevInput();
                }
            }
        }
    };
    renderInputs = ()=>{
        const { activeInput  } = this.state;
        const { numInputs , inputStyle , focusStyle , separator , isDisabled , disabledStyle , hasErrored , errorStyle , shouldAutoFocus , isInputNum , isInputSecure , className ,  } = this.props;
        const inputs = [];
        const otp = this.getOtpValue();
        const placeholder = this.getPlaceholderValue();
        for(let i = 0; i < numInputs; i++){
            inputs.push(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(SingleOtpInput, {
                placeholder: placeholder && placeholder[i],
                index: i,
                focus: activeInput === i,
                value: otp && otp[i],
                onChange: this.handleOnChange,
                onKeyDown: this.handleOnKeyDown,
                onInput: this.handleOnInput,
                onPaste: this.handleOnPaste,
                onFocus: (e)=>{
                    this.setState({
                        activeInput: i
                    });
                    e.target.select();
                },
                onBlur: ()=>this.setState({
                        activeInput: -1
                    })
                ,
                separator: separator,
                inputStyle: inputStyle,
                focusStyle: focusStyle,
                isLastChild: i === numInputs - 1,
                isDisabled: isDisabled,
                disabledStyle: disabledStyle,
                hasErrored: hasErrored,
                errorStyle: errorStyle,
                shouldAutoFocus: shouldAutoFocus,
                isInputNum: isInputNum,
                isInputSecure: isInputSecure,
                className: className
            }, i));
        }
        return inputs;
    };
    render() {
        const { containerStyle  } = this.props;
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: `col-12 `,
                children: this.renderInputs()
            })
        }));
    }
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (OtpInput);


/***/ }),

/***/ 8709:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ signin)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-modal"
var external_react_modal_ = __webpack_require__(9931);
var external_react_modal_default = /*#__PURE__*/__webpack_require__.n(external_react_modal_);
// EXTERNAL MODULE: ./styles/customeModal.module.scss
var customeModal_module = __webpack_require__(6191);
var customeModal_module_default = /*#__PURE__*/__webpack_require__.n(customeModal_module);
// EXTERNAL MODULE: ./components/helpers/auth_services.jsx
var auth_services = __webpack_require__(9062);
// EXTERNAL MODULE: ./components/common/validation/index.js
var validation = __webpack_require__(1396);
;// CONCATENATED MODULE: ./components/common/Modal/forgot.jsx







const Forgot = ({ className , style , close  })=>{
    const { 0: email , 1: setEmail  } = (0,external_react_.useState)("");
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { 0: isError , 1: setError  } = (0,external_react_.useState)({
        emailErrorMsg: "",
        isDisabledEmail: false,
        code: "",
        msg: ""
    });
    const openModal = ()=>{
        setOpen(true);
    };
    const closeModal = ()=>{
        setOpen(false);
    };
    const handleClick = async (e)=>{
        e.preventDefault();
        const response = await (0,auth_services/* forget_password_service */.Vy)(email);
        if (response.code == 200) {
            setOpen(false);
            close(false);
            global.forget = true;
        }
    };
    const validForm = (fieldName, Value)=>{
        let EmailPhoneErrorMsg = isError.emailErrorMsg;
        let isDisabledEmail_phone = isError.isDisabledEmail;
        switch(fieldName){
            case "email":
                EmailPhoneErrorMsg = (0,validation/* validateEmail */.oH)(Value).error;
                isDisabledEmail_phone = (0,validation/* validateEmail */.oH)(Value).isDisabled;
                break;
            default:
                break;
        }
        setError((prev)=>{
            return {
                ...prev,
                isDisabledEmail: isDisabledEmail_phone
            };
        });
        if (isDisabledEmail_phone) {
            setDisabled(true);
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                onClick: openModal,
                className: className,
                style: {
                    textAlign: "right"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                        className: style.floatRight
                    }),
                    "Forgot your Password ?"
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
                className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeLoginModal}`,
                isOpen: open,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (customeModal_module_default()).dialogBox,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (customeModal_module_default()).loginSec,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "row mx-0",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: `col-md-12 col-lg-12 ${(customeModal_module_default()).loginPadding}`,
                                    style: {
                                        paddingTop: "3rem"
                                    },
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            onClick: closeModal,
                                            className: `float-right close mr-0 mt-0 ${(customeModal_module_default()).close}`,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                children: "x"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                            className: "mb-4",
                                            style: {
                                                color: "#b2020f"
                                            },
                                            children: "Forgot Your Password"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "mb-4",
                                            children: "Enter your email address and we will send you a link to reset your password "
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup} mb-3`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Email Address"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                            type: "text",
                                                            id: "contact",
                                                            name: "email",
                                                            value: email,
                                                            placeholder: "Enter email address",
                                                            onChange: (e)=>{
                                                                setEmail(e.target.value);
                                                                validForm("email", e.target.value);
                                                            },
                                                            className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/images/others/icon/email_icon.svg",
                                                            style: {
                                                                width: "10px"
                                                            }
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    class: "row",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            class: "col-12",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                disabled: !isDisabled,
                                                                className: (customeModal_module_default()).sendBtn,
                                                                onClick: handleClick,
                                                                children: "SEND"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-12",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx(signin, {
                                                                loginstyle: `${style.sendBtn1}`,
                                                                text: "Login",
                                                                text1: "Register Now"
                                                            })
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (customeModal_module_default()).smallHide,
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (customeModal_module_default()).bannerSec,
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: (customeModal_module_default()).detailText,
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h6", {
                                                children: [
                                                    " Share your car and earn",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {
                                                    }),
                                                    "an aditional income with ejaro"
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: (customeModal_module_default()).detailTextFooter,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                children: "A Trustworthy Community"
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const forgot = (Forgot);

// EXTERNAL MODULE: ./components/common/Modal/signup.jsx + 9 modules
var signup = __webpack_require__(8524);
;// CONCATENATED MODULE: ./components/common/Modal/signin.jsx








const Login = ({ className , style , text , src , loginstyle , close  })=>{
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    const { 0: email , 1: setEmail  } = (0,external_react_.useState)("");
    const { 0: password , 1: setPassword  } = (0,external_react_.useState)("");
    const { 0: isError , 1: setError  } = (0,external_react_.useState)({
        emailErrorMsg: "",
        isDisabledEmail: false,
        passwordErrorMsg: "",
        isDisabledPassword: false,
        code: "",
        msg: ""
    });
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const openModal = (e)=>{
        e.preventDefault();
        close;
        setOpen(true);
    };
    const closeModal = ()=>{
        setOpen(false);
    };
    const validForm = (fieldName, Value)=>{
        let EmailPhoneErrorMsg = isError.emailErrorMsg;
        let isDisabledEmail_phone = isError.isDisabledEmail;
        let PasswordErrorMsg = isError.passwordErrorMsg;
        let isDisabledPassword = isError.isDisabledPassword;
        switch(fieldName){
            case "email":
                EmailPhoneErrorMsg = (0,validation/* validateEmail */.oH)(Value).error;
                isDisabledEmail_phone = (0,validation/* validateEmail */.oH)(Value).isDisabled;
                break;
            case "password":
                PasswordErrorMsg = (0,validation/* validatePassword */.uo)(Value).error;
                isDisabledPassword = (0,validation/* validatePassword */.uo)(Value).isDisabled;
                break;
            default:
                break;
        }
        setError((prev)=>{
            return {
                ...prev,
                isDisabledEmail: isDisabledEmail_phone,
                isDisabledPassword: isDisabledPassword
            };
        });
    };
    const DisabledData = ()=>{
        setError((prev)=>{
            return {
                ...prev,
                msg: "",
                code: ""
            };
        });
        if (isError.isDisabledEmail && isError.isDisabledPassword) {
            setDisabled(true);
        } else {
            setDisabled(false);
        }
    };
    const handleLogin = async (event)=>{
        event.preventDefault();
        const response = await (0,auth_services/* login_service */.zX)(email, password);
        console.log(response);
        if (response.code == 404) {
            setError((prev)=>{
                return {
                    ...prev,
                    code: response.code,
                    msg: response.message
                };
            });
        }
        if (response.code == 401) {
            setError((prev)=>{
                return {
                    ...prev,
                    code: response.code,
                    msg: "Unauthorized access"
                };
            });
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                onClick: openModal,
                className: `${className} ${loginstyle} `,
                children: [
                    text,
                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                        src: src
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
                className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeLoginModal}`,
                isOpen: open,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (customeModal_module_default()).dialogBox,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (customeModal_module_default()).loginSec,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "row mx-0",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: `col-md-12 col-lg-12 ${(customeModal_module_default()).loginPadding}`,
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            onClick: closeModal,
                                            className: `float-right close mr-0 mt-0 ${(customeModal_module_default()).close}`,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                children: "x"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                            children: "Welcome back!"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            children: "Login to your account"
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            style: {
                                                                color: "#b2020f"
                                                            },
                                                            children: isError.code == 404 && isError.msg
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            style: {
                                                                color: "#b2020f"
                                                            },
                                                            children: isError.code == 401 && isError.msg
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Email Address"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                            type: "text",
                                                            id: "contact",
                                                            name: "email",
                                                            value: email,
                                                            onChange: (e)=>{
                                                                setEmail(e.target.value);
                                                                validForm("email", e.target.value);
                                                                DisabledData();
                                                            },
                                                            placeholder: "Enter email address",
                                                            className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/images/others/icon/email_icon.svg",
                                                            style: {
                                                                width: "10px"
                                                            }
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Password"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                            type: "password",
                                                            id: "contact",
                                                            name: "password",
                                                            value: password,
                                                            autoComplete: "off",
                                                            placeholder: "Password",
                                                            onChange: (e)=>{
                                                                setPassword(e.target.value);
                                                                validForm("password", e.target.value);
                                                                DisabledData();
                                                            },
                                                            className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/images/others/icon/password_icon.svg",
                                                            style: {
                                                                width: "7px"
                                                            }
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "text-right",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(forgot, {
                                                        style: (customeModal_module_default()),
                                                        close: setOpen
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    class: "row",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            class: "col-12",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                disabled: !isDisabled,
                                                                className: (customeModal_module_default()).sendBtn,
                                                                onClick: handleLogin,
                                                                children: "Sign in"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-12",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx(signup/* default */.Z, {
                                                                style: (customeModal_module_default()),
                                                                text: "Don't have an account?",
                                                                text1: "Register Now"
                                                            })
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: (customeModal_module_default()).smallHide,
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (customeModal_module_default()).bannerSec,
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: (customeModal_module_default()).detailText,
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h6", {
                                                children: [
                                                    " Share your car and earn",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {
                                                    }),
                                                    "an aditional income with ejaro"
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: (customeModal_module_default()).detailTextFooter,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                children: "A Trustworthy Community"
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const signin = (Login);


/***/ }),

/***/ 8524:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ signup)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "react-modal"
var external_react_modal_ = __webpack_require__(9931);
var external_react_modal_default = /*#__PURE__*/__webpack_require__.n(external_react_modal_);
// EXTERNAL MODULE: ./styles/customeModal.module.scss
var customeModal_module = __webpack_require__(6191);
var customeModal_module_default = /*#__PURE__*/__webpack_require__.n(customeModal_module);
// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__(2245);
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "moment-hijri"
var external_moment_hijri_ = __webpack_require__(1788);
var external_moment_hijri_default = /*#__PURE__*/__webpack_require__.n(external_moment_hijri_);
// EXTERNAL MODULE: external "react-popper"
var external_react_popper_ = __webpack_require__(2932);
// EXTERNAL MODULE: external "react-onclickoutside"
var external_react_onclickoutside_ = __webpack_require__(2250);
var external_react_onclickoutside_default = /*#__PURE__*/__webpack_require__.n(external_react_onclickoutside_);
// EXTERNAL MODULE: ./styles/calendar.module.css
var calendar_module = __webpack_require__(9020);
var calendar_module_default = /*#__PURE__*/__webpack_require__.n(calendar_module);
;// CONCATENATED MODULE: ./components/Auth/lib/components/DayNames.js



class DayNames extends external_react_.Component {
    state = {
        arabicDayNames: [
            'ح',
            'ن',
            'ث',
            'ر',
            'خ',
            'ج',
            'س'
        ],
        arabicFullDayNames: [
            'احد',
            'اثنين',
            'ثلاثاء',
            'اربعاء',
            'خميس',
            'جمعة',
            'سبت'
        ],
        englishDayNames: [
            'Su',
            'Mo',
            'Tu',
            'We',
            'Th',
            'Fr',
            'Sa'
        ],
        arabicshortName: [
            'Ith',
            'Thu',
            'Arb',
            'Kha',
            'Jum',
            'Sab',
            'Ahd'
        ]
    };
    render() {
        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: (calendar_module_default()).DayNamesList,
            children: this.state.arabicshortName.map((item, key)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (calendar_module_default()).DayName,
                    children: item
                }, key.toString())
            )
        }));
    }
}
/* harmony default export */ const components_DayNames = (DayNames);

;// CONCATENATED MODULE: ./components/Auth/lib/components/MonthsList.js



const MonthListContainer = (external_styled_components_default()).span`
  padding: 5px;
`;
const MonthSelect = (external_styled_components_default()).select`
  width: 100px;
  -webkit-appearance: menulist-button;
  background: transparent;
  height: 25px;
  border-radius: 4px;
  font-family: sans-serif;
  font-size: 14px;
`;
class MonthsList extends external_react_.Component {
    state = {
        months: [
            {
                number: 0,
                name: 'Muharram'
            },
            {
                number: 1,
                name: 'Safar'
            },
            {
                number: 2,
                name: "Rabi'ul-Awwal"
            },
            {
                number: 3,
                name: "Rabi'ul-Akhir"
            },
            {
                number: 4,
                name: 'Jumadal-Ula'
            },
            {
                number: 5,
                name: 'Jumadal-Akhir'
            },
            {
                number: 6,
                name: 'Rajab'
            },
            {
                number: 7,
                name: "Sha'ban"
            },
            {
                number: 8,
                name: 'Ramadan'
            },
            {
                number: 9,
                name: 'Syawwal'
            },
            {
                number: 10,
                name: "Dhul-Qa'da"
            },
            {
                number: 11,
                name: 'Dhul-Hijja'
            }, 
        ]
    };
    render() {
        return(/*#__PURE__*/ jsx_runtime_.jsx(MonthListContainer, {
            children: /*#__PURE__*/ jsx_runtime_.jsx(MonthSelect, {
                onChange: this.props.onChange,
                value: this.props.currentTime.iMonth(),
                children: this.state.months.map((item, key)=>/*#__PURE__*/ jsx_runtime_.jsx("option", {
                        value: item.number,
                        children: item.name
                    }, item.number)
                )
            })
        }));
    }
}
/* harmony default export */ const components_MonthsList = (MonthsList);

;// CONCATENATED MODULE: ./components/Auth/lib/components/YearsList.js

// Hijri year (1356 to 1500)


const YearListContainer = (external_styled_components_default()).span`
  padding: 5px;
`;
const YearSelect = (external_styled_components_default()).select`
  width: 100px;
  -webkit-appearance: menulist-button;
  background: transparent;
  height: 25px;
  border-radius: 4px;
  font-family: sans-serif;
  font-size: 12px;
`;
class YearsList extends external_react_.Component {
    state = {
        minYear: 1356,
        maxYear: 1500
    };
    render() {
        let yearsList = [];
        // Generate a select options of all supported years
        for(let i = this.state.minYear; i <= this.state.maxYear; i = i + 1){
            yearsList.push(/*#__PURE__*/ jsx_runtime_.jsx("option", {
                value: i,
                children: i
            }, i));
        }
        return(/*#__PURE__*/ jsx_runtime_.jsx(YearListContainer, {
            children: /*#__PURE__*/ jsx_runtime_.jsx(YearSelect, {
                onChange: this.props.onChange,
                value: this.props.currentTime.iYear(),
                children: yearsList
            })
        }));
    }
}
/* harmony default export */ const components_YearsList = (YearsList);

;// CONCATENATED MODULE: ./components/Auth/lib/components/MonthDaysView.js



const MonthDays = (external_styled_components_default()).div`
  text-align: right;
  display: flex;
  flex-flow: wrap;
`;
const MonthDay = (external_styled_components_default()).div`
  margin: 2px;
  width: 32px;
  padding: 1px;
  border: 1px solid #fff;
  text-align: center;
  -webkit-box-sizing: unset !important;
  box-sizing: unset;
  border-radius: 4px;
color: #54708b;
text-align: center;
position: relative;
font-family: NeoSansRegular;
padding: 0;
  :hover {
    ${(props)=>props.noHover ? '' : "border: 1px solid transparent"
};
  };
`;
const MonthDayButton = (external_styled_components_default()).button`
  cursor: pointer;
  border: 0px;
  padding: 5px;
  width: 2.505vw;
height: 25px;
line-height: 25px;
border-radius: 5px;
margin-bottom: 1px;
margin-top: 1px;
font-size: .65vw;
  :focus {
    outline: unset;
  }
  border-radius: 4px;
  background-color: ${(props)=>props.selected ? "black;" : "white;"
};
  color: ${(props)=>props.selected ? "white;" : ""
};
  
`;
class MonthDaysView extends (external_react_default()).Component {
    state = {
        englishDayNames: [
            'Su',
            'Mo',
            'Tu',
            'We',
            'Th',
            'Fr',
            'Sa'
        ]
    };
    getMonthStartDayName = ()=>{
        let time = this.props.currentTime;
        time.startOf('iMonth');
        return time.format('dd');
    };
    monthDays = ()=>{
        return this.props.currentTime.iDaysInMonth();
    };
    isSelectedDate = (i)=>{
        let time = this.props.currentTime;
        time.iDate(parseInt(i, 10));
        return this.props.selectedDate === time.format(this.props.dateFormat);
    };
    render() {
        let daysList = [];
        for(let i = this.state.englishDayNames.indexOf(this.getMonthStartDayName()); i > 0; i--){
            daysList.push(/*#__PURE__*/ jsx_runtime_.jsx(MonthDay, {
                noHover: true
            }, daysList.length.toString()));
        }
        for(let i1 = 1; i1 < this.monthDays() + 1; i1++){
            daysList.push(/*#__PURE__*/ jsx_runtime_.jsx(MonthDay, {
                selected: this.isSelectedDate(i1),
                children: /*#__PURE__*/ jsx_runtime_.jsx(MonthDayButton, {
                    selected: this.isSelectedDate(i1),
                    onClick: this.props.setSelectedDate,
                    value: i1,
                    type: "button",
                    children: i1
                })
            }, daysList.length.toString()));
        }
        return(/*#__PURE__*/ jsx_runtime_.jsx(MonthDays, {
            children: daysList
        }));
    }
}
/* harmony default export */ const components_MonthDaysView = (MonthDaysView);

;// CONCATENATED MODULE: ./components/Auth/lib/components/HijriDatePicker.js











const ControlButton = (external_styled_components_default()).button`
  position: absolute;
  border: 0px;
  font-weight: bold;
  font-size: 15px;
  cursor: pointer;
  // background-color: #fff;
  :hover {
    color: #888888
  }
  :focus {
    outline: unset
  }
`;
const PreviousButton = external_styled_components_default()(ControlButton)`
  right: 15px;
`;
const NextButton = external_styled_components_default()(ControlButton)`
  left: 15px;
`;
const MonthName = (external_styled_components_default()).strong`
`;
const YearAndMonthList = (external_styled_components_default()).div`
  margin-top: 10px;
`;
class HijriDatePicker extends external_react_.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedDate: props.selectedDate || "",
            dateFormat: props.dateFormat || 'iYYYYiDD/iMM/',
            currentTime: external_moment_hijri_default()(),
            calenderShown: false
        };
    }
    componentDidMount() {
        if (this.state.selectedDate) {
            this.setState({
                currentTime: external_moment_hijri_default()(this.state.selectedDate, this.state.dateFormat)
            });
        }
    }
    componentDidUpdate(prevProps) {
        const { selectedDate: prevSelectedDate  } = prevProps;
        const { selectedDate: nextSelectedDate  } = this.props;
        if (prevSelectedDate !== nextSelectedDate) {
            this.setState({
                ...this.state,
                selectedDate: nextSelectedDate
            });
        }
    }
    handleClickOutside = (evt)=>{
        this.setState({
            calenderShown: false
        });
    };
    subtractMonth = ()=>{
        this.setState((prevState)=>({
                currentTime: prevState.currentTime.subtract(1, 'iMonth')
            })
        );
    };
    addMonth = ()=>{
        this.setState((prevState)=>({
                currentTime: prevState.currentTime.add(1, 'iMonth')
            })
        );
    };
    setSelectedDate = (event)=>{
        let time = this.state.currentTime;
        time.iDate(parseInt(event.target.value, 10));
        const selectedDate = time.format(this.state.dateFormat);
        this.setState({
            selectedDate,
            calenderShown: false
        });
        this.handleChange(selectedDate);
    };
    getMonthStartDayName = ()=>{
        let time = this.state.currentTime;
        time.startOf('iMonth');
        return time.format('dd');
    };
    handleFocus = (event)=>{
        const { onFocus =()=>{
        }  } = this.props;
        onFocus(event.target.value);
        this.showCalender();
    };
    handleChange = (value)=>{
        const { onChange =()=>{
        }  } = this.props;
        onChange(value);
    };
    showCalender = ()=>{
        this.setState({
            calenderShown: true
        });
    };
    handelMonthChange = (event)=>{
        let time = this.state.currentTime;
        time.iMonth(parseInt(event.target.value, 10));
        this.setState({
            currentTime: time
        });
    };
    handelYearChange = (event)=>{
        let time = this.state.currentTime;
        time.iYear(parseInt(event.target.value, 10));
        this.setState({
            currentTime: time
        });
    };
    handelOnChange = (event)=>{
    // 
    };
    renderYearAndMonthList() {
    }
    render() {
        const { className , name , placeholder , input , disabled  } = this.props;
        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_popper_.Manager, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_popper_.Reference, {
                        children: ({ ref  })=>/*#__PURE__*/ jsx_runtime_.jsx("input", {
                                type: "text",
                                autoComplete: "off",
                                className,
                                name,
                                placeholder,
                                disabled,
                                ...input,
                                value: this.state.selectedDate,
                                ref: ref,
                                onFocus: this.handleFocus,
                                readOnly: true
                            })
                    }),
                    this.state.calenderShown && /*#__PURE__*/ jsx_runtime_.jsx(external_react_popper_.Popper, {
                        placement: "bottom",
                        modifiers: {
                            hide: {
                                enabled: true
                            },
                            preventOverflow: {
                                enabled: true,
                                boundariesElement: 'viewport'
                            }
                        },
                        children: ({ ref , style , placement , arrowProps  })=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (calendar_module_default()).HijriCalender,
                                    ref: ref,
                                    style: style,
                                    "data-placement": placement,
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: (calendar_module_default()).HijriCalenderControls,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx(PreviousButton, {
                                                    onClick: this.subtractMonth,
                                                    type: "button",
                                                    children: '<'
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx(MonthName, {
                                                    children: ' (' + this.state.currentTime.format('iMM') + ') ' + this.state.currentTime.format('iYYYY')
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(NextButton, {
                                                    onClick: this.addMonth,
                                                    type: "button",
                                                    children: [
                                                        " ",
                                                        '>',
                                                        " "
                                                    ]
                                                }),
                                                this.props.quickSelect && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(YearAndMonthList, {
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(components_YearsList, {
                                                            currentTime: this.state.currentTime,
                                                            onChange: this.handelYearChange
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(components_MonthsList, {
                                                            currentTime: this.state.currentTime,
                                                            onChange: this.handelMonthChange
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(components_DayNames, {
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(components_MonthDaysView, {
                                            currentTime: this.state.currentTime,
                                            dateFormat: this.state.dateFormat,
                                            selectedDate: this.state.selectedDate,
                                            setSelectedDate: this.setSelectedDate
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            ref: arrowProps.ref,
                                            style: arrowProps.style
                                        })
                                    ]
                                })
                            })
                    })
                ]
            })
        }));
    }
}
/* harmony default export */ const components_HijriDatePicker = (external_react_onclickoutside_default()(HijriDatePicker));

;// CONCATENATED MODULE: ./components/Auth/lib/index.js

/* harmony default export */ const lib = (components_HijriDatePicker);

;// CONCATENATED MODULE: ./components/Auth/demo/app.js





const app_HijriDatePicker = ({ selectedDate , setDate  })=>{
    const onChange = (value)=>{
        console.log('OnChange -> Value is: ', value);
        setDate(value);
    };
    const onFocus = (value)=>{
        console.log('OnFocus -> Value is: ', value);
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            id: "app",
            children: /*#__PURE__*/ jsx_runtime_.jsx(lib, {
                name: "hijri_date",
                className: "input-style",
                placeholder: "DOB",
                selectedDate: selectedDate,
                dateFormat: "iDD-iMM-iYYYY",
                onChange: onChange,
                onFocus: onFocus,
                quickSelect: true
            })
        })
    }));
};
/* harmony default export */ const app = (app_HijriDatePicker);

// EXTERNAL MODULE: external "react-datepicker"
var external_react_datepicker_ = __webpack_require__(8743);
var external_react_datepicker_default = /*#__PURE__*/__webpack_require__.n(external_react_datepicker_);
// EXTERNAL MODULE: ./node_modules/react-datepicker/dist/react-datepicker.css
var react_datepicker = __webpack_require__(5994);
// EXTERNAL MODULE: ./components/helpers/auth_services.jsx
var auth_services = __webpack_require__(9062);
// EXTERNAL MODULE: external "react-phone-input-2"
var external_react_phone_input_2_ = __webpack_require__(5452);
var external_react_phone_input_2_default = /*#__PURE__*/__webpack_require__.n(external_react_phone_input_2_);
// EXTERNAL MODULE: ./components/common/validation/index.js
var validation = __webpack_require__(1396);
// EXTERNAL MODULE: ./components/common/Modal/otp_input.jsx
var otp_input = __webpack_require__(6683);
;// CONCATENATED MODULE: ./components/common/Modal/signupStep3.jsx







const SignUpStepThree = ({ isOpen , style , text , text1  })=>{
    const { 0: otp1 , 1: setOtp  } = (0,external_react_.useState)("");
    const { 0: isDisable , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { 0: message , 1: setMessage  } = (0,external_react_.useState)("");
    const handleOnchangeOtp = (otp)=>{
        setOtp(otp);
        setMessage("");
        if (otp.length == 4) {
            setDisabled(true);
        }
    };
    const verifiedOtp = async (event)=>{
        event.preventDefault();
        const response = await (0,auth_services/* verify_otp_service */.pr)(otp1);
        if (response.code == 401) {
            setMessage(response.message);
            setOtp("");
            setDisabled(false);
        }
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(external_react_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
            className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeSignUpModal}`,
            isOpen: isOpen,
            // onRequestClose={closeModal}
            style: {
                overflowY: "scroll"
            },
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (customeModal_module_default()).dialogBox,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (customeModal_module_default()).registrationSec,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                        children: "Registration"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                        children: "Please enter your details exactly as it appears on your ID"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                style: {
                                    color: "#b2020f",
                                    textAlign: "center"
                                },
                                children: message
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                class: "row",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "col-md-12",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        className: (customeModal_module_default()).progressBar,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: (customeModal_module_default()).Active
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: (customeModal_module_default()).Active
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: (customeModal_module_default()).Active
                                            })
                                        ]
                                    })
                                })
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                id: "step3",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup} ${(customeModal_module_default()).OtpSec}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Enter your Verification Code Here"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx(otp_input/* default */.Z, {
                                                value: otp1,
                                                onChange: handleOnchangeOtp,
                                                numInputs: 4,
                                                separator: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    children: "  "
                                                }),
                                                isInputNum: true,
                                                name: "otp"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        class: "row",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            class: "col-12",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                onClick: (e)=>{
                                                    verifiedOtp(e);
                                                },
                                                className: (customeModal_module_default()).sendBtn,
                                                disabled: !isDisable,
                                                children: "VERIFY"
                                            })
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const signupStep3 = (SignUpStepThree);

;// CONCATENATED MODULE: ./components/common/Modal/signupStep2.jsx













const SignUpStepTwo = ({ fname , lname , dob , setDob , isOpen , idNumber , setUser , nationality , id , setOpenModalTwo  })=>{
    const { 0: phone1 , 1: setPhone  } = (0,external_react_.useState)("");
    const { 0: countryCode , 1: setCountrycode  } = (0,external_react_.useState)("");
    const { 0: email , 1: setEmail  } = (0,external_react_.useState)("");
    const { 0: isDisable , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { 0: Open , 1: setOpenModalThree  } = (0,external_react_.useState)(false);
    const { 0: gender , 1: setGender  } = (0,external_react_.useState)("");
    const { 0: message , 1: setMessage  } = (0,external_react_.useState)("");
    const { 0: password , 1: setPassword  } = (0,external_react_.useState)({
        pass: "",
        confirmPass: ""
    });
    const { 0: isError , 1: setError  } = (0,external_react_.useState)({
        email_status: false,
        password_status: false,
        confrm_password_status: false
    });
    const closeModal = ()=>{
        setOpenModalTwo(false);
        setDob("");
        setMessage("");
        setDisabled(false);
        setPhone("");
        setUser((prev)=>{
            return {
                ...prev,
                fname: "",
                lname: ""
            };
        });
        setEmail("");
        setPassword((prev)=>{
            return {
                ...prev,
                confirmPass: "",
                pass: ""
            };
        });
    };
    const validForm = (fieldName, Value)=>{
        let isDisabledEmail = isError.email_status;
        let isDisabledPassword = isError.password_status;
        let confrmPassword_status = isError.confrm_password_status;
        switch(fieldName){
            case "email":
                isDisabledEmail = (0,validation/* validateEmail */.oH)(Value).isDisabled;
                break;
            case "pass":
                isDisabledPassword = (0,validation/* validatePassword */.uo)(Value).isDisabled;
                break;
            case "confirmPass":
                confrmPassword_status = (0,validation/* validatePassword */.uo)(Value).isDisabled;
                break;
            default:
                break;
        }
        setError((prev)=>{
            return {
                ...prev,
                email_status: isDisabledEmail,
                password_status: isDisabledPassword,
                confrm_password_status: confrmPassword_status
            };
        });
    };
    const isDisabledChecked = ()=>{
        if (isError.email_status === true && phone1 != '' && isError.password_status === true && isError.confrm_password_status && gender != '') {
            setDisabled(true);
        } else {
            setDisabled(false);
        }
    };
    const handleNext = async (e)=>{
        let now = new Date();
        let dayOfYear = Math.floor((new Date(dob) - new Date(now.getFullYear(), 0, 0)) / (1000 * 60 * 60 * 24));
        let hijriDate = ((now.getFullYear() - 621.5643) * 365.24225 + dayOfYear) / 354.36707;
        let hijriYear = Math.floor(hijriDate);
        let hijriMonth = Math.ceil((hijriDate - Math.floor(hijriDate)) * 354.36707 / 29.530589);
        let hijriDay = Math.floor((hijriDate - Math.floor(hijriDate)) * 354.36707 % 29.530589);
        let hijriDob = `${hijriYear}/${hijriMonth}/${hijriDay}`;
        e.preventDefault();
        let count = countryCode.length;
        let phone_number = phone1;
        phone_number = phone_number.substring(count - 1);
        const response = await (0,auth_services/* verify_contact_service */.Nv)(countryCode, email, phone_number);
        if (response.code == 404) {
            const signupResponse = await (0,auth_services/* user_signup_service */.$4)(countryCode, dob, id =  true ? dob : 0, email, fname, gender, idNumber, lname, phone_number, nationality, password.pass, id);
            if (signupResponse.code == 200) {
                let userId = signupResponse.result.id;
                const OtpResponse = await (0,auth_services/* sent_otp_service */.tz)(countryCode, phone_number, userId);
                console.log(OtpResponse);
                if (OtpResponse.code == 200) {
                    setOpenModalTwo(false);
                    setOpenModalThree(true);
                }
            } else if (signupResponse.code == 422) {
                setMessage(signupResponse.message);
            }
        } else if (response.code == 200) {
            setMessage(response.message);
        }
    };
    const handleOnchangePhone = (phone, country, e)=>{
        let name = "phone";
        let country_code = `+${country.dialCode}`;
        let country_name = country.countryCode;
        setCountrycode(country_code);
        setPhone(phone);
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
                className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeSignUpModal}`,
                isOpen: isOpen,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: (customeModal_module_default()).dialogBox,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (customeModal_module_default()).registrationSec,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: "float-right close mr-0 mt-0 ",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    children: "x"
                                })
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                children: "Registration"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Please enter your details exactly as it appears on your ID"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        class: "row",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-12",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: (customeModal_module_default()).progressBar,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: (customeModal_module_default()).Active
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: (customeModal_module_default()).Active
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                    })
                                                ]
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                        style: {
                                            color: "#b2020f"
                                        },
                                        children: message
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        id: "step2",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "First Name"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "fname",
                                                        name: "fname",
                                                        placeholder: "First Name",
                                                        value: fname,
                                                        onChange: (e)=>{
                                                            setUser((prev)=>{
                                                                return {
                                                                    ...prev,
                                                                    fname: e.target.value
                                                                };
                                                            });
                                                        },
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Last Name"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "lname",
                                                        name: "lname",
                                                        placeholder: "Last Name",
                                                        value: lname,
                                                        onChange: (e)=>{
                                                            setUser((prev)=>{
                                                                return {
                                                                    ...prev,
                                                                    lname: e.target.value
                                                                };
                                                            });
                                                        },
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Date Of Birth"
                                                    }),
                                                    id == "saudi" ? /*#__PURE__*/ jsx_runtime_.jsx(app, {
                                                        selectedDate: dob
                                                    }) : /*#__PURE__*/ jsx_runtime_.jsx((external_react_datepicker_default()), {
                                                        className: "form-control",
                                                        name: "dob",
                                                        selected: dob ? new Date(dob) : null,
                                                        dateFormat: "MM/dd/yyyy",
                                                        dropdownMode: "scroll",
                                                        showYearDropdown: true,
                                                        dateFormatCalendar: "MMMM",
                                                        yearDropdownItemNumber: 15,
                                                        scrollableYearDropdown: true,
                                                        locale: external_moment_.locale.locale,
                                                        onChange: (date)=>{
                                                            setDob(date);
                                                            isDisabledChecked();
                                                        },
                                                        autoComplete: "off",
                                                        maxDate: external_moment_default()().subtract(16, 'years')._d
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup} ${(customeModal_module_default()).phoneInput}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Phone Number"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx((external_react_phone_input_2_default()), {
                                                        inputClass: "form-control",
                                                        country: "sa",
                                                        value: phone1,
                                                        placeholder: "phone*",
                                                        onChange: (phone, country_name)=>{
                                                            handleOnchangePhone(phone, country_name);
                                                            isDisabledChecked();
                                                        },
                                                        prefix: "+",
                                                        copyNumbersOnly: false,
                                                        countryCodeEditable: false
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Email"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "email",
                                                        name: "email",
                                                        placeholder: "email",
                                                        value: email,
                                                        onChange: (e)=>{
                                                            setEmail(e.target.value);
                                                            validForm("email", e.target.value);
                                                            isDisabledChecked();
                                                        },
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Passward "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "password",
                                                        id: "pass",
                                                        name: "pass",
                                                        placeholder: "Passward",
                                                        value: password.pass,
                                                        onChange: (e)=>{
                                                            setPassword((prev)=>{
                                                                return {
                                                                    ...prev,
                                                                    pass: e.target.value
                                                                };
                                                            });
                                                            validForm("pass", e.target.value);
                                                            isDisabledChecked();
                                                        },
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Confirm Password"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "password",
                                                        id: "confirmPass",
                                                        name: "confirmPass",
                                                        placeholder: "Confirm Password",
                                                        value: password.confirmPass,
                                                        onChange: (e)=>{
                                                            setPassword((prev)=>{
                                                                return {
                                                                    ...prev,
                                                                    confirmPass: e.target.value
                                                                };
                                                            });
                                                            validForm("confirmPass", e.target.value);
                                                            isDisabledChecked();
                                                        },
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        style: {
                                                            color: "#b2020f"
                                                        },
                                                        children: password.confirmPass && password.pass !== password.confirmPass ? "password didn't match" : ""
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                        children: [
                                                            "Referral Code ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "(Option)"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "contact",
                                                        name: "contact",
                                                        placeholder: "Referral Code",
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Gender"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "row",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-md-6",
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: (customeModal_module_default()).radio,
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                            id: "male",
                                                                            name: "radio",
                                                                            onChange: (e)=>{
                                                                                setGender(e.target.id);
                                                                                isDisabledChecked();
                                                                            },
                                                                            type: "radio",
                                                                            checked: gender == "male"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            for: "male",
                                                                            className: (customeModal_module_default()).radioLabel,
                                                                            children: "Male"
                                                                        })
                                                                    ]
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-md-6",
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: (customeModal_module_default()).radio,
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                            id: "female",
                                                                            name: "radio",
                                                                            checked: gender == "female",
                                                                            onChange: (e)=>{
                                                                                setGender(e.target.id);
                                                                                isDisabledChecked();
                                                                            },
                                                                            type: "radio"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            for: "female",
                                                                            className: (customeModal_module_default()).radioLabel,
                                                                            children: "Female"
                                                                        })
                                                                    ]
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                class: "row",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    class: "col-12",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                        onClick: (e)=>{
                                                            handleNext(e);
                                                        },
                                                        className: (customeModal_module_default()).sendBtn,
                                                        disabled: !isDisable,
                                                        children: "Next"
                                                    })
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                })
            }),
            Open && /*#__PURE__*/ jsx_runtime_.jsx(signupStep3, {
                isOpen: Open
            })
        ]
    }));
};
/* harmony default export */ const signupStep2 = (SignUpStepTwo);

;// CONCATENATED MODULE: ./components/common/Modal/signup.jsx










const ImgUpload = ({ onChange , src , id  })=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
        htmlFor: `${id}photo`,
        className: "custom-file-upload fas",
        style: {
            width: "100%"
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: `${(customeModal_module_default()).photoSec}`,
                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                    htmlFor: `${id}photo`,
                    src: src
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                id: `${id}photo`,
                type: "file",
                onChange: onChange,
                style: {
                    display: "none"
                },
                accept: ".pdf, .jpeg , .jpg ,.png"
            })
        ]
    })
;
const SignUp = ({ className , style , text , text1 , imagePreviewUrl , img  })=>{
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    const { 0: selectedDate , 1: setDate  } = (0,external_react_.useState)("");
    const { 0: id , 1: setId  } = (0,external_react_.useState)("saudi");
    const { 0: idNumber , 1: setNumber  } = (0,external_react_.useState)("");
    const { 0: message , 1: setMessage  } = (0,external_react_.useState)("");
    const { 0: nationality , 1: setNationality  } = (0,external_react_.useState)("United Arab Emirates");
    const { 0: visaNumber , 1: setVisaNo  } = (0,external_react_.useState)("");
    const { 0: passportNo , 1: setPassportNo  } = (0,external_react_.useState)("");
    const { 0: isOpen2 , 1: setOpenModalTwo  } = (0,external_react_.useState)(false);
    const { 0: isDisable , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { 0: user , 1: setUser  } = (0,external_react_.useState)({
        fname: "",
        lname: ""
    });
    const { 0: isImg , 1: setImg  } = (0,external_react_.useState)("");
    const { 0: imageUrl , 1: setUrl  } = (0,external_react_.useState)(null);
    const { 0: visaPic , 1: setVisaPic  } = (0,external_react_.useState)("");
    const { 0: visaUrl , 1: setvisaUrl  } = (0,external_react_.useState)(null);
    const { 0: isError , 1: setError  } = (0,external_react_.useState)({
        date_status: false
    });
    const openModal = (e)=>{
        e.preventDefault();
        setOpen(true);
    };
    const closeModal = (e)=>{
        e.preventDefault();
        setOpen(false);
        setImg("");
        setUrl("");
        setId("saudi");
        setDisabled(false);
        setDate("");
        setMessage("");
        setNumber("");
        setUser((prev)=>{
            return {
                ...prev,
                fname: "",
                lname: ""
            };
        });
    };
    const handleChange = (e)=>{
        let Id = e.target.value;
        if (!Number(Id)) {
            return;
        }
        setNumber(Id);
    };
    const Disabled = ()=>{
        if (id == "gcc") {
            if (nationality != '' && imageUrl !== null && idNumber != '' && user.fname != '' && user.lname != '' && isError.date_status == true) {
                setDisabled(true);
            } else {
                setDisabled(false);
            }
        } else if (id == "saudi") {
            console.log(id, idNumber !== '');
            if (idNumber != '' && selectedDate != '') {
                setDisabled(true);
            } else {
                setDisabled(false);
            }
        } else if (id == "iqama") {
            if (idNumber != '' && selectedDate != '') {
                setDisabled(true);
            } else {
                setDisabled(false);
            }
        } else if (id == "tourist") {
            if (passportNo != '' && visaNumber != '' && imageUrl !== null && visaUrl != null) {
                setDisabled(true);
            } else {
                setDisabled(false);
            }
        }
    };
    const handleDobResponse = (dob)=>{
        setDate(dob);
        setError((prev)=>{
            return {
                ...prev,
                date_status: true
            };
        });
    };
    const handleClick = async (event)=>{
        event.preventDefault();
        const response = await (0,auth_services/* duplicate_user_service */.bZ)(idNumber ? idNumber : passportNo);
        if (response.code == 404 && id == "iqama") {
            const res = await (0,auth_services/* verify_iqamaNumber_service */.Nx)(selectedDate, idNumber);
            if (res.code == 422) {
                setMessage(res.message);
            }
        } else if (response.code == 404 && id == "saudi") {
            let soudiFormateDate = selectedDate.substring(3);
            const res = await (0,auth_services/* verify_soudiNumber_service */.r8)(soudiFormateDate, idNumber);
            if (res.code == 422) {
                setMessage(res.message);
            } else if (res.code == 200) {
                setUser((prev)=>{
                    return {
                        ...prev,
                        fname: res.result.english_first_name,
                        lname: res.result.english_last_name
                    };
                });
                setDate(res.result.date_of_birth_h);
                setOpen(false);
                setOpenModalTwo(true);
            }
        } else if (response.code == 404 && id == "tourist") {
            const res = await (0,auth_services/* verify_visaInfo_service */.lL)(passportNo, visaNumber);
            if (res.code == 422) {
                setMessage("The Visa number you have entered does not exist in the National Information Center (NIC) records");
            }
        } else if (response.code == 404 && id == "gcc") {
            setOpen(false);
            setOpenModalTwo(true);
        }
    };
    const handleChangeId = (e)=>{
        setId(e.target.value);
        setMessage("");
        setDisabled(false);
        setDate("");
        setNumber("");
        setImg("");
        setUrl("");
        setVisaPic("");
        setvisaUrl("");
        setError((prev)=>{
            return {
                ...prev,
                date_status: false
            };
        });
    };
    // <------------Onchange Image Upload ---------->
    const photoUpload = (e)=>{
        e.preventDefault();
        let type = e.target.files[0].type;
        if (e.target.files && e.target.files[0]) {
            setUrl(URL.createObjectURL(e.target.files[0]));
        } else {
            setImg(e.target.files[0]);
        }
    };
    const idPassportUpload = (e)=>{
        e.preventDefault();
        let type = e.target.files[0].type;
        if (e.target.files && e.target.files[0]) {
            setUrl(URL.createObjectURL(e.target.files[0]));
        } else {
            setImg(e.target.files[0]);
        }
    };
    const visaUpload = (e)=>{
        e.preventDefault();
        let type = e.target.files[0].type;
        if (e.target.files && e.target.files[0]) {
            setvisaUrl(URL.createObjectURL(e.target.files[0]));
        } else {
            setVisaPic(e.target.files[0]);
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                onClick: openModal,
                className: `${style.sendBtn1} ${className}`,
                children: [
                    text,
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: style.floatRight,
                        children: [
                            " ",
                            text1
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
                className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeSignUpModal}`,
                isOpen: open,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: (customeModal_module_default()).dialogBox,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                            onClick: closeModal,
                            className: `float-right close mr-0 mt-0 ${(customeModal_module_default()).close}`,
                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "x"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (customeModal_module_default()).registrationSec,
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                children: "Registration"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Please enter your details exactly as it appears on your ID"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        class: "row",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-12",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: (customeModal_module_default()).progressBar,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: (customeModal_module_default()).Active
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                    })
                                                ]
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                        style: {
                                            color: "#b2020f"
                                        },
                                        children: message
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        id: "step1",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "ID Type"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                        value: id,
                                                        onChange: (e)=>{
                                                            handleChangeId(e);
                                                        },
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "saudi",
                                                                children: " Saudi Id   "
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "iqama",
                                                                children: " Iqama "
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "gcc",
                                                                children: " GCC National "
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "tourist",
                                                                children: " Passport "
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            id == "saudi" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                                children: [
                                                    " ",
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Id Number"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "idNumber",
                                                                placeholder: "Id Number",
                                                                value: idNumber,
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    handleChange(e);
                                                                    Disabled();
                                                                },
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Date of Birth(Hijri)"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx(app, {
                                                                selectedDate: selectedDate,
                                                                setDate: (date)=>{
                                                                    setDate(date);
                                                                    Disabled();
                                                                }
                                                            })
                                                        ]
                                                    }),
                                                    " "
                                                ]
                                            }),
                                            id == "iqama" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                                children: [
                                                    " ",
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Iqama Number"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "idNumber",
                                                                placeholder: "Iqama Number",
                                                                autoComplete: "off",
                                                                value: idNumber,
                                                                onChange: (e)=>{
                                                                    handleChange(e);
                                                                    Disabled();
                                                                },
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup} signUpDatePicker`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Date of Birth"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx((external_react_datepicker_default()), {
                                                                className: "form-control",
                                                                name: "dob",
                                                                selected: selectedDate ? new Date(selectedDate) : null,
                                                                dateFormat: "dd/MM/yyyy",
                                                                dropdownMode: "scroll",
                                                                showYearDropdown: true,
                                                                dateFormatCalendar: "MMMM",
                                                                placeholderText: "DOB",
                                                                yearDropdownItemNumber: 25,
                                                                scrollableYearDropdown: true,
                                                                // locale={locale.locale}
                                                                onChange: (date)=>{
                                                                    handleDobResponse(date);
                                                                    Disabled();
                                                                },
                                                                autoComplete: "off",
                                                                maxDate: external_moment_default()().subtract(18, 'years')._d
                                                            })
                                                        ]
                                                    }),
                                                    " "
                                                ]
                                            }),
                                            id == "gcc" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                                children: [
                                                    " ",
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Nationality"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                                value: nationality,
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    setNationality(e.target.value);
                                                                    Disabled();
                                                                },
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "saudi",
                                                                        children: " Select Country  "
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "Bahrain",
                                                                        children: " Bahrain "
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "Kuwait",
                                                                        children: " Kuwait "
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "Oman",
                                                                        children: " Oman "
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "Qatar",
                                                                        children: " Qatar "
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                        value: "United Arab Emirates",
                                                                        children: " United Arab Emirates "
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Id Number"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "idNumber",
                                                                placeholder: "Id Number",
                                                                value: idNumber,
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    handleChange(e);
                                                                    Disabled();
                                                                },
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "First Name"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "fname",
                                                                placeholder: "First Name",
                                                                value: user.fname,
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    setUser((prev)=>{
                                                                        return {
                                                                            ...prev,
                                                                            fname: e.target.value
                                                                        };
                                                                    });
                                                                    Disabled();
                                                                },
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Last Name"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "lname",
                                                                value: user.lname,
                                                                onChange: (e)=>{
                                                                    setUser((prev)=>{
                                                                        return {
                                                                            ...prev,
                                                                            lname: e.target.value
                                                                        };
                                                                    });
                                                                    Disabled();
                                                                },
                                                                placeholder: "Last Name",
                                                                autoComplete: "off",
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Date of Birth"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx((external_react_datepicker_default()), {
                                                                className: "form-control",
                                                                name: "dob",
                                                                selected: selectedDate ? new Date(selectedDate) : null,
                                                                dateFormat: "MM/dd/yyyy",
                                                                dropdownMode: "scroll",
                                                                showYearDropdown: true,
                                                                dateFormatCalendar: "MMMM",
                                                                placeholderText: "DOB",
                                                                yearDropdownItemNumber: 15,
                                                                scrollableYearDropdown: true,
                                                                // locale={locale.locale}
                                                                onChange: (date)=>{
                                                                    handleDobResponse(date);
                                                                    Disabled();
                                                                },
                                                                autoComplete: "off",
                                                                maxDate: external_moment_default()().subtract(18, 'years')._d
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Id Photo"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx(ImgUpload, {
                                                                id: 1,
                                                                onChange: (e)=>{
                                                                    photoUpload(e);
                                                                    Disabled();
                                                                },
                                                                src: imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            id == "tourist" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Passport Number"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "passportNo",
                                                                placeholder: "Passport Number",
                                                                value: passportNo,
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    setPassportNo(e.target.value);
                                                                    Disabled();
                                                                },
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Saudi Entry Visa Number"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "text",
                                                                id: "contact",
                                                                name: "visaNumber",
                                                                autoComplete: "off",
                                                                onChange: (e)=>{
                                                                    setVisaNo(e.target.value);
                                                                    Disabled();
                                                                },
                                                                placeholder: "Passport Number",
                                                                value: visaNumber,
                                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Passport Photo"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx(ImgUpload, {
                                                                id: 2,
                                                                onChange: (e)=>{
                                                                    idPassportUpload(e);
                                                                    Disabled();
                                                                },
                                                                src: imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Please Upload a clear Photo of Your Passport"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                children: "Saudi Entry Visa Photo"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx(ImgUpload, {
                                                                onChange: (e)=>{
                                                                    visaUpload(e);
                                                                    Disabled();
                                                                },
                                                                id: 3,
                                                                src: visaUrl === null ? "/images/others/icon/placeholder.png" : visaUrl
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Please Upload a clear Photo of Your Saudi Visa"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    }),
                                                    " "
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                class: "row",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    class: "col-12",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                        disabled: !isDisable,
                                                        onClick: (e)=>{
                                                            handleClick(e);
                                                        },
                                                        className: `${(customeModal_module_default()).sendBtn}`,
                                                        children: "Next"
                                                    })
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(signupStep2, {
                id: id,
                idNumber: idNumber,
                nationality: nationality,
                isOpen: isOpen2,
                fname: user.fname,
                setUser: setUser,
                lname: user.lname,
                dob: selectedDate,
                setDob: setDate,
                setOpenModalTwo: setOpenModalTwo
            })
        ]
    }));
};
/* harmony default export */ const signup = (SignUp);


/***/ }),

/***/ 4877:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5836);
/* harmony import */ var _styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8743);
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_2__);





const SearchSec = ({})=>{
    const { 0: open , 1: setOpen  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: dateRange , 1: setDateRange  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        null,
        null
    ]);
    const [startDate, endDate] = dateRange;
    const { 0: isStatus , 1: setStatus  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const showAddressList = ()=>{
        console.log(isStatus);
        setStatus(!isStatus);
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: `${(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().searchSec)}`,
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("form", {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: `col-lg-12 ${(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().inputArea)}`,
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: `row no-gutters ${(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().single)}`,
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: `col-6 ${(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().small)}`,
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().inputWithIcon),
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                src: "/images/others/send_icon.png"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                type: "text",
                                                placeholder: "Enter city, address, airport or hotel",
                                                onClick: showAddressList,
                                                formControlName: "place"
                                            })
                                        ]
                                    }),
                                    isStatus && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().searchSection),
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                            children: [
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                            src: "/images/others/icon/sidebar/send_icon.png"
                                                        }),
                                                        "Current Location"
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                            src: "/images/others/icon/sidebar/recent_icon.png"
                                                        }),
                                                        "Recent Search"
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: `col-5 ${(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().smallHide)}`,
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().inputWithIcon),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            src: "/images/others/calender_icon.png"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_datepicker__WEBPACK_IMPORTED_MODULE_2___default()), {
                                            selectsRange: true,
                                            startDate: startDate,
                                            endDate: endDate,
                                            monthsShown: 2,
                                            minDate: new Date(),
                                            placeholderText: "Select dates",
                                            onChange: (update)=>{
                                                setDateRange(update);
                                            },
                                            withPortal: true
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "col-1",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                    className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().searchBtn),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            src: "/images/others/icon/search_icon.svg",
                                            alt: "",
                                            className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().white)
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            src: "/images/others/icon/search_icon_red.png",
                                            alt: "",
                                            className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_3___default().red)
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SearchSec);


/***/ }),

/***/ 1396:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "oH": () => (/* binding */ validateEmail),
/* harmony export */   "uo": () => (/* binding */ validatePassword)
/* harmony export */ });
/* unused harmony exports validateName, validateAccountNo, validateSuiteName, validateLastName, validateLocation, validatetaskdescription, validateAddress, validateEmailPhone, validateOtp, validateQuessionierField, validateDatepicker, validateLocationAndFlat, validatePhone, validateAccountNumber, validateComment, validateProfileDescription, validateNumber, validateBudget, validateNumberofSeeker, validateEmailNewsletter, validateEmailPhoneDemo */
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const style = {
    paddingTop: "6px",
    paddingBottom: "6px",
    fontSize: "12px"
};
const style_newsletter = {
    paddingTop: "3px",
    paddingBottom: "3px",
    fontSize: "12px"
};
const alert = "alert alert-login";
const validateName = (name)=>{
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name === null || name === void 0 ? void 0 : name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Name.NameError
        };
    } else if (!nameRegex.test(name)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (name.length < 2) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Name.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateAccountNo = (name)=>{
    let nameRegex = /^\w{1,17}$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.AccountNumber.NameError
        };
    } else if (!nameRegex.test(name)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (name.length < 2) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.AccountNumber.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateSuiteName = (name)=>{
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Suite.NameError
        };
    } else if (!nameRegex.test(name)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (name.length < 2) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Suite.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateLastName = (name)=>{
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.LastName.NameError
        };
    } else if (!nameRegex.test(name)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (name.length < 2) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Name.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateLocation = (name)=>{
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Location.NameError
        };
    } else if (!nameRegex.test(name)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (name.length < 2) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Location.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validatetaskdescription = (name)=>{
    let nameRegex = /(?=.*[0-9])*[A-Za-z\\s][A-Za-z0-9\\]$/;
    let notonlynumber_regex = /^[-_ a-zA-Z0-9]+$/;
    //  /^[ A-Za-z0-9_@./#&+-]*$/;
    name = name === null || name === void 0 ? void 0 : name.trim();
    if (name === "" || name === undefined || name === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Name.NameError
        };
    } else if (name.length < 25) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Name.FirstNameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateEmail = (email)=>{
    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    email = email.trim();
    if (email === "" || email === undefined || email === null) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (!emailRegex.test(email)) {
        return {
            isDisabled: false,
            error: ""
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validatePassword = (password)=>{
    let passwordRegex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!@^#$%&*? "])[a-zA-Z0-9^!@#$*%&?]{6,20}$/;
    password = password.trim();
    if (password === "" || password === undefined || password === null) {
        return {
            isDisabled: false,
            error: ""
        };
    } else if (password.length < 7) {
        return {
            isDisabled: false,
            error: "Error"
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateAddress = (address)=>{
    address = address.trim();
    if (address === "" || address === undefined || address === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Addresss.NameError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateEmailPhone = (email_phone)=>{
    // let emailPhoneRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let emailPhoneRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/;
    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let PhoneNumberRegex = /^([0-9]{10})+$/;
    email_phone = email_phone.trim();
    if (email_phone === "" || email_phone === undefined || email_phone === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.EmailPhone.EmailPhoneError
        };
    } else if (email_phone.length <= 10) {
        if (!PhoneNumberRegex.test(email_phone)) {
            return {
                isDisabled: false,
                error: ErrorMessage.Auth.Phone.PhoneNumberError
            };
        } else if (!emailPhoneRegex.test(email_phone)) {
            return {
                isDisabled: false,
                error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError
            };
        } else {
            return {
                isDisabled: true,
                error: ""
            };
        }
    } else if (email_phone.length >= 11 && !emailPhoneRegex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     console.log("Valuu >=  10 check Email")
        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError };
        // }
        // else {
        return {
            isDisabled: false,
            error: "Please Enter Valid Phone Number...."
        };
    // }
    } else if (!emailPhoneRegex.test(email_phone)) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateOtp = (otp)=>{
    otp = email_phone.trim();
    if (otp === "" || otp === undefined || otp === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.EmailPhone.EmailPhoneError
        };
    } else if (password.length === 8) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Password.PasswordLengthError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateQuessionierField = (input)=>{
    input = input.trim();
    if (input === "" || input === undefined || input === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.QuessionierField.inputField
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
// export const validateNumber = (number) => {
//     // let numberRegex = /\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?/;
//     // let numberRegex = /^[1-9][0-9]{9,12}$/;
//     number = number.trim();
//     if (number === "" || number === undefined || number === null) {
//         return { isDisabled: false, error: Please enter Phone number ID. };
//     }
//     // else if (!numberRegex.test(number)) {
//     //     return {  isDisabled: false, error: Please enter valid Phone Number. };
//     // }
//     else {
//         return { isDisabled: true, error: '' };
//     }
// }
// export const UnAuthorizedAccess = (code) => {
//     // code = license.trim();
//     if (code === 404) {
//         return {
//             error:                 User does not access
//         }
//     }
//     else {
//         return { error: '' }
//     }
// }
const validateDatepicker = (date)=>{
    // let passwordRegex = /^ (?=^.{8,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    // date = date.trim();
    if (date === "" || date === undefined || date === null) {
        return {
            status: false,
            isDisabled: false,
            error: /*#__PURE__*/ _jsx("div", {
                className: alert,
                style: style,
                children: "Please enter valid date."
            })
        };
    } else {
        return {
            status: true,
            isDisabled: true,
            error: ''
        };
    }
};
const validateLocationAndFlat = (value)=>{
    // let passwordRegex = /^ (?=^.{8,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    // date = date.trim();
    if (value < 4 || value === undefined || value === null) {
        return {
            status: false,
            isDisabled: false,
            error: /*#__PURE__*/ _jsx("div", {
                className: alert,
                style: style,
                children: "Please enter this field"
            })
        };
    } else {
        return {
            status: true,
            isDisabled: true,
            error: ''
        };
    }
};
const validatePhone = (phone, coutry_code)=>{
    // date = date.trim();
    // let phone_regex = /^[789]\d{9}$/;
    let phone_regex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
    if (phone === "" || phone === undefined || phone === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneError
        };
    } else if (!phone_regex.test(phone)) {
        return {
            isDisabled: false,
            error: "Please enter valid number."
        };
    } else if (phone.length < 10) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneInvalidError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateAccountNumber = (phone)=>{
    // date = date.trim();
    let phone_regex = /^(\d{3}|\d{4})[-](\d{5})$/;
    if (phone === "" || phone === undefined || phone === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneError
        };
    } else if (!phone_regex.test(phone)) {
        return {
            isDisabled: false,
            error: "Please enter valid Account."
        };
    } else if (phone.length < 10) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneInvalidError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateComment = (comment)=>{
    comment = comment === null || comment === void 0 ? void 0 : comment.trim();
    let comments = comment === null || comment === void 0 ? void 0 : comment.split(" ");
    let swear = [
        'arse',
        'ass',
        'asshole',
        'bastard',
        'bitch',
        'bollocks',
        'bugger',
        'bullshit',
        'crap',
        'damn',
        'frigger',
        'fuck',
        'Fuck',
        'suck',
        'Suck'
    ];
    let phone_regex = /^[0-9\b]+$/;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    var Regex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    let symbol = /.+@.+/;
    // console.log(comments,"swear")
    const foundSwears = swear.filter((word)=>comment.includes(word)
    );
    const filterComment = comments.filter((word)=>comments.includes("9999999999")
    );
    if (foundSwears.length) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else if (Regex.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else if (comment === "" || comment === undefined || comment === null) {
        return {
            isDisabled: false,
            error: "Please enter  comment."
        };
    } else if (phone_regex.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else if (email.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else if (comment === null || comment === void 0 ? void 0 : comment.match(symbol)) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else if (comment === null || comment === void 0 ? void 0 : comment.match(phone_regex)) {
        return {
            isDisabled: false,
            error: "Please enter valid comment."
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateProfileDescription = (comment)=>{
    comment = comment === null || comment === void 0 ? void 0 : comment.trim();
    let comments = comment === null || comment === void 0 ? void 0 : comment.split(" ");
    let swear = [
        'arse',
        'ass',
        'asshole',
        'bastard',
        'bitch',
        'bollocks',
        'bugger',
        'bullshit',
        'crap',
        'damn',
        'frigger',
        'fuck',
        'Fuck',
        'suck',
        'Suck'
    ];
    let phone_regex = /^[0-9\b]+$/;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    var Regex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    let symbol = /.+@.+/;
    const foundSwears = swear.filter((word)=>{
        return comment === null || comment === void 0 ? void 0 : comment.includes(word);
    });
    if (foundSwears.length) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else if (Regex.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else if (phone_regex.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else if (email.test(comment)) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else if (comment === null || comment === void 0 ? void 0 : comment.match(symbol)) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else if (comment === null || comment === void 0 ? void 0 : comment.match(phone_regex)) {
        return {
            isDisabled: false,
            error: "Please enter valid data"
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateNumber = (phone)=>{
    // date = date.trim();
    if (phone === "" || phone === undefined || phone === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneError
        };
    } else if (phone.length > 4) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Phone.PhoneInvalidError
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateBudget = (phone)=>{
    // date = date.trim();
    if (phone === "" || phone === undefined || phone === null) {
        return {
            isDisabled: false,
            error: "please enter value"
        };
    } else if (phone.length > 4) {
        return {
            isDisabled: false,
            error: "not null"
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateNumberofSeeker = (phone)=>{
    // date = date.trim();
    if (phone === "" || phone === undefined || phone === null) {
        return {
            isDisabled: false,
            error: "please Select the value"
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateEmailNewsletter = (email)=>{
    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    // let emailRegex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    email = email.trim();
    if (email === "" || email === undefined || email === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Email.EmailEmptyError
        };
    } else if (!emailRegex.test(email)) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.Email.EmailInvalidErrors
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};
const validateEmailPhoneDemo = (email_phone)=>{
    // let emailPhoneRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let phone_regex = /^[0-9\b]+$/;
    let emailPhoneRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/;
    let regex = "@.*[a-zA-Z]";
    let regexNumber = "^[0-9]{10}$";
    email_phone = email_phone.trim();
    let access;
    if (email_phone.length === 10) {
        access = Number(email_phone);
    }
    if (email_phone === "" || email_phone === undefined || email_phone === null) {
        return {
            isDisabled: false,
            error: ErrorMessage.Auth.EmailPhone.EmailPhoneError
        };
    } else if (access !== NaN && access !== undefined && !phone_regex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
        // }
        return {
            isDisabled: false,
            error: "Please Enter valid number"
        };
    } else if (access !== NaN && email_phone === undefined && !phone_regex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
        // }
        return {
            isDisabled: false,
            error: "Please Enter valid numbersaccasc"
        };
    } else if (access === undefined && !email.test(email_phone)) {
        if (email_phone > 10) {
            return {
                isDisabled: false,
                error: "Please Enter valid mobile number"
            };
        }
        return {
            isDisabled: false,
            error: "Please Enter Valid email"
        };
    } else {
        return {
            isDisabled: true,
            error: ''
        };
    }
};


/***/ }),

/***/ 9062:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "zX": () => (/* binding */ login_service),
/* harmony export */   "bZ": () => (/* binding */ duplicate_user_service),
/* harmony export */   "Nx": () => (/* binding */ verify_iqamaNumber_service),
/* harmony export */   "r8": () => (/* binding */ verify_soudiNumber_service),
/* harmony export */   "lL": () => (/* binding */ verify_visaInfo_service),
/* harmony export */   "Nv": () => (/* binding */ verify_contact_service),
/* harmony export */   "$4": () => (/* binding */ user_signup_service),
/* harmony export */   "tz": () => (/* binding */ sent_otp_service),
/* harmony export */   "pr": () => (/* binding */ verify_otp_service),
/* harmony export */   "Vy": () => (/* binding */ forget_password_service),
/* harmony export */   "lm": () => (/* binding */ logout_service)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api_url__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6854);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4585);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jstz__WEBPACK_IMPORTED_MODULE_2__);



const timezone = jstz__WEBPACK_IMPORTED_MODULE_2___default().determine();
const ISSERVER = "undefined" === "undefined";
let GUEST_TOKEN = "";
let SESSION_TOKEN = "";
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token, SESSION_TOKEN = localStorage.token;
}
// LANDING PAGE SERVICES
const login_service = async (email, password)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.LOGIN */ .T5.LOGIN, {
        email: email,
        password: password
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
        // localStorage.clear()
        // window.location.href = "/"
        }
        if (response.code == 200) {
            localStorage.token = response.result.sessionToken;
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  DUPLICATE USER LOGIN 
const duplicate_user_service = async (idNumber)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.DUPLICATE_USER */ .T5.DUPLICATE_USER, {
        idNumber: idNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)
        // }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  VERIFY   IqamaNumber Number 
const verify_iqamaNumber_service = async (dob, iqamaNumber)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.VERIFY_IQAMA_NUMBER */ .T5.VERIFY_IQAMA_NUMBER, {
        dateOfBirth: dob,
        iqamaNumber: iqamaNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)
        // }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// VERIFY SOUDI NUMBER
const verify_soudiNumber_service = async (dob, nin)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.VERIFY_SOUDI_NUMBER */ .T5.VERIFY_SOUDI_NUMBER, {
        dateOfBirth: dob,
        nin: nin
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  VERIFY VISA INFORMATION
const verify_visaInfo_service = async (passportNo, visaNumber)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.VERIFY_VISA_INFO */ .T5.VERIFY_VISA_INFO, {
        passportNo: passportNo,
        visaNumber: visaNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)
        // }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//   CHECK USER CONTACT
const verify_contact_service = async (countryCode, email, mobile, user_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.UPDATE_USER_CONTACT */ .T5.UPDATE_USER_CONTACT, {
        countryCode: countryCode,
        email: email,
        mobile: mobile,
        user_id: user_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  USER SIGNUP
const user_signup_service = async (countrycode, dob, dobHijiri, email, fname, gender, idNumber, lname, mobile, nationality, password, title, referalCode, image)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.SIGNUP */ .T5.SIGNUP, {
        "countryCode": countrycode,
        "dob": dob,
        "dobHijiri": dobHijiri,
        "email": email,
        "fname": fname,
        "gender": gender,
        "idFrontImage": image,
        "idNumber": idNumber,
        "lname": lname,
        "mobile": mobile,
        "nationality": nationality,
        "password": password,
        "referal_code": referalCode,
        "title": title
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        } else if (response.code == 200) {
            localStorage.token = response.result.sessionToken;
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  SEND OTP
const sent_otp_service = async (countrycode, mobile, userId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.SEND_OTP */ .T5.SEND_OTP, {
        countryCode: countrycode,
        mobile: mobile,
        user_id: userId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        }
        //  if(response.code==404){
        //     console.log(response)
        // }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  VERIFY OTP 
const verify_otp_service = async (otp)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.OTP_VERIFY */ .T5.OTP_VERIFY, {
        otp: otp
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    });
    try {
        let response = res.data;
        //  if(response.code==404){
        //     console.log(response)
        // }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  FORGET PASSWORD
const forget_password_service = async (email)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.FORGET_PASSWORD */ .T5.FORGET_PASSWORD, {
        email: email
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  LOGOUT API INTEGRATION
const logout_service = async ()=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.LOGOUT */ .T5.LOGOUT, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    });
    try {
        let response = res.data;
        if (response.code == 200) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};


/***/ }),

/***/ 421:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Un": () => (/* binding */ homepage_service),
/* harmony export */   "YX": () => (/* binding */ session_details_service),
/* harmony export */   "uy": () => (/* binding */ add_favourite_service),
/* harmony export */   "IA": () => (/* binding */ notification_count_service)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api_url__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6854);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4585);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jstz__WEBPACK_IMPORTED_MODULE_2__);



const timezone = jstz__WEBPACK_IMPORTED_MODULE_2___default().determine();
const ISSERVER = "undefined" === "undefined";
let GUEST_TOKEN = "";
let SESSION_TOKEN = "";
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token, SESSION_TOKEN = localStorage.token;
}
// LANDING PAGE SERVICES
const homepage_service = async (lat, lng, userId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.HOMEPAGE */ .T5.HOMEPAGE, {
        latitude: lat,
        longitude: lng,
        user_id: userId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web"
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// USER DETAILS API 
const session_details_service = async ()=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.SESSION_DETAILS */ .T5.SESSION_DETAILS, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// ADD TO FAVOURITE
const add_favourite_service = async (car_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.ADD_TO_FAVOURITE */ .T5.ADD_TO_FAVOURITE, {
        car_id: car_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// NOTIFICATION COUNT
const notification_count_service = async ()=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_1__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_1__/* .API_URL.NOTIFICATION_COUNT */ .T5.NOTIFICATION_COUNT, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 401) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};


/***/ }),

/***/ 5994:
/***/ (() => {



/***/ })

};
;