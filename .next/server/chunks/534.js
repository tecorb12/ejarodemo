"use strict";
exports.id = 534;
exports.ids = [534];
exports.modules = {

/***/ 3534:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4375);
/* harmony import */ var _styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4063);
/* harmony import */ var _styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_common_route__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1191);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1664);







const LinkTag = ({ activeClass , as , href , iconPath , name , count  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_5__["default"], {
            as: as,
            href: href,
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                className: activeClass,
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                    href: "#tabs-1",
                    role: "tab",
                    "data-toggle": "pill",
                    children: [
                        name,
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            children: [
                                count,
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    src: iconPath,
                                    style: {
                                        marginLeft: " 8px"
                                    }
                                })
                            ]
                        })
                    ]
                })
            })
        })
    }));
};
const BookingSideNav = ({ requestCount , historyCount , currentCount  })=>{
    let { pathname  } = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
    let requested_booking = pathname === _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.RequestedBooking.as */ .Z.Booking.RequestedBooking.as;
    let booking_history = pathname === _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.BookingHistory.as */ .Z.Booking.BookingHistory.as;
    let current_booking = pathname === _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.CurrentBooking.as */ .Z.Booking.CurrentBooking.as;
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().showStatus),
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().leftMenu),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                        children: "Bookings"
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                        className: `nav nav-pills nav-stacked ${(_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().NavSec)}`,
                        role: "tablist",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkTag, {
                                activeClass: requested_booking ? (_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().active) : "",
                                as: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.RequestedBooking.as */ .Z.Booking.RequestedBooking.as,
                                href: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.RequestedBooking.href */ .Z.Booking.RequestedBooking.href,
                                iconPath: requested_booking ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png",
                                iconAlt: "Requests",
                                count: requestCount,
                                name: "Requests"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkTag, {
                                activeClass: current_booking ? (_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().active) : "",
                                as: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.CurrentBooking.as */ .Z.Booking.CurrentBooking.as,
                                href: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.CurrentBooking.href */ .Z.Booking.CurrentBooking.href,
                                count: currentCount,
                                iconPath: current_booking ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png",
                                iconAlt: "Current",
                                name: "Current"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkTag, {
                                activeClass: booking_history ? (_styles_bookingList_module_scss__WEBPACK_IMPORTED_MODULE_6___default().active) : "",
                                as: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.BookingHistory.as */ .Z.Booking.BookingHistory.as,
                                href: _components_common_route__WEBPACK_IMPORTED_MODULE_4__/* ["default"].Booking.BookingHistory.href */ .Z.Booking.BookingHistory.href,
                                count: historyCount,
                                iconPath: booking_history ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png",
                                iconAlt: "History",
                                name: "History"
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BookingSideNav);


/***/ })

};
;