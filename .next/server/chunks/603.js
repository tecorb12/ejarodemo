exports.id = 603;
exports.ids = [603];
exports.modules = {

/***/ 6191:
/***/ ((module) => {

// Exports
module.exports = {
	"customeModal": "customeModal_customeModal__qCwRQ",
	"dialogBox": "customeModal_dialogBox___aK1n",
	"close": "customeModal_close__f4XWd",
	"sendBtn": "customeModal_sendBtn__9iKgN",
	"backBtn": "customeModal_backBtn__Bsi8X",
	"continueBtn": "customeModal_continueBtn__FFXQ_",
	"sendBtn1": "customeModal_sendBtn1__PChTd",
	"smallHide": "customeModal_smallHide__sEqr2",
	"bannerSec": "customeModal_bannerSec__puMbJ",
	"detailText": "customeModal_detailText__RN3bk",
	"detailTextFooter": "customeModal_detailTextFooter__wHDH2",
	"customeFormGroup": "customeModal_customeFormGroup__p9dNi",
	"photoSec": "customeModal_photoSec__qRbA4",
	"radio": "customeModal_radio__dALfw",
	"radioLabel": "customeModal_radioLabel__novCP",
	"OtpSec": "customeModal_OtpSec__0PB9n",
	"phoneInput": "customeModal_phoneInput__NxEJR",
	"progressBar": "customeModal_progressBar__6IMLT",
	"Active": "customeModal_Active__gcptt",
	"rangInput": "customeModal_rangInput__aaTtU",
	"customeLoginModal": "customeModal_customeLoginModal__Ag6F2",
	"loginSec": "customeModal_loginSec__Y6qRu",
	"loginPadding": "customeModal_loginPadding__JECpe",
	"customeSignUpModal": "customeModal_customeSignUpModal__2vjLQ",
	"registrationSec": "customeModal_registrationSec__wmcOO",
	"onlyHeading": "customeModal_onlyHeading__MOgoM",
	"colseBtn": "customeModal_colseBtn__6x8TB",
	"action": "customeModal_action__IhofF",
	"actionButton": "customeModal_actionButton__p_mR_",
	"parkBtn": "customeModal_parkBtn__Isvbb",
	"shareBtn": "customeModal_shareBtn__KETu1",
	"imageActionButton": "customeModal_imageActionButton__1_d5H",
	"imageActionSection": "customeModal_imageActionSection__oYIN0",
	"imageArea": "customeModal_imageArea__tpHeD",
	"cropped": "customeModal_cropped__JOLKF",
	"content": "customeModal_content__Ykhs6",
	"HeadingSection": "customeModal_HeadingSection__m4mpv",
	"disable": "customeModal_disable__xBQcL",
	"customeAddCarModal": "customeModal_customeAddCarModal__s7bwl",
	"addCarSec": "customeModal_addCarSec__0L5V_",
	"carBanner": "customeModal_carBanner__6WyH4",
	"mapSec": "customeModal_mapSec__bgt4E",
	"mapDiv": "customeModal_mapDiv__49SyX",
	"editPara": "customeModal_editPara___7Ztu",
	"editButton": "customeModal_editButton__VLjm4",
	"deliveryLocation": "customeModal_deliveryLocation__8AFtS",
	"Calendar": "customeModal_Calendar__fVFdw",
	"calenderHead": "customeModal_calenderHead__b1Y2W",
	"calendarInfo": "customeModal_calendarInfo__pPBSv",
	"box": "customeModal_box__Yd8FT",
	"available": "customeModal_available__iTE0S",
	"not": "customeModal_not__U6AUi",
	"booked": "customeModal_booked__CL4Rc",
	"pending": "customeModal_pending__Jpkd8",
	"bookingDetailSec": "customeModal_bookingDetailSec__eap0_",
	"RadioSec": "customeModal_RadioSec__Ubw8m",
	"floatRight": "customeModal_floatRight__lh5zU",
	"editPriceSec": "customeModal_editPriceSec__vSdKb",
	"editPriceBtn": "customeModal_editPriceBtn__dr_nT",
	"ShowDiv": "customeModal_ShowDiv__wlMP3",
	"instructionSec": "customeModal_instructionSec__kyWAu",
	"photoSection": "customeModal_photoSection__U4r6C",
	"ImgArea": "customeModal_ImgArea__PVAaN",
	"imageBox": "customeModal_imageBox__mWevS",
	"front": "customeModal_front__e0DJq",
	"side": "customeModal_side__SSfmi",
	"back": "customeModal_back__UnUSQ",
	"interior": "customeModal_interior__yjMRU",
	"addNew": "customeModal_addNew__5C6b2",
	"AddText": "customeModal_AddText__qN5Hs",
	"counter": "customeModal_counter__2Zznw",
	"addBtn": "customeModal_addBtn__ZayjB",
	"dragBtn": "customeModal_dragBtn__IGqj1",
	"insuranceSec": "customeModal_insuranceSec__rITT3",
	"detail": "customeModal_detail__jxNbC",
	"optionSec": "customeModal_optionSec___X51m",
	"fileUpload": "customeModal_fileUpload__zfoeK",
	"imgUploadArea": "customeModal_imgUploadArea__jGL94",
	"list": "customeModal_list__f_XxK",
	"toggle": "customeModal_toggle__YV8Yy",
	"Info": "customeModal_Info__Rj3LX",
	"customeCalender": "customeModal_customeCalender__pQi9j",
	"customeRangCalender": "customeModal_customeRangCalender__DqDz4",
	"carProfileModal": "customeModal_carProfileModal__To456",
	"imgArea": "customeModal_imgArea__BKj36",
	"instruction": "customeModal_instruction__udAZ6",
	"infoBlock": "customeModal_infoBlock__vMSd4",
	"infoimg": "customeModal_infoimg__mzh78",
	"infoimg2": "customeModal_infoimg2__le8C_",
	"infoimg3": "customeModal_infoimg3__8uF6K",
	"bookedDate": "customeModal_bookedDate__BGum_",
	"react-datepicker__day": "customeModal_react-datepicker__day__KpaFU",
	"react-datepicker__month-text": "customeModal_react-datepicker__month-text__faJ3q",
	"react-datepicker__quarter-text": "customeModal_react-datepicker__quarter-text__b6kTy",
	"react-datepicker__year-text": "customeModal_react-datepicker__year-text__cATOc",
	"react-datepicker__day--highlighted": "customeModal_react-datepicker__day--highlighted__4zWkF",
	"react-datepicker__month-text--highlighted": "customeModal_react-datepicker__month-text--highlighted__M9qYV",
	"react-datepicker__quarter-text--highlighted": "customeModal_react-datepicker__quarter-text--highlighted__53ORz",
	"react-datepicker__year-text--highlighted": "customeModal_react-datepicker__year-text--highlighted__WnYhE",
	"react-datepicker__day--highlighted-custom-1": "customeModal_react-datepicker__day--highlighted-custom-1__q8xyZ",
	"react-datepicker__month-text--highlighted-custom-1": "customeModal_react-datepicker__month-text--highlighted-custom-1__KPH8n",
	"react-datepicker__quarter-text--highlighted-custom-1": "customeModal_react-datepicker__quarter-text--highlighted-custom-1__vP5Vf",
	"react-datepicker__year-text--highlighted-custom-1": "customeModal_react-datepicker__year-text--highlighted-custom-1__XL6RJ",
	"react-datepicker__day--highlighted-custom-2": "customeModal_react-datepicker__day--highlighted-custom-2__wolWe",
	"react-datepicker__month-text--highlighted-custom-2": "customeModal_react-datepicker__month-text--highlighted-custom-2__bTFhJ",
	"react-datepicker__quarter-text--highlighted-custom-2": "customeModal_react-datepicker__quarter-text--highlighted-custom-2__nLsMa",
	"react-datepicker__year-text--highlighted-custom-2": "customeModal_react-datepicker__year-text--highlighted-custom-2__2GKou"
};


/***/ }),

/***/ 4677:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8557);
/* harmony import */ var google_map_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(google_map_react__WEBPACK_IMPORTED_MODULE_2__);



const AnyReactComponent = ({ text  })=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        style: {
            position: 'absolute',
            transform: 'translate(-50%, -100%)'
        },
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
            alt: "",
            src: "/favicon.png",
            style: {
                width: "26px",
                height: "40px"
            }
        })
    })
;
const SimpleMap = ({ setLocation , location , mapClass  })=>{
    let defaultProps = {
        center: {
            lat: 28.6260606,
            lng: 77.3668853
        },
        zoom: 12
    };
    const handleApiLoaded = (map, maps)=>{
    };
    const changeMarkerPosition = (e)=>{
        console.log(e);
        setLocation((prev)=>{
            return {
                ...prev,
                latitude: e.lat,
                longitude: e.lng
            };
        });
    };
    const onChildMouseMove = (hoverKey, childProps, mouse)=>{
        console.log('move mouse', mouse);
    // Change item data with new coordinates
    // you need set here own store and update function
    // this.setState({
    //   lat: mouse.lat,
    //   lng: mouse.lng
    // }, () => this.props.updateLatLng(this.state.lat, this.state.lng))
    };
    const style = {
        overflow: "hidden",
        height: "455px",
        width: "100%",
        marginTop: "21px"
    };
    const style_two = {
        height: "100%",
        width: "100%",
        position: "relative",
        overflow: "hidden"
    };
    const style_three = {
        height: "100%",
        width: "100%",
        position: "absolute",
        top: "0px",
        left: "0px",
        backgroundColor: "rgb(229, 227, 223)"
    };
    return(// Important! Always set the container height explicitly
    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: `sec2map ${mapClass}`,
        style: style,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            style: style_two,
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((google_map_react__WEBPACK_IMPORTED_MODULE_2___default()), {
                bootstrapURLKeys: {
                    key: "AIzaSyBE5Z9JilAE4jJeOftQEkQnwqunbvCu3BU&libraries=places",
                    language: "en"
                },
                defaultCenter: defaultProps.center,
                defaultZoom: defaultProps.zoom,
                yesIWantToUseGoogleMapApiInternals: true,
                onGoogleApiLoaded: ({ map , maps  })=>handleApiLoaded(map, maps)
                ,
                style: style_three,
                onClick: (e)=>{
                    changeMarkerPosition(e);
                },
                onChildMouseMove: (e)=>{
                    onChildMouseMove(e);
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(AnyReactComponent, {
                    lat: location.latitude,
                    lng: location.longitude,
                    text: "My",
                    hover: "Click me!!"
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SimpleMap);


/***/ }),

/***/ 9500:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "B9": () => (/* binding */ placeAutoComplete),
/* harmony export */   "FG": () => (/* binding */ placeDetails),
/* harmony export */   "B3": () => (/* binding */ getAddressFromLatLong)
/* harmony export */ });
/* unused harmony exports placeDetail, geoCode, getGeocodeAddress */
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

async function placeAutoComplete(payload) {
    // &components=country:CA
    if (true) {
        return axios__WEBPACK_IMPORTED_MODULE_0___default().get(`https://frozen-tundra-07943.herokuapp.com/https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${payload.input}&key=${payload.key}&radius=${payload.radius}&location=${payload.location.lat + "," + payload.location.lng}`).then((response)=>{
            // console.log('Auto Complete Response', response.data.predictions)
            return response.data.predictions;
        }).catch((error)=>{
            console.log("error", error);
            return error.response;
        });
    } else {}
}
function placeDetail(payload) {
    if (true) {
        return Axios.get(`https://frozen-tundra-07943.herokuapp.com/https://maps.googleapis.com/maps/api/place/details/json?key=${payload.key}&placeid=${payload.place_id}`).then((result)=>{
            return result.data.result.geometry.location;
        }, (error)=>{
            console.log('error', error);
            if (error.status !== 500) {
            // Alerts.showAlert(error.data.message, false)
            } else {
            // Alerts.showAlert('Oops! something went wrong')
            }
            return error;
        });
    } else {}
}
function placeDetails(payload) {
    if (true) {
        return axios__WEBPACK_IMPORTED_MODULE_0___default().get(`https://frozen-tundra-07943.herokuapp.com/https://maps.googleapis.com/maps/api/place/details/json?key=${payload.key}&placeid=${payload.place_id}`).then((result)=>{
            return result.data.result;
        }, (error)=>{
            console.log('error', error);
            if (error.status !== 500) {
            // Alerts.showAlert(error.data.message, false)
            } else {
            // Alerts.showAlert('Oops! something went wrong')
            }
            return error;
        });
    } else {}
}
function getAddressFromLatLong(payload) {
    // console.log("Payload of get address ", payload)
    // https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
    if (true) {
        return axios__WEBPACK_IMPORTED_MODULE_0___default().get(`https://frozen-tundra-07943.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?latlng=${payload.lat},${payload.lng}&key=${payload.key}`, {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        }).then((result)=>{
            return result.data.results[0];
        }, (error)=>{
            console.log('error', error);
            if (error.status !== 500) {
            // Alerts.showAlert(error.data.message, false)
            } else {
            // Alerts.showAlert('Oops! something went wrong')
            }
            return error;
        });
    } else {}
}
function geoCode(payload) {
    if (true) {
        console.log("payload : " + JSON.stringify(payload));
        console.log("api name : " + JSON.stringify(Constants.API.GeoCode));
        return Axios.get('json', payload, Constants.API.GeoCode).then((result)=>{
            // console.log('result', result)
            return result;
        }, (error)=>{
            console.log('error', error);
            if (error.status !== 500) {
            // Alerts.showAlert(error.data.message, false)
            } else {
            // Alerts.showAlert('Oops! something went wrong')
            }
            return error;
        });
    } else {}
}
function getGeocodeAddress(payload) {
    if (true) {
        return Axios.post(Constants.API.GetGeocodeAddress, payload).then((result)=>{
            // console.log('result', result)
            return result;
        }, (error)=>{
            console.log('error', error);
            if (error.status !== 500) {
            // Alerts.showAlert(error.data.message, false)
            } else {
            // Alerts.showAlert('Oops! something went wrong')
            }
            return error;
        });
    } else {}
}


/***/ }),

/***/ 4257:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9931);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6191);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3__);





const SuccessResponseModal = ({ open , setSuccessresponse , message  })=>{
    const closeclick = ()=>{
        global.modal = false;
        setSuccessresponse(false);
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
            className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeModal)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().customeSignUpModal)}`,
            isOpen: open,
            onRequestClose: closeclick,
            style: {
                overflowY: "scroll"
            },
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().dialogBox),
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().registrationSec)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().onlyHeading)}`,
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("form", {
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            onClick: closeclick,
                                            className: `float-right close mr-0 mt-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().colseBtn)}`,
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "x"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12 text-center",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                            className: "text-center",
                                            children: "Ejaro"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-12",
                                        style: {
                                            width: "250px"
                                        },
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            style: {
                                                color: "#212529"
                                            },
                                            children: [
                                                " ",
                                                message,
                                                " "
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                class: "row",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    class: "col-12",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                        onClick: closeclick,
                                        style: {
                                            float: "unset"
                                        },
                                        className: ` mt-0 mr-2 mb-3 `,
                                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_3___default().sendBtn),
                                        children: "OKAY"
                                    })
                                })
                            })
                        ]
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SuccessResponseModal);


/***/ }),

/***/ 6854:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_n": () => (/* binding */ BASE_URL),
/* harmony export */   "T5": () => (/* binding */ API_URL)
/* harmony export */ });
/* unused harmony exports Proxy_URL, BASE_URL_TWO */
const Proxy_URL = "https://frozen-tundra-07943.herokuapp.com";
const BASE_URL = "https://www.ejaro.com:3000/api/v1";
const BASE_URL_TWO = "https://www.ejaro.com:3000/v1";
const API_URL = {
    // ------- SIGNIN -------
    LOGIN: "/common/signin",
    SIGNUP: "/common/new/signup",
    DUPLICATE_USER: "/common/duplicate/user",
    VERIFY_REFERALCODE: "/common/verify/referalcode",
    VERIFY_IQAMA_NUMBER: "/common/resident/info",
    VERIFY_SOUDI_NUMBER: "/common/citizen/info/2",
    VERIFY_VISA_INFO: "/common/visa/info",
    SESSION_DETAILS: "/common/session/details",
    YAKEEN: "/common/yakeen/verified/user",
    LOGOUT: "/common/signout",
    SEND_OTP: "/common/send/otp",
    OTP_VERIFY: "/common/verify/otp",
    ADD_CLIENT: "/client/AddClientBasicDetail",
    UPDATE_PASSWORD: "/common/update/password",
    FORGET_PASSWORD: "/common/forget/password",
    BOOKING_LIST: "/common/booking/list",
    BOOKING_CAR_PROFILE: "/web/car/estimate/page",
    CAR_ESTIMATE_PRICE: "/common/booking/price/estimate",
    HOMEPAGE: "/mob/homepage/listing",
    RECENT_PLACES: "/mob/recent/place",
    CAR_LIST: "/mob/cars/list",
    NOTIFICATION_LIST: "/common/activities",
    READ_NOTIFICATION: "/common/read/activities",
    BOOKING_DETAILS: "/common/booking/details",
    PARTICULAR_CHAT_INDEX_LIST: "/chats/messages/index",
    QUICK_MESSAGE_SUGGESTION: "/common/quick/reply",
    CAR_DETAILS: "/mob/car/details",
    ALL_CHAT_LIST: "/chats/index",
    SEND_MESSAGE: "/chats/messages/create",
    BOOKING_TRIP_RECIEPT: "/common/booking/trip/receipt",
    FAVOURITE_LIST: "/web/user/profile",
    ADD_TO_FAVOURITE: "/common/update/favourite",
    SHOW_WALLET: "/common/show/wallet",
    CARD_LIST: "/common/list/cards",
    TRASCTION_LIST: "/common/transactions",
    LiCENSE_DETAILS: "/common/license/details",
    USER_PROFILE: "/web/user/profile",
    SHOW_NOTIFICATION_TYPE: "/common/notification/user",
    DEACTIVATE_REASON: "/deactivate/reasons",
    UPDATE_USER_EMAIL: "/common/update/email",
    UPDATE_USER_CONTACT: "/common/check/contact",
    DELETE_CAR: "/common/car/list/action",
    CAR_CALENDAR_PRICE: "/common/calendar/prices",
    CALENDAR_REVISE: "/common/calendar/price/revised",
    CALENDAR_AVAILIBILITY: "/common/car/availability/calendar",
    PRELOAD_API: "/web/car/preloads",
    FINANCE_COMPANY_LIST: "/common/finance/company/list",
    VEHICLE_MODAL: "/common/models/preloads",
    ADD_NEW_CAR: "/common/new/car/create",
    DUPLICATE_CAR_CHECk: "/common/duplicate/car",
    CAR_UPDATION: "/web/upload/registration",
    YAKEEN_VEHICLE_INFO: "/common/vehicle/info/2",
    VEHICLE_DETAILS: "/web/car/listing/details",
    STEP_THREE_PRICE: "/web/car/create/step/three",
    STEP_FOUR_PHOTO: "/web/car/create/step/four",
    EDIT_PRICE_DETAILS: "/common/pricing/details",
    CAR_UPDATE_FLAT_PRICE: "/common/update/flat/variable/price",
    UPDATE_DISCOUNTS: "/common/update/discounts",
    UPDATE_KM: "/common/car/distance/update",
    CAR_EXTRA_DETAILS: "/common/car/details",
    UPDATE_EXTRA_DETAILS: "/common/update/car/detail",
    INSURANCE_DETAILS: "/mob/insurance/details",
    INSURANCE_LIST: "/common/insurance/company/list",
    SELECT_INSURANCE_TYPE: "/mob/insurance/select",
    IMAGE_UPLOAD: "/common/car/upload/image",
    EARNING: "/common/owner/earning",
    REVIEWS_LIST: "/common/car/reviews/list",
    DATE_AVAILABILITY: "/common/toggle/availablity",
    MY_CAR_IMAGES: "/common/car/images",
    NOTIFICATION_COUNT: "/common/count/notification",
    DELIVERY_LOCATION_LIST: "/common/guest/delivery/location/list",
    DELIVERY_LOCATIONS: "/common/guest/delivery/location",
    DELETE_DELIVERY_ADDRESS: "/common/delete/guest/delivery/location"
};


/***/ }),

/***/ 7819:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "U": () => (/* binding */ Location_key)
/* harmony export */ });
const Location_key = "AIzaSyBE5Z9JilAE4jJeOftQEkQnwqunbvCu3BU";


/***/ })

};
;