"use strict";
exports.id = 186;
exports.ids = [186];
exports.modules = {

/***/ 7883:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Bt": () => (/* binding */ myCar_service),
/* harmony export */   "iG": () => (/* binding */ myCar_details_service),
/* harmony export */   "fW": () => (/* binding */ mycar_action_service),
/* harmony export */   "Es": () => (/* binding */ price_availablity_service),
/* harmony export */   "GY": () => (/* binding */ calendar_availability_service),
/* harmony export */   "II": () => (/* binding */ selected_date_service),
/* harmony export */   "bd": () => (/* binding */ car_all_images_service)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4585);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstz__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _api_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6854);
// USER DETAILS API 



const timezone = jstz__WEBPACK_IMPORTED_MODULE_1___default().determine();
const ISSERVER = "undefined" === "undefined";
let GUEST_TOKEN = "";
let SESSION_TOKEN = "";
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token, SESSION_TOKEN = localStorage.token;
}
//  MY CAR LIST
const myCar_service = async (keyword)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CAR_LIST */ .T5.CAR_LIST, {
        keyword: keyword,
        page: 1,
        per_page: 15
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  MY CAR DETAILS
const myCar_details_service = async (car_id, user_id, latitude, longitude, tripStart, tripEnd)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CAR_DETAILS */ .T5.CAR_DETAILS, {
        "car_id": car_id,
        "user_id": SESSION_TOKEN ? user_id : "",
        "latitude": latitude,
        "longitude": longitude,
        "fromDate": tripStart,
        "tillDate": tripEnd
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  MY CAR DETAILS
const mycar_action_service = async (car_id, requested, unlistReason)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.DELETE_CAR */ .T5.DELETE_CAR, {
        carId: car_id,
        requestFor: requested,
        unlistReason: unlistReason
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  CAR AVAILABLITY PRICE
const price_availablity_service = async (car_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CALENDAR_AVAILIBILITY */ .T5.CALENDAR_AVAILIBILITY, {
        carId: car_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  CAR AVAILABLITY 
const calendar_availability_service = async (car_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CALENDAR_REVISE */ .T5.CALENDAR_REVISE, {
        carId: car_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// DATE SELECTED FOR AVAILABILITY
const selected_date_service = async (car_id, startDate)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.DATE_AVAILABILITY */ .T5.DATE_AVAILABILITY, {
        carId: car_id,
        startDate: startDate
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// MY CAR ALL IMAGES
const car_all_images_service = async (car_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.MY_CAR_IMAGES */ .T5.MY_CAR_IMAGES, {
        carId: car_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};


/***/ })

};
;