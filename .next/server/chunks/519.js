"use strict";
exports.id = 519;
exports.ids = [519];
exports.modules = {

/***/ 8191:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9931);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6191);
/* harmony import */ var _styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _helpers_booking_services_jsx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3551);
/* harmony import */ var _helpers_keys__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7819);
/* harmony import */ var _AddCarModal_geolocation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9500);
/* harmony import */ var _AddCarModal_Map_contact_us_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4677);
/* harmony import */ var _succefully_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4257);









const PickUpLocationModal = ({ className , style , carId , place , text , type , estimatePric , setEstimatePrice  })=>{
    const { 0: open , 1: setOpen  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isStatus , 1: setStaus  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isDivSatus , 1: setDivSatus  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isDelivery , 1: setDelivery  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const { 0: pickUpDetail , 1: setPickupAddress  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    const { 0: deliveryLocation , 1: setDeliveryLocation  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    const { 0: hiddenData , 1: setHiddenData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: editButton , 1: setEditData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: isSuccess , 1: setSuccessModal  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    let userID =  false && 0;
    const { 0: location , 1: setLocation  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
        latitude: "",
        longitude: "",
        country: "",
        zip: "",
        state: "",
        city: "",
        streetAddress: ""
    });
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(async ()=>{
        let payload = {
            lat: location.latitude,
            lng: location.longitude,
            key: _helpers_keys__WEBPACK_IMPORTED_MODULE_7__/* .Location_key */ .U
        };
        const currentAddress = await (0,_AddCarModal_geolocation__WEBPACK_IMPORTED_MODULE_4__/* .getAddressFromLatLong */ .B3)(payload);
        getCurrentAddress(currentAddress);
    }, [
        location.latitude
    ]);
    const getCurrentAddress = async (currentAddress)=>{
        var ref;
        setLocation((prev)=>{
            return {
                ...prev,
                streetAddress: currentAddress.formatted_address
            };
        });
        let type = (ref = currentAddress.address_components) === null || ref === void 0 ? void 0 : ref.map((item)=>{
            switch(item.types[0]){
                case "country":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            country: item.long_name
                        };
                    });
                    break;
                case "postal_code":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            zip: item.long_name
                        };
                    });
                    break;
                case "administrative_area_level_1":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            state: item.long_name
                        };
                    });
                    break;
                case "locality":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            city: item.long_name
                        };
                    });
                    break;
            }
        });
    };
    const isEditChecked = (e)=>{
        e.preventDefault();
        if (isDivSatus === false) {
            setDivSatus(true);
        } else {
            setDivSatus(false);
        }
    };
    const isDisabledChecked = (e)=>{
        e.preventDefault();
        if (isStatus === false) {
            setStaus(true);
        } else {
            setStaus(false);
        }
    };
    const EditDelivery = (e)=>{
        e.preventDefault();
        setDelivery(!isDelivery);
    };
    const openModal = (e)=>{
        e.preventDefault();
        deliveryLocationList();
        setOpen(true);
    };
    const deliveryLocationList = async ()=>{
        const response = await (0,_helpers_booking_services_jsx__WEBPACK_IMPORTED_MODULE_3__/* .car_delivery_location_list_service */ .EU)(carId, userID);
        if (response.code == 200) {
            setPickupAddress(response.result.pickupAddress);
            setDeliveryLocation(response.result.deliveryLocations);
            setLocation((prev)=>{
                return {
                    ...prev,
                    latitude: response.result.pickupAddress.latitude,
                    longitude: response.result.pickupAddress.longitude
                };
            });
        }
    };
    const handleDeleteAddress = async (e, deliveryLocationId)=>{
        e.preventDefault();
        const response = await (0,_helpers_booking_services_jsx__WEBPACK_IMPORTED_MODULE_3__/* .delete_address_service */ .xt)(deliveryLocationId);
        if (response.code == 200) {
            let temp_array = deliveryLocation === null || deliveryLocation === void 0 ? void 0 : deliveryLocation.filter((item)=>item.id != deliveryLocationId
            );
            setDeliveryLocation([
                ...temp_array
            ]);
            setSuccessModal(true);
        }
    };
    const closeModal = (e)=>{
        e.preventDefault();
        setOpen(false);
        setDelivery(true);
        setHiddenData(false);
    // setEditData(false);
    };
    const handleClick = (e)=>{
        e.preventDefault();
        setHiddenData(true);
    };
    const handleEdit = (e)=>{
        e.preventDefault();
        setEditData(true);
    };
    const handleDeliveryLocation = async ()=>{
        const response = await (0,_helpers_booking_services_jsx__WEBPACK_IMPORTED_MODULE_3__/* .car_delivery_location_service */ .yb)(carId, location.latitude, location.longitude, location.city, location.state, location.country, location.streetAddress, location.zip, userID);
        console.log(response);
        if (response.code == 200) {
            const estimatePriceResponse = await (0,_helpers_booking_services_jsx__WEBPACK_IMPORTED_MODULE_3__/* .car_estimate_price_service */ ._)(carId, location.latitude, location.longitude, response.delivery.id, userID);
            setOpen(false);
            setDelivery(true);
            setHiddenData(false);
            setEditData(false);
            if (estimatePriceResponse.code == 200) {
                setEstimatePrice(response.result);
            }
        }
    };
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: [
            type == "carDetail" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: style.formGroup,
                    onClick: openModal,
                    children: [
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    src: "/images/others/icon/city_address_icon.svg",
                                    alt: ""
                                }),
                                "Pick up & delivery location"
                            ]
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: `${style.floatRight} float-right`,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                src: "/images/others/icon/carProfile/red_slider.png",
                                alt: ""
                            })
                        })
                    ]
                })
            }),
            type == "bookingDetail" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: style.smallHide,
                    onClick: openModal,
                    children: "Edit"
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeModal)} ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeAddCarModal)}`,
                isOpen: open,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().dialogBox),
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            onClick: closeModal,
                            className: `float-right close mr-0 mt-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().close)}`,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                children: "x"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().registrationSec),
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("form", {
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "row",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "col-12",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                        children: "Pick-up & Return Location"
                                                    }),
                                                    isDivSatus == false || type == "carDetail" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                children: " Pick up & return at your location is included in the rental price for no additional fee "
                                                            }),
                                                            " "
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "col-12",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().addCarSec),
                                                    children: [
                                                        isDivSatus == false || type == "carDetail" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                className: "row",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                        className: "col-10",
                                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                            className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().editPara),
                                                                            children: " B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India "
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                        className: "col-2",
                                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                            className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().editButton),
                                                                            onClick: (e)=>{
                                                                                isEditChecked(e);
                                                                            },
                                                                            children: isDivSatus == false ? "Edit" : "Done"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().mapSec),
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_AddCarModal_Map_contact_us_map__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                                                mapClass: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().mapDiv),
                                                                setLocation: setLocation,
                                                                location: location
                                                            })
                                                        }),
                                                        hiddenData && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                            onClick: handleEdit,
                                                            children: "Edit"
                                                        }),
                                                        isDivSatus == false || type == "carDetail" && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().deliveryLocation),
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                        children: "Delivery Locations"
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                        children: " Enable delivery option to increase your rentals! Guests like the option of having the vehicle delivered to them  "
                                                                    }),
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                        children: [
                                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: "row",
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                        className: "col-10",
                                                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                            children: " Enable Recommended Pricing "
                                                                                        })
                                                                                    }),
                                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                        className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().toggle)} col-2`,
                                                                                        children: [
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                                type: "checkbox",
                                                                                                id: "switch",
                                                                                                onChange: (e)=>{
                                                                                                    isDisabledChecked(e);
                                                                                                }
                                                                                            }),
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                                for: "switch"
                                                                                            })
                                                                                        ]
                                                                                    })
                                                                                ]
                                                                            }),
                                                                            isStatus == true && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        className: "mb-1",
                                                                                        children: "Delivery fee"
                                                                                    }),
                                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("select", {
                                                                                        children: [
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                                                value: "",
                                                                                                children: " Free Delivery "
                                                                                            }),
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                                                value: "25",
                                                                                                children: " 25 "
                                                                                            }),
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                                                value: "50",
                                                                                                children: " 50 "
                                                                                            }),
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                                                value: "75",
                                                                                                children: " 75 "
                                                                                            }),
                                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
                                                                                                value: "100",
                                                                                                children: " 500 "
                                                                                            })
                                                                                        ]
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().Info),
                                                                                        children: " 0.0 SAR is the recommended price "
                                                                                    })
                                                                                ]
                                                                            }),
                                                                            isStatus == false && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                                                children: " "
                                                                            })
                                                                        ]
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        type == "bookingDetail" && isDelivery == true && !hiddenData && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().bookingDetailSec),
                                                                children: [
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().RadioSec),
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                                className: "mb-0",
                                                                                children: "Pick-up Location"
                                                                            }),
                                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "radio",
                                                                                        id: "test1",
                                                                                        name: "radio-group"
                                                                                    }),
                                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("label", {
                                                                                        for: "test1",
                                                                                        children: [
                                                                                            place,
                                                                                            " "
                                                                                        ]
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                        className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().floatRight)} float-right`,
                                                                                        children: "No Charge"
                                                                                    })
                                                                                ]
                                                                            }),
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                                children: " The exact address will be sent once you book the vehicle "
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().RadioSec),
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                                className: "mb-0",
                                                                                children: "Delivery Location"
                                                                            }),
                                                                            deliveryLocation.length > 0 ? deliveryLocation.map((item, index)=>{
                                                                                return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                                                    children: [
                                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                            type: "radio",
                                                                                            id: item.id,
                                                                                            name: "radio-group"
                                                                                        }),
                                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("label", {
                                                                                            for: item.id,
                                                                                            children: [
                                                                                                item.city + " " + item.state + " " + item.country,
                                                                                                " "
                                                                                            ]
                                                                                        }),
                                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                                            className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().floatRight)} float-right`,
                                                                                            children: [
                                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                                    onClick: EditDelivery,
                                                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                                                        src: "/images/icons/edit.svg"
                                                                                                    })
                                                                                                }),
                                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                                                        src: "/images/icons/delete.svg",
                                                                                                        onClick: (e)=>{
                                                                                                            handleDeleteAddress(e, item.id);
                                                                                                        }
                                                                                                    })
                                                                                                }),
                                                                                                item.guestLocationDeliveryPrice == 0 ? "Free Delivery" : item.guestLocationDeliveryPrice
                                                                                            ]
                                                                                        })
                                                                                    ]
                                                                                }));
                                                                            }) : ""
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: (_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().RadioSec),
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                                className: "mb-0",
                                                                                children: "Have it Delivered To You"
                                                                            }),
                                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "radio",
                                                                                        id: "test2",
                                                                                        onClick: (e)=>{
                                                                                            handleClick(e);
                                                                                        },
                                                                                        name: "radio-group"
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        for: "test2",
                                                                                        children: "Enter a delivery address  "
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                        className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().floatRight)} float-right`,
                                                                                        children: "25 SAR"
                                                                                    })
                                                                                ]
                                                                            })
                                                                        ]
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        isDivSatus == true || isDelivery == false || editButton && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                    children: location.streetAddress
                                                                }),
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                    className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                    children: [
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                            children: "Address"
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                            type: "text",
                                                                            id: "",
                                                                            name: "streetAddress",
                                                                            placeholder: "123456789",
                                                                            value: location.streetAddress,
                                                                            autoComplete: "off",
                                                                            className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().stepbarInput)}`
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                    className: "row",
                                                                    children: [
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                            className: "col-md-6",
                                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        children: "City"
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "text",
                                                                                        id: "",
                                                                                        name: "city",
                                                                                        placeholder: "City",
                                                                                        value: location.city,
                                                                                        autoComplete: "off",
                                                                                        className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().stepbarInput)}`
                                                                                    })
                                                                                ]
                                                                            })
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                            className: "col-md-6",
                                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        children: "State"
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "text",
                                                                                        id: "",
                                                                                        name: "state",
                                                                                        placeholder: "State",
                                                                                        value: location.state,
                                                                                        autoComplete: "off",
                                                                                        className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().stepbarInput)}`
                                                                                    })
                                                                                ]
                                                                            })
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                            className: "col-md-6",
                                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        children: "Country"
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "text",
                                                                                        id: "",
                                                                                        name: "country",
                                                                                        placeholder: "Country",
                                                                                        value: location.country,
                                                                                        autoComplete: "off",
                                                                                        className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().stepbarInput)}`
                                                                                    })
                                                                                ]
                                                                            })
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                            className: "col-md-6",
                                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: `form-group px-0 ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().customeFormGroup)}`,
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
                                                                                        children: "Zip Code"
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                        type: "text",
                                                                                        id: "",
                                                                                        name: "zip",
                                                                                        placeholder: "Code",
                                                                                        value: location.zip,
                                                                                        autoComplete: "off",
                                                                                        className: `form-control ${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().stepbarInput)}`
                                                                                    })
                                                                                ]
                                                                            })
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "col-12",
                                                children: editButton ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: (e)=>{
                                                        e.preventDefault();
                                                        setHiddenData(false);
                                                        setEditData(false);
                                                    },
                                                    className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().sendBtn)}`,
                                                    children: "SAVE"
                                                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: (e)=>{
                                                        e.preventDefault();
                                                        handleDeliveryLocation(e);
                                                    },
                                                    className: `${(_styles_customeModal_module_scss__WEBPACK_IMPORTED_MODULE_8___default().sendBtn)}`,
                                                    children: "SAVE"
                                                })
                                            })
                                        ]
                                    })
                                })
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_succefully_modal__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                open: isSuccess,
                setSuccessresponse: setSuccessModal,
                message: "Delivery Location Deleted Successfully"
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PickUpLocationModal);


/***/ })

};
;