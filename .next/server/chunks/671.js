"use strict";
exports.id = 671;
exports.ids = [671];
exports.modules = {

/***/ 6790:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "B": () => (/* binding */ IdContext),
/* harmony export */   "v": () => (/* binding */ IdProvider)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const IdContext = /*#__PURE__*/ react__WEBPACK_IMPORTED_MODULE_1___default().createContext();
const IdProvider = (props)=>{
    const { 0: carId , 1: setCarId  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const { 0: userId , 1: setUserId  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const { 0: uploadRegistration , 1: setData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const { 0: newCarData , 1: setNewCarData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const { 0: userData , 1: setUserData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(IdContext.Provider, {
        value: {
            carId,
            setCarId,
            userId,
            setUserId,
            newCarData,
            setNewCarData,
            uploadRegistration,
            setData,
            userData,
            setUserData
        },
        children: props.children
    }));
};



/***/ }),

/***/ 4375:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ AddCarModal)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-modal"
var external_react_modal_ = __webpack_require__(9931);
var external_react_modal_default = /*#__PURE__*/__webpack_require__.n(external_react_modal_);
// EXTERNAL MODULE: ./styles/customeModal.module.scss
var customeModal_module = __webpack_require__(6191);
var customeModal_module_default = /*#__PURE__*/__webpack_require__.n(customeModal_module);
// EXTERNAL MODULE: ./components/helpers/new_vehicle_service.jsx
var new_vehicle_service = __webpack_require__(5931);
// EXTERNAL MODULE: ./components/MyContext/IdDetailsContext.js
var IdDetailsContext = __webpack_require__(6790);
// EXTERNAL MODULE: ./components/helpers/modify_car_details_service.jsx
var modify_car_details_service = __webpack_require__(8656);
// EXTERNAL MODULE: ./components/common/Modal/succefully_modal.jsx
var succefully_modal = __webpack_require__(4257);
;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/insurance.jsx






const Insurance = ({ setOpen  })=>{
    const { 0: id , 1: setId  } = (0,external_react_.useState)("");
    const { 0: policytype , 1: setpolicytype  } = (0,external_react_.useState)("");
    const { 0: provider , 1: setProvider  } = (0,external_react_.useState)("");
    const { 0: companyList , 1: setList  } = (0,external_react_.useState)([]);
    const { newCarData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    const handleChangeId = (e)=>{
        setId(e.target.value);
    };
    const selectChangeId = (e)=>{
        setpolicytype(e.target.value);
    };
    (0,external_react_.useEffect)(()=>{
        insurance_details();
    }, []);
    const insurance_details = async ()=>{
        let carId = newCarData.id;
        const response = await (0,modify_car_details_service/* car_insurance_service */.NJ)(carId);
        if (response.code == 200) {
            const res2 = await (0,modify_car_details_service/* car_insurance_list_service */.$F)();
            if (res2.code == 200) {
                setList(res2.company_list);
            }
            setId(response.insurance.is_Insured);
            setProvider(response.insurance.provider);
            setpolicytype(response.insurance.policyType);
        }
    };
    const Continue = async (e)=>{
        e.preventDefault();
        let carId = newCarData.id;
        const response = await (0,modify_car_details_service/* select_insurance_policy_service */.PG)(carId, provider, id, policytype);
        console.log(response);
        if (response.code == 200) {
            global.modal = true;
            window.location.reload();
            setOpen(false);
        }
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx("form", {
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "row",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "col-12",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                        children: "INSURANCE"
                    })
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "col-12",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (customeModal_module_default()).addCarSec,
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (customeModal_module_default()).insuranceSec,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    children: "You must have a comprehensive insurance policy."
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    children: "By law, all vehicles within Ejaro's community must have a comprehensive insurance policy to cover both vehicle owner and renter. Choose our specially designed policy and we got your back !"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (customeModal_module_default()).detail,
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup} mb-3`,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                    children: "Is your car insured?"
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                    autoComplete: "off",
                                                    value: id,
                                                    onChange: (e)=>{
                                                        handleChangeId(e);
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "",
                                                            children: " Please Select "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "true",
                                                            children: " Yes "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "false",
                                                            children: " No "
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        id == "true" && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                    children: "Choose your current insurance provider from our list of register companies "
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Who is your insurance provider?"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            autoComplete: "off",
                                                            value: provider,
                                                            onChange: (e)=>{
                                                                setProvider(e.target.value);
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select Provider "
                                                                }),
                                                                companyList === null || companyList === void 0 ? void 0 : companyList.map((item, index)=>{
                                                                    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                            value: item.name,
                                                                            id: item.id,
                                                                            children: [
                                                                                " ",
                                                                                item.name,
                                                                                " "
                                                                            ]
                                                                        })
                                                                    }));
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Policy type"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            autoComplete: "off",
                                                            value: policytype,
                                                            onChange: (e)=>{
                                                                selectChangeId(e);
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select Provider "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "Comprehensive policy",
                                                                    children: " Comprehensive policy "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "Third party liability (TPL)",
                                                                    children: "Third party liability (TPL) "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        (id == "false" || id == "true" && policytype) && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: (customeModal_module_default()).optionSec,
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                        className: "mb-0 mt-3",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "(Recommended)"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                                class: "float-right",
                                                                children: [
                                                                    "Explore",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/help/about_icon.png",
                                                                        alt: "",
                                                                        style: {
                                                                            maxWidth: "13px",
                                                                            marginLeft: "5px",
                                                                            marginTop: "-3px"
                                                                        }
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                                        className: "mt-0",
                                                        children: " Ejaro's exclusive policy "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                            class: "clearfix",
                                                            style: {
                                                                fontFamily: "NeoSansRegular"
                                                            },
                                                            children: [
                                                                "Ejaro's exclusive comprehensive peer-to-peer ",
                                                                /*#__PURE__*/ jsx_runtime_.jsx("br", {
                                                                }),
                                                                " insurance policy in partnership with Medgulf",
                                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    class: "float-right",
                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/insurance/recommended.png",
                                                                        alt: ""
                                                                    })
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-12 px-0",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: `${(customeModal_module_default()).sendBtn}`,
                                                                onClick: (e)=>{
                                                                    Continue(e);
                                                                },
                                                                children: "Buy Policy"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: `${(customeModal_module_default()).backBtn}`,
                                                                onClick: (e)=>{
                                                                    Continue(e);
                                                                },
                                                                children: "CONTINUE WITHOUT EJARO POLICY"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const insurance = (Insurance);

// EXTERNAL MODULE: ./components/helpers/keys.jsx
var keys = __webpack_require__(7819);
// EXTERNAL MODULE: ./components/common/Modal/AddCarModal/geolocation.js
var geolocation = __webpack_require__(9500);
// EXTERNAL MODULE: ./components/common/Modal/AddCarModal/Map/contact_us_map.js
var contact_us_map = __webpack_require__(4677);
;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/progress.jsx


const ProgressSection = ({ step  })=>{
    const stepFunc = ()=>{
        let list = Array(7).fill("", 1).map((data, index)=>{
            if (index <= step) {
                return(/*#__PURE__*/ jsx_runtime_.jsx("li", {
                    className: (customeModal_module_default()).Active
                }));
            } else {
                return(/*#__PURE__*/ jsx_runtime_.jsx("li", {
                    className: ""
                }));
            }
        });
        return list;
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        class: "row",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "col-md-12 px-0",
            children: /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                className: (customeModal_module_default()).progressBar,
                children: stepFunc().map((data)=>data
                )
            })
        })
    }));
};
/* harmony default export */ const progress = (ProgressSection);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/pickupLocation.jsx







const PickUpLocation = ({ counter , location: location1 , setLocation , setCounter  })=>{
    var ref1;
    const { 0: isStatus , 1: setStaus  } = (0,external_react_.useState)(false);
    const { 0: locationStatus , 1: setLocationStatus  } = (0,external_react_.useState)(false);
    const { 0: suggestion , 1: setSuggestion  } = (0,external_react_.useState)([]);
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const permission = async (term)=>{
        const location = window.navigator && window.navigator.geolocation;
        if (location) {
            location.getCurrentPosition(async (position)=>{
                setLocation((prev)=>{
                    return {
                        ...prev,
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    };
                });
                let payload = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    key: keys/* Location_key */.U
                };
                const currentAddress = await (0,geolocation/* getAddressFromLatLong */.B3)(payload);
                getCurrentAddress(currentAddress);
                currentAddress.formatted_address !== '' && setDisabled(true);
            }, (error)=>{
                setLocation((prev)=>{
                    return {
                        ...prev,
                        latitude: 'err-latitude',
                        longitude: 'err-longitude'
                    };
                });
            });
        }
    };
    (0,external_react_.useEffect)(async ()=>{
        let payload = {
            lat: location1.latitude,
            lng: location1.longitude,
            key: keys/* Location_key */.U
        };
        permission();
        const currentAddress = await (0,geolocation/* getAddressFromLatLong */.B3)(payload);
        getCurrentAddress(currentAddress);
    }, [
        location1.latitude
    ]);
    const isDisabledChecked = ()=>{
        if (isStatus === false) {
            setStaus(true);
        } else {
            setStaus(false);
        }
    };
    const handleCounter = (e)=>{
        e.preventDefault();
        setCounter(1);
    };
    const handleAddress = async (e)=>{
        let value = e.target.value;
        setLocation((prev)=>{
            return {
                ...prev,
                streetAddress: value
            };
        });
        setLocationStatus(false);
        let payload = {
            input: value,
            location: {
                lat: "28.6508353",
                lng: "77.267595"
            },
            key: keys/* Location_key */.U,
            radius: '100'
        };
        if (value.length >= 5) {
            let getValue = await (0,geolocation/* placeAutoComplete */.B9)(payload);
            setSuggestion(getValue);
        }
    };
    const getCurrentAddress = async (currentAddress)=>{
        var ref;
        setLocation((prev)=>{
            return {
                ...prev,
                streetAddress: currentAddress.formatted_address
            };
        });
        let type = (ref = currentAddress.address_components) === null || ref === void 0 ? void 0 : ref.map((item)=>{
            switch(item.types[0]){
                case "country":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            country: item.long_name
                        };
                    });
                    break;
                case "postal_code":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            zip: item.long_name
                        };
                    });
                    break;
                case "administrative_area_level_1":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            state: item.long_name
                        };
                    });
                    break;
                case "locality":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            city: item.long_name
                        };
                    });
                    break;
            }
        });
    };
    const onClickItem = async (item1)=>{
        setSuggestion([]);
        setLocation((prev)=>{
            return {
                ...prev,
                streetAddress: item1.description
            };
        });
        setLocationStatus(true);
        let payload = {
            key: keys/* Location_key */.U,
            place_id: item1.place_id
        };
        let getLATLong = await (0,geolocation/* placeDetails */.FG)(payload);
        let type = getLATLong.address_components.map((item)=>{
            switch(item.types[0]){
                case "country":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            country: item.long_name
                        };
                    });
                    break;
                case "postal_code":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            zip: item.long_name
                        };
                    });
                    break;
                case "administrative_area_level_1":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            state: item.long_name
                        };
                    });
                    break;
                case "locality":
                    setLocation((prev)=>{
                        return {
                            ...prev,
                            city: item.long_name
                        };
                    });
                    break;
            }
        });
        localStorage.Defaultlat = getLATLong.geometry.location.lat;
        localStorage.Defaultlng = getLATLong.geometry.location.lng;
        setLocation((prev)=>{
            return {
                ...prev,
                latitude: getLATLong.geometry.location.lat,
                longitude: getLATLong.geometry.location.lng
            };
        });
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-12",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    children: "Pick-up & Return Location"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    children: " Pick up & return at your location is included in the rental price for no additional fee "
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: (customeModal_module_default()).carBanner,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                            src: "/images/others/icon/help/image_three.png"
                                        })
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "row",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-10",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                    className: (customeModal_module_default()).editPara,
                                                    children: [
                                                        " ",
                                                        location1.streetAddress,
                                                        " "
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-2",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: (customeModal_module_default()).editButton,
                                                    onClick: (e)=>{
                                                        isDisabledChecked(e);
                                                    },
                                                    children: isStatus == false ? "Edit" : "Done"
                                                })
                                            })
                                        ]
                                    }),
                                    isStatus == false && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: (customeModal_module_default()).mapSec,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(contact_us_map/* default */.Z, {
                                                mapClass: (customeModal_module_default()).mapDiv,
                                                setLocation: setLocation,
                                                location: location1
                                            })
                                        })
                                    }),
                                    isStatus == true && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Address"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "",
                                                        name: "streetAddress",
                                                        onChange: (e)=>{
                                                            handleAddress(e);
                                                        },
                                                        placeholder: "address",
                                                        value: location1.streetAddress,
                                                        autoComplete: "off",
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    }),
                                                    ((ref1 = location1.streetAddress) === null || ref1 === void 0 ? void 0 : ref1.length) > 1 && !locationStatus && /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                        className: suggestion.length === 0 ? "" : (customeModal_module_default()).list,
                                                        children: suggestion && suggestion.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                onClick: ()=>onClickItem(item)
                                                                ,
                                                                children: item.description
                                                            })
                                                        )
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-md-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "City"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "text",
                                                                    id: "",
                                                                    name: "idNumber",
                                                                    placeholder: "city",
                                                                    value: location1.city,
                                                                    autoComplete: "off",
                                                                    className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-md-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "State"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "text",
                                                                    id: "",
                                                                    name: "idNumber",
                                                                    placeholder: "state",
                                                                    value: location1.state,
                                                                    autoComplete: "off",
                                                                    className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-md-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "Country"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "text",
                                                                    id: "",
                                                                    name: "country",
                                                                    placeholder: "Country",
                                                                    value: location1.country,
                                                                    autoComplete: "off",
                                                                    className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-md-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "Zip Code"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "text",
                                                                    id: "",
                                                                    name: "zip",
                                                                    placeholder: "Code",
                                                                    value: location1.zip,
                                                                    autoComplete: "off",
                                                                    className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                onClick: (e)=>{
                                    handleCounter(e);
                                },
                                disabled: !isDisabled,
                                className: `${(customeModal_module_default()).sendBtn}`,
                                children: "CONFIRM LOCATION"
                            })
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const pickupLocation = (PickUpLocation);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/pricing.jsx







const Pricing = ({ counter , setCounter , lcplateModal , setLcPlateModal , lcPlatemessage  })=>{
    var ref;
    const { 0: recommendedPrice , 1: IsRecommendedPrice  } = (0,external_react_.useState)("");
    const { 0: priceDetails , 1: setPriceDetail  } = (0,external_react_.useState)({
        priceMethod: "",
        dailyPrice: 0,
        weeklyPrice: 0,
        holiDayPrice: 0,
        weeklyDiscount: 0,
        monthlyDiscount: 0,
        IsKmDay: "",
        IsKmWeek: "",
        IsKmMonth: "",
        ExtraFees: 0
    });
    const { 0: errorMsg , 1: setErrorMsg  } = (0,external_react_.useState)("");
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { uploadRegistration  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    (0,external_react_.useEffect)(()=>{
        IsRecommendedPrice(uploadRegistration === null || uploadRegistration === void 0 ? void 0 : uploadRegistration.prices.recommendedPrice);
        setPriceDetail((prev)=>{
            return {
                ...prev,
                dailyPrice: uploadRegistration === null || uploadRegistration === void 0 ? void 0 : uploadRegistration.prices.recommendedPrice,
                weeklyPrice: uploadRegistration === null || uploadRegistration === void 0 ? void 0 : uploadRegistration.prices.recommendedPrice
            };
        });
    }, [
        uploadRegistration === null || uploadRegistration === void 0 ? void 0 : (ref = uploadRegistration.prices) === null || ref === void 0 ? void 0 : ref.recommendedPrice
    ]);
    const { 0: isStatus , 1: setStaus  } = (0,external_react_.useState)(false);
    const updateState = (data)=>{
        setPriceDetail((state)=>{
            let newObj = {
                ...state,
                ...data
            };
            if (newObj.priceMethod == "daily") {
                if (newObj.dailyPrice && newObj.weeklyDiscount && newObj.monthlyDiscount && newObj.IsKmDay && newObj.IsKmWeek && newObj.IsKmMonth && newObj.ExtraFees) {
                    setDisabled(true);
                }
            } else if (newObj.priceMethod == "custom") {
                if (newObj.weeklyPrice && newObj.holiDayPrice && newObj.weeklyDiscount && newObj.monthlyDiscount && newObj.IsKmDay && newObj.IsKmWeek && newObj.IsKmMonth && newObj.ExtraFees) {
                    setDisabled(true);
                }
            } else {
                setDisabled(false);
            }
            return {
                ...state,
                ...data
            };
        });
    };
    const handlChange = (e)=>{
        let name = [
            e.target.name
        ];
        if (name == "ExtraFees" && Number(e.target.value) > 3) {
            setErrorMsg("Extra KM can not exceed 3 sar");
        } else {
            updateState({
                [e.target.name]: e.target.value
            });
            setErrorMsg("");
        }
    };
    const toggleChecked = ()=>{
        setStaus((prev)=>{
            setDisabled(!prev);
            return !prev;
        });
    };
    const handleClick = async (e)=>{
        e.preventDefault();
        let CarID = uploadRegistration.id;
        if (isStatus) {
            const response = await (0,new_vehicle_service/* recommended_price_service */.hn)(CarID, recommendedPrice, isStatus);
            if (response.code == 200) {
                setCounter(4);
            }
        } else {
            let price = priceDetails.priceMethod == "daily" ? priceDetails.dailyPrice : priceDetails.weeklyPrice;
            const response = await (0,new_vehicle_service/* recommended_price_service */.hn)(CarID, price, isStatus, priceDetails.IsKmDay, priceDetails.IsKmMonth, priceDetails.IsKmWeek, priceDetails.monthlyDiscount, priceDetails.weeklyDiscount, priceDetails.ExtraFees, priceDetails.priceMethod, priceDetails.holiDayPrice);
            console.log(response);
            if (response.code == 200) {
                setCounter(4);
            }
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                children: "Pricing"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Competitive prices generate more requests."
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                style: {
                                                    width: "100%",
                                                    whiteSpace: "unset",
                                                    textAlign: "left"
                                                },
                                                children: "Start with the lowest possible price to get your first rentals and move your listing up the search results. You can always increase it later on if necessary. "
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-10",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: " Enable Recommended Pricing "
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `${(customeModal_module_default()).toggle} col-2`,
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "checkbox",
                                                                id: "switch",
                                                                onChange: (e)=>{
                                                                    toggleChecked(e);
                                                                }
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                for: "switch"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            isStatus == true && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "text",
                                                        id: "",
                                                        name: "idNumber",
                                                        value: `${recommendedPrice} SAR Is the recommended price`,
                                                        readOnly: true,
                                                        autoComplete: "off",
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: (customeModal_module_default()).Info,
                                                        children: "Your vehicle is more likely to be rented at this price "
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    isStatus == false && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "Pricing method"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                        autoComplete: "off",
                                                        value: priceDetails.priceMethod,
                                                        name: "priceMethod",
                                                        onChange: (e)=>{
                                                            handlChange(e);
                                                        },
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "",
                                                                children: " Choose a pricing method "
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "daily",
                                                                children: " Daily Pricing"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                value: "custom",
                                                                children: " Custom Pricing "
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            priceDetails.priceMethod == "daily" && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Daily Price"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                            type: "text",
                                                            id: "",
                                                            name: "dailyPrice",
                                                            value: priceDetails.dailyPrice,
                                                            autoComplete: "off",
                                                            onChange: (e)=>{
                                                                handlChange(e);
                                                            },
                                                            className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                        })
                                                    ]
                                                })
                                            }),
                                            priceDetails.priceMethod == "custom" && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-6",
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                        children: "Weekday price"
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                        type: "text",
                                                                        id: "",
                                                                        name: "weeklyPrice",
                                                                        value: priceDetails.weeklyPrice,
                                                                        autoComplete: "off",
                                                                        onChange: (e)=>{
                                                                            handlChange(e);
                                                                        },
                                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-6",
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                        children: "Weekend / Holidays price"
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                        type: "number",
                                                                        id: "",
                                                                        name: "holiDayPrice",
                                                                        placeholder: "0",
                                                                        value: priceDetails.holiDayPrice,
                                                                        autoComplete: "off",
                                                                        className: `form-control`,
                                                                        onChange: (e)=>{
                                                                            handlChange(e);
                                                                        }
                                                                    })
                                                                ]
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "Weekly discount"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "number",
                                                                    id: "",
                                                                    name: "weeklyDiscount",
                                                                    placeholder: "Enter price",
                                                                    value: priceDetails.weeklyDiscount,
                                                                    autoComplete: "off",
                                                                    className: `form-control`,
                                                                    onChange: (e)=>{
                                                                        handlChange(e);
                                                                    }
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-6",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "Monthly discount"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                    type: "number",
                                                                    id: "",
                                                                    name: "monthlyDiscount",
                                                                    placeholder: "0",
                                                                    value: priceDetails.monthlyDiscount,
                                                                    autoComplete: "off",
                                                                    className: `form-control`,
                                                                    onChange: (e)=>{
                                                                        handlChange(e);
                                                                    }
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-4",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "KM/Day"
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                                    autoComplete: "off",
                                                                    name: "IsKmDay",
                                                                    value: priceDetails.IsKmDay,
                                                                    onChange: (e)=>{
                                                                        handlChange(e);
                                                                    },
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "",
                                                                            children: " Select Value   "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "150",
                                                                            children: " 150 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "200",
                                                                            children: " 200 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "250",
                                                                            children: " 250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "300",
                                                                            children: " 300 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "350",
                                                                            children: " 350 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "400",
                                                                            children: " 400 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "450",
                                                                            children: " 450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "500",
                                                                            children: " 500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "550",
                                                                            children: " 550 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "600",
                                                                            children: " 600 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "650",
                                                                            children: " 650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "700",
                                                                            children: " 700 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "750",
                                                                            children: " 750 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "800",
                                                                            children: " 800 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "850",
                                                                            children: " 850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "900",
                                                                            children: " 900 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "950",
                                                                            children: " 950 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1000",
                                                                            children: " 1000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1050",
                                                                            children: " 1050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1100",
                                                                            children: " 1100 "
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-4",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "KM/Week"
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                                    autoComplete: "off",
                                                                    name: "IsKmWeek",
                                                                    value: priceDetails.IsKmWeek,
                                                                    onChange: (e)=>{
                                                                        handlChange(e);
                                                                    },
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "",
                                                                            children: " Select Value   "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1050",
                                                                            children: " 1050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1250",
                                                                            children: " 1250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1450",
                                                                            children: " 1450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1650",
                                                                            children: " 1650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "1880",
                                                                            children: " 1880 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "2050",
                                                                            children: " 2050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "2250",
                                                                            children: " 2250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "2450",
                                                                            children: " 2450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "2650",
                                                                            children: " 2650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "2850",
                                                                            children: " 2850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "3050",
                                                                            children: " 3050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "3250",
                                                                            children: " 3250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "3450",
                                                                            children: " 3450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "3650",
                                                                            children: " 3650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "3850",
                                                                            children: " 3850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4050",
                                                                            children: " 4050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4250",
                                                                            children: " 4250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4450",
                                                                            children: " 4450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4650",
                                                                            children: " 4650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4850",
                                                                            children: " 4850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5050",
                                                                            children: " 5050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5250",
                                                                            children: " 5250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5450",
                                                                            children: " 5450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5650",
                                                                            children: " 5650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5850",
                                                                            children: " 5850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6050",
                                                                            children: " 6050 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6250",
                                                                            children: " 6250 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6450",
                                                                            children: " 6450 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6650",
                                                                            children: " 6650 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6850",
                                                                            children: " 6850 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "7050",
                                                                            children: " 7050 "
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-4",
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                    children: "KM/Month"
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                                    autoComplete: "off",
                                                                    name: "IsKmMonth",
                                                                    value: priceDetails.IsKmMonth,
                                                                    onChange: (e)=>{
                                                                        handlChange(e);
                                                                    },
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "",
                                                                            children: " Select Value   "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "4500",
                                                                            children: " 4500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5000",
                                                                            children: " 5000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "5500",
                                                                            children: " 5500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6000",
                                                                            children: " 6000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "6500",
                                                                            children: " 6500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "7000",
                                                                            children: " 7000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "7500",
                                                                            children: " 7500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "8000",
                                                                            children: " 8000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "8500",
                                                                            children: " 8500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "9000",
                                                                            children: " 9000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "9500",
                                                                            children: " 9500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "10000",
                                                                            children: " 10000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "10500",
                                                                            children: " 10500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "11000",
                                                                            children: " 11000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "11500",
                                                                            children: " 11500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "12000",
                                                                            children: " 12000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "12500",
                                                                            children: " 12500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "13000",
                                                                            children: " 13000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "13500",
                                                                            children: " 13500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "14000",
                                                                            children: " 14000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "14500",
                                                                            children: " 14500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "15000",
                                                                            children: " 15000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "15500",
                                                                            children: " 15500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "16000",
                                                                            children: " 16000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "16500",
                                                                            children: " 16500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "17000",
                                                                            children: " 17000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "17500",
                                                                            children: " 17500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "18000",
                                                                            children: " 18000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "18500",
                                                                            children: " 18500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "19000",
                                                                            children: " 19000 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "19500",
                                                                            children: " 19500 "
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                            value: "20000",
                                                                            children: " 20000 "
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: " Extra KM Fees "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                        type: "number",
                                                        id: "",
                                                        name: "ExtraFees",
                                                        onChange: (e)=>{
                                                            handlChange(e);
                                                        },
                                                        placeholder: "Renters will be chargedfor every additional KM",
                                                        value: priceDetails.ExtraFees,
                                                        autoComplete: "off",
                                                        className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: `${(customeModal_module_default()).Info} mt-1`,
                                                        style: {
                                                            fontFamily: "NeoSansBold"
                                                        },
                                                        children: "0.1 SAR is the recommended price"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "text-danger ng-star-inserted",
                                                        style: {
                                                            fontSize: "8px",
                                                            fontFamily: "NeoSansBold"
                                                        },
                                                        children: errorMsg
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: `${(customeModal_module_default()).sendBtn}`,
                                onClick: (e)=>{
                                    handleClick(e);
                                },
                                disabled: !isDisabled,
                                children: "SAVE"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(succefully_modal/* default */.Z, {
                message: `Your vehicle is now verified. Plate number is : ${lcPlatemessage}`,
                open: lcplateModal,
                setSuccessresponse: setLcPlateModal
            })
        ]
    }));
};
/* harmony default export */ const pricing = (Pricing);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/vehicleDelivery.jsx







const VehicleDelivery = ({ counter , setCounter  })=>{
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { setData , newCarData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    const { 0: message , 1: setMessage  } = (0,external_react_.useState)("");
    const { 0: isOpen , 1: setOpen  } = (0,external_react_.useState)(false);
    const { 0: deliveryOption , 1: setDeliveryOption  } = (0,external_react_.useState)({
        duration: "",
        shortestDuration: "",
        longestDuration: "",
        description: "",
        deliveryFess: "Free Delivery"
    });
    const updateState = (data)=>{
        setDeliveryOption((state)=>{
            let newObj = {
                ...state,
                ...data
            };
            if (newObj.duration && newObj.shortestDuration && newObj.longestDuration) {
                setDisabled(true);
            }
            return {
                ...state,
                ...data
            };
        });
    };
    const { 0: isStatus , 1: setStaus  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
    }, [
        isStatus
    ]);
    const toggleChecked = ()=>{
        setStaus((prev)=>{
            // setDisabled(!prev)
            return !prev;
        });
    };
    // const onChangeHandler = useCallback(
    //     ({target:{name,value}}) => setDeliveryOption(state => ({ ...state, [name]:value }), [])
    //   );
    const onChangeHandler = (e)=>{
        updateState({
            [e.target.name]: e.target.value
        });
    };
    const handleClick = async (e)=>{
        e.preventDefault();
        let CarID = newCarData.id;
        const response = await (0,new_vehicle_service/* vehicle_photos_service */.Ab)(CarID, deliveryOption.duration, isStatus, deliveryOption.description, deliveryOption.deliveryFess, deliveryOption.longestDuration, deliveryOption.shortestDuration);
        if (response.code == 200) {
            setCounter(5);
            setData(response.result);
        } else if (response.code == 422) {
            setMessage(response.message);
            setOpen(true);
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                children: "Vehicle delivery"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                            children: "Enable delivery option to increase your rentals! Guests like the option of having the vehicle delivered to them "
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "row",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "col-10",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: " Enable delivery option  "
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `${(customeModal_module_default()).toggle} col-2`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                            type: "checkbox",
                                                            id: "switch",
                                                            onChange: (e)=>{
                                                                toggleChecked(e);
                                                            }
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            for: "switch"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    isStatus == true && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                    children: "Delivery fee"
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                    autoComplete: "off",
                                                    name: "deliveryFess",
                                                    value: deliveryOption.deliveryFess,
                                                    onChange: (e)=>{
                                                        onChangeHandler(e);
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "Free Delivery",
                                                            children: " Free Delivery "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "25",
                                                            children: " 25 "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "50",
                                                            children: " 50 "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "75",
                                                            children: " 75 "
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "100",
                                                            children: " 100 "
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: (customeModal_module_default()).Info,
                                                    children: "0.0 SAR is the recommended price "
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                        className: "mt-2 mb-2",
                                        children: "Notices"
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Advance notice needed before any rental"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                autoComplete: "off",
                                                name: "duration",
                                                value: deliveryOption.duration,
                                                onChange: (e)=>{
                                                    onChangeHandler(e);
                                                },
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "",
                                                        children: " Select Duration "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "1",
                                                        children: "  1 hour  "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "2",
                                                        children: "  2 hours  "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "3",
                                                        children: "  3 hours   "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "6",
                                                        children: "  6 hours   "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "12",
                                                        children: "  12 hours   "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "24",
                                                        children: "  1 day   "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "48",
                                                        children: "   2 days    "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "72",
                                                        children: "   3 days    "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "168",
                                                        children: "   1 week    "
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "row",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-12",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                        children: "How long would you like youe rental to last?"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Shortest"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            autoComplete: "off",
                                                            name: "shortestDuration",
                                                            value: deliveryOption.shortestDuration,
                                                            onChange: (e)=>{
                                                                onChangeHandler(e);
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select shortest duration "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "1",
                                                                    children: " 1 Day "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "2",
                                                                    children: " 2 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "3",
                                                                    children: " 3 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "5",
                                                                    children: " 5 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "7",
                                                                    children: " 1 Week "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "14",
                                                                    children: " 2 Week "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "30",
                                                                    children: " 1 Month "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Longest"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            autoComplete: "off",
                                                            name: "longestDuration",
                                                            value: deliveryOption.longestDuration,
                                                            onChange: (e)=>{
                                                                onChangeHandler(e);
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select Longest duration "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "1",
                                                                    children: " 1 Day "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "2",
                                                                    children: " 2 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "3",
                                                                    children: " 3 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "5",
                                                                    children: " 5 Days "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "7",
                                                                    children: " 1 Week "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "14",
                                                                    children: " 2 Week "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "30",
                                                                    children: " 1 Month "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: " Car description(optional) "
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("textarea", {
                                                type: "text",
                                                name: "description",
                                                value: deliveryOption.description,
                                                onChange: (e)=>{
                                                    onChangeHandler(e);
                                                },
                                                placeholder: "Renters will be chargedfor every additional KM",
                                                className: `form-control `
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: `${(customeModal_module_default()).sendBtn}`,
                                onClick: (e)=>{
                                    handleClick(e);
                                },
                                disabled: !isDisabled,
                                children: "Next"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(succefully_modal/* default */.Z, {
                message: message,
                open: isOpen,
                setSuccessresponse: setOpen
            })
        ]
    }));
};
/* harmony default export */ const vehicleDelivery = (VehicleDelivery);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/vehicleDetail.jsx






const VehicleDetail = ({ counter , vehicleData , location , setCounter , vehicleYear , setyear  })=>{
    const { 0: allModals , 1: setAllModals  } = (0,external_react_.useState)([]);
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { setNewCarData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    const { 0: isError , 1: setError  } = (0,external_react_.useState)({
        code: "",
        msg: ""
    });
    const { 0: vehicleDetails , 1: setVehicleDetails  } = (0,external_react_.useState)({
        vehicleName: "",
        carModal: "",
        transmison: "",
        fuel: "",
        carmileage: "",
        doors: "",
        seats: ""
    });
    let vehicleYears = vehicleData.years;
    let vehicleMake = vehicleData.makes;
    let vehicleTransmision = vehicleData.transmissionOptions;
    let fuelTypes = vehicleData.fuelTypes;
    let mileage = vehicleData.odometerOptions;
    const updateState = (data)=>{
        setVehicleDetails((state)=>{
            let newObj = {
                ...state,
                ...data
            };
            if (newObj.vehicleName && newObj.carModal && newObj.transmison && newObj.fuel && newObj.carmileage && newObj.doors && newObj.seats) {
                setDisabled(true);
            }
            return {
                ...state,
                ...data
            };
        });
    };
    const handlChange = (e)=>{
        updateState({
            [e.target.name]: e.target.value
        });
    };
    //  ALL MODEL OF VEHICLE
    const VehicleModal = async (e)=>{
        // setCarModel("")
        const vehicle_modal = await (0,new_vehicle_service/* vehicle_modal_service */.f2)(e);
        if (vehicle_modal.code == 200) {
            setAllModals(vehicle_modal.models);
        }
    };
    // HANDLE STEP TWO
    const handleClick = async (e)=>{
        e.preventDefault();
        if (vehicleYear && vehicleDetails.vehicleName && vehicleDetails.carModal && vehicleDetails.transmison && vehicleDetails.fuel && vehicleDetails.carmileage && vehicleDetails.doors && vehicleDetails.seats) {
            setDisabled(true);
            const response = await (0,new_vehicle_service/* new_car_service */.u2)(location.city, location.country, location.latitude, location.longitude, location.state, location.streetAddress, location.zip, vehicleDetails.doors, vehicleDetails.fuel, vehicleDetails.vehicleName, vehicleDetails.carModal, vehicleDetails.carmileage, vehicleDetails.seats, vehicleDetails.transmison, vehicleYear.id);
            if (response.code == 200) {
                setCounter(2);
                setNewCarData(response.result);
                setyear((prev)=>{
                    var ref, ref1;
                    return {
                        ...prev,
                        value: (ref = response.result) === null || ref === void 0 ? void 0 : (ref1 = ref.year) === null || ref1 === void 0 ? void 0 : ref1.title
                    };
                });
            } else if (response.code == 404) {
                setError((prev)=>{
                    return {
                        ...prev,
                        code: response.code,
                        msg: response.message
                    };
                });
                setDisabled(false);
            }
        } else {
            const response = await (0,new_vehicle_service/* new_car_service */.u2)(location.city, location.country, location.latitude, location.longitude, location.state, location.streetAddress, location.zip, vehicleDetails.doors, vehicleDetails.fuel, vehicleDetails.vehicleName, vehicleDetails.carModal, vehicleDetails.carmileage, vehicleDetails.seats, vehicleDetails.transmison, vehicleYear.id);
            if (response.code == 200) {
                setCounter(2);
                setNewCarData(response.result);
                setyear((prev)=>{
                    var ref, ref2;
                    return {
                        ...prev,
                        value: (ref = response.result) === null || ref === void 0 ? void 0 : (ref2 = ref.year) === null || ref2 === void 0 ? void 0 : ref2.title
                    };
                });
            }
        }
    };
    // HANDLE MODEL YEAR ID AND TITLE
    const handleModelYear = (e)=>{
        let index = e.target.selectedIndex;
        const optionElement = e.target.childNodes[index];
        const optionElementId = optionElement.getAttribute('id');
        setyear((prev)=>{
            return {
                ...prev,
                id: optionElementId,
                value: e.target.value
            };
        });
    // optionElementId && setDisabled(true)
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                style: {
                    color: "#b2020f",
                    textAlign: "center",
                    fontSize: "20px"
                },
                children: isError.code == 404 && isError.msg
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                children: "Vehicle Details"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Vehicle Model Year"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("select", {
                                                value: vehicleYear.value,
                                                id: vehicleDetails.id,
                                                onChange: (e)=>{
                                                    handleModelYear(e);
                                                },
                                                autoComplete: "off",
                                                children: vehicleYears.map((item, index)=>{
                                                    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                        id: item.id,
                                                        value: item.title,
                                                        children: [
                                                            " ",
                                                            item.title,
                                                            " "
                                                        ]
                                                    }));
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Make "
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                autoComplete: "off",
                                                value: vehicleDetails.vehicleName,
                                                name: "vehicleName",
                                                onChange: (e)=>{
                                                    handlChange(e);
                                                    VehicleModal(e.target.value);
                                                },
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "",
                                                        children: " Select Make "
                                                    }),
                                                    vehicleMake.map((item, index)=>{
                                                        return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                value: item.id,
                                                                children: [
                                                                    " ",
                                                                    item.title,
                                                                    " "
                                                                ]
                                                            }, item.id)
                                                        }));
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                children: [
                                                    "Model ",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "(Select Make First)"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                value: vehicleDetails.carModal,
                                                name: "carModal",
                                                onChange: (e)=>{
                                                    handlChange(e);
                                                },
                                                autoComplete: "off",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "saudi",
                                                        children: " Select Model "
                                                    }),
                                                    allModals.map((item, index)=>{
                                                        return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                value: item.id,
                                                                children: [
                                                                    " ",
                                                                    item.title,
                                                                    " "
                                                                ]
                                                            }, item.id)
                                                        }));
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "row",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Transmission"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            value: vehicleDetails.transmison,
                                                            name: "transmison",
                                                            onChange: (e)=>{
                                                                handlChange(e);
                                                            },
                                                            autoComplete: "off",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select Transmission "
                                                                }),
                                                                vehicleTransmision.map((item, index)=>{
                                                                    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                            value: item.value,
                                                                            children: [
                                                                                " ",
                                                                                item.label,
                                                                                " "
                                                                            ]
                                                                        }, item.label)
                                                                    }));
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Fuel Type"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            value: vehicleDetails.fuel,
                                                            name: "fuel",
                                                            onChange: (e)=>{
                                                                handlChange(e);
                                                            },
                                                            autoComplete: "off",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select Fuel Type "
                                                                }),
                                                                fuelTypes.map((item, index)=>{
                                                                    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                            value: item.id,
                                                                            children: [
                                                                                " ",
                                                                                item.label,
                                                                                " "
                                                                            ]
                                                                        }, item.id)
                                                                    }));
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Mileage"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                value: vehicleDetails.carmileage,
                                                name: "carmileage",
                                                onChange: (e)=>{
                                                    handlChange(e);
                                                },
                                                autoComplete: "off",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "",
                                                        children: " Select Mileage "
                                                    }),
                                                    mileage.map((item, index)=>{
                                                        return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                value: item,
                                                                children: [
                                                                    " ",
                                                                    item,
                                                                    " "
                                                                ]
                                                            }, item)
                                                        }));
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "row",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Doors"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            value: vehicleDetails.doors,
                                                            name: "doors",
                                                            onChange: (e)=>{
                                                                handlChange(e);
                                                            },
                                                            autoComplete: "off",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select no. of doors "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "2",
                                                                    children: "  2 Doors  "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "3",
                                                                    children: "  3 Doors  "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "4",
                                                                    children: "  4 Doors  "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "5",
                                                                    children: "  5 Doors  "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-6",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: "Seats"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                            value: vehicleDetails.seats,
                                                            name: "seats",
                                                            onChange: (e)=>{
                                                                handlChange(e);
                                                            },
                                                            autoComplete: "off",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "",
                                                                    children: " Select no. of Seats  "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "2",
                                                                    children: " 2 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "3",
                                                                    children: " 3 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "4",
                                                                    children: " 4 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "5",
                                                                    children: " 5 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "6",
                                                                    children: " 6 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "7",
                                                                    children: " 7 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "8",
                                                                    children: " 8 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "9",
                                                                    children: " 9 Seats "
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                                    value: "10",
                                                                    children: " 10 Seats "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: `${(customeModal_module_default()).sendBtn}`,
                                disabled: !isDisabled,
                                onClick: (e)=>{
                                    handleClick(e);
                                },
                                children: "SAVE"
                            })
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const vehicleDetail = (VehicleDetail);

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: ./components/helpers/api_url.jsx
var api_url = __webpack_require__(6854);
// EXTERNAL MODULE: external "jstz"
var external_jstz_ = __webpack_require__(4585);
var external_jstz_default = /*#__PURE__*/__webpack_require__.n(external_jstz_);
// EXTERNAL MODULE: external "react-image-crop"
var external_react_image_crop_ = __webpack_require__(6141);
var external_react_image_crop_default = /*#__PURE__*/__webpack_require__.n(external_react_image_crop_);
;// CONCATENATED MODULE: ./components/common/ImageCropper/index.jsx




// import demoImage from "../../../public/images/others/book-car/bg.png";
function ImageCropper(props) {
    const { imageToCrop , onImageCropped , classStyle  } = props;
    const { 0: cropConfig1 , 1: setCropConfig  } = (0,external_react_.useState)(// default crop config
    {
        unit: "%",
        width: 30,
        aspect: 16 / 9
    });
    const { 0: imageRef1 , 1: setImageRef  } = (0,external_react_.useState)();
    async function cropImage(crop) {
        if (imageRef1 && crop.width && crop.height) {
            const croppedImage = await getCroppedImage(imageRef1, crop, "croppedImage.jpeg" // destination filename
            );
            // calling the props function to expose
            // croppedImage to the parent component
            onImageCropped(croppedImage);
        }
    }
    function getCroppedImage(sourceImage, cropConfig, fileName) {
        // creating the cropped image from the source image
        const canvas = document.createElement("canvas");
        const scaleX = sourceImage.naturalWidth / sourceImage.width;
        const scaleY = sourceImage.naturalHeight / sourceImage.height;
        canvas.width = cropConfig.width;
        canvas.height = cropConfig.height;
        const ctx = canvas.getContext("2d");
        ctx.drawImage(sourceImage, cropConfig.x * scaleX, cropConfig.y * scaleY, cropConfig.width * scaleX, cropConfig.height * scaleY, 0, 0, cropConfig.width, cropConfig.height);
        return new Promise((resolve, reject)=>{
            canvas.toBlob((blob)=>{
                // returning an error
                if (!blob) {
                    reject(new Error("Canvas is empty"));
                    return;
                }
                blob.name = fileName;
                // creating a Object URL representing the Blob object given
                const croppedImageUrl = window.URL.createObjectURL(blob);
                resolve(croppedImageUrl);
            }, "/images/others/book-car/bg.png");
        });
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx((external_react_image_crop_default()), {
        src: imageToCrop,
        className: classStyle,
        crop: cropConfig1,
        ruleOfThirds: true,
        onImageLoaded: (imageRef)=>setImageRef(imageRef)
        ,
        onComplete: (cropConfig)=>cropImage(cropConfig)
        ,
        onChange: (cropConfig)=>setCropConfig(cropConfig)
        ,
        crossorigin: "anonymous" // to avoid CORS-related problems
    }));
}
ImageCropper.defaultProps = {
    onImageCropped: ()=>{
    }
};
/* harmony default export */ const common_ImageCropper = (ImageCropper);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/imagePreviewAction.jsx








const timezone = external_jstz_default().determine();
const ImagePreviewModal = ({ open , close , imageUrl , image , carId , curImgIndex , setUrl  })=>{
    const { 0: croppedImage1 , 1: setCroppedImage  } = (0,external_react_.useState)(undefined);
    const { 0: isImgResize , 1: setIsImgResize  } = (0,external_react_.useState)(1);
    const closeclick = ()=>{
        close(false);
        console.log("data");
    };
    const handleImageUpload = async (e)=>{
        e.preventDefault();
        let imageType = curImgIndex == 0 ? "front" : curImgIndex == 1 ? "side" : curImgIndex == 2 ? "back" : "interior";
        const formdata = new FormData();
        formdata.append("image", imageUrl);
        formdata.append("carId", carId);
        formdata.append("imageType", imageType);
        const imageUploadResponse = await external_axios_default().post(api_url/* BASE_URL */._n + api_url/* API_URL.IMAGE_UPLOAD */.T5.IMAGE_UPLOAD, formdata, {
            headers: {
                "Content-Type": "application/json",
                "deviceType": "web",
                "accessToken": localStorage.token,
                "timeZone": timezone.name()
            }
        });
        if (imageUploadResponse.data.code == 200) {
            setUrl((prev)=>{
                prev[curImgIndex].isUrl = imageUrl;
                return [
                    ...prev
                ];
            });
        }
        close(false);
        console.log("show image", imageUploadResponse);
    };
    // const rotate = (image) =>{
    //     // let rotateAngle = Number(image.getAttribute("rotangle")) + 90;
    //     // image.setAttribute("style", "transform: rotate(" + rotateAngle + "deg)");
    //     // image.setAttribute("rotangle", "" + rotateAngle);
    //     image("style", "transform: rotate(" + rotateAngle + "deg)")
    //   }
    const RotateLeft = ()=>{
        var angle = $('.imageDiv').data('angle') + 90 || 90;
        $('.imageDiv').css({
            'transform': 'rotate(' + angle + 'deg)'
        });
        $('.imageDiv').data('angle', angle);
    };
    const RotateRight = ()=>{
        var angle = $('.imageDiv').data('angle') - 90 || -90;
        $('.imageDiv').css({
            'transform': 'rotate(' + angle + 'deg)'
        });
        $('.imageDiv').data('angle', angle);
    };
    // var count = 0.1
    // var zoom = 1;  
    // let total = 0
    // const ZoomIn = () =>{   
    //     console.log("thisggg",total,zoom);  
    //         total = zoom + count
    //         $('.image').css('transform',`scale(${total})`);
    //         count = count + 0.1         
    //         }
    // const ZoomOut = () =>{
    //     console.log("current",total,zoom);
    //     if(total > 1){
    //         total = total - 0.1
    //         $('.image').css('transform',`scale(${total})`); 
    //     }else{
    //     total = zoom.toFixed(1) - 0.1
    //     count = count - 0.1; 
    //     $('.image').css('transform',`scale(${total})`);
    //     }
    // }
    const ZoomIn = ()=>{
        setIsImgResize((prev)=>prev + 0.1
        );
        $('.image').css('transform', `scale(${isImgResize})`);
    };
    const ZoomOut = ()=>{
        setIsImgResize((prev)=>{
            console.log("scal size", prev.toFixed(1));
            if (prev.toFixed(1) > 0.1) {
                prev = prev.toFixed(1) - 0.1;
            }
            return prev;
        });
        $('.image').css('transform', `scale(${isImgResize})`);
    };
    const Reset = ()=>{
        $('.imageDiv').css({
            'transform': 'rotate( 0deg)'
        });
        $('.image').css('transform', `scale(1)`);
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(external_react_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
            className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeSignUpModal}`,
            isOpen: open,
            onRequestClose: closeclick,
            style: {
                overflowY: "scroll"
            },
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (customeModal_module_default()).dialogBox,
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: `${(customeModal_module_default()).registrationSec}`,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "row",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-12",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    onClick: closeclick,
                                    className: `float-right close mr-0 mt-0 ${(customeModal_module_default()).colse}`,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        children: "x"
                                    })
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-12 text-center pb-4",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    children: "Crop Image "
                                })
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: `col-12 ${(customeModal_module_default()).imageActionButton}`,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        onClick: RotateLeft,
                                        children: "Rotate Left"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        onClick: RotateRight,
                                        children: "Rotate Right"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        onClick: Reset,
                                        children: "Reset"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        onClick: ZoomIn,
                                        children: "Zoom In"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        onClick: ZoomOut,
                                        children: "Zoom Out"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: `col-12 mt-2 ${(customeModal_module_default()).imageActionSection}`,
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "row ",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "col-6",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                    children: "Cropper"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: `${(customeModal_module_default()).imageArea} imageDiv`,
                                                    id: "image",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(common_ImageCropper, {
                                                        imageToCrop: imageUrl,
                                                        alt: "",
                                                        classStyle: `image `,
                                                        onImageCropped: (croppedImage)=>setCroppedImage(croppedImage)
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "col-6",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                    children: "Preview"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: `${(customeModal_module_default()).imageArea} ${(customeModal_module_default()).cropped} imageDiv `,
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        alt: "",
                                                        src: croppedImage1 ? croppedImage1 : imageUrl,
                                                        className: "image "
                                                    })
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-12",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    onClick: handleImageUpload,
                                    className: `${(customeModal_module_default()).sendBtn}`,
                                    children: "Upload"
                                })
                            })
                        ]
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const imagePreviewAction = (ImagePreviewModal);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/vehiclePhotos.jsx






const VehiclePhotos = ({ counter , setCounter  })=>{
    const { 0: imageUrl , 1: setUrl  } = (0,external_react_.useState)([
        {
            id: 0,
            isUrl: null
        },
        {
            id: 1,
            isUrl: null
        },
        {
            id: 2,
            isUrl: null
        },
        {
            id: 3,
            isUrl: null
        },
        {
            id: 4,
            isUrl: null
        }
    ]);
    const { 0: curImgIndex , 1: setcurImgIndex  } = (0,external_react_.useState)(null);
    const { 0: curSelectedImg , 1: setcurSelectedImg  } = (0,external_react_.useState)(null);
    const { 0: image , 1: setImg  } = (0,external_react_.useState)("");
    const { 0: isModal , 1: setModal  } = (0,external_react_.useState)(false);
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { newCarData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    // <------------Onchange Image Upload ---------->
    let carId = newCarData === null || newCarData === void 0 ? void 0 : newCarData.id;
    const photoUpload = (e, indexValue)=>{
        e.preventDefault();
        setModal(true);
        let type = e.target.files[0].type;
        if (e.target.files && e.target.files[0]) {
            // setUrl(
            //     URL.createObjectURL(e.target.files[0]),
            // );
            setcurImgIndex(indexValue);
            // setUrl(
            //  (prev)=>{
            //      prev[indexValue].isUrl=URL.createObjectURL(e.target.files[0]);
            //      return([...prev])
            //  }
            // )
            setcurSelectedImg(URL.createObjectURL(e.target.files[0]));
            curImgIndex == 3 && setDisabled(true);
        }
        setImg(e.target.files[0]);
    };
    const handleClick = (e)=>{
        e.preventDefault();
        setCounter(6);
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                children: "VEHICLE PHOTOS"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `${(customeModal_module_default()).instructionSec}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                                children: "Add some attractive photos of your vehicle"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: " The better photos you have the more likely it will catch the eyes of renters! Make sure you add some beautiful photos, you can always add more later on if necessary. "
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        className: "row",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-1",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/images/others/icon/sucess_icon_sel.svg",
                                                                    alt: ""
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-10",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Use landscape format"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        className: "row",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-1",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/images/others/icon/sucess_icon_sel.svg",
                                                                    alt: ""
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-10",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Follow our angle guidelines"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        className: "row",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-1",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/images/others/icon/sucess_icon_sel.svg",
                                                                    alt: ""
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-10",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Keep the background clear and natural"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        className: "row",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-1",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/images/others/icon/sucess_icon_sel.svg",
                                                                    alt: ""
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-10",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Use only natural daylight"
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: (customeModal_module_default()).photoSection,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "row ",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "col-12 mb-4",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: ` ${(customeModal_module_default()).customeFormGroup}`,
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                            children: " Car description(optional) "
                                                        })
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row no-gutters",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `col-md-6 ${(customeModal_module_default()).ImgArea}`,
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `${(customeModal_module_default()).imageBox} ${(customeModal_module_default()).front}`,
                                                                style: {
                                                                    backgroundImage: `url(${imageUrl[0].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[0].isUrl})`
                                                                },
                                                                children: [
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                        htmlFor: `photo`,
                                                                        className: "custom-file-upload fas",
                                                                        style: {
                                                                            width: "100%"
                                                                        },
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                                htmlFor: `photo`,
                                                                                alt: "",
                                                                                className: (customeModal_module_default()).addBtn,
                                                                                src: "images/others/icon/edit_images.svg"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                                id: `photo`,
                                                                                type: "file",
                                                                                name: "image",
                                                                                style: {
                                                                                    display: "none"
                                                                                },
                                                                                accept: ".pdf, .jpeg , .jpg ,.png",
                                                                                onChange: (e)=>{
                                                                                    photoUpload(e, 0);
                                                                                }
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/help/drag_and_drop.png",
                                                                        className: (customeModal_module_default()).dragBtn,
                                                                        alt: ""
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: (customeModal_module_default()).counter,
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            children: "1"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                children: [
                                                                    "A ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("b", {
                                                                        children: "front photo"
                                                                    }),
                                                                    "  that stands out,It is the first one renters see"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `col-md-6 ${(customeModal_module_default()).ImgArea}`,
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `${(customeModal_module_default()).imageBox} ${(customeModal_module_default()).side}`,
                                                                style: {
                                                                    backgroundImage: `url(${imageUrl[1].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[1].isUrl})`
                                                                },
                                                                children: [
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                        htmlFor: `photo1`,
                                                                        className: "custom-file-upload fas",
                                                                        style: {
                                                                            width: "100%"
                                                                        },
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                                alt: "",
                                                                                htmlFor: `photo1`,
                                                                                className: (customeModal_module_default()).addBtn,
                                                                                src: "images/others/icon/edit_images.svg"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                                id: `photo1`,
                                                                                type: "file",
                                                                                style: {
                                                                                    display: "none"
                                                                                },
                                                                                accept: ".pdf, .jpeg , .jpg ,.png",
                                                                                onChange: (e)=>{
                                                                                    photoUpload(e, 1);
                                                                                }
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/help/drag_and_drop.png",
                                                                        className: (customeModal_module_default()).dragBtn,
                                                                        alt: ""
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: (customeModal_module_default()).counter,
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            children: "2"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                children: [
                                                                    "A ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("b", {
                                                                        children: "side photo"
                                                                    }),
                                                                    " to get an idea of vehicle size"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `col-md-6 ${(customeModal_module_default()).ImgArea}`,
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `${(customeModal_module_default()).imageBox} ${(customeModal_module_default()).back}`,
                                                                style: {
                                                                    backgroundImage: `url(${imageUrl[2].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[2].isUrl})`
                                                                },
                                                                children: [
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                        htmlFor: `photo3`,
                                                                        className: "custom-file-upload fas",
                                                                        style: {
                                                                            width: "100%"
                                                                        },
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                                htmlFor: `photo3`,
                                                                                alt: "",
                                                                                className: (customeModal_module_default()).addBtn,
                                                                                src: "images/others/icon/edit_images.svg"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                                id: `photo3`,
                                                                                type: "file",
                                                                                style: {
                                                                                    display: "none"
                                                                                },
                                                                                accept: ".pdf, .jpeg , .jpg ,.png",
                                                                                onChange: (e)=>{
                                                                                    photoUpload(e, 2);
                                                                                }
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/help/drag_and_drop.png",
                                                                        className: (customeModal_module_default()).dragBtn,
                                                                        alt: ""
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: (customeModal_module_default()).counter,
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            children: "3"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                children: [
                                                                    "A ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("b", {
                                                                        children: "side photo"
                                                                    }),
                                                                    " that showcases the vehicle overall"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: `col-md-6 ${(customeModal_module_default()).ImgArea}`,
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `${(customeModal_module_default()).imageBox} ${(customeModal_module_default()).interior}`,
                                                                style: {
                                                                    backgroundImage: `url(${imageUrl[3].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[3].isUrl})`
                                                                },
                                                                children: [
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                        htmlFor: `photo4`,
                                                                        className: "custom-file-upload fas",
                                                                        style: {
                                                                            width: "100%"
                                                                        },
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                                htmlFor: `photo4`,
                                                                                alt: "",
                                                                                className: (customeModal_module_default()).addBtn,
                                                                                src: "images/others/icon/edit_images.svg"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                                id: `photo4`,
                                                                                type: "file",
                                                                                style: {
                                                                                    display: "none"
                                                                                },
                                                                                accept: ".pdf, .jpeg , .jpg ,.png",
                                                                                onChange: (e)=>{
                                                                                    photoUpload(e, 3);
                                                                                }
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/images/others/icon/help/drag_and_drop.png",
                                                                        className: (customeModal_module_default()).dragBtn,
                                                                        alt: ""
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: (customeModal_module_default()).counter,
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                                            children: "4"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                children: [
                                                                    "A ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("b", {
                                                                        children: "side photo"
                                                                    }),
                                                                    " helps renters picture themselves driving"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: `col-md-6 ${(customeModal_module_default()).ImgArea} ml-0`,
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `${(customeModal_module_default()).imageBox} ${(customeModal_module_default()).addNew}`,
                                                            style: {
                                                                backgroundImage: `url(${imageUrl[4].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[4].isUrl})`
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                                    htmlFor: `photo5`,
                                                                    className: "custom-file-upload fas",
                                                                    style: {
                                                                        width: "100%"
                                                                    },
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                            htmlFor: `photo5`,
                                                                            alt: "",
                                                                            className: (customeModal_module_default()).addBtn,
                                                                            src: "images/others/icon/edit_images.svg"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                            id: `photo5`,
                                                                            type: "file",
                                                                            style: {
                                                                                display: "none"
                                                                            },
                                                                            accept: ".pdf, .jpeg , .jpg ,.png",
                                                                            onChange: (e)=>{
                                                                                photoUpload(e, 4);
                                                                            }
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: (customeModal_module_default()).AddText,
                                                                    children: "Add more photos"
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: `${(customeModal_module_default()).sendBtn}`,
                                disabled: !isDisabled,
                                onClick: handleClick,
                                children: "Next"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(imagePreviewAction, {
                setUrl: setUrl,
                curImgIndex: curImgIndex,
                carId: carId,
                imageUrl: curSelectedImg,
                image: image,
                open: isModal,
                close: setModal
            })
        ]
    }));
};
/* harmony default export */ const vehiclePhotos = (VehiclePhotos);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/vehicleVerification.jsx







const replaceEnglishToArabic = (s)=>{
    let translate = {
        "أ": "A",
        "ب": "B",
        "ح": "J",
        "د": "D",
        "ر": "R",
        "س": "S",
        "ص": "X",
        "ط": "T",
        "ه": "E",
        "ع": "P",
        "ق": "G",
        "ك": "K",
        "ل": "L",
        "م": "Z",
        "ن": "N",
        "هـ": "H",
        "و": "U",
        "ى": "V",
        "ي": "V" // probably more to come
    };
    return s.replace(s, function(match) {
        return translate[match];
    });
};
const VehicleVerification = ({ counter , vehicleYear , setyear , vehicleData , companyList , setCounter , setLcPlateModal , setLcPlateMessage  })=>{
    const { 0: ownership , 1: setOwnership  } = (0,external_react_.useState)("");
    const { 0: companyName , 1: setCName  } = (0,external_react_.useState)("");
    const { 0: sequenceNo , 1: setsequenceNo  } = (0,external_react_.useState)("");
    const { 0: isDisabled , 1: setDisabled  } = (0,external_react_.useState)(false);
    const { newCarData , setData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    const { 0: message , 1: setMessage  } = (0,external_react_.useState)("");
    const { 0: imageUrl , 1: setUrl  } = (0,external_react_.useState)(null);
    const { 0: successModal , 1: setSuccessresponse  } = (0,external_react_.useState)(false);
    // <------------Onchange Image Upload ---------->
    const photoUpload = (e)=>{
        e.preventDefault();
        let type = e.target.files[0].type;
        if (e.target.files && e.target.files[0]) {
            setUrl(URL.createObjectURL(e.target.files[0]));
        } else {
            setImg(e.target.files[0]);
        }
    };
    const handleChangeId = (e)=>{
        setOwnership(e.target.value);
        setDisabled(false);
    };
    let vehicleYears = vehicleData.years;
    const DisabledChecked = ()=>{
        if (sequenceNo && vehicleYear && ownership && imageUrl !== null) {
            if (ownership == "On Lease" && companyName) {
                setDisabled(true);
            } else {
                setDisabled(true);
            }
        } else {
            setDisabled(false);
        }
    };
    const handleChange = (e)=>{
        let Id = e.target.value;
        if (!Number(Id)) {
            return;
        }
        setsequenceNo(Id);
    };
    const handleClick = async (e)=>{
        e.preventDefault();
        let carId = newCarData.id;
        e.preventDefault();
        setDisabled(true);
        const response = await (0,new_vehicle_service/* duplicate_car_service */.jL)(carId, sequenceNo);
        if (response.code == 404) {
            var ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
            let res2 = await (0,new_vehicle_service/* yakeen_new_car_service */.ke)(sequenceNo, vehicleYear.value);
            let one = replaceEnglishToArabic(((ref = res2.result) === null || ref === void 0 ? void 0 : ref.plate_text1) || '');
            let two = replaceEnglishToArabic(((ref1 = res2.result) === null || ref1 === void 0 ? void 0 : ref1.plate_text2) || '');
            let three = replaceEnglishToArabic(((ref2 = res2.result) === null || ref2 === void 0 ? void 0 : ref2.plate_text3) || '');
            let lcPlateNumber = ((ref3 = res2.result) === null || ref3 === void 0 ? void 0 : ref3.plate_number) + " " + one + two + three;
            let plate_text1 = (ref4 = res2.result) === null || ref4 === void 0 ? void 0 : ref4.plate_text1;
            let plate_text2 = (ref5 = res2.result) === null || ref5 === void 0 ? void 0 : ref5.plate_text2;
            let plate_text3 = (ref6 = res2.result) === null || ref6 === void 0 ? void 0 : ref6.plate_text3;
            let vehicle_maker_code = (ref7 = res2.result) === null || ref7 === void 0 ? void 0 : ref7.vehicle_maker_code;
            let vehicle_model_code = (ref8 = res2.result) === null || ref8 === void 0 ? void 0 : ref8.vehicle_model_code;
            let chassis_number = (ref9 = res2.result) === null || ref9 === void 0 ? void 0 : ref9.chassis_number;
            if (res2.code == 200) {
                let updateCarReg = await (0,new_vehicle_service/* update_car_registration_service */.WM)(plate_text1, plate_text2, plate_text3, carId, chassis_number, lcPlateNumber, ownership, imageUrl, sequenceNo, vehicle_maker_code, vehicle_model_code, vehicleYear.id);
                if (updateCarReg.code == 200) {
                    setData(updateCarReg.result);
                    setCounter(3);
                    setLcPlateModal(true);
                    setLcPlateMessage(updateCarReg.result.lcPlateNumber);
                }
            } else if (res2.code == 422) {
                setSuccessresponse(true);
                setMessage(res2.message);
            }
        } else if (response.code == 200) {
            setSuccessresponse(true);
            setMessage(response.message);
        }
    };
    const handleModelYear = (e)=>{
        let index = e.target.selectedIndex;
        const optionElement = e.target.childNodes[index];
        const optionElementId = optionElement.getAttribute('id');
        setyear((prev)=>{
            return {
                ...prev,
                id: optionElementId,
                value: e.target.value
            };
        });
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(progress, {
                step: counter
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("form", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                children: "Vehicle Verification"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (customeModal_module_default()).addCarSec,
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Vehicle sequence number"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                type: "text",
                                                id: "",
                                                name: "sequenceNo",
                                                onChange: (e)=>{
                                                    handleChange(e);
                                                    DisabledChecked();
                                                },
                                                placeholder: "Sequence No",
                                                value: sequenceNo,
                                                autoComplete: "off",
                                                className: `form-control ${(customeModal_module_default()).stepbarInput}`
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Vehicle Model Year"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                autoComplete: "off",
                                                value: vehicleYear.value,
                                                id: vehicleYear.id,
                                                onChange: (e)=>{
                                                    handleModelYear(e);
                                                    DisabledChecked();
                                                },
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: " ",
                                                        id: "",
                                                        children: " Vehicle Model Year  "
                                                    }),
                                                    vehicleYears.map((item, index)=>{
                                                        return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                id: item.id,
                                                                value: item.title,
                                                                children: [
                                                                    " ",
                                                                    item.title,
                                                                    " "
                                                                ]
                                                            })
                                                        }));
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: "Ownership"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                autoComplete: "off",
                                                value: ownership,
                                                onChange: (e)=>{
                                                    handleChangeId(e);
                                                    DisabledChecked();
                                                },
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "",
                                                        children: " Select ownership "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "Fully Owned",
                                                        children: " Fully Owned "
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                        value: "On Lease",
                                                        children: " On Lease "
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    ownership == "On Lease" && /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                    children: "Finance Company"
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                                    value: companyName,
                                                    onChange: (e)=>{
                                                        setCName(e.target.value);
                                                        DisabledChecked();
                                                    },
                                                    autoComplete: "off",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                            value: "",
                                                            children: " Select Finance Company "
                                                        }),
                                                        companyList.map((item, index)=>{
                                                            return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("option", {
                                                                    value: item.name,
                                                                    children: [
                                                                        " ",
                                                                        item.name,
                                                                        " "
                                                                    ]
                                                                }, item.id)
                                                            }));
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: `form-group px-0 ${(customeModal_module_default()).customeFormGroup}`,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("label", {
                                                children: " Upload Vehicle Registration card"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: (customeModal_module_default()).fileUpload,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                        children: "Vehicle Registration card"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                        className: (customeModal_module_default()).imgUploadArea,
                                                        htmlFor: "photo",
                                                        type: "button",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                className: (customeModal_module_default()).spanChange,
                                                                children: imageUrl === null ? "Upload" : "change"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/images/others/icon/edit_profile.png",
                                                                alt: "",
                                                                style: {
                                                                    height: "18px",
                                                                    width: "18px"
                                                                }
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: `${(customeModal_module_default()).photoSec}`,
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    htmlFor: `photo`,
                                                                    src: imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                                type: "file",
                                                                id: "photo",
                                                                style: {
                                                                    display: "none"
                                                                },
                                                                accept: ".pdf, .jpeg , .jpg ,.png",
                                                                onChange: (e)=>{
                                                                    photoUpload(e);
                                                                }
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: `${(customeModal_module_default()).sendBtn}`,
                                onClick: (e)=>{
                                    handleClick(e);
                                },
                                disabled: !isDisabled,
                                children: "SAVE"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(succefully_modal/* default */.Z, {
                message: message,
                open: successModal,
                setSuccessresponse: setSuccessresponse
            })
        ]
    }));
};
/* harmony default export */ const vehicleVerification = (VehicleVerification);

;// CONCATENATED MODULE: ./components/common/Modal/AddCarModal/index.jsx














const AddNewCar = ({ style , text , carId  })=>{
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    const { 0: counter , 1: setCounter  } = (0,external_react_.useState)(0);
    const { 0: vehicleData , 1: setVehicleData  } = (0,external_react_.useState)([]);
    const { 0: vehicleYear , 1: setyear  } = (0,external_react_.useState)({
        id: "",
        value: ""
    });
    const { 0: vehicleYearId , 1: setyearId  } = (0,external_react_.useState)("");
    const { 0: companyList , 1: setCompanyList  } = (0,external_react_.useState)([]);
    const { 0: lcplateModal , 1: setLcPlateModal  } = (0,external_react_.useState)(false);
    const { 0: lcPlatemessage , 1: setLcPlateMessage  } = (0,external_react_.useState)("");
    const { setNewCarData , setData  } = (0,external_react_.useContext)(IdDetailsContext/* IdContext */.B);
    const { 0: location , 1: setLocation  } = (0,external_react_.useState)({
        city: "",
        country: "",
        latitude: "",
        longitude: "",
        state: "",
        streetAddress: "",
        zip: ""
    });
    const openModal = (e)=>{
        e.preventDefault();
        setOpen(true);
        car_preloads();
    };
    const closeModal = (e)=>{
        e.preventDefault();
        setCounter(0);
        setOpen(false);
    };
    const car_preloads = async ()=>{
        const response = await (0,new_vehicle_service/* preload_service */.zN)();
        setVehicleData(response);
        if (response.code === 200) {
            if (carId) {
                let listing_detils = await (0,new_vehicle_service/* car_listing_details_service */._A)(carId);
                if (listing_detils.code == 200) {
                    var ref;
                    console.log(listing_detils);
                    setCounter(listing_detils.result.doors + 1);
                    setyear((ref = listing_detils.result.year) === null || ref === void 0 ? void 0 : ref.id);
                    setNewCarData(listing_detils.result);
                    setData(listing_detils.result);
                }
            }
            let res = await (0,new_vehicle_service/* finanace_list_service */.$n)();
            if (res.code == 200) {
                setCompanyList(res.company_list);
            }
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                onClick: openModal,
                className: style,
                children: text
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_modal_default()), {
                className: `${(customeModal_module_default()).customeModal} ${(customeModal_module_default()).customeAddCarModal}`,
                isOpen: open,
                onRequestClose: closeModal,
                style: {
                    overflowY: "scroll"
                },
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: (customeModal_module_default()).dialogBox,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                            onClick: closeModal,
                            className: `float-right close mr-0 mt-0 ${(customeModal_module_default()).close}`,
                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "x"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (customeModal_module_default()).registrationSec,
                            children: [
                                counter == 0 && /*#__PURE__*/ jsx_runtime_.jsx(pickupLocation, {
                                    counter: counter,
                                    setCounter: setCounter,
                                    location: location,
                                    setLocation: setLocation
                                }),
                                counter == 1 && /*#__PURE__*/ jsx_runtime_.jsx(vehicleDetail, {
                                    counter: counter,
                                    setCounter: setCounter,
                                    vehicleYear: vehicleYear,
                                    setyear: setyear,
                                    vehicleData: vehicleData,
                                    location: location,
                                    setLocation: setLocation
                                }),
                                counter == 2 && /*#__PURE__*/ jsx_runtime_.jsx(vehicleVerification, {
                                    counter: counter,
                                    vehicleData: vehicleData,
                                    companyList: companyList,
                                    setCounter: setCounter,
                                    vehicleYear: vehicleYear,
                                    setyear: setyear,
                                    setLcPlateModal: setLcPlateModal,
                                    setLcPlateMessage: setLcPlateMessage
                                }),
                                counter == 3 && /*#__PURE__*/ jsx_runtime_.jsx(pricing, {
                                    counter: counter,
                                    setCounter: setCounter,
                                    lcplateModal: lcplateModal,
                                    lcPlatemessage: lcPlatemessage,
                                    setLcPlateMessage: setLcPlateMessage,
                                    setLcPlateModal: setLcPlateModal
                                }),
                                counter == 4 && /*#__PURE__*/ jsx_runtime_.jsx(vehicleDelivery, {
                                    counter: counter,
                                    setCounter: setCounter
                                }),
                                counter == 5 && /*#__PURE__*/ jsx_runtime_.jsx(vehiclePhotos, {
                                    counter: counter,
                                    setCounter: setCounter
                                }),
                                counter == 6 && /*#__PURE__*/ jsx_runtime_.jsx(insurance, {
                                    counter: counter,
                                    setCounter: setCounter,
                                    setOpen: setOpen
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const AddCarModal = (AddNewCar);


/***/ }),

/***/ 8656:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "jP": () => (/* binding */ car_modify_price_service),
/* harmony export */   "zL": () => (/* binding */ car_update_price_service),
/* harmony export */   "ZY": () => (/* binding */ car_update_discounts_service),
/* harmony export */   "a8": () => (/* binding */ car_update_Km_service),
/* harmony export */   "g1": () => (/* binding */ car_extra_details_service),
/* harmony export */   "_Z": () => (/* binding */ update_extra_details_service),
/* harmony export */   "NJ": () => (/* binding */ car_insurance_service),
/* harmony export */   "$F": () => (/* binding */ car_insurance_list_service),
/* harmony export */   "PG": () => (/* binding */ select_insurance_policy_service),
/* harmony export */   "uU": () => (/* binding */ earning_service),
/* harmony export */   "T9": () => (/* binding */ review_list_service)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4585);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstz__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _api_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6854);



const timezone = jstz__WEBPACK_IMPORTED_MODULE_1___default().determine();
const ISSERVER = "undefined" === "undefined";
let GUEST_TOKEN = "";
let SESSION_TOKEN = "";
if (!ISSERVER) {
    GUEST_TOKEN = localStorage.g_token, SESSION_TOKEN = localStorage.token;
}
// EDIT CAR PRICE DETAILS
const car_modify_price_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.EDIT_PRICE_DETAILS */ .T5.EDIT_PRICE_DETAILS, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// UPDATE FLAT PRICES 
const car_update_price_service = async (carId, weekdaysPrice, weekendPrice)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CAR_UPDATE_FLAT_PRICE */ .T5.CAR_UPDATE_FLAT_PRICE, {
        carId: carId,
        weekdaysPrice: weekdaysPrice,
        weekendPrice: weekendPrice
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// UPDATE DISCOUNTS 
const car_update_discounts_service = async (carId, discountOnMonthBooking, discountOnWeekBooking)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.UPDATE_DISCOUNTS */ .T5.UPDATE_DISCOUNTS, {
        car_id: carId,
        discountOnMonthBooking: discountOnMonthBooking,
        discountOnWeekBooking: discountOnWeekBooking
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// UPDATE DAY WEEKLY MONTHLY KM 
const car_update_Km_service = async (carId, maxDayMiles, maxMonthMiles, maxWeekMiles, pricePerExtraMile)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.UPDATE_KM */ .T5.UPDATE_KM, {
        car_id: carId,
        maxDayMiles: maxDayMiles,
        maxMonthMiles: maxMonthMiles,
        maxWeekMiles: maxWeekMiles,
        pricePerExtraMile: pricePerExtraMile
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// UPDATE DAY WEEKLY MONTHLY KM 
const car_extra_details_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CAR_EXTRA_DETAILS */ .T5.CAR_EXTRA_DETAILS, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// UPDATE EXTRA DETAILS OF VEHICLE 
const update_extra_details_service = async (carId, seat, doors, ownedship, financeCompany, financeId, fuelId, odometer, advanceNotice, transmission, minDays, maxDays, smoking, description)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.UPDATE_EXTRA_DETAILS */ .T5.UPDATE_EXTRA_DETAILS, {
        "carId": carId,
        "description": description,
        "seats": seat,
        "doors": doors,
        "ownership": ownedship,
        "finance_company_id": financeId,
        "finance": financeCompany,
        "fuelTypeId": fuelId,
        "odometer": odometer,
        "advanceNotice": advanceNotice,
        "transmission": transmission,
        "minTripDays": minDays,
        "maxTripDays": maxDays,
        "smoking": smoking
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// CAR INSURANCE API
const car_insurance_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.INSURANCE_DETAILS */ .T5.INSURANCE_DETAILS, {
        car_id: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  INSURANCE COMPANY LIST
const car_insurance_list_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.INSURANCE_LIST */ .T5.INSURANCE_LIST, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  SELECT INSURANCE POLICY 
const select_insurance_policy_service = async (carId, provider, status, type)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.SELECT_INSURANCE_TYPE */ .T5.SELECT_INSURANCE_TYPE, {
        car_id: carId,
        insuranceProvider: provider,
        is_Insured: status,
        policyType: type
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  OWNER EARNING 
const earning_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.EARNING */ .T5.EARNING, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//   REVIEW LIST 
const review_list_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.REVIEWS_LIST */ .T5.REVIEWS_LIST, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};


/***/ }),

/***/ 5931:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "zN": () => (/* binding */ preload_service),
/* harmony export */   "$n": () => (/* binding */ finanace_list_service),
/* harmony export */   "f2": () => (/* binding */ vehicle_modal_service),
/* harmony export */   "u2": () => (/* binding */ new_car_service),
/* harmony export */   "jL": () => (/* binding */ duplicate_car_service),
/* harmony export */   "ke": () => (/* binding */ yakeen_new_car_service),
/* harmony export */   "WM": () => (/* binding */ update_car_registration_service),
/* harmony export */   "_A": () => (/* binding */ car_listing_details_service),
/* harmony export */   "hn": () => (/* binding */ recommended_price_service),
/* harmony export */   "Ab": () => (/* binding */ vehicle_photos_service)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4585);
/* harmony import */ var jstz__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jstz__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _api_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6854);



const timezone = jstz__WEBPACK_IMPORTED_MODULE_1___default().determine();
const ISSERVER = "undefined" === "undefined";
let GUEST_TOKEN = "";
let SESSION_TOKEN = "";
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token, SESSION_TOKEN = localStorage.token;
}
//  PRELOAD SERVICES
const preload_service = async ()=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.PRELOAD_API */ .T5.PRELOAD_API, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  FINANCE LIST SERVICES
const finanace_list_service = async ()=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().get(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.FINANCE_COMPANY_LIST */ .T5.FINANCE_COMPANY_LIST, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  VEHICLE MODAL  SERVICES
const vehicle_modal_service = async (makeId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.VEHICLE_MODAL */ .T5.VEHICLE_MODAL, {
        makeId: makeId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  CREATE NEW CAR 
const new_car_service = async (city, country, latitude, longitude, state, streetAddress, zip, doors, fuelType, makeId, modelId, odometer, seats, transmission, yearId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.ADD_NEW_CAR */ .T5.ADD_NEW_CAR, {
        address: {
            city: city,
            country: country,
            latitude: latitude,
            longitude: longitude,
            state: state,
            streetAddress: streetAddress,
            zip: zip
        },
        car: {
            doors: doors,
            fuelType: fuelType,
            makeId: makeId,
            modelId: modelId,
            odometer: odometer,
            seats: seats,
            transmission: transmission,
            yearId: yearId
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  CHECK CAR ALREADY EXIST
const duplicate_car_service = async (carId, vehicleSeqNo)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.DUPLICATE_CAR_CHECk */ .T5.DUPLICATE_CAR_CHECk, {
        carId: carId,
        vehicleSeqNo: vehicleSeqNo
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// NEW CAR YAKEEN API
const yakeen_new_car_service = async (sequenceNumber, modelYear)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.YAKEEN_VEHICLE_INFO */ .T5.YAKEEN_VEHICLE_INFO, {
        "sequenceNumber": sequenceNumber,
        "modelYear": modelYear
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  NEW CAR UPDATE REGISTRATION
const update_car_registration_service = async (arPlateText1, arPlateText2, arPlateText3, carId, chassisNumber, lcPlateNumber, ownership, vehicleRegImg, vehicleSeqNo, vehicle_maker_code, vehicle_model_code, yearId, finance, finance_company_id)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.CAR_UPDATION */ .T5.CAR_UPDATION, {
        arbLeft: arPlateText1,
        arbMiddle: arPlateText2,
        arbRight: arPlateText3,
        carId: carId,
        chassisNumber: chassisNumber,
        lcPlateNumber: lcPlateNumber,
        ownership: ownership,
        vehicleRegImg: vehicleRegImg,
        vehicleSeqNo: vehicleSeqNo,
        vehicle_maker_code: vehicle_maker_code,
        vehicle_model_code: vehicle_model_code,
        yearId: yearId,
        finance: finance,
        finance_company_id: finance_company_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
// MODIFY VEHICLE DETAILS
const car_listing_details_service = async (carId)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.VEHICLE_DETAILS */ .T5.VEHICLE_DETAILS, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  STEP THIRD PRICE API
const recommended_price_service = async (carId, minPrice, recommendedPricing, maxDayMiles, maxMonthMiles, maxWeekMiles, monthlyDiscount, weeklyDiscount, pricePerExtraMile, pricingMethod, weekendPrice)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.STEP_THREE_PRICE */ .T5.STEP_THREE_PRICE, {
        car: {
            carId: carId,
            minPrice: minPrice,
            recommendedPricing: recommendedPricing,
            maxDayMiles: maxDayMiles,
            maxMonthMiles: maxMonthMiles,
            maxWeekMiles: maxWeekMiles,
            monthlyDiscount: monthlyDiscount,
            pricePerExtraMile: pricePerExtraMile,
            pricingMethod: pricingMethod,
            weekendPrice: weekendPrice,
            weeklyDiscount: weeklyDiscount
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
};
//  STEP Fourth PHOTO API
const vehicle_photos_service = async (carId, advanceNotice, deliveryOption, description, guestLocationDeliveryPrice, maxTripDays, minTripDays)=>{
    let res = await axios__WEBPACK_IMPORTED_MODULE_0___default().post(_api_url__WEBPACK_IMPORTED_MODULE_2__/* .BASE_URL */ ._n + _api_url__WEBPACK_IMPORTED_MODULE_2__/* .API_URL.STEP_FOUR_PHOTO */ .T5.STEP_FOUR_PHOTO, {
        car: {
            advanceNotice: advanceNotice,
            carId: carId,
            deliveryOption: deliveryOption,
            description: description,
            guestLocationDeliveryPrice: guestLocationDeliveryPrice,
            maxTripDays: maxTripDays,
            minTripDays: minTripDays
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    });
    try {
        let response = res.data;
        if (response.code === 345) {
            localStorage.clear();
            window.location.href = "/";
        }
        return response;
    } catch (error) {
        console.log("Error", error);
        return error;
    }
} //  IMAGE UPLOAD IN MODAL
;


/***/ })

};
;