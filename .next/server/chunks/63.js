exports.id = 63;
exports.ids = [63];
exports.modules = {

/***/ 4063:
/***/ ((module) => {

// Exports
module.exports = {
	"BookingProfileSec": "bookingList_BookingProfileSec__dpFhn",
	"containerMd": "bookingList_containerMd__rxgnK",
	"showStatus": "bookingList_showStatus__WtIJd",
	"leftMenu": "bookingList_leftMenu__ieD0V",
	"active": "bookingList_active__jbxNW",
	"smallCol": "bookingList_smallCol__CwO6v",
	"NoVehicle": "bookingList_NoVehicle__msKQt",
	"NodataSec": "bookingList_NodataSec__aiGUs",
	"imageSec": "bookingList_imageSec__JYM7R",
	"buttons": "bookingList_buttons__4_lUI",
	"formGroup": "bookingList_formGroup__eFR_O",
	"bookVehicle": "bookingList_bookVehicle__qhddp",
	"shareVehicle": "bookingList_shareVehicle__Esi4a",
	"BookingSec": "bookingList_BookingSec__rQeDv",
	"container": "bookingList_container__Nyra1",
	"carDetail": "bookingList_carDetail__Pv6_r",
	"smallShow": "bookingList_smallShow__sDxQe",
	"carImg": "bookingList_carImg__GeddC",
	"carInfo": "bookingList_carInfo__bB1_9",
	"carInfoUpper": "bookingList_carInfoUpper__FGb0a",
	"carStatus": "bookingList_carStatus__ulc8d",
	"status": "bookingList_status__CiqYF",
	"pendingStatus": "bookingList_pendingStatus__PMXy2",
	"acceptStatus": "bookingList_acceptStatus__9Xs1e",
	"declineStatus": "bookingList_declineStatus__EyP0h",
	"floatRight": "bookingList_floatRight__8TZZX",
	"tripinfo": "bookingList_tripinfo__PJdlq",
	"viewbtn": "bookingList_viewbtn__7V4Hj",
	"editSec": "bookingList_editSec__AlteO",
	"action": "bookingList_action__BUwoe"
};


/***/ })

};
;