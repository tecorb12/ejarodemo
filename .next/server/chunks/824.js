exports.id = 824;
exports.ids = [824];
exports.modules = {

/***/ 5681:
/***/ ((module) => {

// Exports
module.exports = {
	"mainNav": "header_mainNav__O1Sna",
	"navbarBrand": "header_navbarBrand__9fDgs",
	"navCollapse": "header_navCollapse__B1j1Y",
	"navbarNav": "header_navbarNav__6IxrD",
	"navItem": "header_navItem__u4b_C",
	"HeaderDiv": "header_HeaderDiv__87ny8",
	"navDropdown": "header_navDropdown__3HdgC",
	"navLink": "header_navLink___09CT",
	"dropdownMenu": "header_dropdownMenu__WKfA3",
	"dropdownItem": "header_dropdownItem__evGat",
	"navProfileDropdown": "header_navProfileDropdown__gaeKe",
	"profileImg": "header_profileImg__LL5nh",
	"count": "header_count__ruC3W",
	"floatRight": "header_floatRight__mUDrO",
	"menuImg": "header_menuImg__oq_tD",
	"dropDownItem": "header_dropDownItem__FVrsu",
	"bold": "header_bold__Rn4So",
	"float-right": "header_float-right__XVqNq",
	"signupMenu": "header_signupMenu__lt3yO",
	"mainNavFixedTop": "header_mainNavFixedTop__My7A9",
	"searchBar": "header_searchBar__KK8SU"
};


/***/ }),

/***/ 2824:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5681);
/* harmony import */ var _styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1664);
/* harmony import */ var _common_route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1191);
/* harmony import */ var _common_Modal_invite_earn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6716);
/* harmony import */ var _common_Modal_signin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8709);
/* harmony import */ var _common_Modal_signup__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8524);
/* harmony import */ var _helpers_auth_services__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9062);
/* harmony import */ var _common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4375);
/* harmony import */ var _MyContext_IdDetailsContext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6790);
/* harmony import */ var _helpers_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(421);
/* harmony import */ var _common_Search__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4877);
/* harmony import */ var _styles_search_module_scss__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(5836);
/* harmony import */ var _styles_search_module_scss__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_13__);














// const LinkTag = ({
//     name, bold_className, as, img, spanClassName, icon, href, count, text }) => {
//     return (
//         <Fragment>
//             <li role="menuitem">
//                 <Link href={href} as={as}>
//                     <a className={`${bold_className} ${styles.dropDownItem}  dropdown-item`}>
//                         <img className={styles.menuImg} src={icon} alt="" />
//                         {name}
//                         <span className={styles.floatRight}> {count} <span>{text}</span>
//                             <img src={img}
//                                 alt="" /></span>
//                         {/* <span>  </span> */}
//                     </a>
//                 </Link>
//             </li>
//         </Fragment>
//     )
// }
const Logout = ({ logout  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
        role: "menuitem",
        onClick: logout,
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
            className: ` ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)} dropdown-item`,
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                    src: "/images/others/icon/sidebar/sign_out.png",
                    alt: ""
                }),
                "Sign Out",
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight)
                })
            ]
        })
    }));
};
const AuthenticationHeader = ()=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
        className: `dropdown-menu ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropdownMenu)} `,
        "aria-labelledby": "exampleModal1",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Modal_signup__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                    className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)} dropdown-item`,
                    text: "Sign up",
                    style: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default())
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Modal_signin__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                    className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)} dropdown-item`,
                    style: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default()),
                    text: "Log in"
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                role: "menuitem",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                    href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Help.href */ .Z.Help.href,
                    as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Help.href */ .Z.Help.href,
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                        className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                        children: "Help"
                    })
                })
            })
        ]
    }));
};
const Header = ()=>{
    let token =  false && 0;
    const { userData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(_MyContext_IdDetailsContext__WEBPACK_IMPORTED_MODULE_9__/* .IdContext */ .B);
    const { 0: count , 1: setCount  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    const { 0: currentBooking , 1: setCurrentBooking  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        token && notificationCount();
    }, []);
    const notificationCount = async ()=>{
        const response = await (0,_helpers_service__WEBPACK_IMPORTED_MODULE_10__/* .notification_count_service */ .IA)();
        console.log(response);
        if (response.code == 200) {
            setCount(response.notificationCount);
            setCurrentBooking(response.bookingRequest);
        }
    };
    const handleLogout = ()=>{
        (0,_helpers_auth_services__WEBPACK_IMPORTED_MODULE_7__/* .logout_service */ .lm)();
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().header),
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("nav", {
            className: `navbar navbar-expand-lg navbar-dark fixed-top ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().mainNav)}  `,
            id: "landingHeader",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                    className: `navbar-brand  ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navbarBrand)}`,
                    href: "",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                        src: "/images/others/ejaro_logo_text.png",
                        alt: "Ejaro"
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                    className: "navbar-toggler navbar-toggler-right",
                    type: "button",
                    "data-toggle": "collapse",
                    "data-target": "#navbarResponsive",
                    "aria-controls": "navbarResponsive",
                    "aria-expanded": "false",
                    "aria-label": "Toggle navigation",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "navbar-toggler-icon"
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: `collapse navbar-collapse ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navCollapse)}`,
                    id: "navbarResponsive",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_13___default().SeachFix),
                            id: "fixSearch",
                            style: {
                                display: "none"
                            },
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Search__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                                style: (_styles_search_module_scss__WEBPACK_IMPORTED_MODULE_13___default())
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                            className: `navbar-nav ml-auto ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navbarNav)}`,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: `nav-item ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navItem)} `,
                                children: token == undefined || token == '' ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Modal_signin__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default()),
                                    text: "Share Your Vehicle"
                                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Modal_AddCarModal__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                                    text: "Share Your Vehicle",
                                    style: `nav-link scroll ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navLink)}`
                                })
                            })
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: ` ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().HeaderDiv)} mt-2 mt-md-0 form-inline`,
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navDropdown)}`,
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                            className: `nav-link dropdown-toggle ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navLink)}`,
                                            id: "messagesDropdown",
                                            href: "#",
                                            "data-toggle": "dropdown",
                                            "aria-haspopup": "true",
                                            "aria-expanded": "false",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    src: "/images/others/network_icon.png",
                                                    alt: "",
                                                    style: {
                                                        width: "16px"
                                                    }
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    src: "/images/others/icon/arrow_down.png",
                                                    alt: "",
                                                    style: {
                                                        width: "15px"
                                                    }
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: `dropdown-menu ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropdownMenu)}`,
                                            "aria-labelledby": "messagesDropdown",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                    className: `dropdown-item ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropdownItem)}`,
                                                    href: "#",
                                                    children: "English"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                    className: `dropdown-item ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropdownItem)}`,
                                                    href: "#",
                                                    children: "عربى"
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navProfileDropdown)}`,
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                            className: `nav-link dropdown-toggle ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().navLink)}`,
                                            id: token ? "exampleModal" : "exampleModal1",
                                            href: "#",
                                            "data-toggle": "dropdown",
                                            "aria-haspopup": "true",
                                            "aria-expanded": "false",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    src: "/images/others/menu_icon.png",
                                                    style: {
                                                        width: "15px",
                                                        marginTop: "17px",
                                                        float: "left",
                                                        display: "inline-block"
                                                    },
                                                    alt: ""
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().profileImg),
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        alt: "",
                                                        src: (userData === null || userData === void 0 ? void 0 : userData.image) ? userData === null || userData === void 0 ? void 0 : userData.image : "/images/others/icon/userProfile.png"
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().count),
                                                    children: "1"
                                                })
                                            ]
                                        }),
                                        token ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                            className: `dropdown-menu ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropdownMenu)}`,
                                            "aria-labelledby": "exampleModal",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].myCar.SharedCar.href */ .Z.myCar.SharedCar.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].myCar.SharedCar.as */ .Z.myCar.SharedCar.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().bold)} ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/my_cars.png",
                                                                    alt: ""
                                                                }),
                                                                "My Cars",
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight),
                                                                    children: [
                                                                        " 1",
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Booking.RequestedBooking.as */ .Z.Booking.RequestedBooking.as,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Booking.RequestedBooking.as */ .Z.Booking.RequestedBooking.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().bold)} ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/booking_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Bookings",
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight),
                                                                    children: [
                                                                        " ",
                                                                        currentBooking,
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Notification.href */ .Z.Notification.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Notification.as */ .Z.Notification.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().bold)} ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            style: {
                                                                fontFamily: "NeoSansBold !important"
                                                            },
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/my_cars.png",
                                                                    alt: ""
                                                                }),
                                                                "Message",
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight),
                                                                    children: [
                                                                        " ",
                                                                        count,
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        }),
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                            src: "/images/others/icon/carProfile/red_slider.png",
                                                                            alt: ""
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].TrasctionList.href */ .Z.TrasctionList.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].TrasctionList.as */ .Z.TrasctionList.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/wallet_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Wallet",
                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight),
                                                                    children: [
                                                                        " 21 ",
                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                            children: "SAR"
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].FavouriteList.href */ .Z.FavouriteList.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].FavouriteList.as */ .Z.FavouriteList.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: ` ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/favourite_icon.png",
                                                                    alt: ""
                                                                }),
                                                                "Favorites",
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().floatRight)
                                                                })
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_Modal_invite_earn__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                                                        className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)} dropdown-item`,
                                                        style: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default())
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].FavouriteList.href */ .Z.FavouriteList.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].FavouriteList.as */ .Z.FavouriteList.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/setting.png",
                                                                    alt: ""
                                                                }),
                                                                "Settings"
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                    role: "menuitem",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                        href: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Help.href */ .Z.Help.href,
                                                        as: _common_route__WEBPACK_IMPORTED_MODULE_3__/* ["default"].Help.as */ .Z.Help.as,
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                                            className: `$ ${(_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().dropDownItem)}  dropdown-item`,
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: (_styles_header_module_scss__WEBPACK_IMPORTED_MODULE_12___default().menuImg),
                                                                    src: "/images/others/icon/sidebar/help.png",
                                                                    alt: ""
                                                                }),
                                                                "Help"
                                                            ]
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Logout, {
                                                    logout: handleLogout
                                                })
                                            ]
                                        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(AuthenticationHeader, {
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Header);


/***/ })

};
;