import moment from 'moment';
import React, {  } from 'react';
import HijirDatePicker from '../lib';
import './ExampleStyle.module.css';

const HijriDatePicker =({selectedDate,setDate})=>  {
  

 const onChange = (value) => {
    console.log('OnChange -> Value is: ', value)
  setDate(value)
  }

 const onFocus = (value) => {
    console.log('OnFocus -> Value is: ', value)
  }

    return (
      <>
      <div id="app">
        <HijirDatePicker
          name="hijri_date"
          className="input-style"
          placeholder="DOB"
          selectedDate={selectedDate} 
          dateFormat="iDD-iMM-iYYYY"
          onChange={onChange}
          onFocus={onFocus}
          quickSelect
        />
      </div>
     
      </>
    );
}

export default HijriDatePicker;




