import React, { Component } from 'react'
import styles from '../../../../styles/calendar.module.css'





class DayNames extends Component {
    state = {
      arabicDayNames: ['ح', 'ن', 'ث', 'ر', 'خ', 'ج', 'س'],
      arabicFullDayNames: ['احد', 'اثنين', 'ثلاثاء', 'اربعاء', 'خميس', 'جمعة', 'سبت'],
      englishDayNames: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      arabicshortName: ['Ith','Thu','Arb','Kha','Jum','Sab','Ahd']
    }
    
    render() {
      return (
        <div className={styles.DayNamesList}>
          {
            this.state.arabicshortName.map((item, key) =>  <div className={styles.DayName} key={key.toString()}>{item}</div>)
          }
        </div>
      )
    }
  }
  export default DayNames;