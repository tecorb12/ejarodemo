import React, { Component } from 'react'
import styled from 'styled-components'

const MonthListContainer = styled.span`
  padding: 5px;
`

const MonthSelect = styled.select`
  width: 100px;
  -webkit-appearance: menulist-button;
  background: transparent;
  height: 25px;
  border-radius: 4px;
  font-family: sans-serif;
  font-size: 14px;
`

class MonthsList extends Component {
  state = {
    months: [
      {number: 0, name: 'Muharram'},
      {number: 1, name: 'Safar'},
      {number: 2, name: "Rabi'ul-Awwal"},
      {number: 3, name: "Rabi'ul-Akhir"},
      {number: 4, name: 'Jumadal-Ula'},
      {number: 5, name: 'Jumadal-Akhir'},
      {number: 6, name: 'Rajab'},
      {number: 7, name: "Sha'ban"},
      {number: 8, name: 'Ramadan'},
      {number: 9, name: 'Syawwal'},
      {number: 10, name: "Dhul-Qa'da"},
      {number: 11, name: 'Dhul-Hijja'},
    ]
  }

  render() {
    return (
      <MonthListContainer>
        <MonthSelect onChange={this.props.onChange} value={this.props.currentTime.iMonth()}>
          {
            this.state.months.map((item, key) => <option key={item.number} value={item.number}>{item.name}</option>)
          }
        </MonthSelect>
      </MonthListContainer>
    )
  }
}

export default MonthsList