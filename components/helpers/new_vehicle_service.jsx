import axios from "axios"
import jstz from 'jstz';
import { API_URL, BASE_URL } from "./api_url";


const timezone = jstz.determine();
const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}

//  PRELOAD SERVICES
export const preload_service = async () => {
    let res = await axios.get(BASE_URL + API_URL.PRELOAD_API, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }

        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


//  FINANCE LIST SERVICES
export const finanace_list_service = async () => {
    let res = await axios.get(BASE_URL + API_URL.FINANCE_COMPANY_LIST, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  VEHICLE MODAL  SERVICES
export const vehicle_modal_service = async (makeId) => {
    let res = await axios.post(BASE_URL + API_URL.VEHICLE_MODAL, {
        makeId: makeId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CREATE NEW CAR 

export const new_car_service = async (city, country, latitude, longitude, state, streetAddress, zip, doors, fuelType, makeId, modelId, odometer, seats, transmission, yearId) => {
    let res = await axios.post(BASE_URL + API_URL.ADD_NEW_CAR, {
        address: {
            city: city,
            country: country,
            latitude: latitude,
            longitude: longitude,
            state: state,
            streetAddress: streetAddress,
            zip: zip
        },
        car: {
            doors: doors,
            fuelType: fuelType,
            makeId: makeId,
            modelId: modelId,
            odometer: odometer,
            seats: seats,
            transmission: transmission,
            yearId: yearId,
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CHECK CAR ALREADY EXIST

export const duplicate_car_service = async (carId, vehicleSeqNo) => {
    let res = await axios.post(BASE_URL + API_URL.DUPLICATE_CAR_CHECk, {
        carId: carId,
        vehicleSeqNo: vehicleSeqNo
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// NEW CAR YAKEEN API

export const yakeen_new_car_service = async (sequenceNumber, modelYear) => {
    let res = await axios.post(BASE_URL + API_URL.YAKEEN_VEHICLE_INFO, {
        "sequenceNumber": sequenceNumber,
        "modelYear": modelYear
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  NEW CAR UPDATE REGISTRATION
export const update_car_registration_service = async (arPlateText1, arPlateText2, arPlateText3, carId, chassisNumber, lcPlateNumber, ownership, vehicleRegImg, vehicleSeqNo, vehicle_maker_code, vehicle_model_code, yearId, finance, finance_company_id,) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_UPDATION, {
        arbLeft: arPlateText1,
        arbMiddle: arPlateText2,
        arbRight: arPlateText3,
        carId: carId,
        chassisNumber: chassisNumber,
        lcPlateNumber: lcPlateNumber,
        ownership: ownership,
        vehicleRegImg: vehicleRegImg,
        vehicleSeqNo: vehicleSeqNo,
        vehicle_maker_code: vehicle_maker_code,
        vehicle_model_code: vehicle_model_code,
        yearId: yearId,
        finance: finance,
        finance_company_id: finance_company_id,
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// MODIFY VEHICLE DETAILS


export const car_listing_details_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.VEHICLE_DETAILS, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  STEP THIRD PRICE API

export const recommended_price_service = async (carId, minPrice, recommendedPricing,maxDayMiles,maxMonthMiles,maxWeekMiles,monthlyDiscount,weeklyDiscount,pricePerExtraMile,pricingMethod,weekendPrice) => {
    let res = await axios.post(BASE_URL + API_URL.STEP_THREE_PRICE, {
        car: {
            carId: carId,
            minPrice: minPrice,
            recommendedPricing: recommendedPricing,
            maxDayMiles: maxDayMiles,
            maxMonthMiles: maxMonthMiles,
            maxWeekMiles: maxWeekMiles,
            monthlyDiscount: monthlyDiscount,
            pricePerExtraMile: pricePerExtraMile,
            pricingMethod: pricingMethod,
            weekendPrice: weekendPrice,
            weeklyDiscount: weeklyDiscount
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  STEP Fourth PHOTO API

export const vehicle_photos_service = async (carId, advanceNotice, deliveryOption,description,guestLocationDeliveryPrice,maxTripDays,minTripDays) => {
    let res = await axios.post(BASE_URL + API_URL.STEP_FOUR_PHOTO, {
        car: {
            advanceNotice: advanceNotice,
            carId: carId,
            deliveryOption: deliveryOption,
            description:description,
            guestLocationDeliveryPrice: guestLocationDeliveryPrice,
            maxTripDays: maxTripDays,
            minTripDays: minTripDays
        }
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  IMAGE UPLOAD IN MODAL

