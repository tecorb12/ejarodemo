import axios from "axios"
import { API_URL, BASE_URL, BASE_URL_TWO } from "./api_url"

import jstz from 'jstz';
const timezone = jstz.determine();
const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}


// LANDING PAGE SERVICES
export const login_service = async (email, password) => {
    let res = await axios.post(BASE_URL + API_URL.LOGIN, {
        email: email,
        password: password
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            // localStorage.clear()
            // window.location.href = "/"
        }
        if (response.code == 200) {
            localStorage.token = response.result.sessionToken
            window.location.href = "/"

        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  DUPLICATE USER LOGIN 

export const duplicate_user_service = async (idNumber) => {
    let res = await axios.post(BASE_URL + API_URL.DUPLICATE_USER, {
        idNumber: idNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)

        // }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  VERIFY   IqamaNumber Number 
export const verify_iqamaNumber_service = async (dob, iqamaNumber) => {
    let res = await axios.post(BASE_URL + API_URL.VERIFY_IQAMA_NUMBER, {
        dateOfBirth: dob,
        iqamaNumber: iqamaNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)

        // }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// VERIFY SOUDI NUMBER
export const verify_soudiNumber_service = async (dob, nin) => {
    let res = await axios.post(BASE_URL + API_URL.VERIFY_SOUDI_NUMBER, {
        dateOfBirth: dob,
        nin: nin
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
       
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  VERIFY VISA INFORMATION

export const verify_visaInfo_service = async (passportNo, visaNumber) => {
    let res = await axios.post(BASE_URL + API_URL.VERIFY_VISA_INFO, {
        passportNo: passportNo,
        visaNumber: visaNumber
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        // if (response.code === 401) {
        //     localStorage.clear()
        //     window.location.href = "/"
        // }
        //  if(response.code==404){
        //     console.log(response)

        // }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}
//   CHECK USER CONTACT
export const verify_contact_service = async (countryCode, email, mobile, user_id) => {
    let res = await axios.post(BASE_URL + API_URL.UPDATE_USER_CONTACT, {
        countryCode: countryCode,
        email: email,
        mobile: mobile,
        user_id: user_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data

        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  USER SIGNUP

export const user_signup_service = async (countrycode, dob, dobHijiri, email, fname, gender, idNumber, lname, mobile, nationality, password, title, referalCode, image) => {
    let res = await axios.post(BASE_URL + API_URL.SIGNUP, {
        "countryCode": countrycode,
        "dob": dob,
        "dobHijiri": dobHijiri,
        "email": email,
        "fname": fname,
        "gender": gender,
        "idFrontImage": image,
        "idNumber": idNumber,
        "lname": lname,
        "mobile": mobile,
        "nationality": nationality,
        "password": password,
        "referal_code": referalCode,
        "title": title
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        else if (response.code == 200) {
            localStorage.token = response.result.sessionToken

        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  SEND OTP

export const sent_otp_service = async (countrycode, mobile, userId) => {
    let res = await axios.post(BASE_URL + API_URL.SEND_OTP, {
        countryCode: countrycode,
        mobile: mobile,
        user_id: userId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        //  if(response.code==404){
        //     console.log(response)

        // }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  VERIFY OTP 

export const verify_otp_service = async (otp) => {
    let res = await axios.post(BASE_URL + API_URL.OTP_VERIFY, {
        otp: otp
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    })
    try {
        let response = res.data

        //  if(response.code==404){
        //     console.log(response)

        // }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  FORGET PASSWORD

export const forget_password_service = async (email) => {
    let res = await axios.post(BASE_URL + API_URL.FORGET_PASSWORD,{
        email: email
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


//  LOGOUT API INTEGRATION

export const logout_service = async () => {
    let res = await axios.get(BASE_URL + API_URL.LOGOUT, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": localStorage.token
        }
    })
    try {
        let response = res.data
        if (response.code == 200) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}



