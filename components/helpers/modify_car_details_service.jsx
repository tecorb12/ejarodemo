
import axios from "axios"
import jstz from 'jstz';
import { API_URL, BASE_URL } from "./api_url";

const timezone = jstz.determine();
const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}

// EDIT CAR PRICE DETAILS

export const car_modify_price_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.EDIT_PRICE_DETAILS, {
        carId: carId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}



// UPDATE FLAT PRICES 

export const car_update_price_service = async (carId,weekdaysPrice,weekendPrice) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_UPDATE_FLAT_PRICE, {
        carId: carId,
        weekdaysPrice: weekdaysPrice,
        weekendPrice: weekendPrice
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// UPDATE DISCOUNTS 

export const car_update_discounts_service = async (carId,discountOnMonthBooking,discountOnWeekBooking) => {
    let res = await axios.post(BASE_URL + API_URL.UPDATE_DISCOUNTS, {
        car_id: carId,
        discountOnMonthBooking: discountOnMonthBooking,
        discountOnWeekBooking: discountOnWeekBooking
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// UPDATE DAY WEEKLY MONTHLY KM 

export const car_update_Km_service = async (carId,maxDayMiles,maxMonthMiles,maxWeekMiles,pricePerExtraMile) => {
    let res = await axios.post(BASE_URL + API_URL.UPDATE_KM, {
        car_id: carId,
        maxDayMiles: maxDayMiles,
        maxMonthMiles: maxMonthMiles,
        maxWeekMiles: maxWeekMiles,
        pricePerExtraMile: pricePerExtraMile
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// UPDATE DAY WEEKLY MONTHLY KM 

export const car_extra_details_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_EXTRA_DETAILS, {
        carId: carId,
      
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// UPDATE EXTRA DETAILS OF VEHICLE 

export const update_extra_details_service = async (carId,seat,doors,ownedship,financeCompany,financeId,fuelId,odometer,advanceNotice,transmission,minDays, maxDays,smoking,description) => {
    let res = await axios.post(BASE_URL + API_URL.UPDATE_EXTRA_DETAILS, {
        "carId": carId,
        "description":description,
        "seats":seat,
        "doors":doors,
        "ownership":ownedship,
        "finance_company_id":financeId,
        "finance":financeCompany,
        "fuelTypeId":fuelId,
        "odometer": odometer,
        "advanceNotice": advanceNotice,
        "transmission": transmission,
        "minTripDays": minDays,
        "maxTripDays": maxDays,
        "smoking":smoking
      
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// CAR INSURANCE API

export const car_insurance_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.INSURANCE_DETAILS, {
        car_id: carId,
       
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  INSURANCE COMPANY LIST

export const car_insurance_list_service = async (carId) => {
    let res = await axios.get(BASE_URL + API_URL.INSURANCE_LIST, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  SELECT INSURANCE POLICY 

export const select_insurance_policy_service = async (carId,provider,status,type) => {
    let res = await axios.post(BASE_URL + API_URL.SELECT_INSURANCE_TYPE,{
        car_id: carId,
        insuranceProvider: provider,
        is_Insured: status,
        policyType: type
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


//  OWNER EARNING 

export const earning_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.EARNING,{
        carId: carId,
       
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//   REVIEW LIST 

export const review_list_service = async (carId) => {
    let res = await axios.post(BASE_URL + API_URL.REVIEWS_LIST,{
        carId: carId,
       
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


