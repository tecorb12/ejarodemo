import axios from "axios"
import { API_URL, BASE_URL } from "./api_url"

import jstz from 'jstz';
const timezone = jstz.determine();

const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}

// LANDING PAGE SERVICES
export const homepage_service = async (lat,lng,userId) => {
    let res = await axios.post(BASE_URL + API_URL.HOMEPAGE, {
        latitude: lat,
        longitude: lng,
        user_id: userId
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// USER DETAILS API 

export const session_details_service = async () => {
    let res = await axios.get(BASE_URL + API_URL.SESSION_DETAILS, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// ADD TO FAVOURITE

export const add_favourite_service = async (car_id) => {
    let res = await axios.post(BASE_URL + API_URL.ADD_TO_FAVOURITE, {
        car_id: car_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// NOTIFICATION COUNT

export const notification_count_service = async () => {
    let res = await axios.get(BASE_URL + API_URL.NOTIFICATION_COUNT, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 401) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}