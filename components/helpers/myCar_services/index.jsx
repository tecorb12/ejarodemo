

// USER DETAILS API 
import axios from "axios"
import jstz from 'jstz';
const timezone = jstz.determine();
import { API_URL, BASE_URL } from "../api_url"
const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}

//  MY CAR LIST
export const myCar_service = async (keyword) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_LIST, {
        keyword: keyword,
        page: 1,
        per_page: 15
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  MY CAR DETAILS
export const myCar_details_service = async (car_id, user_id,latitude,longitude,tripStart,tripEnd) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_DETAILS, {
        "car_id": car_id,
        "user_id": SESSION_TOKEN ?user_id : "",
        "latitude": latitude,
        "longitude": longitude,
        "fromDate": tripStart,
        "tillDate": tripEnd
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  MY CAR DETAILS
export const mycar_action_service = async (car_id, requested, unlistReason) => {
    let res = await axios.post(BASE_URL + API_URL.DELETE_CAR, {
        carId: car_id,
        requestFor: requested,
        unlistReason: unlistReason
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CAR AVAILABLITY PRICE
export const price_availablity_service = async (car_id) => {
    let res = await axios.post(BASE_URL + API_URL.CALENDAR_AVAILIBILITY, {
        carId: car_id,

    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CAR AVAILABLITY 
export const calendar_availability_service = async (car_id) => {
    let res = await axios.post(BASE_URL + API_URL.CALENDAR_REVISE, {
        carId: car_id,

    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// DATE SELECTED FOR AVAILABILITY

export const selected_date_service = async (car_id, startDate) => {
    let res = await axios.post(BASE_URL + API_URL.DATE_AVAILABILITY, {
        carId: car_id,
        startDate: startDate
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

// MY CAR ALL IMAGES

export const car_all_images_service = async (car_id) => {
    let res = await axios.post(BASE_URL + API_URL.MY_CAR_IMAGES, {
        carId: car_id,
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


