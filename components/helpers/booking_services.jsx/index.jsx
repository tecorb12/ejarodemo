import axios from "axios"
import jstz from 'jstz';
const timezone = jstz.determine();
import { API_URL, BASE_URL } from "../api_url"
const ISSERVER = typeof window === "undefined";
let GUEST_TOKEN = ""
let SESSION_TOKEN = ""
if (!ISSERVER) {
    // SECURITY_TOKEN = localStorage.getItem("token")
    GUEST_TOKEN = localStorage.g_token,
        SESSION_TOKEN = localStorage.token
}

//  BOOKING LIST
export const booking_list_service = async (keyword) => {
    let res = await axios.post(BASE_URL + API_URL.BOOKING_LIST, {
        keyword: keyword,
        page: 1,
        per_page: 15
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CAR BOOKING PROFILE

export const booking_car_profile_service = async (carId, latitude, longitude, tripStart, tripEnd, user_id, promoCode) => {
    let res = await axios.post(BASE_URL + API_URL.BOOKING_CAR_PROFILE, {
        carId: carId,
        latitude: latitude,
        longitude: longitude,
        promoCode: promoCode,
        tripEnd: tripStart,
        tripStart: tripEnd,
        user_id: user_id
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  CAR ESTIMATE PRICE 
// export const car_estimate_price_service = async (carId, latitude, longitude, tripStart, tripEnd, user_id, promoCode) => {
    // let res = await axios.post(BASE_URL + API_URL.CAR_ESTIMATE_PRICE, {
//         carId: carId,
//         latitude: latitude,
//         longitude: longitude,
//         promoCode: promoCode,
//         tripStart: tripStart,
//         tripEnd: tripEnd,
//         user_id: user_id
//     }, {
//         headers: {
//             "Content-Type": "application/json",
//             "timeZone": timezone.name(),
//             "deviceType": "web",
//             "accessToken": SESSION_TOKEN
//         }
//     })
//     try {
//         let response = res.data
//         if (response.code === 345) {
//             localStorage.clear()
//             window.location.href = "/"
//         }
//         return response
//     } catch (error) {
//         console.log("Error", error)
//         return error

//     }
// }

//  DELIVERY LOCATION LIST

export const car_delivery_location_list_service = async (carId,user_id) => {
    let res = await axios.post(BASE_URL + API_URL.DELIVERY_LOCATION_LIST, {
        carId: carId,
        user_id:user_id

    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  ADD DIFFERENT DELIVERY LOCATION
export const car_delivery_location_service = async (carId, latitude, longitude, city, state, country, streetAddress,zip,user_id) => {
    let res = await axios.post(BASE_URL + API_URL.DELIVERY_LOCATIONS, {
        carId: carId,
        city: city,
        country: country,
        latitude: latitude,
        longitude: longitude,
        state: state,
        streetAddress: streetAddress,
        user_id: user_id,
        zip: zip

    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}


//  CALCULATE BOOKING ESTIMATE PRICES

export const car_estimate_price_service = async (carId, latitude, longitude, tripStart, tripEnd, user_id,delivery_location_id, promoCode) => {
    let res = await axios.post(BASE_URL + API_URL.CAR_ESTIMATE_PRICE, {
        carId: carId,
        delivery_location_id: delivery_location_id,
        latitude: latitude,
        longitude: longitude,
        promoCode: promoCode,
        tripEnd: tripEnd,
        user_id: user_id,
        tripStart: tripStart

    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}

//  DELETE DELIVERY ADDRESS

export const delete_address_service = async (delivery_location_id) => {
    let res = await axios.post(BASE_URL + API_URL.DELETE_DELIVERY_ADDRESS, {
        id: delivery_location_id,
    }, {
        headers: {
            "Content-Type": "application/json",
            "timeZone": timezone.name(),
            "deviceType": "web",
            "accessToken": SESSION_TOKEN
        }
    })
    try {
        let response = res.data
        if (response.code === 345) {
            localStorage.clear()
            window.location.href = "/"
        }
        return response
    } catch (error) {
        console.log("Error", error)
        return error

    }
}
