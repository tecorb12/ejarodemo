import { Fragment } from "react"
import styles from "../../styles/footer.module.scss"
import { FooterImage } from "../common/image_urls"
const Footer = () => {
    return (
        <Fragment>
            <div className={styles.Footer} >
                <div className="container-fluid">

                    <div className={`pt-4 no-gutters pb-2 row ${styles.saprator}`}>
                        <div className="col-md-3 col-sm-6 col-">
                            <h3>Get Started</h3>
                            <ul>
                                <li><a>Get the iPhone App</a></li>
                                <li><a>Get the Android App</a></li>
                                <li><a>Car rentel alternatives</a></li>
                                <li><a>Make money with your car</a></li>
                                <li><a>Calculator</a></li>
                                <li><a>All-Star Hosts</a></li>
                                <li><a>Ejaro for bussiness</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3 col-sm-6 col-">
                            <h3>Learn More</h3>
                            <ul>
                                <li><a>How ejaro works</a></li>
                                <li><a>Policies</a></li>
                                <li><a>Trust &amp; safety</a></li>
                                <li><a>Owner Tools</a></li>
                                <li><a>Traveler FAQ's</a></li>
                            </ul>
                        </div>
                        <div className="col-md-4 col-sm-6 col-">
                            <h3>Top cities</h3>
                            <div className="row">
                                <div className="col-md-6">
                                    <ul>
                                        <li><a>Atlanta</a></li>
                                        <li><a>Montreal</a></li>
                                        <li><a>Boston</a></li>
                                        <li><a>San Diego</a></li>
                                        <li><a>Chicago</a></li>
                                        <li><a>San Francisco</a></li>
                                        <li><a>Denver</a></li>
                                        <li><a>Seattle</a></li>
                                    </ul>
                                </div>
                                <div className="col-md-6">
                                    <ul>
                                        <li><a>Honolulu</a></li>
                                        <li><a>Toronto</a></li>
                                        <li><a>Houston</a></li>
                                        <li><a>Washington DC</a></li>
                                        <li><a>Los Angeles</a></li>
                                        <li><a>View Airports</a></li>
                                        <li><a>Ejaro for bussiness</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-2 col-sm-6 col-">
                            <div className="">
                                <h3>Talk to us</h3>
                                <div className="row">
                                    <div className="col-md-12">

                                        <ul>
                                            <li><a>Read our blog</a></li>
                                            <li><a>Contact customer support</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.aboutEjaro} id="footer">
                        <div className={styles.copyright}>
                            <div className="row">
                                <div
                                    className="col-md-3 order-sm-3 order-md-1 order-lg-1 order-3 text-md-left"
                                >
                                    <p>All rights reserved - Vehicle Sharing Co Ltd Ejaro © 2020-21</p>
                                </div>
                                <div className="col-md-6 order-sm-1 order-sm-2 order-lg-2 order-1">
                                    <ul className={`m-auto ${styles.link}`}>
                                        <li>
                                            <a
                                                href="https://www.ejaro.com/pdf/Terms_and_Conditions_Agreement.pdf" target="_blank"
                                            >Terms &amp; Conditions</a
                                            >
                                        </li>
                                        <li>
                                            <a href="https://www.ejaro.com/pdf/Privacy_Policy.pdf" target="_blank"
                                            >Privacy Policy</a
                                            >
                                        </li>
                                        <li>
                                            <a
                                                href="https://www.ejaro.com/pdf/Ejaro_Cancellation_Policy.pdf" target="_blank"
                                            >Cancellation &amp; Refund Policy</a
                                            >
                                        </li>
                                        <li>
                                            <a href="https://www.ejaro.com/pdf/Payment_Terms.pdf" target="_blank">
                                                Payment Policy</a
                                            >
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-md-3 order-sm-2 order-sm-3 order-lg-3 order-2 text-md-right" >
                                    <ul className="m-auto mx-md-auto">
                                        <li>
                                            <a href="https://www.facebook.com/ejaroapp/" >
                                                <img src={FooterImage.facebook} />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/ejaro.app/">
                                                <img src={FooterImage.instagram} />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/EjaroApp" >
                                                <img src={FooterImage.twitter} />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/channel/UCMjS_rF-ybOzUDSTRmm0W5g" >
                                                <img src={FooterImage.youtube} />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </Fragment>
    )
}
export default Footer