import { Component } from "react";
import Slider from "react-slick";
// import { Responsive } from "./responsive";
import styles from './slider.module.css'


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    let classStatus = className === "slick-arrow slick-next slick-disabled"
    return (
        <div
            className={`${classStatus === true ? "" : styles.owlNext} ${className}`}
            style={{ ...style, display: "block", }}
            onClick={onClick}
        >
        </div>
    );
}
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    let classStatus = className === "slick-arrow slick-prev slick-disabled"
    return (
        <div
            className={`${classStatus === true ? "" : styles.owlPrev} ${className}`}
            style={{ ...style, display: "block", }}
            onClick={onClick}
        >
        </div>
    );
}


class SimpleSlider extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        var settings = {
            dots: true,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />,
            slidesToShow: 5,
            slidesToScroll: 1,
            initialSlide:0,
            autoplay: true,
            speed: 5000,
            // autoplaySpeed: 5000,
            slickNext:2
            // beforeChange: (current, next) => this.setState({ infinite: true }),



        };
        return (
            <div className={`container-fluid `} >
                <div className={`row`}>
                    <div className={`col-12 ${styles.sliderSection} px-0`}>
                        <Slider ref={c => (this.slider = c)}  {...settings}>
                            {this.props.children}
                        </Slider>
                    </div>
                </div>
            </div>
        );
    }

}
export default SimpleSlider
