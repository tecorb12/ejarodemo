export const home_image = {
       comptative_price :"/images/others/home/price.png",
       more_option :"/images/others/home/option.png",
       quick_easy :"/images/others/home/quick_service.png",
       BrandLogo :{
               logo1:"/images/others/magnitt.png",
               logo2:"/images/others/startup.png",
               logo3:"/images/others/waya.png",
       },
       CardLogo :{
        logo1:"/images/others/mastercard.png",
        logo2:"/images/others/visa-padding.png",
        logo3:"/images/others/casa_mada.png",  
        logo4:"/images/others/transport_authority.png",    
        logo5:"/images/others/vision.png",
       },
       Download :{
               app_store:"/images/others/home/app_store1.png",
               google_store:"/images/others/home/google_play1.png",
       },
       mobile_img:"/images/others/mobile_mockup.png",
}
export const FooterImage = {
        facebook:"/images/others/home/facebook.png",
        instagram:"/images/others/home/instagram.png",
        twitter:"/images/others/home/twitter.png",
        youtube:"/images/others/home/youtube_icon.png",
}