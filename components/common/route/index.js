

export const onClickNextPage = (url) => {
    let URL = url === undefined ? "all_post" : url
    return URL;
}
const Routes = ({
    Auth: {
        login: {
            as: "/auth/login",
            href: "/auth/login"
        },
        signup: {
            as: "/auth/SignUp",
            href: "/auth/signup"
        },
        otpVerify: {
            as: "/auth/otp",
            href: "/auth/otp"
        },
        forgot: {
            as: "/auth/forgot",
            href: "/auth/forgot"
        },
        reset_password: {
            as: "/auth/reset_password/[reset_password]",
            href: "/auth/reset_password/[reset_password]"
        },
       
        landing_page:{
            as:"/",
            path:"/"
        }
        
    },
    myCar: {
        SharedCar: {
            as: "/myCar/shared_car",
            href: "/myCar/shared_car"
        },
        ParkedCar:{
            as:"/myCar/parked_car",
            href:"/myCar/parked_car"

        }

    },
    Booking:{
        RequestedBooking:{
            as:"/booking_list/request",
            href:"/booking_list/request"
        },
       BookingHistory:{
        as:"/booking_list/history",
        href:"/booking_list/history",
    },
         CurrentBooking:{
        as:"/booking_list/current",
        href:"/booking_list/current",
    }
    },
    
    Earnings: {
        earnings: {
            as: "/earnings",
            href: "/earnings"
        }
    },
    ReferFriend: {
        referFriend: {
            as: "/refer_friends",
            href: "/refer_friends"
        }
    },
    
    Help:{
        as:"/Help/genral",
        href:"/Help/genral"

    },

    
    
    Notification:{
        as:"/message/notificationList",
        href:"/message/notificationList"
    },
    Message:{
        as:"/messages/[id]",
        href:"/messages/[index]"
    },
    Payment :{
        as:"/payment_section",
        href:"/payment_section"
    },
    Term :{
       as:"/terms",
       href:"/terms"
    },
    Privacy_Policy:{
        as:"/policy",
        href:"/policy"
    },
    FavouriteList:{
        as:"/favouritesList",
        href:"/favouritesList"
    },
    LicenseDetails:{
        as:"/setting/license",
        href:"/setting/license"
    },
    TrasctionList:{
        as:"/trasaction/allList",
        href:"/trasaction/allList"
    }

})
export default Routes