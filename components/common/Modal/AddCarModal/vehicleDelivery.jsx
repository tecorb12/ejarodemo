import { useCallback, useContext, useEffect, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { vehicle_photos_service } from "../../../helpers/new_vehicle_service"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import SuccessResponseModal from "../succefully_modal"
import ProgressSection from "./progress"
const VehicleDelivery = ({ counter, setCounter }) => {
    const [isDisabled, setDisabled] = useState(false)
    const { setData, newCarData } = useContext(IdContext)
    const [message, setMessage] = useState("")
    const [isOpen, setOpen] = useState(false)
    const [deliveryOption, setDeliveryOption] = useState({
        duration: "",
        shortestDuration: "",
        longestDuration: "",
        description: "",
        deliveryFess: "Free Delivery"
    })
    const updateState = (data) => {

        setDeliveryOption((state) => {
            let newObj = { ...state, ...data }
            if (newObj.duration && newObj.shortestDuration && newObj.longestDuration) {
                setDisabled(true)
            }
            return ({ ...state, ...data })
        })
    };

    const [isStatus, setStaus] = useState(false)

    useEffect(() => {

    }, [isStatus])

    const toggleChecked = () => {
        setStaus((prev) => {
            // setDisabled(!prev)
            return (!prev)
        })


    }
    // const onChangeHandler = useCallback(
    //     ({target:{name,value}}) => setDeliveryOption(state => ({ ...state, [name]:value }), [])
    //   );
    const onChangeHandler = (e) => {
        updateState({
            [e.target.name]: e.target.value
        })

    }
    const handleClick = async (e) => {
        e.preventDefault()
        let CarID = newCarData.id
        const response = await vehicle_photos_service(CarID, deliveryOption.duration, isStatus, deliveryOption.description,
            deliveryOption.deliveryFess, deliveryOption.longestDuration, deliveryOption.shortestDuration)
        if (response.code == 200) {
            setCounter(5)
            setData(response.result)
        }
        else if (response.code == 422) {
            setMessage(response.message)
            setOpen(true)
        }

    }



    return (
        <div>
            <ProgressSection step={counter} />

            <form>
                <div className="row">
                    <div className="col-12">
                        <h5>Vehicle delivery</h5>
                    </div>
                    <div className="col-12">
                        <div className={styles.addCarSec}>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <label>Enable delivery option to increase your rentals! Guests like the option of having the vehicle delivered to them </label>
                            </div>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <div className="row">
                                    <div className="col-10">
                                        <label> Enable delivery option  </label>
                                    </div>
                                    <div className={`${styles.toggle} col-2`}>
                                        <input type="checkbox" id="switch" onChange={(e) => {
                                            toggleChecked(e);
                                        }} />
                                        <label for="switch"></label>
                                    </div>
                                </div>
                            </div>
                            {isStatus == true && <>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Delivery fee</label>
                                    <select autoComplete="off" name="deliveryFess" value={deliveryOption.deliveryFess} onChange={(e) => { onChangeHandler(e) }}>
                                        <option value="Free Delivery"> Free Delivery </option>
                                        <option value="25"> 25 </option>
                                        <option value="50"> 50 </option>
                                        <option value="75"> 75 </option>
                                        <option value="100"> 100 </option>
                                    </select>
                                    <span className={styles.Info}>0.0 SAR is the recommended price </span>
                                </div>
                            </>}

                            <h5 className="mt-2 mb-2">Notices</h5>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <label>Advance notice needed before any rental</label>
                                <select autoComplete="off" name="duration" value={deliveryOption.duration} onChange={(e) => { onChangeHandler(e) }}>
                                    <option value=""> Select Duration </option>
                                    <option value="1">  1 hour  </option>
                                    <option value="2">  2 hours  </option>
                                    <option value="3">  3 hours   </option>
                                    <option value="6">  6 hours   </option>
                                    <option value="12">  12 hours   </option>
                                    <option value="24">  1 day   </option>
                                    <option value="48">   2 days    </option>
                                    <option value="72">   3 days    </option>
                                    <option value="168">   1 week    </option>


                                </select>
                            </div>

                            <div className="row">
                                <div className="col-12">
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>How long would you like youe rental to last?</label>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Shortest</label>
                                        <select autoComplete="off" name="shortestDuration" value={deliveryOption.shortestDuration} onChange={(e) => { onChangeHandler(e) }}>
                                            <option value=""> Select shortest duration </option>
                                            <option value="1"> 1 Day </option>
                                            <option value="2"> 2 Days </option>
                                            <option value="3"> 3 Days </option>
                                            <option value="5"> 5 Days </option>
                                            <option value="7"> 1 Week </option>
                                            <option value="14"> 2 Week </option>
                                            <option value="30"> 1 Month </option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Longest</label>
                                        <select autoComplete="off" name="longestDuration" value={deliveryOption.longestDuration} onChange={(e) => { onChangeHandler(e) }}>
                                            <option value=""> Select Longest duration </option>
                                            <option value="1"> 1 Day </option>
                                            <option value="2"> 2 Days </option>
                                            <option value="3"> 3 Days </option>
                                            <option value="5"> 5 Days </option>
                                            <option value="7"> 1 Week </option>
                                            <option value="14"> 2 Week </option>
                                            <option value="30"> 1 Month </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <label> Car description(optional) </label>
                                <textarea type="text" name="description" value={deliveryOption.description} onChange={(e) => { onChangeHandler(e) }}
                                    placeholder="Renters will be chargedfor every additional KM"
                                    className={`form-control `}
                                ></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="col-12">
                        <button className={`${styles.sendBtn}`} onClick={(e) => { handleClick(e) }} disabled={!isDisabled}>
                            Next
                        </button>
                    </div>
                </div>
            </form>
            <SuccessResponseModal message={message} open={isOpen} setSuccessresponse={setOpen} />

        </div>
    )
}
export default VehicleDelivery