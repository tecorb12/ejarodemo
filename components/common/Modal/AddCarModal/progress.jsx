import styles from "../../../../styles/customeModal.module.scss"
const ProgressSection = ({ step }) => {

    const stepFunc = () => {
        let list = Array(7).fill("", 1).map((data, index) => {
            if (index <= step) {
                return <li className={styles.Active}></li>
            }
            else {
                return <li className={""}></li>
            }
        })
        return list
    }

    return (
        <div class="row">
            <div className="col-md-12 px-0">
                <ul className={styles.progressBar}>
                    {stepFunc().map((data) => (data))}
                </ul>
            </div>
        </div>
    )
}
export default ProgressSection