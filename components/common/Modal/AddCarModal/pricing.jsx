import { useContext, useEffect, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { recommended_price_service } from "../../../helpers/new_vehicle_service"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import SuccessResponseModal from "../succefully_modal"
import ProgressSection from "./progress"
const Pricing = ({ counter, setCounter, lcplateModal, setLcPlateModal, lcPlatemessage }) => {
    const [recommendedPrice, IsRecommendedPrice] = useState("")
    const [priceDetails, setPriceDetail] = useState({
        priceMethod: "",
        dailyPrice: 0,
        weeklyPrice: 0,
        holiDayPrice: 0,
        weeklyDiscount: 0,
        monthlyDiscount: 0,
        IsKmDay: "",
        IsKmWeek: "",
        IsKmMonth: "",
        ExtraFees: 0
    })
    const [errorMsg, setErrorMsg] = useState("")
    const [isDisabled, setDisabled] = useState(false)
    const { uploadRegistration } = useContext(IdContext)
    useEffect(() => {
        IsRecommendedPrice(uploadRegistration?.prices.recommendedPrice)
        setPriceDetail((prev) => {
            return {
                ...prev,
                dailyPrice: uploadRegistration?.prices.recommendedPrice,
                weeklyPrice: uploadRegistration?.prices.recommendedPrice
            }
        })
    }, [uploadRegistration?.prices?.recommendedPrice])

    const [isStatus, setStaus] = useState(false)

    const updateState = (data) => {
        setPriceDetail((state) => {
            let newObj = { ...state, ...data }
            if (newObj.priceMethod == "daily") {
                if (newObj.dailyPrice && newObj.weeklyDiscount && newObj.monthlyDiscount && newObj.IsKmDay &&
                    newObj.IsKmWeek && newObj.IsKmMonth && newObj.ExtraFees) {
                    setDisabled(true)
                }
            }
            else if (newObj.priceMethod == "custom") {
                if (newObj.weeklyPrice && newObj.holiDayPrice && newObj.weeklyDiscount && newObj.monthlyDiscount && newObj.IsKmDay &&
                    newObj.IsKmWeek && newObj.IsKmMonth && newObj.ExtraFees) {
                    setDisabled(true)
                }
            }
            else {
                setDisabled(false)

            }
            return ({ ...state, ...data })
        })
    };
    const handlChange = (e) => {
        let name = [e.target.name];
        if (name == "ExtraFees" && Number(e.target.value) > 3) {
            setErrorMsg("Extra KM can not exceed 3 sar")
        }
        else {
            updateState({
                [e.target.name]: e.target.value
            })
            setErrorMsg("")

        }

    }
    const toggleChecked = () => {
        setStaus((prev) => {
            setDisabled(!prev)
            return (!prev)
        })


    }



    const handleClick = async (e) => {
        e.preventDefault()
        let CarID = uploadRegistration.id
        if (isStatus) {
            const response = await recommended_price_service(CarID, recommendedPrice, isStatus)
            if (response.code == 200) {
                setCounter(4)
            }
        }
        else {
            let price = priceDetails.priceMethod == "daily" ? priceDetails.dailyPrice : priceDetails.weeklyPrice
            const response = await recommended_price_service(CarID, price, isStatus, priceDetails.IsKmDay, priceDetails.IsKmMonth,
                priceDetails.IsKmWeek, priceDetails.monthlyDiscount, priceDetails.weeklyDiscount, priceDetails.ExtraFees, priceDetails.priceMethod, priceDetails.holiDayPrice)
            console.log(response)
            if (response.code == 200) {
                setCounter(4)
            }
        }
    }
    return (
        <div>
            <ProgressSection step={counter} />
            <form>
                <div className="row">
                    <div className="col-12">
                        <h5>Pricing</h5>
                    </div>
                    <div className="col-12">
                        <div className={styles.addCarSec}>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <label>Competitive prices generate more requests.</label>
                                <p style={{ width: "100%", whiteSpace: "unset", textAlign: "left" }}>
                                    Start with the lowest possible price to get your first rentals and move your
                                    listing up the search results. You can always increase it later on if necessary. </p>
                            </div>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <div className="row">
                                    <div className="col-10">
                                        <label> Enable Recommended Pricing </label>
                                    </div>
                                    <div className={`${styles.toggle} col-2`}>
                                        <input type="checkbox" id="switch" onChange={(e) => {
                                            toggleChecked(e);
                                        }} />
                                        <label for="switch"></label>
                                    </div>
                                </div>
                                {isStatus == true && <>
                                    <input type="text" id="" name="idNumber"
                                        value={`${recommendedPrice} SAR Is the recommended price`} readOnly autoComplete="off"
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                    <span className={styles.Info}>Your vehicle is more likely to be rented at this price </span>
                                </>}
                            </div>
                            {isStatus == false && <>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Pricing method</label>
                                    <select autoComplete="off" value={priceDetails.priceMethod} name="priceMethod" onChange={(e) => { handlChange(e); }}>
                                        <option value=""> Choose a pricing method </option>
                                        <option value="daily"> Daily Pricing</option>
                                        <option value="custom"> Custom Pricing </option>
                                    </select>
                                </div>
                                {priceDetails.priceMethod == "daily" && <>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Daily Price</label>
                                        <input type="text" id="" name="dailyPrice"
                                            value={priceDetails.dailyPrice} autoComplete="off"
                                            onChange={(e) => { handlChange(e); }}
                                            className={`form-control ${styles.stepbarInput}`}
                                        />
                                    </div>
                                </>}
                                {priceDetails.priceMethod == "custom" && <>
                                    <div className="row">
                                        <div className="col-6">
                                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                <label>Weekday price</label>
                                                <input type="text" id="" name="weeklyPrice"
                                                    value={priceDetails.weeklyPrice} autoComplete="off"
                                                    onChange={(e) => { handlChange(e) }}
                                                    className={`form-control ${styles.stepbarInput}`}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                <label>Weekend / Holidays price</label>
                                                <input type="number" id="" name="holiDayPrice"
                                                    placeholder="0" value={priceDetails.holiDayPrice} autoComplete="off"
                                                    className={`form-control`} onChange={(e) => { handlChange(e); }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </>}
                                <div className="row">
                                    <div className="col-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>Weekly discount</label>
                                            <input type="number" id="" name="weeklyDiscount"
                                                placeholder="Enter price" value={priceDetails.weeklyDiscount} autoComplete="off"
                                                className={`form-control`} onChange={(e) => { handlChange(e); }}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>Monthly discount</label>
                                            <input type="number" id="" name="monthlyDiscount"
                                                placeholder="0" value={priceDetails.monthlyDiscount} autoComplete="off"
                                                className={`form-control`} onChange={(e) => { handlChange(e); }}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>KM/Day</label>
                                            <select autoComplete="off" name="IsKmDay" value={priceDetails.IsKmDay} onChange={(e) => { handlChange(e); }}>
                                                <option value=""> Select Value   </option>
                                                <option value="150"> 150 </option>
                                                <option value="200"> 200 </option>
                                                <option value="250"> 250 </option>
                                                <option value="300"> 300 </option>
                                                <option value="350"> 350 </option>
                                                <option value="400"> 400 </option>
                                                <option value="450"> 450 </option>
                                                <option value="500"> 500 </option>
                                                <option value="550"> 550 </option>
                                                <option value="600"> 600 </option>
                                                <option value="650"> 650 </option>
                                                <option value="700"> 700 </option>
                                                <option value="750"> 750 </option>
                                                <option value="800"> 800 </option>
                                                <option value="850"> 850 </option>
                                                <option value="900"> 900 </option>
                                                <option value="950"> 950 </option>
                                                <option value="1000"> 1000 </option>
                                                <option value="1050"> 1050 </option>
                                                <option value="1100"> 1100 </option>



                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>KM/Week</label>
                                            <select autoComplete="off" name="IsKmWeek" value={priceDetails.IsKmWeek} onChange={(e) => { handlChange(e); }}>
                                                <option value=""> Select Value   </option>
                                                <option value="1050" > 1050 </option>
                                                <option value="1250" > 1250 </option>
                                                <option value="1450" > 1450 </option>
                                                <option value="1650" > 1650 </option>
                                                <option value="1880" > 1880 </option>
                                                <option value="2050" > 2050 </option>
                                                <option value="2250" > 2250 </option>
                                                <option value="2450" > 2450 </option>
                                                <option value="2650" > 2650 </option>
                                                <option value="2850" > 2850 </option>
                                                <option value="3050" > 3050 </option>
                                                <option value="3250" > 3250 </option>
                                                <option value="3450" > 3450 </option>
                                                <option value="3650" > 3650 </option>
                                                <option value="3850" > 3850 </option>
                                                <option value="4050" > 4050 </option>
                                                <option value="4250" > 4250 </option>
                                                <option value="4450" > 4450 </option>
                                                <option value="4650" > 4650 </option>
                                                <option value="4850" > 4850 </option>
                                                <option value="5050" > 5050 </option>
                                                <option value="5250" > 5250 </option>
                                                <option value="5450" > 5450 </option>
                                                <option value="5650" > 5650 </option>
                                                <option value="5850" > 5850 </option>
                                                <option value="6050" > 6050 </option>
                                                <option value="6250" > 6250 </option>
                                                <option value="6450" > 6450 </option>
                                                <option value="6650" > 6650 </option>
                                                <option value="6850" > 6850 </option>
                                                <option value="7050" > 7050 </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>KM/Month</label>
                                            <select autoComplete="off" name="IsKmMonth" value={priceDetails.IsKmMonth} onChange={(e) => { handlChange(e) }}>
                                                <option value=""> Select Value   </option>
                                                <option value="4500" > 4500 </option>
                                                <option value="5000" > 5000 </option>
                                                <option value="5500" > 5500 </option>
                                                <option value="6000" > 6000 </option>
                                                <option value="6500" > 6500 </option>
                                                <option value="7000" > 7000 </option>
                                                <option value="7500" > 7500 </option>
                                                <option value="8000" > 8000 </option>
                                                <option value="8500" > 8500 </option>
                                                <option value="9000" > 9000 </option>
                                                <option value="9500" > 9500 </option>
                                                <option value="10000" > 10000 </option>
                                                <option value="10500" > 10500 </option>
                                                <option value="11000" > 11000 </option>
                                                <option value="11500" > 11500 </option>
                                                <option value="12000" > 12000 </option>
                                                <option value="12500" > 12500 </option>
                                                <option value="13000" > 13000 </option>
                                                <option value="13500" > 13500 </option>
                                                <option value="14000" > 14000 </option>
                                                <option value="14500" > 14500 </option>
                                                <option value="15000" > 15000 </option>
                                                <option value="15500" > 15500 </option>
                                                <option value="16000" > 16000 </option>
                                                <option value="16500" > 16500 </option>
                                                <option value="17000" > 17000 </option>
                                                <option value="17500" > 17500 </option>
                                                <option value="18000" > 18000 </option>
                                                <option value="18500" > 18500 </option>
                                                <option value="19000" > 19000 </option>
                                                <option value="19500" > 19500 </option>
                                                <option value="20000" > 20000 </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label> Extra KM Fees </label>
                                    <input type="number" id="" name="ExtraFees" onChange={(e) => { handlChange(e) }}
                                        placeholder="Renters will be chargedfor every additional KM" value={priceDetails.ExtraFees} autoComplete="off"
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                    <span className={`${styles.Info} mt-1`} style={{ fontFamily: "NeoSansBold" }}>0.1 SAR is the recommended price</span>
                                    <span className="text-danger ng-star-inserted" style={{ fontSize: "8px", fontFamily: "NeoSansBold" }}>{errorMsg}</span>
                                </div>
                            </>}
                        </div>

                    </div>


                    <div className="col-12">
                        <button className={`${styles.sendBtn}`} onClick={(e) => { handleClick(e) }} disabled={!isDisabled}>
                            SAVE
                        </button>
                    </div>
                </div>
            </form>
            <SuccessResponseModal message={`Your vehicle is now verified. Plate number is : ${lcPlatemessage}`} open={lcplateModal} setSuccessresponse={setLcPlateModal} />

        </div>
    )
}
export default Pricing