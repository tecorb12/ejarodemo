import { useContext, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import ImagePreviewModal from "./imagePreviewAction"
import ProgressSection from "./progress"
const VehiclePhotos = ({ counter,setCounter }) => {
    const [imageUrl, setUrl] = useState(
        [
            {id:0, isUrl:null},{id:1,isUrl:null},{id:2,isUrl:null},{id:3,isUrl:null},{id:4,isUrl:null}

        ]
    )
    const [curImgIndex , setcurImgIndex]= useState(null)
    const [curSelectedImg , setcurSelectedImg ]= useState(null)
    const[image, setImg] = useState("")
    const [isModal, setModal] = useState(false)
    const [isDisabled, setDisabled] = useState(false)

  const { newCarData } = useContext(IdContext)

    // <------------Onchange Image Upload ---------->

    let carId = newCarData?.id
    const photoUpload = (e,indexValue) => {
        e.preventDefault();
        setModal(true)
        let type = e.target.files[0].type
        if (e.target.files && e.target.files[0]) {

            // setUrl(
            //     URL.createObjectURL(e.target.files[0]),
            // );
            setcurImgIndex(indexValue)
            // setUrl(
            //  (prev)=>{
            //      prev[indexValue].isUrl=URL.createObjectURL(e.target.files[0]);
            //      return([...prev])
            //  }
            // )
            setcurSelectedImg(
                URL.createObjectURL(e.target.files[0]), 
            )
            curImgIndex==3 && setDisabled(true)
        }
         
            setImg(
                e.target.files[0]
            )
        

    }
   const handleClick=(e)=>{
    e.preventDefault()
    setCounter(6)

   }

    return (
        <div>
            <ProgressSection step={counter} />

            <form>
                <div className="row">
                    <div className="col-12">
                        <h5>VEHICLE PHOTOS</h5>
                    </div>
                    <div className="col-12">
                        <div className={styles.addCarSec}>
                            <div className={`${styles.instructionSec}`}>
                                <h4>Add some attractive photos of your vehicle</h4>
                                <p> The better photos you have the more likely it will catch the eyes of renters! Make sure you add some beautiful photos, you can always add more later on if necessary. </p>
                                <ul>
                                    <li className="row">
                                        <div className="col-1">
                                            <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />
                                        </div>
                                        <div className="col-10">
                                            <p>Use landscape format</p>
                                        </div>
                                    </li>
                                    <li className="row">
                                        <div className="col-1">
                                            <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />
                                        </div>
                                        <div className="col-10">
                                            <p>Follow our angle guidelines</p>
                                        </div>
                                    </li>
                                    <li className="row">
                                        <div className="col-1">
                                            <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />
                                        </div>
                                        <div className="col-10">
                                            <p>Keep the background clear and natural</p>
                                        </div>
                                    </li>
                                    <li className="row">
                                        <div className="col-1">
                                            <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />
                                        </div>
                                        <div className="col-10">
                                            <p>Use only natural daylight</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div className={styles.photoSection}>
                                <div className="row ">
                                    <div className="col-12 mb-4">
                                        <div className={` ${styles.customeFormGroup}`}>
                                            <label> Car description(optional) </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row no-gutters">
                                    <div className={`col-md-6 ${styles.ImgArea}`}>
                                        <div className={`${styles.imageBox} ${styles.front}`} style={{ backgroundImage: `url(${imageUrl[0].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[0].isUrl})` }}>
                                            <label htmlFor={`photo`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`}  src={imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl} />
                                            </div> */}
                                                <img htmlFor={`photo`} alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                <input id={`photo`} type="file" name="image" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" onChange={(e) => { photoUpload(e ,0) }} />
                                            </label>
                                            <img src="/images/others/icon/help/drag_and_drop.png" className={styles.dragBtn} alt="" />
                                            <div className={styles.counter}><label>1</label></div>
                                        </div>
                                        <p>A <b>front photo</b>  that stands out,It is the first one renters see</p>
                                    </div>
                                    <div className={`col-md-6 ${styles.ImgArea}`}>
                                        <div className={`${styles.imageBox} ${styles.side}`}  style={{ backgroundImage: `url(${imageUrl[1].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[1].isUrl})` }}>
                                            <label htmlFor={`photo1`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`} src={src} />
                                            </div> */}
                                                <img alt="" htmlFor={`photo1`} className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                <input id={`photo1`} type="file" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png"   onChange={(e) => { photoUpload(e,1) }}/>
                                            </label>
                                            <img src="/images/others/icon/help/drag_and_drop.png" className={styles.dragBtn} alt="" />
                                            <div className={styles.counter}><label>2</label></div>
                                        </div>
                                        <p>A <b>side photo</b> to get an idea of vehicle size</p>
                                    </div>
                                    <div className={`col-md-6 ${styles.ImgArea}`}>
                                        <div className={`${styles.imageBox} ${styles.back}`}   style={{ backgroundImage: `url(${imageUrl[2].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[2].isUrl})` }}>
                                            <label htmlFor={`photo3`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`} src={src} />
                                            </div> */}
                                                <img htmlFor={`photo3`} alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                <input id={`photo3`} type="file" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" onChange={(e) => { photoUpload(e,2) }}/>
                                            </label>
                                            <img src="/images/others/icon/help/drag_and_drop.png" className={styles.dragBtn} alt="" />
                                            <div className={styles.counter}><label>3</label></div>
                                        </div>
                                        <p>A <b>side photo</b> that showcases the vehicle overall</p>
                                    </div>
                                    <div className={`col-md-6 ${styles.ImgArea}`}>
                                        <div className={`${styles.imageBox} ${styles.interior}`}   style={{ backgroundImage: `url(${imageUrl[3].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[3].isUrl})` }}>
                                            <label htmlFor={`photo4`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`} src={src} />
                                            </div> */}
                                                <img htmlFor={`photo4`} alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                <input id={`photo4`} type="file" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" onChange={(e) => { photoUpload(e,3) }}/>
                                            </label>
                                            <img src="/images/others/icon/help/drag_and_drop.png" className={styles.dragBtn} alt="" />
                                            <div className={styles.counter}><label>4</label></div>
                                        </div>
                                        <p>A <b>side photo</b> helps renters picture themselves driving</p>
                                    </div>
                                    <div className={`col-md-6 ${styles.ImgArea} ml-0`}>
                                        <div className={`${styles.imageBox} ${styles.addNew}`}  style={{ backgroundImage: `url(${imageUrl[4].isUrl === null ? "/images/others/icon/placeholder.png" : imageUrl[4].isUrl})` }}>
                                            <label htmlFor={`photo5`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`} src={src} />
                                            </div> */}
                                                <img htmlFor={`photo5`} alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                <input id={`photo5`} type="file" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" onChange={(e) => { photoUpload(e,4) }}/>
                                         
                                            </label>
                                            <p className={styles.AddText}>Add more photos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col-12">
                        <button className={`${styles.sendBtn}`} disabled={!isDisabled} onClick={handleClick}>
                            Next
                    </button>
                    </div>
                </div>
            </form>
            {/* <EditPriceModal open={isModal} style={styles}/> */}
            <ImagePreviewModal setUrl={setUrl} curImgIndex={curImgIndex} carId={carId} imageUrl={curSelectedImg} image={image} open={isModal} close={setModal}/>
        </div>

    )
}
export default VehiclePhotos