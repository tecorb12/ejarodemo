
import { Fragment, useContext, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../../styles/customeModal.module.scss"
import { car_listing_details_service, finanace_list_service, preload_service } from "../../../helpers/new_vehicle_service";
import { IdContext } from "../../../MyContext/IdDetailsContext";
import Insurance from "./insurance";
import PickUpLocation from "./pickupLocation";
import Pricing from "./pricing";
import ProgressSection from "./progress";
import VehicleDelivery from "./vehicleDelivery";
import VehicleDetail from "./vehicleDetail";
import VehiclePhotos from "./vehiclePhotos";
import VehicleVerification from "./vehicleVerification";

const AddNewCar = ({ style, text,carId }) => {
    const [open, setOpen] = useState(false)
    const [counter, setCounter] = useState(0)
    const [vehicleData, setVehicleData] = useState([])
    const [vehicleYear, setyear] = useState({
        id:"",
        value:""
    })
    const [vehicleYearId, setyearId] = useState("")

    const [companyList, setCompanyList] = useState([])
    const [lcplateModal, setLcPlateModal] = useState(false)
  const [lcPlatemessage,setLcPlateMessage] = useState("")
    
  const {setNewCarData,setData} = useContext(IdContext)

    const [location, setLocation] = useState({
        city: "",
        country: "",
        latitude: "",
        longitude: "",
        state: "",
        streetAddress: "",
        zip: ""
    })
    const openModal = (e) => {
        e.preventDefault()
        setOpen(true)
        car_preloads()

    }
    const closeModal = (e) => {
        e.preventDefault()
        setCounter(0)
        setOpen(false)
    }

    const car_preloads = async () => {
        const response = await preload_service()
        setVehicleData(response)
        if (response.code === 200 ) {
            if(carId){
           let listing_detils = await car_listing_details_service(carId)
                 if(listing_detils.code==200){
                     console.log(listing_detils)
                    setCounter(listing_detils.result.doors+1)
                    setyear(listing_detils.result.year?.id)
                    setNewCarData(listing_detils.result)
                    setData(listing_detils.result)
                 }
            }
           let res = await finanace_list_service()
           if(res.code==200){
               setCompanyList(res.company_list)
           }
           
        }
    }


    return (
        <Fragment>
            <button onClick={openModal} className={style}>
                {text}
            </button>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
              <span>x</span>
            </button>
                    <div className={styles.registrationSec}>
                        {/* <ProgressSection /> */}
                        {counter == 0 && <PickUpLocation counter={counter} setCounter={setCounter} location={location} setLocation={setLocation} />}
                        {counter == 1 && <VehicleDetail counter={counter} setCounter={setCounter} vehicleYear={vehicleYear} setyear={setyear}
                            vehicleData={vehicleData} location={location} setLocation={setLocation}  />}
                        {counter == 2 && <VehicleVerification counter={counter} vehicleData={vehicleData} companyList={companyList} 
                         setCounter={setCounter} vehicleYear={vehicleYear} setyear={setyear}  setLcPlateModal={setLcPlateModal} setLcPlateMessage={setLcPlateMessage}/>}
                        {counter == 3 && <Pricing counter={counter} setCounter={setCounter} lcplateModal={lcplateModal} lcPlatemessage={lcPlatemessage} setLcPlateMessage={setLcPlateMessage} setLcPlateModal={setLcPlateModal}/>}
                        {counter == 4 && <VehicleDelivery counter={counter} setCounter={setCounter} />}
                        {counter == 5 && <VehiclePhotos counter={counter} setCounter={setCounter} />}
                        {counter == 6 && <Insurance counter={counter} setCounter={setCounter} setOpen={setOpen}/>}
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default AddNewCar;