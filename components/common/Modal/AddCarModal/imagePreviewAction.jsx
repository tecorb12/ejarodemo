
import axios from "axios";
import { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../../styles/customeModal.module.scss"
import { API_URL, BASE_URL } from "../../../helpers/api_url";
import jstz from 'jstz';
import ImageCropper from "../../ImageCropper";
const timezone = jstz.determine();
const ImagePreviewModal = ({ open, close, imageUrl, image, carId, curImgIndex, setUrl }) => {

    const [croppedImage, setCroppedImage] = useState(undefined);
    const [isImgResize ,setIsImgResize] = useState(1)
    const closeclick = () => {
        close(false)
        console.log("data")
    }

    const handleImageUpload = async (e) => {
        e.preventDefault()
        let imageType = curImgIndex==0 ? "front" : curImgIndex==1 ? "side" : curImgIndex==2 ? "back" : "interior"
        const formdata = new FormData()
        formdata.append("image", imageUrl)
        formdata.append("carId", carId)
        formdata.append("imageType", imageType)
        const imageUploadResponse = await axios.post(BASE_URL + API_URL.IMAGE_UPLOAD, formdata, {
            headers: {
                "Content-Type": "application/json",
                "deviceType": "web",
                "accessToken": localStorage.token,
                "timeZone": timezone.name(),
            }
        })
        if (imageUploadResponse.data.code == 200) {
            setUrl(
                (prev) => {
                    prev[curImgIndex].isUrl = imageUrl;
                    return ([...prev])
                }
            )
        }
        close(false)

        console.log("show image", imageUploadResponse)
    }

    // const rotate = (image) =>{
    //     // let rotateAngle = Number(image.getAttribute("rotangle")) + 90;
    //     // image.setAttribute("style", "transform: rotate(" + rotateAngle + "deg)");
    //     // image.setAttribute("rotangle", "" + rotateAngle);
    //     image("style", "transform: rotate(" + rotateAngle + "deg)")
    //   }
    const RotateLeft = () => {
        var angle = ($('.imageDiv').data('angle') + 90) || 90;
        $('.imageDiv').css({ 'transform': 'rotate(' + angle + 'deg)' });
        $('.imageDiv').data('angle', angle);
    }
    const RotateRight = () =>{
        var angle = ($('.imageDiv').data('angle') - 90) || -90;
        $('.imageDiv').css({'transform': 'rotate(' + angle + 'deg)'});
        $('.imageDiv').data('angle', angle);
    }
 
    // var count = 0.1
    // var zoom = 1;  
    // let total = 0
    // const ZoomIn = () =>{   
    //     console.log("thisggg",total,zoom);  

    //         total = zoom + count
    //         $('.image').css('transform',`scale(${total})`);
    //         count = count + 0.1         
    //         }
 
    // const ZoomOut = () =>{
    //     console.log("current",total,zoom);
    //     if(total > 1){
    //         total = total - 0.1
    //         $('.image').css('transform',`scale(${total})`); 
    //     }else{
    //     total = zoom.toFixed(1) - 0.1
    //     count = count - 0.1; 
    //     $('.image').css('transform',`scale(${total})`);
    //     }
      
    // }

    const ZoomIn = () =>{
          setIsImgResize((prev)=> prev + 0.1)
          $('.image').css('transform',`scale(${isImgResize})`);
    }

    const ZoomOut = () =>{
        setIsImgResize((prev)=>{
            console.log("scal size" , prev.toFixed(1))
            if(prev.toFixed(1) >0.1){
                prev = prev.toFixed(1) - 0.1
            } 
            return(prev)
        })
        $('.image').css('transform',`scale(${isImgResize})`);
    }
    const Reset = () => {
        $('.imageDiv').css({ 'transform': 'rotate( 0deg)' });
        $('.image').css('transform', `scale(1)`);
    }
    return (
        <Fragment>
            {/* <button onClick={openClick}>{text}</button> */}
            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={open}
                onRequestClose={closeclick}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={`${styles.registrationSec}`}>
                        {/* <button onClick={closeclick} className="float-right close mr-0 mt-0 ">
                            <span>x</span>
                        </button> */}

                        <div className="row">
                            <div className="col-12">
                                <button onClick={closeclick} className={`float-right close mr-0 mt-0 ${styles.colse}`}>
                                    <span>x</span>
                                </button>
                            </div>
                            <div className="col-12 text-center pb-4">
                                <h5>Crop Image </h5>

                            </div>
                            <div className={`col-12 ${styles.imageActionButton}`}>
                                <button onClick={RotateLeft}>Rotate Left</button>
                                <button onClick={RotateRight}>Rotate Right</button>
                                <button onClick={Reset}>Reset</button>
                                <button onClick={ZoomIn}>Zoom In</button>
                                <button onClick={ZoomOut}>Zoom Out</button>

                            </div>
                            <div className={`col-12 mt-2 ${styles.imageActionSection}`}>
                                <div className="row ">
                                    <div className="col-6">
                                        <h5>Cropper</h5>
                                        <div className={`${styles.imageArea} imageDiv`} id="image">
                                            <ImageCropper imageToCrop={imageUrl} alt="" classStyle={`image `}
                                                onImageCropped={(croppedImage) => setCroppedImage(croppedImage)}
                                            // onClick={rotate("/images/others/cars/car_list_7.png")}
                                            />
                                        </div>
                                    </div>

                                    <div className="col-6">
                                        <h5>Preview</h5>
                                        <div className={`${styles.imageArea} ${styles.cropped} imageDiv `}>
                                            <img alt="" src={croppedImage ? croppedImage : imageUrl} className="image " />
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div className="col-12">
                                <button onClick={handleImageUpload} className={`${styles.sendBtn}`}>
                                    Upload
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </ReactModal>
        </Fragment>
    )
}

export default ImagePreviewModal;