import { useContext, useEffect, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { new_car_service, vehicle_modal_service } from "../../../helpers/new_vehicle_service"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import ProgressSection from "./progress"
const VehicleDetail = ({ counter, vehicleData, location, setCounter, vehicleYear, setyear }) => {
 
  const [allModals, setAllModals] = useState([])
  const [isDisabled, setDisabled] = useState(false)
  const { setNewCarData } = useContext(IdContext)
  const [isError, setError] = useState({
    code: "", msg: "",
  })
  const [vehicleDetails, setVehicleDetails] = useState({
    vehicleName: "",
    carModal: "",
    transmison: "",
    fuel: "",
    carmileage: "",
    doors: "",
    seats: ""
  })

  let vehicleYears = vehicleData.years
  let vehicleMake = vehicleData.makes
  let vehicleTransmision = vehicleData.transmissionOptions
  let fuelTypes = vehicleData.fuelTypes
  let mileage = vehicleData.odometerOptions

  const updateState = (data) => {

    setVehicleDetails((state) => {
      let newObj = { ...state, ...data }
      if (newObj.vehicleName && newObj.carModal && newObj.transmison && newObj.fuel && newObj.carmileage && newObj.doors && newObj.seats ) {
        setDisabled(true)
      }
      return ({ ...state, ...data })
    })
  };

  const handlChange = (e) => {
    updateState({
      [e.target.name]: e.target.value
    })


  }

  //  ALL MODEL OF VEHICLE
  const VehicleModal = async (e) => {
    // setCarModel("")
    const vehicle_modal = await vehicle_modal_service(e)
    if (vehicle_modal.code == 200) {
      setAllModals(vehicle_modal.models)
    }
  }


  // HANDLE STEP TWO
  const handleClick = async (e) => {

    e.preventDefault()
    if (vehicleYear && vehicleDetails.vehicleName && vehicleDetails.carModal && vehicleDetails.transmison && vehicleDetails.fuel
      && vehicleDetails.carmileage && vehicleDetails.doors && vehicleDetails.seats) {
      setDisabled(true)
      const response = await new_car_service(location.city, location.country, location.latitude, location.longitude, location.state,
        location.streetAddress, location.zip
        , vehicleDetails.doors, vehicleDetails.fuel, vehicleDetails.vehicleName, vehicleDetails.carModal,
        vehicleDetails.carmileage, vehicleDetails.seats, vehicleDetails.transmison, vehicleYear.id)
      if (response.code == 200) {
        setCounter(2)
        setNewCarData(response.result)
        setyear((prev) => { return { ...prev, value: response.result?.year?.title } })


      }
      else if (response.code == 404) {
        setError((prev) => { return { ...prev, code: response.code, msg: response.message } })
        setDisabled(false)

      }
    }

    else {
      const response = await new_car_service(location.city, location.country, location.latitude, location.longitude, location.state,
        location.streetAddress, location.zip
        , vehicleDetails.doors, vehicleDetails.fuel, vehicleDetails.vehicleName, vehicleDetails.carModal,
        vehicleDetails.carmileage, vehicleDetails.seats, vehicleDetails.transmison, vehicleYear.id)

      if (response.code == 200) {
        setCounter(2)
        setNewCarData(response.result)
        setyear((prev) => { return { ...prev, value: response.result?.year?.title } })


      }


    }
  }

  // HANDLE MODEL YEAR ID AND TITLE
  const handleModelYear = (e) => {
    let index = e.target.selectedIndex
    const optionElement = e.target.childNodes[index];
    const optionElementId = optionElement.getAttribute('id');
    setyear((prev) => { return { ...prev, id: optionElementId, value: e.target.value } })
    // optionElementId && setDisabled(true)
  }

  return (
    <div>
      <ProgressSection step={counter} />
      <p style={{ color: "#b2020f", textAlign: "center", fontSize: "20px" }}>{isError.code == 404 && isError.msg}</p>

      <form>
        <div className="row">

          <div className="col-12">
            <h5>Vehicle Details</h5>
          </div>
          <div className="col-12">
            <div className={styles.addCarSec}>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Vehicle Model Year</label>
                <select value={vehicleYear.value} id={vehicleDetails.id} onChange={(e) => { handleModelYear(e); }} autoComplete="off"  >
                  {/* <option value="" id=""> Vehicle Model Year  </option> onChange={(e) => { handleModelYear(e); }} */}
                  {
                    vehicleYears.map((item, index) => {
                      return (
                        <option id={item.id} value={item.title}> {item.title} </option>
                      )
                    })

                  }
                </select>
              </div>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Make </label>
                <select autoComplete="off" value={vehicleDetails.vehicleName} name="vehicleName" onChange={(e) => { handlChange(e); VehicleModal(e.target.value) }} >
                  <option value=""> Select Make </option>
                  {
                    vehicleMake.map((item, index) => {
                      return (
                        <>
                          <option key={item.id} value={item.id}> {item.title} </option>

                        </>
                      )
                    })
                  }
                </select>
              </div>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Model <span>(Select Make First)</span></label>
                <select value={vehicleDetails.carModal} name="carModal" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                  <option value="saudi"> Select Model </option>
                  {
                    allModals.map((item, index) => {
                      return (
                        <>
                          <option key={item.id} value={item.id}> {item.title} </option>

                        </>
                      )
                    })

                  }

                </select>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Transmission</label>
                    <select value={vehicleDetails.transmison} name="transmison" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                      <option value=""> Select Transmission </option>
                      {
                        vehicleTransmision.map((item, index) => {
                          return (
                            <>
                              <option key={item.label} value={item.value}> {item.label} </option>

                            </>
                          )
                        })

                      }
                    </select>
                  </div>
                </div>
                <div className="col-6">
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Fuel Type</label>
                    <select value={vehicleDetails.fuel} name="fuel" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                      <option value=""> Select Fuel Type </option>
                      {
                        fuelTypes.map((item, index) => {
                          return (
                            <>
                              <option key={item.id} value={item.id}> {item.label} </option>
                            </>
                          )
                        })
                      }
                    </select>
                  </div>
                </div>
              </div>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Mileage</label>
                <select value={vehicleDetails.carmileage} name="carmileage" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                  <option value=""> Select Mileage </option>
                  {
                    mileage.map((item, index) => {
                      return (
                        <>
                          <option key={item} value={item}> {item} </option>

                        </>
                      )
                    })

                  }

                </select>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Doors</label>
                    <select value={vehicleDetails.doors} name="doors" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                      <option value=""> Select no. of doors </option>
                      <option value="2">  2 Doors  </option>
                      <option value="3">  3 Doors  </option>
                      <option value="4">  4 Doors  </option>
                      <option value="5">  5 Doors  </option>
                    </select>
                  </div>
                </div>
                <div className="col-6">
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Seats</label>
                    <select value={vehicleDetails.seats} name="seats" onChange={(e) => { handlChange(e); }} autoComplete="off" >
                      <option value=""> Select no. of Seats  </option>
                      <option value="2"> 2 Seats </option>
                      <option value="3"> 3 Seats </option>
                      <option value="4"> 4 Seats </option>
                      <option value="5"> 5 Seats </option>
                      <option value="6"> 6 Seats </option>
                      <option value="7"> 7 Seats </option>
                      <option value="8"> 8 Seats </option>
                      <option value="9"> 9 Seats </option>
                      <option value="10"> 10 Seats </option>
                    </select>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div className="col-12">
            <button className={`${styles.sendBtn}`} disabled={!isDisabled} onClick={(e) => { handleClick(e) }}>
              SAVE
            </button>
          </div>
        </div>
      </form>
    </div>
  )
}
export default VehicleDetail