import { useContext, useEffect, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { car_insurance_list_service, car_insurance_service, select_insurance_policy_service } from "../../../helpers/modify_car_details_service"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import SuccessResponseModal from "../succefully_modal"

const Insurance = ({ setOpen }) => {
    const [id, setId] = useState("")
    const [policytype, setpolicytype] = useState("")
    const [provider, setProvider] = useState("")

    const [companyList, setList] = useState([])

    const { newCarData } = useContext(IdContext)
    const handleChangeId = (e) => {
        setId(e.target.value);
    }
    const selectChangeId = (e) => {
        setpolicytype(e.target.value);
    }
    useEffect(() => {
        insurance_details()
    }, [])
    const insurance_details = async () => {
        let carId = newCarData.id
        const response = await car_insurance_service(carId)
        if (response.code == 200) {
            const res2 = await car_insurance_list_service()
            if (res2.code == 200) {
                setList(res2.company_list)
            }
            setId(response.insurance.is_Insured)
            setProvider(response.insurance.provider)
            setpolicytype(response.insurance.policyType)
        }
    }
    const Continue = async (e) => {
        e.preventDefault()
        let carId = newCarData.id
        const response = await select_insurance_policy_service(carId, provider, id, policytype)
        console.log(response)
        if (response.code == 200) {
            global.modal = true
            window.location.reload()
            setOpen(false)
        }
    }
    return (
        <form>
            <div className="row">
                <div className="col-12">
                    <h5>INSURANCE</h5>
                </div>
                <div className="col-12">
                    <div className={styles.addCarSec}>
                        <div className={styles.insuranceSec}>
                            <h6>You must have a comprehensive insurance policy.</h6>
                            <p>By law, all vehicles within Ejaro's community must have a comprehensive insurance policy to cover both vehicle owner and renter.
                                Choose our specially designed policy and we got your back !</p>

                            <div className={styles.detail}>
                                <div className={`form-group px-0 ${styles.customeFormGroup} mb-3`}>
                                    <label>Is your car insured?</label>
                                    <select autoComplete="off" value={id} onChange={(e) => { handleChangeId(e) }} >
                                        <option value=""  > Please Select </option>
                                        <option value="true"> Yes </option>
                                        <option value="false"> No </option>
                                    </select>
                                </div>
                                {id == "true" && <>
                                    <p>Choose your current insurance provider from our list of register companies </p>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Who is your insurance provider?</label>
                                        <select autoComplete="off" value={provider} onChange={(e) => { setProvider(e.target.value) }}>
                                            <option value=""> Select Provider </option>
                                            {
                                                companyList?.map((item, index) => {
                                                    return (
                                                        <>
                                                            <option value={item.name} id={item.id}> {item.name} </option>
                                                        </>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Policy type</label>
                                        <select autoComplete="off" value={policytype} onChange={(e) => { selectChangeId(e) }} >
                                            <option value=""> Select Provider </option>
                                            <option value="Comprehensive policy"> Comprehensive policy </option>
                                            <option value="Third party liability (TPL)">Third party liability (TPL) </option>
                                        </select>
                                    </div>
                                </>}
                                {(id == "false" || (id == "true" && policytype)) && <>

                                    <div className={styles.optionSec}>
                                        <p className="mb-0 mt-3">
                                            <span>(Recommended)</span>
                                            <span class="float-right">Explore
                                                <img src="/images/others/icon/help/about_icon.png" alt="" style={{ maxWidth: "13px", marginLeft: "5px", marginTop: "-3px" }} />

                                            </span>
                                        </p>
                                        <h4 className="mt-0"> Ejaro's exclusive policy </h4>
                                        <p>
                                            <span class="clearfix" style={{ fontFamily: "NeoSansRegular" }}>Ejaro's exclusive comprehensive peer-to-peer <br /> insurance policy in partnership with Medgulf
                                                <span class="float-right">
                                                    <img src="/images/others/icon/insurance/recommended.png" alt="" />
                                                </span>
                                            </span>
                                        </p>
                                        <div className="col-12 px-0">
                                            <button className={`${styles.sendBtn}`} onClick={(e) => { Continue(e) }}>
                                                Buy Policy
                                            </button>
                                            <button className={`${styles.backBtn}`} onClick={(e) => { Continue(e) }} >
                                                CONTINUE WITHOUT EJARO POLICY
                                            </button>
                                        </div>
                                    </div>
                                </>}

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </form>
    )
}
export default Insurance