import { useEffect, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { Location_key } from "../../../helpers/keys"
import { getAddressFromLatLong, placeAutoComplete, placeDetails } from "./geolocation"
import SimpleMap from "./Map/contact_us_map"
import ProgressSection from "./progress"

const PickUpLocation = ({counter, location, setLocation, setCounter }) => {
    const [isStatus, setStaus] = useState(false)
    const [locationStatus, setLocationStatus] = useState(false)
    const [suggestion, setSuggestion] = useState([])
    const [isDisabled, setDisabled] = useState(false)


    const permission = async(term) => {

        const location = window.navigator && window.navigator.geolocation
    
        if (location) {    

          location.getCurrentPosition(async(position) => {
            setLocation((prev) => {
                return {
                    ...prev,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            })
            let payload = {
                lat : position.coords.latitude,
                lng : position.coords.longitude,
                key :Location_key
            }

            const currentAddress =  await getAddressFromLatLong(payload);
            getCurrentAddress(currentAddress)
            currentAddress.formatted_address!=='' && setDisabled(true)

    
    
          }, (error) => {
            setLocation((prev) => { return { ...prev, latitude: 'err-latitude', longitude: 'err-longitude' } })
    
          })
        }
    
      }
      useEffect(async()=>{
        let payload = {
            lat : location.latitude,
            lng : location.longitude,
            key :Location_key
        }
          permission()
          const currentAddress =  await getAddressFromLatLong(payload);
          getCurrentAddress(currentAddress)
      },[location.latitude])
     
     
     

    const isDisabledChecked = () => {
        if (isStatus === false) {
            setStaus(true)
        }
        else {
            setStaus(false)

        }
    }
    const handleCounter = (e) => {
        e.preventDefault()
        setCounter(1)
    }
    const handleAddress = async (e) => {
        let value = e.target.value
        setLocation((prev) => {
            return {
                ...prev,
                streetAddress: value,
            }
        })
        setLocationStatus(false)
        let payload = {
            input: value,
            location: {
                lat: "28.6508353", lng: "77.267595"
            },
            key: Location_key,
            radius: '100'
        }
       

        if (value.length >= 5) {
            let getValue = await placeAutoComplete(payload)
            setSuggestion(getValue)
        }

    }
    const getCurrentAddress = async (currentAddress) => {
        setLocation((prev) => {
            return {
                ...prev,
                streetAddress: currentAddress.formatted_address,
            }
        })
        
        let type = currentAddress.address_components?.map(item => {
            switch (item.types[0]) {
                case "country":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            country: item.long_name,
                        }
                    })
                    break;
                case "postal_code":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            zip: item.long_name,
                        }
                    })
                    break;
                case "administrative_area_level_1":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            state: item.long_name,
                        }
                    })
                    break;
                case "locality":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            city: item.long_name,
                        }
                    })
                    break;
            }
        })
    }

    const onClickItem = async (item) => {
        setSuggestion([])
        setLocation((prev) => {
            return {
                ...prev,
                streetAddress: item.description,
            }
        })
        setLocationStatus(true)
        let payload = {
            key: Location_key,
            place_id: item.place_id
        }
        let getLATLong = await placeDetails(payload)
        let type = getLATLong.address_components.map(item => {
            switch (item.types[0]) {
                case "country":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            country: item.long_name,
                        }
                    })
                    break;
                case "postal_code":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            zip: item.long_name,
                        }
                    })
                    break;
                case "administrative_area_level_1":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            state: item.long_name,
                        }
                    })
                    break;
                case "locality":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            city: item.long_name,
                        }
                    })
                    break;
            }
        })
        localStorage.Defaultlat = getLATLong.geometry.location.lat
        localStorage.Defaultlng = getLATLong.geometry.location.lng
        setLocation((prev) => {
            return {
                ...prev,
                latitude: getLATLong.geometry.location.lat,
                longitude: getLATLong.geometry.location.lng
            }
        })

    };
    return (
        <div>
            <ProgressSection step={counter} />
            <form>
                <div className="row">
                    <div className="col-12">
                        <h5>Pick-up & Return Location</h5>
                        <p> Pick up & return at your location is included in the rental price for no additional fee </p>
                    </div>
                    <div className="col-12">
                        <div className={styles.addCarSec}>
                            <div className={styles.carBanner}>
                                <img src="/images/others/icon/help/image_three.png" />
                            </div>
                            <div className="row">
                                <div className="col-10">
                                    <p className={styles.editPara}> {location.streetAddress} </p></div>
                                <div className="col-2">
                                    <span className={styles.editButton} onClick={(e) => { isDisabledChecked(e) }}>
                                        {isStatus == false ? "Edit" : "Done"}
                                    </span>
                                </div>
                            </div>
                            {isStatus == false && <>
                                <div className={styles.mapSec}>
                                    <SimpleMap mapClass={styles.mapDiv} setLocation={setLocation}location={location}/>

                                </div>
                            </>}
                            {isStatus == true && <>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Address</label>
                                    <input type="text" id="" name="streetAddress" onChange={(e) => { handleAddress(e) }}
                                        placeholder="address" value={location.streetAddress} autoComplete="off"
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                    {location.streetAddress?.length > 1 && !locationStatus && (
                                        <ul className={suggestion.length === 0 ? "" : styles.list} >
                                            {suggestion && suggestion.map(item => (
                                                <li onClick={() => onClickItem(item)} >{item.description}</li>

                                            ))}

                                        </ul>
                                    )}
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>City</label>
                                            <input type="text" id="" name="idNumber"
                                                placeholder="city" value={location.city} autoComplete="off"
                                                className={`form-control ${styles.stepbarInput}`}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>State</label>
                                            <input type="text" id="" name="idNumber"
                                                placeholder="state" value={location.state} autoComplete="off"
                                                className={`form-control ${styles.stepbarInput}`}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>Country</label>
                                            <input type="text" id="" name="country"
                                                placeholder="Country" value={location.country} autoComplete="off"
                                                className={`form-control ${styles.stepbarInput}`}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>Zip Code</label>
                                            <input type="text" id="" name="zip"
                                                placeholder="Code" value={location.zip} autoComplete="off"
                                                className={`form-control ${styles.stepbarInput}`}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </>}
                        </div>
                    </div>
                    <div className="col-12">
                        <button onClick={(e) => { handleCounter(e) }} disabled={!isDisabled} className={`${styles.sendBtn}`}>
                            CONFIRM LOCATION
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}
export default PickUpLocation