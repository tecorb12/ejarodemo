import { useContext, useState } from "react"
import styles from "../../../../styles/customeModal.module.scss"
import { duplicate_car_service, update_car_registration_service, yakeen_new_car_service } from "../../../helpers/new_vehicle_service"
import { IdContext } from "../../../MyContext/IdDetailsContext"
import SuccessResponseModal from "../succefully_modal"
import ProgressSection from "./progress"

const replaceEnglishToArabic = (s) => {
  let translate = {
    "أ": "A",
    "ب": "B",
    "ح": "J",
    "د": "D",
    "ر": "R",
    "س": "S",
    "ص": "X",
    "ط": "T",
    "ه": "E",
    "ع": "P",
    "ق": "G",
    "ك": "K",
    "ل": "L",
    "م": "Z",
    "ن": "N",
    "هـ": "H",
    "و": "U",
    "ى": "V",
    "ي": "V"   // probably more to come
  };
  return (s.replace(s, function (match) {
    return translate[match];
  }));
}
const VehicleVerification = ({ counter, vehicleYear, setyear, vehicleData, companyList, setCounter, setLcPlateModal, setLcPlateMessage }) => {
  const [ownership, setOwnership] = useState("")
  const [companyName, setCName] = useState("")
  const [sequenceNo, setsequenceNo] = useState("")
  const [isDisabled, setDisabled] = useState(false)
  const { newCarData, setData } = useContext(IdContext)
  const [message, setMessage] = useState("")
  const [imageUrl, setUrl] = useState(null)
  const [successModal, setSuccessresponse] = useState(false)


  // <------------Onchange Image Upload ---------->
  const photoUpload = e => {
    e.preventDefault();
    let type = e.target.files[0].type
    if (e.target.files && e.target.files[0]) {

      setUrl(
        URL.createObjectURL(e.target.files[0]),
      );
    }
    else {
      setImg(
        e.target.files[0]
      )
    }

  }
  const handleChangeId = (e) => {
    setOwnership(e.target.value);
    setDisabled(false)
  }
  let vehicleYears = vehicleData.years
  const DisabledChecked = () => {
    if (sequenceNo && vehicleYear && ownership && imageUrl !== null) {
      if (ownership == "On Lease" && companyName) {
        setDisabled(true)

      }
      else {
        setDisabled(true)

      }
    }
    else {
      setDisabled(false)
    }
  }
  const handleChange = (e) => {
    let Id = e.target.value;

    if (!Number(Id)) {
      return;
    }

    setsequenceNo(Id)
  }


  const handleClick = async (e) => {
    e.preventDefault()
    let carId = newCarData.id
    e.preventDefault()
    setDisabled(true)
    const response = await duplicate_car_service(carId, sequenceNo)
    if (response.code == 404) {
      let res2 = await yakeen_new_car_service(sequenceNo, vehicleYear.value)
      let one = replaceEnglishToArabic(res2.result?.plate_text1 || '')
      let two = replaceEnglishToArabic(res2.result?.plate_text2 || '')
      let three = replaceEnglishToArabic(res2.result?.plate_text3 || '')
      let lcPlateNumber = res2.result?.plate_number + " " + one + two + three;
      let plate_text1 = res2.result?.plate_text1
      let plate_text2 = res2.result?.plate_text2
      let plate_text3 = res2.result?.plate_text3
      let vehicle_maker_code = res2.result?.vehicle_maker_code
      let vehicle_model_code = res2.result?.vehicle_model_code
      let chassis_number = res2.result?.chassis_number

      if (res2.code == 200) {
        let updateCarReg = await update_car_registration_service(plate_text1, plate_text2, plate_text3, carId, chassis_number, lcPlateNumber, ownership, imageUrl, sequenceNo, vehicle_maker_code, vehicle_model_code, vehicleYear.id)

        if (updateCarReg.code == 200) {
          setData(updateCarReg.result)
          setCounter(3)
          setLcPlateModal(true)
          setLcPlateMessage(updateCarReg.result.lcPlateNumber)
        }
      }
      else if (res2.code == 422) {
        setSuccessresponse(true)
        setMessage(res2.message)

      }

    }
    else if (response.code == 200) {
      setSuccessresponse(true)
      setMessage(response.message)
    }




  }
  const handleModelYear = (e) => {
    let index = e.target.selectedIndex
    const optionElement = e.target.childNodes[index];
    const optionElementId = optionElement.getAttribute('id');

    setyear((prev) => { return { ...prev, id: optionElementId, value: e.target.value } })

  }

  return (
    <div>
      <ProgressSection step={counter} />
      <form>
        <div className="row">
          <div className="col-12">
            <h5>Vehicle Verification</h5>
          </div>
          <div className="col-12">
            <div className={styles.addCarSec}>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Vehicle sequence number</label>
                <input type="text" id="" name="sequenceNo" onChange={(e) => { handleChange(e); DisabledChecked() }}
                  placeholder="Sequence No" value={sequenceNo} autoComplete="off"
                  className={`form-control ${styles.stepbarInput}`}
                />
              </div>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Vehicle Model Year</label>

                <select autoComplete="off" value={vehicleYear.value} id={vehicleYear.id} onChange={(e) => { handleModelYear(e); DisabledChecked() }}>
                  <option value=" " id="" > Vehicle Model Year  </option>
                  {
                    vehicleYears.map((item, index) => {
                      return (
                        <>
                          <option id={item.id} value={item.title}> {item.title} </option>


                        </>
                      )
                    })

                  }
                </select>
              </div>
              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label>Ownership</label>
                <select autoComplete="off" value={ownership} onChange={(e) => { handleChangeId(e); DisabledChecked() }}>
                  <option value=""> Select ownership </option>
                  <option value="Fully Owned"> Fully Owned </option>
                  <option value="On Lease"> On Lease </option>
                </select>
              </div>
              {ownership == "On Lease" && <>
                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                  <label>Finance Company</label>
                  <select value={companyName} onChange={(e) => { setCName(e.target.value); DisabledChecked() }} autoComplete="off" >
                    <option value="" > Select Finance Company </option>
                    {
                      companyList.map((item, index) => {
                        return (
                          <>
                            <option key={item.id} value={item.name}> {item.name} </option>

                          </>
                        )
                      })

                    }

                  </select>
                </div>
              </>}

              <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                <label> Upload Vehicle Registration card</label>
                <div className={styles.fileUpload}>
                  <h6>Vehicle Registration card</h6>

                  <label className={styles.imgUploadArea} htmlFor="photo" type="button"
                  >
                    <span className={styles.spanChange} >{imageUrl === null ? "Upload" : "change"}</span>
                    <img src="/images/others/icon/edit_profile.png"
                      alt=""
                      style={{ height: "18px", width: "18px" }}
                    />
                    <div className={`${styles.photoSec}`}   >
                      <img htmlFor={`photo`} src={imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl} />
                    </div>
                    <input type="file" id="photo" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" onChange={(e) => { photoUpload(e) }} />

                  </label>
                </div>
              </div>
            </div>

          </div>

          <div className="col-12">
            <button className={`${styles.sendBtn}`} onClick={(e) => { handleClick(e) }} disabled={!isDisabled}>
              SAVE
            </button>
          </div>
        </div>
      </form>
      <SuccessResponseModal message={message} open={successModal} setSuccessresponse={setSuccessresponse} />

    </div>
  )
}
export default VehicleVerification