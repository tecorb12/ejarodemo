import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div style={{position: 'absolute', transform: 'translate(-50%, -100%)'}} >
    
    <img alt="" src="/favicon.png" style={{width:"26px", height:"40px"}}  />
    </div>;

const SimpleMap=({setLocation,location,mapClass})=> {
   
    
    let defaultProps = {
        center: {
            lat:  28.6260606,
            lng: 77.3668853
        },
        zoom: 12
    };
   
    const handleApiLoaded = (map, maps) => {
      };
     const changeMarkerPosition =(e)=>{
         console.log(e)
         setLocation((prev) => {
            return {
                ...prev,
                latitude: e.lat,
                longitude:e.lng
            }
        })
      }
    
     const onChildMouseMove=(hoverKey, childProps, mouse)=>{
        console.log('move mouse',mouse)
        // Change item data with new coordinates
        // you need set here own store and update function
        // this.setState({
        //   lat: mouse.lat,
        //   lng: mouse.lng
        // }, () => this.props.updateLatLng(this.state.lat, this.state.lng))
      }
  
      
        const style = {
            overflow: "hidden",
            height: "455px",
            width: "100%",
            marginTop: "21px"
        }

        const style_two = {
            height: "100%",width: "100%",position: "relative",overflow: "hidden"
        }
        const style_three = {
            height: "100%", width: "100%", position: "absolute", top: "0px", left: "0px", backgroundColor: "rgb(229, 227, 223)"
        }
       
        return (
            // Important! Always set the container height explicitly

            <div className={`sec2map ${mapClass}` } style={style}>

                <div style={style_two}>
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: "AIzaSyBE5Z9JilAE4jJeOftQEkQnwqunbvCu3BU&libraries=places" , language:"en" }}
                        defaultCenter={defaultProps.center}
                        defaultZoom={defaultProps.zoom}
                        yesIWantToUseGoogleMapApiInternals= {true}
                        onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
                        style={style_three}
                        onClick={(e)=>{changeMarkerPosition(e)}}
                        onChildMouseMove={(e)=>{onChildMouseMove(e)}}
                    >
                        <AnyReactComponent
                            lat={location.latitude}
                            lng={location.longitude}
                            text="My"
                            hover="Click me!!"
                        />
                    </GoogleMapReact>
                </div>
            </div>
        );
}
export default SimpleMap;