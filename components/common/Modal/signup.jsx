
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import HijriDatePicker from "../../Auth/demo/app";
import moment from 'moment';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { duplicate_user_service, verify_iqamaNumber_service, verify_soudiNumber_service, verify_visaInfo_service } from "../../helpers/auth_services";
import SignUpStepTwo from "./signupStep2";


const ImgUpload = ({ onChange, src, id }) =>
  <label htmlFor={`${id}photo`} className="custom-file-upload fas" style={{ width: "100%" }}>
    <div className={`${styles.photoSec}`}   >
      <img htmlFor={`${id}photo`} src={src} />
    </div>
    <input id={`${id}photo`} type="file" onChange={onChange} style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" />
  </label>
const SignUp = ({ className, style, text, text1, imagePreviewUrl, img }) => {

  const [open, setOpen] = useState(false)
  const [selectedDate, setDate] = useState("")
  const [id, setId] = useState("saudi")
  const [idNumber, setNumber] = useState("")
  const [message, setMessage] = useState("")
  const [nationality, setNationality] = useState("United Arab Emirates")
  const [visaNumber, setVisaNo] = useState("")
  const [passportNo, setPassportNo] = useState("")
  const [isOpen2, setOpenModalTwo] = useState(false)
  const [isDisable, setDisabled] = useState(false)
  const [user, setUser] = useState({
    fname: "",
    lname: ""
  })
  const [isImg, setImg] = useState("")
  const [imageUrl, setUrl] = useState(null)
  const [visaPic, setVisaPic] = useState("")
  const [visaUrl, setvisaUrl] = useState(null)

  const [isError, setError] = useState({
    date_status: false
  })
  const openModal = (e) => {
    e.preventDefault()
    setOpen(true)
  }
  const closeModal = (e) => {
    e.preventDefault()
    setOpen(false)
    setImg("")
    setUrl("")
    setId("saudi")
    setDisabled(false)
    setDate("")
    setMessage("")
    setNumber("")
    setUser((prev) => { return { ...prev, fname: "", lname: "" } })

  }
  const handleChange = (e) => {
    let Id = e.target.value;

    if (!Number(Id)) {
      return;
    }

    setNumber(Id)
  }
  const Disabled = () => {
    if (id == "gcc") {
      if (nationality != '' && imageUrl !== null && idNumber != '' && user.fname != '' && user.lname != '' && isError.date_status == true) {
        setDisabled(true)
      }
      else {
        setDisabled(false)

      }

    }
    else if (id == "saudi") {
      console.log(id, idNumber !== '')
      if (idNumber != '' && selectedDate != '') {
        setDisabled(true)
      }
      else {
        setDisabled(false)

      }
    }
    else if (id == "iqama") {
      if (idNumber != '' && selectedDate != '') {
        setDisabled(true)
      }
      else {
        setDisabled(false)
      }
    }
    else if (id == "tourist") {
      if (passportNo != '' && visaNumber != '' && imageUrl !== null && visaUrl != null) {
        setDisabled(true)
      }
      else {
        setDisabled(false)
      }
    }

  }

  const handleDobResponse = (dob) => {

    setDate(dob)
    setError((prev) => { return { ...prev, date_status: true } })
  }
  const handleClick = async event => {
    event.preventDefault()

    const response = await duplicate_user_service(idNumber ? idNumber : passportNo)
    if (response.code == 404 && id == "iqama") {
      const res = await verify_iqamaNumber_service(selectedDate, idNumber)
      if (res.code == 422) {
        setMessage(res.message)
      }
    }
    else if (response.code == 404 && id == "saudi") {
    let soudiFormateDate = selectedDate.substring(3);
      const res = await verify_soudiNumber_service(soudiFormateDate, idNumber)
      if (res.code == 422) {
        setMessage(res.message)
      }
      else if (res.code == 200) {
        setUser((prev) => { return { ...prev, fname: res.result.english_first_name, lname: res.result.english_last_name } })
        setDate(res.result.date_of_birth_h)
        setOpen(false)
        setOpenModalTwo(true)
      }
    }
    else if (response.code == 404 && id == "tourist") {
      const res = await verify_visaInfo_service(passportNo, visaNumber)
      if (res.code == 422) {
        setMessage("The Visa number you have entered does not exist in the National Information Center (NIC) records")
      }
    }
    else if (response.code == 404 && id == "gcc") {
      setOpen(false)
      setOpenModalTwo(true)
    }
  }
  const handleChangeId = (e) => {
    setId(e.target.value);
    setMessage("");
    setDisabled(false);
    setDate("")
    setNumber("")
    setImg("")
    setUrl("")
    setVisaPic("")
    setvisaUrl("")
    setError((prev) => { return { ...prev, date_status: false } })

  }
  // <------------Onchange Image Upload ---------->
  const photoUpload = e => {
    e.preventDefault();
    let type = e.target.files[0].type
    if (e.target.files && e.target.files[0]) {

      setUrl(
        URL.createObjectURL(e.target.files[0]),
        // imagePreviewUrl: ImagePath.Preview.pdf,
      );
    }
    else {
      setImg(
        e.target.files[0]
      )
    }

  }
  const idPassportUpload = e => {
    e.preventDefault();
    let type = e.target.files[0].type
    if (e.target.files && e.target.files[0]) {

      setUrl(
        URL.createObjectURL(e.target.files[0]),
        // imagePreviewUrl: ImagePath.Preview.pdf,
      );
    }
    else {
      setImg(
        e.target.files[0]
      )
    }

  }
  const visaUpload = e => {
    e.preventDefault();
    let type = e.target.files[0].type
    if (e.target.files && e.target.files[0]) {

      setvisaUrl(
        URL.createObjectURL(e.target.files[0]),
        // imagePreviewUrl: ImagePath.Preview.pdf,
      );
    }
    else {
      setVisaPic(
        e.target.files[0]
      )
    }

  }

  return (
    <Fragment>
      <button onClick={openModal} className={`${style.sendBtn1} ${className}`}>
        {text}
        <span className={style.floatRight}> {text1}
        </span>
        </button>
      <ReactModal
        className={`${styles.customeModal} ${styles.customeSignUpModal}`}
        isOpen={open}
        onRequestClose={closeModal}
        style={
          { overflowY: "scroll" }}
      >
        <div className={styles.dialogBox}>
        <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
              <span>x</span>
            </button>
          <div className={styles.registrationSec}>
          
            <form>
              <div>
                <h5>Registration</h5>
                <p>Please enter your details exactly as it appears on your ID</p>
              </div>
              <div class="row">
                <div className="col-md-12">
                  <ul className={styles.progressBar}>
                    <li className={styles.Active}></li>
                    <li></li>
                    <li></li>
                  </ul>
                </div>
              </div>
              <h4 style={{ color: "#b2020f" }}>{message}</h4>
              {/* -------------------step one--------- */}
              <div id="step1">
                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                  <label>ID Type</label>
                  <select value={id} onChange={(e) => { handleChangeId(e) }}>
                    <option value="saudi"> Saudi Id   </option>
                    <option value="iqama"> Iqama </option>
                    <option value="gcc"> GCC National </option>
                    <option value="tourist"> Passport </option>

                  </select>
                </div>
                {/* ---------------First Section------------------ */}
                {id == "saudi" && <> <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                  <label>Id Number</label>
                  <input type="text" id="contact" name="idNumber"
                    placeholder="Id Number" value={idNumber} autoComplete="off"
                    onChange={(e) => { handleChange(e); Disabled() }}
                    className={`form-control ${styles.stepbarInput}`}
                  />
                </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Date of Birth(Hijri)</label>
                    <HijriDatePicker
                      selectedDate={selectedDate}
                      setDate={(date) => { setDate(date); Disabled() }}
                    />
                  </div> </>}
                {/* ---------------Second Section------------------ */}
                {id == "iqama" && <> <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                  <label>Iqama Number</label>
                  <input type="text" id="contact" name="idNumber"
                    placeholder="Iqama Number" autoComplete="off" value={idNumber}
                    onChange={(e) => { handleChange(e); Disabled() }}
                    className={`form-control ${styles.stepbarInput}`}
                  />
                </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup} signUpDatePicker`}>
                    <label>Date of Birth</label>
                    <DatePicker
                      className="form-control"
                      name="dob"
                      selected={selectedDate ? new Date(selectedDate) : null}
                      dateFormat="dd/MM/yyyy"
                      dropdownMode="scroll"
                      showYearDropdown={true}
                      dateFormatCalendar="MMMM"
                      placeholderText="DOB"
                      yearDropdownItemNumber={25}
                      scrollableYearDropdown={true}
                      // locale={locale.locale}
                      onChange={date => { handleDobResponse(date); Disabled() }}
                      autoComplete="off"
                      maxDate={moment().subtract(18, 'years')._d}

                    />
                  </div> </>}
                {/* ---------------Third Section------------------ */}
                {id == "gcc" && <> <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                  <label>Nationality</label>
                  <select value={nationality} autoComplete="off" onChange={(e) => { setNationality(e.target.value); Disabled() }}>
                    <option value="saudi"> Select Country  </option>
                    <option value="Bahrain"> Bahrain </option>
                    <option value="Kuwait"> Kuwait </option>
                    <option value="Oman"> Oman </option>
                    <option value="Qatar"> Qatar </option>
                    <option value="United Arab Emirates"> United Arab Emirates </option>

                  </select>
                </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Id Number</label>
                    <input type="text" id="contact" name="idNumber"
                      placeholder="Id Number" value={idNumber} autoComplete="off"
                      onChange={(e) => { handleChange(e); Disabled() }}
                      className={`form-control ${styles.stepbarInput}`}
                    />
                  </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>First Name</label>
                    <input type="text" id="contact" name="fname"
                      placeholder="First Name" value={user.fname} autoComplete="off"
                      onChange={(e) => { setUser((prev) => { return { ...prev, fname: e.target.value } }); Disabled() }}
                      className={`form-control ${styles.stepbarInput}`}
                    />
                  </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Last Name</label>
                    <input type="text" id="contact" name="lname" value={user.lname}
                      onChange={(e) => { setUser((prev) => { return { ...prev, lname: e.target.value } }); Disabled() }}
                      placeholder="Last Name" autoComplete="off"
                      className={`form-control ${styles.stepbarInput}`}
                    />
                  </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Date of Birth</label>
                    <DatePicker
                      className="form-control"
                      name="dob"
                      selected={selectedDate ? new Date(selectedDate) : null}
                      dateFormat="MM/dd/yyyy"
                      dropdownMode="scroll"
                      showYearDropdown={true}
                      dateFormatCalendar="MMMM"
                      placeholderText="DOB"
                      yearDropdownItemNumber={15}
                      scrollableYearDropdown={true}
                      // locale={locale.locale}
                      onChange={date => { handleDobResponse(date); Disabled() }}
                      autoComplete="off"
                      maxDate={moment().subtract(18, 'years')._d}

                    />
                  </div>
                  <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                    <label>Id Photo</label>

                    <ImgUpload
                      id={1}
                      onChange={(e) => { photoUpload(e); Disabled() }}
                      src={imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl}
                    />
                  </div>
                </>
                }
                {/* ---------------Forth Section------------------ */}
                {id == "tourist" &&
                  <>
                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                      <label>Passport Number</label>
                      <input type="text" id="contact" name="passportNo"
                        placeholder="Passport Number" value={passportNo} autoComplete="off"
                        onChange={(e) => { setPassportNo(e.target.value); Disabled() }}
                        className={`form-control ${styles.stepbarInput}`}
                      />
                    </div>
                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                      <label>Saudi Entry Visa Number</label>
                      <input type="text" id="contact" name="visaNumber" autoComplete="off"
                        onChange={(e) => { setVisaNo(e.target.value); Disabled() }}
                        placeholder="Passport Number" value={visaNumber}
                        className={`form-control ${styles.stepbarInput}`}
                      />
                    </div>

                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                      <label>Passport Photo</label>
                      <ImgUpload
                        id={2}
                        onChange={(e) => { idPassportUpload(e); Disabled() }}
                        src={imageUrl === null ? "/images/others/icon/placeholder.png" : imageUrl}
                      />

                      <p>Please Upload a clear Photo of Your Passport</p>
                    </div>
                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                      <label>Saudi Entry Visa Photo</label>
                      <ImgUpload
                        onChange={(e) => { visaUpload(e); Disabled() }}
                        id={3}
                        src={visaUrl === null ? "/images/others/icon/placeholder.png" : visaUrl}
                      />
                      <p>Please Upload a clear Photo of Your Saudi Visa</p>
                    </div>
                    <div>
                    </div> </>}
                <div class="row">
                  <div class="col-12">
                    <button disabled={!isDisable} onClick={(e) => { handleClick(e) }} className={`${styles.sendBtn}`}>
                      Next
                    </button>
                  </div>
                </div>
              </div>

            </form>
          </div>
        </div>
      </ReactModal>

      {<SignUpStepTwo id={id} idNumber={idNumber}
        nationality={nationality} isOpen={isOpen2} fname={user.fname} setUser={setUser}
        lname={user.lname} dob={selectedDate} setDob={setDate} setOpenModalTwo={setOpenModalTwo} />}

      {/* id == "gcc"  &&  */}
    </Fragment>

  )
}

export default SignUp;