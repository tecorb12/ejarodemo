import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import { login_service } from "../../helpers/auth_services";
import { validateEmail, validatePassword } from "../validation";
import Forgot from "./forgot";
import SignUp from "./signup";


const Login = ({ className, style, text, src, loginstyle,close }) => {
    const [open, setOpen] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [isError, setError] = useState({
        emailErrorMsg: "", isDisabledEmail: false,
        passwordErrorMsg: "", isDisabledPassword: false,
        code: "", msg: "",
    })
    const [isDisabled, setDisabled] = useState(false)
    const openModal = (e) => {
        e.preventDefault()
        close
        setOpen(true)
    }
    const closeModal = () => {
        setOpen(false)
    }
    const validForm = (fieldName, Value) => {
        let EmailPhoneErrorMsg = isError.emailErrorMsg
        let isDisabledEmail_phone = isError.isDisabledEmail
        let PasswordErrorMsg = isError.passwordErrorMsg
        let isDisabledPassword = isError.isDisabledPassword
        switch (fieldName) {
            case "email":
                EmailPhoneErrorMsg = validateEmail(Value).error
                isDisabledEmail_phone = validateEmail(Value).isDisabled
                break;
            case "password":
                PasswordErrorMsg = validatePassword(Value).error
                isDisabledPassword = validatePassword(Value).isDisabled
                break;
            default:
                break;
        }
        setError((prev) => { return { ...prev, isDisabledEmail: isDisabledEmail_phone, isDisabledPassword: isDisabledPassword } })
    }
    const DisabledData = () => {
        setError((prev) => { return { ...prev, msg: "", code: "" } })
        if (isError.isDisabledEmail && isError.isDisabledPassword) {
            setDisabled(true)
        }
        else {
            setDisabled(false)
        }
    }
    const handleLogin = async (event) => {
        event.preventDefault()
        const response = await login_service(email, password)
        console.log(response)
        if (response.code == 404) {
            setError((prev) => { return { ...prev, code: response.code, msg: response.message } })
        }
        if (response.code == 401) {
            setError((prev) => { return { ...prev, code: response.code, msg: "Unauthorized access" } })
        }
    }
    return (
        <Fragment>
            <button onClick={openModal} className={`${className} ${loginstyle} `}>
                {/* <span className={style.floatRight}>
                </span> */}
                {text}
                <img src={src} />
            </button>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeLoginModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>

                    <div className={styles.loginSec}>

                        <div className="row mx-0">
                            <div className={`col-md-12 col-lg-12 ${styles.loginPadding}`}>
                                <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                                    <span>x</span>
                                </button>
                                <h3>Welcome back!</h3>
                                <h6>Login to your account</h6>

                                <form>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>

                                        <p style={{ color: "#b2020f" }}>{isError.code == 404 && isError.msg}</p>
                                        <p style={{ color: "#b2020f" }}>{isError.code == 401 && isError.msg}</p>

                                    </div>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Email Address</label>
                                        <input type="text" id="contact" name="email" value={email}
                                            onChange={(e) => { setEmail(e.target.value); validForm("email", e.target.value); DisabledData() }}
                                            placeholder="Enter email address"
                                            className={`form-control ${styles.stepbarInput}`}
                                        />
                                        <img src="/images/others/icon/email_icon.svg" style={{ width: "10px" }} />
                                    </div>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        <label>Password</label>
                                        <input type="password" id="contact" name="password" value={password} autoComplete="off"
                                            placeholder="Password" onChange={(e) => { setPassword(e.target.value); validForm("password", e.target.value); DisabledData() }}
                                            className={`form-control ${styles.stepbarInput}`}
                                        />
                                        <img src="/images/others/icon/password_icon.svg" style={{ width: "7px" }} />
                                    </div>
                                    <div className="text-right">
                                        <Forgot style={styles} close={setOpen}/>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button disabled={!isDisabled} className={styles.sendBtn} onClick={handleLogin}>
                                                Sign in
                                            </button>
                                        </div>
                                        <div className="col-12">
                                            <SignUp style={styles} text="Don't have an account?" text1="Register Now" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div className={styles.smallHide}>
                            <div className={styles.bannerSec}>
                                <div className={styles.detailText}>
                                    <h6> Share your car and earn
                                        <br></br>
                                        an aditional income with ejaro
                                    </h6>
                                </div>
                                <div className={styles.detailTextFooter}>
                                    <h6>A Trustworthy Community</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ReactModal>
        </Fragment>
    )
}
export default Login;
