
import { Fragment, useContext, useState } from "react";
import ReactModal from 'react-modal'
import styles from "../../../../styles/customeModal.module.scss"
import {update_extra_details_service } from "../../../helpers/modify_car_details_service";

const ExtradeatilModal = ({ style,carId, setSeats,financeCom,setTripDays,companyList,setFinance, setOwnership, setMileage, odometer, text, setDoors, id, fuel, selectedText, setFuelType, headingText, label, setTransmission }) => {

    const [open, setOpen] = useState(false)
    const[finance, setFinanceCompany] = useState({
        id:"",
        name:""
    })
  
   

    const openModal = (e) => {
        e.preventDefault()
        setOpen(true)

    }
    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
    }
   
    const handleFuelChange = (e) => {
        let index = e.target.selectedIndex
        const optionElement = e.target.childNodes[index];
        const optionElementId = optionElement.getAttribute('id');
        setFuelType((prev) => { return { ...prev, label: e.target.value, id: optionElementId } })
    }
    const handleFinanceChange=(e)=>{
        let index = e.target.selectedIndex
        const optionElement = e.target.childNodes[index];
        const optionElementId = optionElement.getAttribute('id');
        setFinanceCompany((prev) => { return { ...prev, name: e.target.value, id: optionElementId } })
    }

    return (
        <Fragment>
            <div className={style.formGroup} onClick={openModal}>
                <span className={`pl-0`} >
                    {text}
                </span>
                <span className={`${style.floatRight} float-right`}>
                    {selectedText}<img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>

            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                        <span>x</span>
                    </button>
                    <div className={styles.registrationSec}>
                        <form className="mt-0">
                            <div className="row">
                                <div className="col-12 text-center">
                                    <h5>{headingText}</h5>
                                </div>
                                <div className="col-12">
                                    <div className={styles.addCarSec}>
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>{label}</label>
                                            {text == "Transmission" && <>
                                                <select value={selectedText == "Automatic" ? "automatic" : "manual"}
                                                    onChange={(e) => { setTransmission(e.target.value) }}>
                                                    <option value="">Select Transmission</option>
                                                    <option value="automatic">Automatic</option>
                                                    <option value="manual">Manual</option>
                                                </select>
                                            </>}
                                            {text == "Fuel Type" && <>
                                                <select value={selectedText} id={id} onChange={(e) => { handleFuelChange(e) }} >
                                                    <option value="" disabled="">Select Fuel</option>
                                                    {
                                                        fuel?.map((item, index) => {
                                                            return (
                                                                <>
                                                                    <option value={item.label} id={item.id}> {item.label} </option>
                                                                </>
                                                            )
                                                        })
                                                    }


                                                </select>
                                            </>}
                                            {text == "Mileage" && <>
                                                <select value={selectedText} onChange={(e) => { setMileage(e.target.value) }}>
                                                    <option value="" disabled="">Select Mileage</option>
                                                    {
                                                        odometer?.map((item, index) => {
                                                            return (
                                                                <>
                                                                    <option key={item} value={item}> {item} </option>

                                                                </>
                                                            )
                                                        })

                                                    }


                                                </select>
                                            </>}
                                            {text == "Doors" && <>
                                                <select value={selectedText} onChange={(e) => { setDoors(e.target.value) }}>
                                                    <option value="" disabled="">Select Doors</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </>}
                                            {text == "Seats" && <>
                                                <select value={selectedText} onChange={(e) => { setSeats(e.target.value) }}>
                                                    <option value="" disabled="">Select Seats</option>
                                                    <option value="2">2 Seats</option>
                                                    <option value="3">3 Seats</option>
                                                    <option value="4">4 Seats</option>
                                                    <option value="5">5 Seats</option>
                                                    <option value="6">6 Seats</option>
                                                    <option value="7">7 Seats</option>
                                                    <option value="8">8 Seats</option>
                                                    <option value="9">9 Seats</option>
                                                    <option value="10">10 Seats</option>

                                                </select>
                                            </>}
                                            {text == "Ownership" && <>
                                                <select value={selectedText} onChange={(e)=>{setOwnership(e.target.value)}}>
                                                    <option value="" disabled="">Select Ownership</option>
                                                    <option value="Fully Owned">Fully Owned</option>
                                                    <option value="On Lease">On Lease</option>

                                                </select>
                                                {selectedText == "On Lease" && <>
                                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                        <label>Finance Company</label>
                                                        <select value={financeCom} id={finance.id} onChange={(e) => { setFinance(e.target.value); handleFinanceChange()  }} autoComplete="off" >
                                                            <option value="" > Select Finance Company </option>
                                                            {
                                                                companyList?.map((item, index) => {
                                                                    return (
                                                                        <>
                                                                            <option id={item.id} value={item.name}> {item.name} </option>

                                                                        </>
                                                                    )
                                                                })

                                                            }

                                                        </select>
                                                    </div>
                                                </>}
                                            </>}
                                            {text == "Advance notice before rental ?" && <>
                                                <select value={selectedText}
                                                 onChange={(e)=>{setTripDays((prev)=>{return{...prev,maxNoticeHours:e.target.value}})}}>
                                                    <option value="" disabled="">Select Duration</option>
                                                    <option value="1"> 1 hour </option>
                                                    <option value="2"> 2 hours </option>
                                                    <option value="3"> 3 hours </option>
                                                    <option value="6"> 6 hours </option>
                                                    <option value="12"> 12 hours </option>
                                                    <option value="24"> 1 day </option>
                                                    <option value="48"> 2 days </option>
                                                    <option value="72"> 3 days </option>
                                                    <option value="168"> 1 week </option>
                                                </select>
                                            </>}
                                            {text == "Shortest rental time" && <>
                                                <select value={selectedText}
                                                 onChange={(e)=>{setTripDays((prev)=>{return{...prev,min:e.target.value}})}}>
                                                    <option value="" disabled="">Select Duration</option>
                                                    <option value="1"> 1 day </option>
                                                    <option value="2"> 2 days </option>
                                                    <option value="3"> 3 days </option>
                                                    <option value="5"> 5 days </option>
                                                    <option value="7"> 1 week </option>
                                                    <option value="14"> 2 weeks </option>
                                                    <option value="30"> 1 month </option>

                                                </select>
                                            </>}
                                            {text == "Longest rental time" && <>
                                                <select value={selectedText}
                                                 onChange={(e)=>{setTripDays((prev)=>{return{...prev,max:e.target.value}})}}>
                                                    <option value="" disabled="">Select Duration</option>
                                                    <option value="1"> 1 day </option>
                                                    <option value="2"> 2 days </option>
                                                    <option value="3"> 3 days </option>
                                                    <option value="5"> 5 days </option>
                                                    <option value="7"> 1 week </option>
                                                    <option value="14"> 2 weeks </option>
                                                    <option value="30"> 1 month </option>
                                                </select>
                                            </>}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">
                                    <button onClick={closeModal} className={`${styles.sendBtn}`}>
                                        SAVE
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>
    )
}

export default ExtradeatilModal

