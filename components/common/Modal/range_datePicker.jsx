
import React, { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import { price_availablity_service } from "../../helpers/myCar_services";
import DatePicker from 'react-datepicker';
// import "react-datepicker/dist/react-datepicker.css";

import moment from 'moment';

const PriceCalendar = ({ carId, dateRange, setDateRange, setShowStartDate, setShowEndDate, style }) => {
    const [startDate, endDate] = dateRange;
    const [open, setOpen] = useState(false)
    const [date, setCalendarDate] = useState("")
    // const [minVal, setMinVal] = useState(0);
    // const [maxVal, setMaxVal] = useState(24);
    const [monthRelation, setMonth] = useState("")
    const openClick = (e) => {
        e.preventDefault()
        setOpen(true)
    }
    useEffect(() => {
        calendar_date_availability()
    }, [carId])
    const calendar_date_availability = async () => {
        const response = await price_availablity_service(carId)
        if (response.code == 200) {
            setCalendarDate(response.value)
        }
    }
    const closeModal = () => {
        setOpen(false)
    }
    const handleClick = (e) => {
        e.preventDefault()
        setOpen(false)
        setShowStartDate(moment(dateRange[0])?.format("MMMM DD"))
        setShowEndDate(moment(dateRange[1])?.format("MMMM DD"))

    }
    const handleMonthChange = (monthBeingViewed) => {
        const currentMonth = moment();
        if (monthBeingViewed.isSame(currentMonth, 'month')) {
            setMonth('current');
        } else if (monthBeingViewed.isAfter(currentMonth, 'month')) {
            setMonth('future');
        } else {
            setMonth('past');
        }
    }


    return (
        <Fragment>
            <div className={style.formGroup} onClick={(e) => { openClick(e) }}>
                <span className={`${style.floatRight} float-right`}>
                    Edit
                </span>
            </div>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal} ${styles.customeRangCalender}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>

                    <div className={styles.registrationSec}>
                        <form className="mt-0">
                            <div className="row">
                                <div className="col-12">
                                    <button onClick={closeModal} className="float-right close mr-0 mt-0 ">
                                        <span>x</span>
                                    </button></div>
                                <div className="col-12 rangCalender mt-4">
                                    <DatePicker
                                        selectsRange={true}
                                        startDate={startDate}
                                        endDate={endDate}
                                        monthsShown={2}
                                        minDate={new Date()}
                                        onMonthChange={handleMonthChange}
                                        inline
                                        onChange={(update) => {
                                            setDateRange(update);
                                        }}
                                        withPortal
                                    />


                                </div>
                            </div>
                            <div className={`row ${styles.rangInput} pt-4`}>
                                <div className="col-3">
                                    <label for="min">Pickup Date</label>
                                </div>
                                <div className="col-9">
                                    {/* <input type="range" name="pickup" min="0" max="11"/> */}
                                    <input
                                        id="min"
                                        className="min"
                                        // style={styles.min}
                                        name="min"
                                        type="range"
                                        step="1"
                                        // min={minVal}
                                        // max={maxVal}
                                        // value={minVal}
                                        onChange={(e) => {
                                            setMinVal(Number(e.target.value))
                                        }}

                                    />

                                </div>
                            </div>
                            <div className={`row ${styles.rangInput} pt-4`}>
                                <div className="col-3 ">
                                    <label for="return">Return Date</label>
                                </div>
                                <div className="col-9">
                                    <input type="range" name="return" min="0" max="11" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-4 ml-auto">
                                    <button onClick={handleClick} className={`${styles.sendBtn}`}>Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>





            </ReactModal>
        </Fragment>

    )
}

export default PriceCalendar;