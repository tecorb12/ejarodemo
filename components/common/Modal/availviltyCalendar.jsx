
import { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import 'react-phone-input-2/lib/style.css'
import { calendar_availability_service, selected_date_service } from "../../helpers/myCar_services";
import moment from 'moment'
import fake_json from '../../fake_json/price_two.json'
import DatePicker from 'react-datepicker';
import { addDays } from 'date-fns';
import subDays from 'date-fns/subDays'


const PriceCalendar = ({ carId, style }) => {
    const [open, setOpen] = useState(false)
    const [count, setCount] = useState(0)

    const [dateState, setDateState] = useState(new Date())
    const [array, setArray] = useState(fake_json.result)
    const [calendarDate, setCalendarDate] = useState([])
    const [status, setStatus] = useState("")
    const [emptyCalendarDate, setEmptyCaledarDate] = useState([])
    const [selectedDate, setDate] = useState("")
    let value = moment().add(count, 'month')
    const startDate = value.clone().startOf('month').startOf('week')
    const endDate = value.clone().endOf('month').endOf('week')
    const day = startDate.clone().subtract(1, "day")
    let emptyCalendar = []

    while (day.isBefore(endDate)) {
        emptyCalendar.push(Array(7).fill(0).map(() => day.add(1, "day").clone()))
    }
    const openClick = (e) => {
        e.preventDefault()
        setOpen(true)
    }
    useEffect(() => {
        calendar_date_availability()
    }, [])
    const calendar_date_availability = async () => {
        const response = await calendar_availability_service(carId)
        if (response.code == 200) {
            setCalendarDate(response.value)
            // demoArray()
        }
    }
    const closeModal = () => {
        setOpen(false)
    }

    const changeDate = (e) => {
        setDateState(e)
    }
    const LeftButton = () => {
        if (count > 0) {
            setCount((prev) => prev - 1)
        }
        else {
        }
    }
    const RightButton = () => {
        if (count <= 8) {
            setCount((prev) => prev + 1)
        }
        else {

        }
    }

    // const demoArray = () => {

    //     console.log("----", calendarDate);
    //     for (let i = 0; i < calendarDate.length; i++) {
    //         let isCatList = emptyCalendar?.map((item, i) => {
    //             return item.map((list) => {
    //                 return ({ ...list, currentStatus: calendarDate[i].status });
    //             })
    //         })
    //         setEmptyCaledarDate(isCatList)
    //     }




    // }

    let disabledDates=[]
    let dateCustomClasses=[]
  let bookedDisabled=  calendarDate.forEach(element => {

        if(element.status=="accepted"){
          dateCustomClasses.push({
            date:new Date(element.date ),
            classes:[styles.bookedDate]
          })
          disabledDates.push(new Date(element.date ))
        }
    })
    // console.log(dateCustomClasses)
    let unavailable = calendarDate.filter((item) => item.status == "unavailable")
    let avail = calendarDate.filter((item) => item.status == "available")
    let accepted = calendarDate.filter((item) => item.status == "accepted")
    let pending = calendarDate.filter((item) => item.status == "pending")



    let fil_unavail = unavailable.map((data) => {
        return new Date(data.date)
    })
    let fil_avail = avail.map((data) => {
        return new Date(data.date)
    })
    let fil_booked = accepted.map((data) => {
        return new Date(data.date)
    })
    let fil_pending = pending.map((data) => {
        return new Date(data.date)
    })
   


    let unavailable_arr = []
    let available_arr = []
    let booked_arr = []
    let pending_arr = []

    unavailable_arr.push(...fil_unavail)
    available_arr.push(...fil_avail)
    booked_arr.push(...fil_booked)
    pending_arr.push(...fil_pending)


    const highlightWithRanges = [

        {
            "react-datepicker__day--highlighted-custom-1": unavailable_arr
        },
        {
            "react-datepicker__day--highlighted-custom-2": available_arr
        },
        {
            "react-datepicker__day--highlighted-custom-3": booked_arr
        },
        {
            "react-datepicker__day--highlighted-custom-4": pending_arr
        }
    ];
    const handleDobResponse = async (date) => {
        setDate(date)
        const response = await selected_date_service(carId, date+1)
        if(response.code==200){
            date =  "react-datepicker__day--highlighted-custom-1"
            calendar_date_availability()
        }

    }
    return (
        <Fragment>
            <div className={style.formGroup} onClick={openClick}>
                <span>
                    <img src="/images/others/icon/update_your_calender.svg" alt="" />
                    Update your calender
                </span>
                <span className={`${style.floatRight} float-right`}>
                    <img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal} ${styles.customeCalender}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>

                    <div className={styles.registrationSec}>
                        <form className="mt-0">
                            <div className="row">
                                <div className="col-12">
                                    <button onClick={closeModal} className="float-right close mr-0 mt-0 ">
                                        <span>x</span>
                                    </button></div>
                                <div className="col-12">
                                    <div className={styles.addCarSec}>
                                        <div className={`${styles.Calendar} extradetailCalender`}>
                                            <DatePicker
                                                className="form-control"
                                                name="dob"
                                                selected={selectedDate ? new Date(selectedDate) : null}
                                                dateFormat="MM/dd/yyyy"
                                                dropdownMode="scroll"
                                                // showYearDropdown={true}
                                                // dateFormatCalendar="MMMM"
                                                // yearDropdownItemNumber={15}
                                                scrollableYearDropdown={true}
                                                onChange={date => { handleDobResponse(date); }}
                                                autoComplete="off"
                                                inline
                                                disabled={[bookedDisabled]}
                                                dayClassName={date => date.getTime() === new Date('04/25/2022').getTime() ? 'disabled-date' : undefined}
                                                highlightDates={highlightWithRanges}
                                                
                                            />

                                            <div className={styles.calendarInfo}>
                                                <p >Select a date to modify your availability</p>
                                                <ul className="clear-fix">
                                                    <li >
                                                        <div className={`${styles.box} ${styles.available}`}>
                                                        </div>
                                                        <p >Available</p>
                                                    </li>
                                                    <li >
                                                        <div className={`${styles.box} ${styles.not}`}></div>
                                                        <p >Not in use</p>
                                                    </li>
                                                    <li >
                                                        <div className={`${styles.box} ${styles.booked}`}></div>
                                                        <p >Booked</p>
                                                    </li>
                                                    <li >
                                                        <div className={`${styles.box} ${styles.pending}`}></div>
                                                        <p >Pending</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>





            </ReactModal>
        </Fragment>

    )
}

export default PriceCalendar;