import { Fragment, useEffect, useState } from "react"
import ReactModal from "react-modal"
import styles from "../../../../styles/customeModal.module.scss"
import { car_delivery_location_list_service, car_delivery_location_service, car_estimate_price_service, delete_address_service, delivery_charge_service } from "../../../helpers/booking_services.jsx"
import { Location_key } from "../../../helpers/keys"
import { getAddressFromLatLong } from "../AddCarModal/geolocation"
import SimpleMap from "../AddCarModal/Map/contact_us_map"
import SuccessResponseModal from "../succefully_modal"
const PickUpLocationModal = ({ className, style, carId, place, text, type,estimatePric,setEstimatePrice }) => {
    const [open, setOpen] = useState(false)

    const [isStatus, setStaus] = useState(false)
    const [isDivSatus, setDivSatus] = useState(false)
    const [isDelivery, setDelivery] = useState(true)
    const [pickUpDetail, setPickupAddress] = useState("")
    const [deliveryLocation, setDeliveryLocation] = useState([])
    const [hiddenData, setHiddenData] = useState(false)
    const [editButton, setEditData] = useState(false)
    const[isSuccess,setSuccessModal ] = useState(false)
let userID = typeof window !== "undefined" && localStorage.user
    const [location, setLocation] = useState({
        latitude: "",
        longitude: "",
        country: "",
        zip: "",
        state: "",
        city: "",
        streetAddress: ""
    })

    useEffect(async () => {
        let payload = {
            lat: location.latitude,
            lng: location.longitude,
            key: Location_key
        }
        const currentAddress = await getAddressFromLatLong(payload);
        getCurrentAddress(currentAddress)
    }, [location.latitude])
    const getCurrentAddress = async (currentAddress) => {
        setLocation((prev) => {
            return {
                ...prev,
                streetAddress: currentAddress.formatted_address,
            }
        })

        let type = currentAddress.address_components?.map(item => {
            switch (item.types[0]) {
                case "country":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            country: item.long_name,
                        }
                    })
                    break;
                case "postal_code":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            zip: item.long_name,
                        }
                    })
                    break;
                case "administrative_area_level_1":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            state: item.long_name,
                        }
                    })
                    break;
                case "locality":
                    setLocation((prev) => {
                        return {
                            ...prev,
                            city: item.long_name,
                        }
                    })
                    break;
            }
        })
    }
    const isEditChecked = (e) => {
        e.preventDefault()
        if (isDivSatus === false) {
            setDivSatus(true)
        }
        else {
            setDivSatus(false)

        }
    }
    const isDisabledChecked = (e) => {
        e.preventDefault()
        if (isStatus === false) {
            setStaus(true)
        }
        else {
            setStaus(false)

        }
    }
    const EditDelivery = (e) => {
        e.preventDefault()
        setDelivery(!isDelivery);
    }
    const openModal = (e) => {
        e.preventDefault()
        deliveryLocationList()
        setOpen(true)
    }
    const deliveryLocationList = async () => {
        
        const response = await car_delivery_location_list_service(carId,userID)
        if (response.code == 200) {
            setPickupAddress(response.result.pickupAddress)
            setDeliveryLocation(response.result.deliveryLocations)
            setLocation((prev) => {
                return {
                    ...prev,
                    latitude: response.result.pickupAddress.latitude,
                    longitude: response.result.pickupAddress.longitude
                }
            })
        }

    }
    const handleDeleteAddress=async(e,deliveryLocationId)=>{
        e.preventDefault()
        const response = await delete_address_service(deliveryLocationId)
        if (response.code == 200) {
            let temp_array = deliveryLocation?.filter(item => item.id != deliveryLocationId)
            setDeliveryLocation([...temp_array])
            setSuccessModal(true)
        }

    }
    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
        setDelivery(true);
        setHiddenData(false);
        // setEditData(false);


    }
    const handleClick = (e) => {
        e.preventDefault()
        setHiddenData(true);
    }
    const handleEdit = (e) => {
        e.preventDefault()
        setEditData(true);
    }
    const handleDeliveryLocation=async()=>{
        const response = await car_delivery_location_service(carId,location.latitude,location.longitude,
            location.city,location.state,location.country,location.streetAddress,
            location.zip, userID)
            console.log(response)
            if(response.code==200){
               const estimatePriceResponse = await car_estimate_price_service(carId,location.latitude,location.longitude,response.delivery.id,userID)
                setOpen(false)
                setDelivery(true);
                setHiddenData(false);
                setEditData(false);
                if (estimatePriceResponse.code == 200) {
                    setEstimatePrice(response.result)
                }
            }
    }

    return (
        <Fragment>
            {type == "carDetail" && <>
                <div className={style.formGroup} onClick={openModal}>
                    <span >
                        <img
                            src="/images/others/icon/city_address_icon.svg"
                            alt=""
                        />
                        Pick up & delivery location
                    </span>
                    <span className={`${style.floatRight} float-right`}>
                        <img
                            src="/images/others/icon/carProfile/red_slider.png"
                            alt=""
                        /></span>
                </div>
            </>}
            {type == "bookingDetail" && <>
                <span className={style.smallHide} onClick={openModal}>Edit</span>
            </>}
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                        <span>x</span>
                    </button>
                    <div className={styles.registrationSec}>
                        <div>
                            <form>
                                <div className="row">
                                    <div className="col-12">
                                        <h5>Pick-up & Return Location</h5>
                                        {isDivSatus == false || type == "carDetail" && <> <p> Pick up & return at your location is included in the rental price for no additional fee </p> </>}
                                    </div>
                                    <div className="col-12">
                                        <div className={styles.addCarSec}>
                                            {isDivSatus == false || type == "carDetail" && <>
                                                <div className="row">
                                                    <div className="col-10">
                                                        <p className={styles.editPara}> B-053, B Block, Sector 63, Noida, Uttar Pradesh 201301, India </p></div>
                                                    <div className="col-2">
                                                        <span className={styles.editButton} onClick={(e) => { isEditChecked(e) }}>
                                                            {isDivSatus == false ? "Edit" : "Done"}
                                                        </span>
                                                    </div>
                                                </div>
                                            </>}
                                            <div className={styles.mapSec}>
                                                <SimpleMap mapClass={styles.mapDiv} setLocation={setLocation} location={location} />
                                            </div>
                                            {hiddenData && <button onClick={handleEdit}>Edit</button>}
                                            {isDivSatus == false || type == "carDetail" && <>
                                                <div className={styles.deliveryLocation}>
                                                    <h5>Delivery Locations</h5>
                                                    <p> Enable delivery option to increase your rentals! Guests like the option of having the vehicle delivered to them  </p>
                                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                        <div className="row">
                                                            <div className="col-10">
                                                                <label> Enable Recommended Pricing </label>
                                                            </div>
                                                            <div className={`${styles.toggle} col-2`}>
                                                                <input type="checkbox" id="switch" onChange={(e) => {
                                                                    isDisabledChecked(e)
                                                                }} />
                                                                <label for="switch"></label>
                                                            </div>
                                                        </div>
                                                        {isStatus == true && <>
                                                            <label className="mb-1">Delivery fee</label>
                                                            <select  >
                                                                <option value=""> Free Delivery </option>
                                                                <option value="25"> 25 </option>
                                                                <option value="50"> 50 </option>
                                                                <option value="75"> 75 </option>
                                                                <option value="100"> 500 </option>
                                                            </select>
                                                            <span className={styles.Info}> 0.0 SAR is the recommended price </span>

                                                        </>}
                                                        {isStatus == false && <> </>}
                                                    </div>
                                                </div>
                                            </>}

                                            {/* -------------------------BookingProfile-------------------- */}
                                            {type == "bookingDetail" && isDelivery == true && !hiddenData && <>
                                                <div className={styles.bookingDetailSec}>
                                                    <div className={styles.RadioSec}>
                                                        <h5 className="mb-0">Pick-up Location</h5>
                                                        <p>
                                                            <input type="radio" id="test1" name="radio-group" />
                                                            <label for="test1">{place} </label>
                                                            <span className={`${styles.floatRight} float-right`}>No Charge</span>
                                                        </p>
                                                        <p> The exact address will be sent once you book the vehicle </p>
                                                    </div>
                                                    <div className={styles.RadioSec}>
                                                        <h5 className="mb-0">Delivery Location</h5>
                                                        {deliveryLocation.length > 0 ?
                                                           deliveryLocation.map((item,index)=>{
                                                               return(
                                                                <p>
                                                                <input type="radio" id={item.id} name="radio-group" />
                                                                <label for={item.id}>{item.city + " " + item.state + " " + item.country} </label>
                                                                <span className={`${styles.floatRight} float-right`}>
                                                                    <button onClick={EditDelivery}>
                                                                        <img src="/images/icons/edit.svg" />
                                                                    </button>
                                                                    <button>
                                                                        <img src="/images/icons/delete.svg" onClick={(e)=>{handleDeleteAddress(e,item.id)}}/>
                                                                    </button>
                                                                   {item.guestLocationDeliveryPrice==0 ? "Free Delivery" : item.guestLocationDeliveryPrice }</span>


                                                            </p>
                                                               )
                                                           })
                                                            : ""
                                                        }
                                                    </div>
                                                    <div className={styles.RadioSec}>
                                                        <h5 className="mb-0">Have it Delivered To You</h5>
                                                        <p>
                                                            <input type="radio" id="test2" onClick={(e) => { handleClick(e) }} name="radio-group" />
                                                            <label for="test2">Enter a delivery address  </label>
                                                            <span className={`${styles.floatRight} float-right`}>25 SAR</span>
                                                        </p>
                                                    </div>
                                                </div>

                                            </>}

                                            {/* -----------------------Edit BookingProfile--------- */}
                                            {isDivSatus == true || isDelivery == false || editButton && <>
                                                <p>{location.streetAddress}</p>

                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                    <label>Address</label>
                                                    <input type="text" id="" name="streetAddress"
                                                        placeholder="123456789" value={location.streetAddress} autoComplete="off"
                                                        className={`form-control ${styles.stepbarInput}`}
                                                    />
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                            <label>City</label>
                                                            <input type="text" id="" name="city"
                                                                placeholder="City" value={location.city} autoComplete="off"
                                                                className={`form-control ${styles.stepbarInput}`}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                            <label>State</label>
                                                            <input type="text" id="" name="state"
                                                                placeholder="State" value={location.state} autoComplete="off"
                                                                className={`form-control ${styles.stepbarInput}`}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                            <label>Country</label>
                                                            <input type="text" id="" name="country"
                                                                placeholder="Country" value={location.country} autoComplete="off"
                                                                className={`form-control ${styles.stepbarInput}`}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                            <label>Zip Code</label>
                                                            <input type="text" id="" name="zip"
                                                                placeholder="Code" value={location.zip} autoComplete="off"
                                                                className={`form-control ${styles.stepbarInput}`}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </>}

                                        </div>
                                    </div>
                                    <div className="col-12">
                                        {
                                            editButton ?
                                                <button onClick={(e) => { e.preventDefault(); setHiddenData(false); setEditData(false) }} className={`${styles.sendBtn}`}>
                                                    SAVE
                                                </button>
                                                :
                                                <button onClick={(e) => { e.preventDefault(); handleDeliveryLocation(e)}} className={`${styles.sendBtn}`}>
                                                    SAVE
                                                </button>
                                        }

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </ReactModal>
            <SuccessResponseModal open={isSuccess} setSuccessresponse={setSuccessModal} message="Delivery Location Deleted Successfully"/>
        </Fragment>
    )
}
export default PickUpLocationModal