import { Router } from "next/router"
import { Fragment, useEffect, useState } from "react"
import ReactModal from "react-modal"
import styles from "../../../../styles/customeModal.module.scss"
import { car_modify_price_service, car_update_discounts_service, car_update_Km_service, car_update_price_service } from "../../../helpers/modify_car_details_service"


const EditPriceModal = ({ carId, style, text }) => {
    const [open, setOpen] = useState(false)
    const [isStatus, setStaus] = useState(false)
    const [isPriceStatus, setPriceStaus] = useState(false)
    const [isCustPriceStatus, setCustPriceStaus] = useState(false)
    const [isDiscountStatus, setDiscountStatus] = useState(false)
    const [isKmStatus, setKmStatus] = useState(false)
    const [priceDetails, setPriceDetails] = useState("")
    const [flatDailyPrice, setFlatPrice] = useState("")
    const [weekdaysPrice, setweekdays] = useState("")
    const [holidayPrice, setholidayPrice] = useState("")

    const [WeeklyDiscount, setWeeklyDiscount] = useState("")
    const [MonthlyDiscount, setMonthlyDiscount] = useState("")
    const [dailyKm, setdailyKm] = useState("")
    const [monthlyKm, setmonthlyKm] = useState("")
    const [weeklyKm, setweeklyKm] = useState("")
    const [pricePerExtraMile, setExtraPriceMiles] = useState("")
    const [ErrorMsg, setErrorMsg] = useState("")

    const openModal = (e) => {
        e.preventDefault()
        setOpen(true)
        price_details(carId)

    }


    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
    }
    const isDisabledChecked = () => {
        setStaus((prev) => {
            return (!prev)
        })
    }
    const price_details = async (car_id) => {
        const response = await car_modify_price_service(car_id)
        if (response.code == 200) {
            setPriceDetails(response.result)
            setFlatPrice(response.result?.prices?.flatPrice.daily)
            setWeeklyDiscount(response.result?.discountOnWeekBooking)
            setMonthlyDiscount(response.result?.discountOnMonthBooking)
            setdailyKm(response.result?.maxDayMiles)
            setweeklyKm(response.result?.maxWeekMiles)
            setmonthlyKm(response.result?.maxMonthMiles)
            setExtraPriceMiles(response.result?.pricePerExtraMile)
            setweekdays(response.result?.prices?.variablePrice.weekdays)
            setholidayPrice(response.result?.prices?.variablePrice.weekend)
        }
    }
    const DailyPrice = (e) => {
        if (isPriceStatus === false) {
            setPriceStaus(true)
            setCustPriceStaus(false)
            setDiscountStatus(false)
            setKmStatus(false)
        }
        else {
            setPriceStaus(false)

        }
    }
    const CustomPrice = (e) => {
        if (isCustPriceStatus === false) {
            setCustPriceStaus(true)
            setPriceStaus(false)
            setDiscountStatus(false)
            setKmStatus(false)
        }
        else {
            setCustPriceStaus(false)

        }
    }
    const Discount = (e) => {
        if (isDiscountStatus === false) {
            setDiscountStatus(true)
            setPriceStaus(false)
            setCustPriceStaus(false)
            setKmStatus(false)
        }
        else {
            setDiscountStatus(false)
        }
    }
    const KmIncule = (e) => {
        if (isKmStatus === false) {
            setKmStatus(true)
            setPriceStaus(false)
            setCustPriceStaus(false)
            setDiscountStatus(false)
        }
        else {
            setKmStatus(false)
        }
    }
    const updateprice = async (e) => {
        e.preventDefault()
        let car_id = priceDetails?.id
        const response = await car_update_price_service(car_id, flatDailyPrice)
        console.log(response)
        if (response.code == 200) {
            setPriceStaus(false)
            price_details(car_id)
        }
    }
    const updateCustomprice = async (e) => {
        e.preventDefault()
        let car_id = priceDetails?.id
        const response = await car_update_price_service(car_id, weekdaysPrice,holidayPrice)
        if (response.code == 200) {
            setCustPriceStaus(false)
            price_details(car_id)
        }
    }
    const updateWeeklyMonthlyDiscount = async (e) => {
        e.preventDefault()
        let car_id = priceDetails?.id
        const response = await car_update_discounts_service(car_id, MonthlyDiscount, WeeklyDiscount)
        if (response.code == 200) {
            setDiscountStatus(false)
            price_details(car_id)
        }
    }
    const updateWeeklyMonthlyKilometer = async (e) => {
        e.preventDefault()
        let car_id = priceDetails?.id
        const response = await car_update_Km_service(car_id, dailyKm, monthlyKm, weeklyKm,pricePerExtraMile)
        if (response.code == 200) {
            setKmStatus(false)
            price_details(car_id)
        }
    }
    const handleExtraCharge = (e) => {
        let Id = e.target.value;

        if (Number(Id) > 3) {
            setErrorMsg("Extra KM can not exceed 3 sar")
        }
        else {
            setExtraPriceMiles(Id)
            setErrorMsg("")

        }
    }
    let flatPrice = priceDetails?.prices?.flatPrice.daily
    let weekdays = priceDetails?.prices?.variablePrice.weekdays
    let weekend = priceDetails?.prices?.variablePrice.weekend
    let discountOnMonthBooking = priceDetails?.discountOnMonthBooking
    let discountOnWeekBooking = priceDetails?.discountOnWeekBooking
    let maxDayMiles = priceDetails?.maxDayMiles
    let recommendedPrice = priceDetails?.recommendedPrice
    return (
        <Fragment>
            <div className={style.formGroup} onClick={openModal}>
                <span >
                    <img
                        src="/images/others/icon/edit_price.svg"
                        alt="" />
                    Edit your price & KM included
                </span>
                <span className={`${style.floatRight} float-right`}>
                    <img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                                            <span>x</span>
                                        </button>
                    <div className={styles.registrationSec}>
                        <div>
                            <form className="mt-0">
                                <div className="row">
                                    <div className="col-12">
                                        <h5 className="text-center">EDIT YOUR PRICE</h5>
                                    </div>
                                    <div className="col-12">
                                        <div className={styles.addCarSec}>
                                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                <div className="row">
                                                    <div className="col-10">
                                                        <label> Enable Recommended Pricing  </label>
                                                    </div>
                                                    <div className={`${styles.toggle} col-2`}>
                                                        <input type="checkbox" id="switch" onChange={(e) => { isDisabledChecked(e) }} />
                                                        <label for="switch"></label>
                                                    </div>
                                                    {isStatus == true && <>
                                                        <div className="col-12">   <input type="text" id="" name="idNumber"
                                                            readOnly value={`${recommendedPrice} SAR Is the recommended price`} autoComplete="off"
                                                            className={`form-control ${styles.stepbarInput}`}
                                                        />
                                                            <span className={styles.Info}> Your vehicle is more likely to be rented at this price  </span>
                                                        </div>

                                                    </>}
                                                </div>
                                            </div>

                                            <div className={`${styles.editPriceSec}`}>
                                                <h4>Competitive prices generate more requests.</h4>
                                                <p> Start with the lowest possible price to get your first rentals and move your listing up the search results. You can always increase it later on if necessary.  </p>
                                                {isStatus == false && <>
                                                    <div className={styles.editPriceBtn} onClick={DailyPrice}>
                                                        <span >
                                                            Daily price range
                                                        </span>
                                                        <span className={`${styles.floatRight} float-right`}>{flatPrice} SAR
                                                            <img
                                                                src="/images/others/icon/carProfile/red_slider.png"
                                                                alt=""
                                                                className="ml-2"
                                                            /></span>
                                                    </div>
                                                    {isPriceStatus == true &&
                                                        <>
                                                            <div className={styles.ShowDiv}>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <p>Set your Minimum and Maximum daily price, and we will recommended a price with in the range</p>
                                                                    <label>Flat daily price range</label>
                                                                    <input type="number" id="" name="flatDailyPrice" onChange={(e) => { setFlatPrice(e.target.value) }}
                                                                        placeholder="Edit price rang" value={flatDailyPrice}
                                                                        className={`form-control ${styles.stepbarInput}`}
                                                                    />
                                                                    <button onClick={updateprice} className={`${styles.sendBtn}`}>
                                                                        UPDATE
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </>
                                                    }

                                                    <div className={styles.editPriceBtn} onClick={CustomPrice}>
                                                        <span >
                                                            Custom Pricing
                                                        </span>
                                                        <span className={`${styles.floatRight} float-right`}>{weekdays} SAR- {weekend} SAR
                                                            <img
                                                                src="/images/others/icon/carProfile/red_slider.png"
                                                                alt=""
                                                                className="ml-2"
                                                            /></span>
                                                    </div>
                                                                  {/* CUSTOME PRICE */}

                                                    {isCustPriceStatus == true &&
                                                        <>
                                                            <div className={styles.ShowDiv}>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <p>Set your Minimum and Maximum daily price, and we will recommended a price with in the range</p>
                                                                   <div className="row">
                                                                       <div className="col-6">
                                                                       <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                        <label>Weekday Prices</label>
                                                                        <input type="number" id="" name="flatDailyPrice" onChange={(e) => { setweekdays(e.target.value) }}
                                                                            placeholder="Edit price rang" value={weekdaysPrice}
                                                                            className={`form-control ${styles.stepbarInput}`}
                                                                        />
                                                                    </div>
                                                                       </div>
                                                                       <div className="col-6">
                                                                       <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                        <label>Weekend / Holidays Prices</label>
                                                                        <input type="number" id="" name="flatDailyPrice" onChange={(e) => { setholidayPrice(e.target.value) }}
                                                                            placeholder="Edit price rang" value={holidayPrice}
                                                                            className={`form-control ${styles.stepbarInput}`}
                                                                        />
                                                                    </div>
                                                                       </div>
                                                                   </div>

                                                                    <button onClick={updateCustomprice} className={`${styles.sendBtn}`}>
                                                                        SAVE
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </>
                                                    }
                                                </>}
                                                <div className={styles.editPriceBtn} onClick={Discount}>
                                                    <span >
                                                        Discounts
                                                    </span>
                                                    <span className={`${styles.floatRight} float-right`}>{discountOnWeekBooking}% weekly - {discountOnMonthBooking}% monthly
                                                        <img
                                                            src="/images/others/icon/carProfile/red_slider.png"
                                                            alt=""
                                                            className="ml-2"
                                                        /></span>
                                                </div>

                                                {/* WEEKLY/WEKKEND DISCOUNTS  */}
                                                {isDiscountStatus == true &&
                                                    <>
                                                        <div className={styles.ShowDiv}>
                                                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                <p>Set your Minimum and Maximum daily price, and we will recommended a price with in the range</p>
                                                                <div className="row">
                                                                    <div className="col-6">
                                                                        <label>Weekly Discount</label>
                                                                        <input type="number" id="" name="WeeklyDiscount" onChange={(e) => { setWeeklyDiscount(e.target.value) }}
                                                                            placeholder="Edit price rang" value={WeeklyDiscount}
                                                                            className={`form-control ${styles.stepbarInput}`}
                                                                        />
                                                                        <span className={styles.Info}>10% is the recommended discount</span>

                                                                    </div>
                                                                    <div className="col-6">
                                                                        <label>Monthly Discount</label>
                                                                        <input type="number" id="" name="MonthlyDiscount" onChange={(e) => { setMonthlyDiscount(e.target.value) }}
                                                                            placeholder="Edit price rang" value={MonthlyDiscount}
                                                                            className={`form-control ${styles.stepbarInput}`}
                                                                        />
                                                                        <span className={styles.Info}>20% is the recommended discount</span>

                                                                    </div>
                                                                </div>
                                                                <button onClick={updateWeeklyMonthlyDiscount} className={`${styles.sendBtn}`}>
                                                                    SAVE
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </>
                                                }
                                                <div className={styles.editPriceBtn} onClick={KmIncule}>
                                                    <span >
                                                        KMs included
                                                    </span>
                                                    <span className={`${styles.floatRight} float-right`}>{maxDayMiles} SAR
                                                        <img
                                                            src="/images/others/icon/carProfile/red_slider.png"
                                                            alt=""
                                                            className="ml-2"
                                                        /></span>
                                                </div>
                                                {/* UPDATE DAILY WEEKEND AND MONTHLY KILOMETER  */}

                                                {isKmStatus == true &&
                                                    <>

                                                        <div className={styles.ShowDiv}>
                                                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                <p>Set your Minimum and Maximum daily price, and we will recommended a price with in the range</p>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <label>Daily/km</label>
                                                                    <select autoComplete="off" value={dailyKm} onChange={(e) => { setdailyKm(e.target.value) }}>
                                                                        <option value=""> Select Value   </option>
                                                                        <option value="150"> 150 </option>
                                                                        <option value="200"> 200 </option>
                                                                        <option value="250"> 250 </option>
                                                                        <option value="300"> 300 </option>
                                                                        <option value="350"> 350 </option>
                                                                        <option value="400"> 400 </option>
                                                                        <option value="450"> 450 </option>
                                                                        <option value="500"> 500 </option>
                                                                        <option value="550"> 550 </option>
                                                                        <option value="600"> 600 </option>
                                                                        <option value="650"> 650 </option>
                                                                        <option value="700"> 700 </option>
                                                                        <option value="750"> 750 </option>
                                                                        <option value="800"> 800 </option>
                                                                        <option value="850"> 850 </option>
                                                                        <option value="900"> 900 </option>
                                                                        <option value="950"> 950 </option>
                                                                        <option value="1000"> 1000 </option>
                                                                        <option value="1050"> 1050 </option>
                                                                        <option value="1100"> 1100 </option>



                                                                    </select>
                                                                </div>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <label>Week/km</label>
                                                                    <select autoComplete="off" value={weeklyKm} onChange={(e) => { setweeklyKm(e.target.value); }}>
                                                                        <option value=""> Select Value   </option>
                                                                        <option value="1050" > 1050 </option>
                                                                        <option value="1250" > 1250 </option>
                                                                        <option value="1450" > 1450 </option>
                                                                        <option value="1650" > 1650 </option>
                                                                        <option value="1880" > 1880 </option>
                                                                        <option value="2050" > 2050 </option>
                                                                        <option value="2250" > 2250 </option>
                                                                        <option value="2450" > 2450 </option>
                                                                        <option value="2650" > 2650 </option>
                                                                        <option value="2850" > 2850 </option>
                                                                        <option value="3050" > 3050 </option>
                                                                        <option value="3250" > 3250 </option>
                                                                        <option value="3450" > 3450 </option>
                                                                        <option value="3650" > 3650 </option>
                                                                        <option value="3850" > 3850 </option>
                                                                        <option value="4050" > 4050 </option>
                                                                        <option value="4250" > 4250 </option>
                                                                        <option value="4450" > 4450 </option>
                                                                        <option value="4650" > 4650 </option>
                                                                        <option value="4850" > 4850 </option>
                                                                        <option value="5050" > 5050 </option>
                                                                        <option value="5250" > 5250 </option>
                                                                        <option value="5450" > 5450 </option>
                                                                        <option value="5650" > 5650 </option>
                                                                        <option value="5850" > 5850 </option>
                                                                        <option value="6050" > 6050 </option>
                                                                        <option value="6250" > 6250 </option>
                                                                        <option value="6450" > 6450 </option>
                                                                        <option value="6650" > 6650 </option>
                                                                        <option value="6850" > 6850 </option>
                                                                        <option value="7050" > 7050 </option>
                                                                    </select>
                                                                </div>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <label>Month/km</label>
                                                                    <select autoComplete="off" value={monthlyKm} onChange={(e) => { setmonthlyKm(e.target.value); }}>
                                                                        <option value=""> Select Value   </option>
                                                                        <option value="4500" > 4500 </option>
                                                                        <option value="5000" > 5000 </option>
                                                                        <option value="5500" > 5500 </option>
                                                                        <option value="6000" > 6000 </option>
                                                                        <option value="6500" > 6500 </option>
                                                                        <option value="7000" > 7000 </option>
                                                                        <option value="7500" > 7500 </option>
                                                                        <option value="8000" > 8000 </option>
                                                                        <option value="8500" > 8500 </option>
                                                                        <option value="9000" > 9000 </option>
                                                                        <option value="9500" > 9500 </option>
                                                                        <option value="10000" > 10000 </option>
                                                                        <option value="10500" > 10500 </option>
                                                                        <option value="11000" > 11000 </option>
                                                                        <option value="11500" > 11500 </option>
                                                                        <option value="12000" > 12000 </option>
                                                                        <option value="12500" > 12500 </option>
                                                                        <option value="13000" > 13000 </option>
                                                                        <option value="13500" > 13500 </option>
                                                                        <option value="14000" > 14000 </option>
                                                                        <option value="14500" > 14500 </option>
                                                                        <option value="15000" > 15000 </option>
                                                                        <option value="15500" > 15500 </option>
                                                                        <option value="16000" > 16000 </option>
                                                                        <option value="16500" > 16500 </option>
                                                                        <option value="17000" > 17000 </option>
                                                                        <option value="17500" > 17500 </option>
                                                                        <option value="18000" > 18000 </option>
                                                                        <option value="18500" > 18500 </option>
                                                                        <option value="19000" > 19000 </option>
                                                                        <option value="19500" > 19500 </option>
                                                                        <option value="20000" > 20000 </option>
                                                                    </select>
                                                                </div>
                                                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                                                    <label>Extra KM Fees</label>
                                                                    <p style={{ marginBottom: "7px" }}>Renters will be charged for every additional Km</p>
                                                                    <input type="text" id="" name="pricePerExtraMile"
                                                                        placeholder="Edit price rang" value={pricePerExtraMile} onChange={(e) => { handleExtraCharge(e) }}
                                                                        className={`form-control ${styles.stepbarInput}`}
                                                                    />
                                                                    <span className={styles.Info}>0.25 SAR is the recommended price</span>
                                                                    <span className="text-danger ng-star-inserted" style={{ fontSize: "8px", fontFamily: "NeoSansBold" }}>{ErrorMsg}</span>

                                                                </div>
                                                                <button onClick={updateWeeklyMonthlyKilometer} className={`${styles.sendBtn}`}>
                                                                    SAVE
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <button onClick={closeModal} className={`${styles.sendBtn}`}>
                                            SAVE
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </ReactModal>
        </Fragment>
    )
}
export default EditPriceModal