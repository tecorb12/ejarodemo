
import { Fragment, useContext, useState } from "react";
import ReactModal from 'react-modal'
import styles from "../../../styles/customeModal.module.scss"

const PromocodeModal = ({  classStyle,promocode,setPromoCode,car_Estimate_price }) => {

    const [open, setOpen] = useState(false)
    const openModal = (e) => {
        e.preventDefault()
        setOpen(true)

    }
    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
    }
    const handleClick= async()=>{
       const response = await car_Estimate_price(promocode)
       setOpen(false)
      
    }


    return (
        <Fragment>

            <div className={`${classStyle.formGroupBold} mb-2`} onClick={openModal}>
                <span >Promo Code</span>
                <span className={`${classStyle.floatRight} float-right`} style={{ cursor: "pointer" }}>Promo Code?
                <img src="/images/others/icon/carProfile/apply_code.svg" alt="" /></span>
            </div>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeAddCarModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                        <span>x</span>
                    </button>
                    <div className={styles.registrationSec}>
                        <form className="mt-0">
                            <div className="row">
                                <div className="col-12 text-center">
                                    <h5>Promo code</h5>
                                    <p style={{color: "grey"}}>Only one promo code can be applied per rental</p>
                                </div>
                                <div className="col-12">
                                    <div className={styles.addCarSec}>
                                        <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                            <label>Promo Code</label>
                                            <input type="text" name="promocode" value={promocode}
                                            onChange={(e)=>{setPromoCode(e.target.value)}}
                                             autoComplete="off"
                                            placeholder="Enter coupon code" 
                                            className={`form-control ${styles.stepbarInput}`}
                                        />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">
                                    <button onClick={(e)=>{e.preventDefault(); handleClick()}} className={`${styles.sendBtn}`}>
                                    UPDATE 
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>
    )
}

export default PromocodeModal

