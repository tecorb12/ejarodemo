import React, { Fragment } from "react";
import Modal from 'react-modal';
import StepperScreen from './stepper';
import Describe_Job from './descibe_job';
import Date_and_Time from './Date_and_Time';
import Budget_duration from './Budget_and_duration';
import styles from './modal.module.css';
import Job_review from './job_review';
import { Popup } from "../../common/Swal_Modal/swal";
import { validateName } from '../../common/validation';
import { postJobActions } from '../../redux/action/postJob_action'
import { connect } from "react-redux";
import axios from "axios";
import ModalSkeleton from '../../common/Loader/skeltonLoader';
import { withRouter } from "next/router";
import { BASE_URL, commonConst } from "../../redux/helpers/keys";
import jstz from 'jstz';
const timezone = jstz.determine();
import moment from "moment";
import { IdContext } from "../../MyContext/IdDetailsContext";
import { ApiUrl } from "../../redux/helpers/api_url";
var subtitle;
export const dateAndTime = React.createRef();
export const budgetAndDuration = React.createRef();
export const describeJobRef = React.createRef();

class PostAJob extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            info: "",
            task_title: "", task_detail_error: "", task_detail_status: true, task_id: "", job_max_price: "", getofferTaxForSeeker: "",
            job_id: "", min_duration: "",
            task_detail: "", task_title_error: "", task_title_status: true, image: "", imageURL: "",
            radioInpersonButton: "person", defaultlocation: "", location: "", selected_location: "",
            date: "", hour: "", time: "", flatUnit: "", radioButton: "total",radioLocationbutton:"Other",
            seeker: "",
            total: "",
            hourly_rate: "",
            text: "",
            totalAamount: "",
            hourly_amount: 1,
            hourly_time: "",
            isJobPostModal: false,
            isJobDescription: false,
            currentStep: 1, initialminute: "",
            jobs: [], postal_code: "", default_postal_code: "",
            Othercoordinate: {
                lat: "", lng: ""
            },
            coordinate: {
                lat: "",
                lng: ""
            }, base_budget: "", isDisabled: false,
            guestToken: "", sessionToken: "", hour: "", minute: "", both_type: "2", hourChargeOndemand: "",
            budget: "", duration: "", isLoading: false, inputFormat: "", isLoadingConfirm: false
        }
    }
    componentDidMount() {
        if (typeof window !== "undefined") {
            this.setState({
                coordinate: {
                    lat: localStorage.DefaultCureentlat,
                    lng: localStorage.DefaultCurrentlng,
                },
                guestToken: localStorage.guestToken,
                sessionToken: localStorage.token
            })
        }
        describeJobRef.current?.Empty()
    }
    componentDidUpdate(prevProps) {
        if (prevProps.jobs !== this.props.jobs || prevProps.isLoading !== this.props.isLoading || prevProps.default_postal_code !== this.props.default_postal_code) {
            this.setState({
                jobs: this.props.jobs,
                isLoading: this.props.isLoading,
                default_postal_code: this.props.default_postal_code
            })
        }
    }
    handleJobType = () => {
        this.props.job_type()
    }
    openModal = () => {
        this.props.job_type()
        this.setState({
            isJobPostModal: true,
            // document.body.style.overflow = 'hidden';
        })
    }
    openNextModal = () => {
        this.setState({
            isJobDescription: true,
            isJobPostModal: false,


        })
    }
    goBackModal = () => {
        this.setState({
            isJobDescription: false,
            isJobPostModal: true
        })
    }
    closeModal = () => {
        this.setState({
            isJobPostModal: false
        })
    }
    NextcloseModal = () => {
        this.context.setTitle(true)
        this.context.setIsError("")
        this.setState({
            isJobDescription: false,
            currentStep: 1,
            task_detail: "",
            task_title: "",
            task_id: "",
            time: "",
            imageURL: "",
            date: "",
            image: "",
            seeker: "",
            location: "",
            flatUnit: "",
            hourly_time: "",
            hourly_amount: "",
            totalAamount: "",
            hour: "", minute: "",
        })
    }
    afterOpenModal = () => {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#f00';
    }
    handleInfo = (e) => {
        e.target.value

    }
    continue = () => {
        event.preventDefault();
        this.openNextModal()
        this.setState({ currentStep: 1 })
        // this.props.modify_job("34")

    }

    handleTaskTitle = (task_title, task_id, max_price, description, image, imageURL, job_id, hour, minute, base_budget, hourChargeOndemand, initialminute, getofferTaxForSeeker) => {
        this.setState({
            task_title: task_title,
            task_id: task_id,
            job_max_price: max_price,
            task_detail: description,
            image: image,
            imageURL: imageURL,
            // task_title_error: task_title_error,
            // task_title_status: task_title_status,
            // task_detail_error: task_detail_error,
            // task_detail_status: task_detail_status,
            job_id: job_id,
            hour: hour,
            minute: minute,
            base_budget: base_budget,
            hourChargeOndemand: hourChargeOndemand,
            initialminute: initialminute,
            getofferTaxForSeeker: getofferTaxForSeeker
        })
    }
    handleBudgetDuration = (total, hourly, totalAamount, hourly_amount, hourly_time, info, duration, seeker, hour, minute, radioButton, both_type) => {
        const { job_max_price, base_budget, hourChargeOndemand, initialminute } = this.state;
        let final_amount = Number(both_type) == 1 && ((Number(hour) == base_budget) && (minute == initialminute)) ? (job_max_price * seeker * hourChargeOndemand)?.toFixed(2) : duration ? (job_max_price * (Number(hour) + minute / 60) * hourChargeOndemand * Number(seeker)).toFixed(2) : totalAamount ? totalAamount : hourly_amount * hourly_time * seeker
        this.setState({
            total: total,
            hourly_rate: hourly,
            totalAamount: totalAamount,
            hourly_amount: hourly_amount,
            hourly_time: hourly_time,
            info: info,
            duration: duration,
            seeker: seeker,
            hour: hour,
            minute: minute,
            radioButton: radioButton,
            both_type: both_type,
            budget: (final_amount)
        })
    }
    handleDateTime = (radioInpersonButton, seeker, defaultlocation, location, date, time, inputFormat, flatUnit, Othercoordinate, postal_code,radioLocationbutton) => {
        this.setState({
            // defaultlocation:defaultlocation,
            radioInpersonButton: radioInpersonButton,
            defaultlocation: defaultlocation,
            location: location,
            selected_location: defaultlocation ? defaultlocation : location,
            date: date,
            time: time,
            seeker: seeker,
            postal_code: postal_code,
            inputFormat: inputFormat,
            flatUnit: flatUnit,
            radioLocationbutton:radioLocationbutton,
            Othercoordinate: {
                lat: Othercoordinate.lat,
                lng: Othercoordinate.lng
            }
        })
    }


    actionClick = async (type, e) => {
        e.preventDefault();

        if (type === 'SUBMIT') {
            return <Job_review />
        }
    }

    handlClick = (clickType) => { //next step
        const { currentStep,radioLocationbutton, seeker, task_id, getofferTaxForSeeker,Othercoordinate, time, date, both_type, totalAamount, hour, minute, flatUnit, hourly_amount, hourly_time, defaultlocation, coordinate, location, PhoneNumber, task_title_error, task_title_status } = this.state;
        let status = false;
        let Errormsg = '';
        let task_title = this.state.task_title?.trim()
        let task_detail = this.state.task_detail.trim()
        status = validateName(task_title).isDisabled;
        Errormsg = validateName(task_title).error;
        let nameRegex = /(?=.*[0-9])*[A-Za-z\\s][A-Za-z0-9\\s]+$/;
        let nextStep = currentStep;
        let info = this.state.info === 2 ? this.state.info : (both_type ?  Number(both_type) : 1)

        if (nextStep < 4) {
            if (nextStep < 2 && (task_title != '' && task_id != '')) {
                if (task_detail.length < 25) {
                    this.context.setTitle(false)
                    this.context.setIsError("Please enter at least 25 characters")

                    describeJobRef.current?.nullErrorCall()

                }
                else {
                    clickType === 'NEXT' ? nextStep++ : nextStep;

                }
            }
            else if (nextStep <= 2 && task_title !== '' && task_detail !== '' && date && time && seeker) {
                console.log("Hit")

                if (defaultlocation === '' && location === '') {
                    dateAndTime.current?.nullErrorCall()
                }
                else {
                    clickType === 'NEXT' ? nextStep++ : nextStep;
                }
            }
            else if (nextStep >= 3) {

                if (info === 1 || info === 3) {

                    if (hour === '00' || hour === '') {

                        budgetAndDuration.current?.nullErrorCall()
                        if (minute !== '') {
                            console.log("Hit")
                            if (minute !== "00") {
                                clickType === 'Submit' ? nextStep++ : nextStep
                            }
                        }
                    }
                    else {
                        clickType === 'Submit' ? nextStep++ : nextStep
                    }
                }
                else if (info === 2) {
                    console.log("Hit")

                    if (totalAamount === '') {
                        console.log("Hit")

                        budgetAndDuration.current?.nullErrorCall()
                        if (hourly_amount !== '' && hourly_time !== '' && hourly_amount * hourly_time * seeker > getofferTaxForSeeker) {
                            clickType === 'Submit' ? nextStep++ : nextStep
                        }
                    }
                    else if (totalAamount && totalAamount < getofferTaxForSeeker) {
                        console.log("Hit")

                        budgetAndDuration.current?.nullErrorCall()
                    }
                    else {
                        clickType === 'Submit' ? nextStep++ : nextStep
                    }
                }
            }
            else {
                describeJobRef.current?.nullErrorCall()
                dateAndTime.current?.nullErrorCall()
                budgetAndDuration.current?.nullErrorCall()
                // this.setState({
                //     first_name_status: false,
                //     first_name_error: "Please enter valid first_name address. ",
                //     title_Class: styles.titleBox
                // })
            }
        }
        this.setState({
            currentStep: nextStep
        })
    }
    onBackAction = (state) => {
        let { task_title, task_detail, PhoneNumber, Image1, Image2 } = this.state;
        let nextStep = state;

        if (nextStep > 1) {
            if (task_title.length < 0 && task_detail.length < 0) {
            } else {
                nextStep--;
            }
        }
        this.setState({
            currentStep: nextStep
        })
    }
    onEditButton = (state) => {
        let step = state;
        if (step === 4) {
            this.setState({
                currentStep: step - 3
            })
        }
    }
    confirmjobPosted = async () => {
        const { task_title, hourly_time, hourly_amount, task_detail, postal_code, default_postal_code, image, hour, minute, task_id, both_type, flatUnit, radioInpersonButton, time, sessionToken, selected_location, seeker, date, coordinate, Othercoordinate, budget } = this.state;
        let message = "Your job has been posted"
        let image_url = "images/auth/get_offer_job_posted.png"
        let btnText = "Done"
        let message1 = "Our Team will allot a Job Seeker for this Job within 24 hours. Check your email for further updates!"
        let image_url1 = "/images/others/on_demand_job_posted.png"
        let guest_message = "Your job will be posted once you signup or login on our platform"
        this.setState({ isLoadingConfirm: true })
        let info = this.state.info === 2 ? this.state.info : Number(both_type)
        const formData = new FormData();
        formData.append("job_id", info);
        formData.append("question1", task_title);
        formData.append("question2", task_detail)
        formData.append("image", image)
        formData.append("question3", radioInpersonButton)
        formData.append("latitude", Othercoordinate.lat ? Othercoordinate.lat : coordinate.lat,)
        formData.append("longitude", Othercoordinate.lng ? Othercoordinate.lng : coordinate.lng)
        formData.append("full_address", selected_location)
        formData.append("date", date)
        formData.append("hour", time)
        formData.append("total_seeker", seeker)
        formData.append("budget", budget)
        formData.append("flat", flatUnit)
        formData.append("job_subcategory_id", task_id)
        formData.append("hour_duration", hourly_time ? hourly_amount : info == 1 ? hour : "")
        formData.append("min_duration", hourly_time ? "" : minute)
        formData.append("postal_code", postal_code ? postal_code : default_postal_code)
        const response = await axios.post(BASE_URL+ApiUrl.TEMP_JOB, formData,
            {
                headers: {
                    "Content-Type": "application/json",
                    "deviceType": "web",
                    "sessionToken": sessionToken,
                    "guestToken": sessionToken ? null : localStorage.guestToken,
                    "timeZone": timezone.name()

                }
            })
        if (response.data.code === 200 && sessionToken) {
            let jobId = response.data.temp_job_details.id
            let amount = response.data.temp_job_details.budget
            if (response.data.code === 200 && info === 2) {
                this.setState({ isJobDescription: false, isLoadingConfirm: false, isDisabled: true })
                Popup(message, image_url, btnText, "/dashboard/job_board/all_post")
            }
            else if (info === 1) {
                this.setState({ isJobDescription: false, isLoadingConfirm: false, isDisabled: true })
                localStorage.pay = response.data.temp_job_details.id;
                global.job_id = 1
                this.props.router.replace(`/payment_section/cart_detail/${jobId}?id=${"d1"}&price=${amount}`)
            }
        }
        else if (response.data.code == 422) {
            this.setState({ isJobDescription: false, isLoadingConfirm: false, isDisabled: true })
        }
        else if (response.data.code === 200 && localStorage.guestToken) {
            let jobId = response.data.temp_job_details.id
            let amount = response.data.temp_job_details.budget
            if (info === 2) {
                localStorage.getoffer = true
                this.setState({ isJobDescription: false, isLoadingConfirm: false, isDisabled: true })
                this.props.router.replace("/auth/signup")
            }
            if (info === 1) {
                localStorage.pay = jobId;
                localStorage.amount = amount
                this.setState({ isJobDescription: false, isLoadingConfirm: false, isDisabled: true })
                this.props.router.replace("/auth/signup")
            }
        }
        else {
            this.setState({ isLoadingConfirm: false })
            Popup(message, image_url, btnText, "/auth/login")
        }
    }
    footerView = (countValue) => {
        const { isLoadingConfirm, isDisabled } = this.state
        switch (countValue) {
            case 1: return (
                <div>

                    <button className={`mt-0 ${styles.continueBtn}`} onClick={() => { this.handlClick('NEXT') }} >Next</button>
                    {/* <button className={`mt-0 ${styles.backBtn}`} onClick={this.goBackModal} ><img src="/images/others/left-arrow.svg" class="mr-2" />Go Back</button> */}
                </div>

            );
            case 2: return (
                <div>
                    <button className={`mt-0 ${styles.continueBtn}`} onClick={() => { this.handlClick('NEXT') }} >Next</button>
                    <button className={`mt-0 ${styles.backBtn}`} onClick={() => { this.onBackAction(this.state.currentStep) }} ><img src="/images/others/left-arrow.svg" class="mr-2" />Go Back</button>
                </div>

            );
            case 3: return (
                <div>
                    <button className={`mt-0 ${styles.continueBtn}`} onClick={() => { this.handlClick('Submit') }} >Submit</button>
                    <button className={`mt-0 ${styles.backBtn}`} onClick={() => { this.onBackAction(this.state.currentStep) }} ><img src="/images/others/left-arrow.svg" class="mr-2" />Go Back</button>
                </div>

            );
            case 4: return (
                <div>
                    <button className={`mt-0 ${styles.continueBtn}`} disabled={isLoadingConfirm} onClick={this.confirmjobPosted}>

                        {isLoadingConfirm && (
                            <i
                                className="fa fa-spinner fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        )}
                        {isLoadingConfirm && <span>please wait </span>}
                        {!isLoadingConfirm && <span>Confirm</span>}
                    </button>
                    <button disabled={isLoadingConfirm} className={`mt-0 ${styles.backBtn} ${styles.editFinal}`} onClick={() => { this.onEditButton(this.state.currentStep) }} >Edit</button>
                </div>

            )
            default:
                return (
                    null
                )
        }
    }
    onSetInfoValue = (id) => {
        this.setState({
            info: id
        })
    }
    render() {
        const { hourChargeOndemand,radioLocationbutton, getofferTaxForSeeker, initialminute, postal_code, imageURL, base_budget, both_type, isLoading, job_max_price, job_id, task_id, min_duration, radioButton, Othercoordinate, flatUnit, hour, minute, inputFormat, radioInpersonButton, selected_location, task_title, totalAamount, task_detail, defaultlocation, duration, time, task_title_error, task_title_status, image, isJobDescription, in_person, info, online, other, date, location, seeker, hourly_rate, total, hourly_amount, hourly_time, currentStep, jobs, coordinate } = this.state
        const StepsArray = [
            "Describe Your Job", "Choose Date & Time", "Budget & Duration", "Job Preview"
        ];
        const jobs_type = jobs.map(jobType => {
            return (
                <div className="col-12">
                    <div onClick={() => this.onSetInfoValue(jobType.id)} class={Number(info) === jobType.id ? "radioBtnSec + selected" : "radioBtnSec"}>
                        <label class="radioBtnLabel" for={jobType.id}>
                            <div class={styles.radioSecImg}>
                                <img src={jobType.image} />
                            </div>
                            <div class={styles.radioSecContent}>
                                <h3 class="radioBtnLabelTitle">
                                    {jobType.title}
                                    <span className={styles.infoIcon}><img src="/images/others/about_icon_blue.png" />
                                        <span className={styles.tooltiptext}>{jobType.long_desp}</span></span>
                                </h3>
                                <span class="radioBtnLabelTitleSubtext">
                                    {jobType.short_desp}
                                </span>
                                <input type="radio" name="sharing"
                                    id={jobType.id} value="standard" className="radioBtnInput"
                                    checked={Number(info) === jobType.id}
                                    onChange={(e) => { this.setState({ info: e.target.id }) }}
                                />
                                <div class="radioBtnIndicator"></div>
                            </div> </label>
                    </div>
                </div>
            )
        })
        return (
            <Fragment>
                <button className={this.props.className} type="submit"
                    onClick={() => this.continue()} style={{ color: "#fff", display: "inline-block" }}>{this.props.name}
                </button>

                <Modal
                    isOpen={isJobDescription}
                    // onAfterOpen={this.afterOpenModal}
                    className={`${styles.customeModal} ${styles.customePostModal}`}
                    onRequestClose={this.NextcloseModal}
                    shouldCloseOnOverlayClick={true}
                    onAfterOpen={() => document.body.style.overflow = 'hidden'}
                    onAfterClose={() => document.body.style.overflow = 'unset'}
                    ariaHideApp={false}
                >
                    <div class={styles.modalHeader}>
                        <div class="row">
                            <div class="col-10">
                                <h4>Post a Job</h4>
                            </div>
                            <div class="col-2">
                                <button className={styles.closeBtn} onClick={() => { this.NextcloseModal() }}><img src="/images/auth/cancel_icon.svg" /></button><br />
                            </div>
                        </div>
                    </div>
                    <div className={styles.stepBarSec}>
                        <div>
                            <StepperScreen steps={StepsArray} currentStepNumber={currentStep} />
                        </div>
                        <div className={styles.stepBarContent}>
                            {
                                currentStep === 1 ? <Describe_Job
                                    task_title={task_title ? task_title : this.props.title}
                                    task_detail={task_detail}
                                    job_id={job_id}
                                    minute={minute}
                                    hour={hour}
                                    image={image}
                                    base_budget={base_budget}
                                    imageURL={imageURL}
                                    task_title_error={task_title_error}
                                    task_title_status={task_title_status}
                                    task_detail_status={this.state.task_detail_status}
                                    task_id={task_id ? task_id : this.props.taskId}
                                    job_max_price={job_max_price}
                                    initialminute={initialminute}
                                    hourChargeOndemand={hourChargeOndemand}
                                    handle_task_title={this.handleTaskTitle}
                                    ref={describeJobRef}

                                />
                                    : null
                            }
                            {
                                this.state.currentStep === 2 ? <Date_and_Time
                                    radioInpersonButton={radioInpersonButton}
                                    defaultlocation={defaultlocation}
                                    location={location}
                                    date={date}
                                    seeker={seeker}
                                    time={time}
                                    flatUnit={flatUnit}
                                    postal_code={postal_code}
                                    radioLocationbutton={radioLocationbutton}
                                    handle_Date_time={this.handleDateTime}
                                    Othercoordinate={Othercoordinate}
                                    ref={dateAndTime}
                                /> : null
                            }
                            {
                                this.state.currentStep === 3 ? <Budget_duration
                                    total_work={total}
                                    hourly_work={hourly_rate}
                                    total_amount={totalAamount}
                                    hourly_time={hourly_time}
                                    hourly_amount={hourly_amount}
                                    info={job_id}
                                    min_duration={min_duration}
                                    duration={duration}
                                    seeker={seeker}
                                    hour={hour}
                                    minute={minute}
                                    initialminute={initialminute}
                                    base_budget={base_budget}
                                    radioButton={radioButton}
                                    onDemandBudget={job_max_price}
                                    hourChargeOndemand={hourChargeOndemand}
                                    both_type={both_type}
                                    getofferTaxForSeeker={getofferTaxForSeeker}
                                    handle_Budget={this.handleBudgetDuration}
                                    ref={budgetAndDuration}
                                /> : null
                            }
                            {
                                this.state.currentStep === 4 ? <Job_review
                                    title={task_title}
                                    description={task_detail}
                                    location={selected_location}
                                    shedule={date}
                                    budget={this.state.budget}
                                    total_time_minute={time}
                                    total_amount={totalAamount}
                                    hourly_amount={hourly_amount}
                                    onDemandBudget={job_max_price}
                                    hourly_time={hourly_time}
                                    duration={duration}
                                    base_budget={base_budget}
                                    budget={this.state.budget}
                                    info={job_id}
                                    both_type={both_type}
                                    hourChargeOndemand={hourChargeOndemand}
                                    flatUnit={flatUnit}
                                    hour={hour}
                                    minute={minute}
                                    seeker={seeker}

                                /> : null
                            }
                            <div>
                                <div className="mb-4">
                                    {/* <FooterButton onPressnext={() => { this.handlClick('NEXT') }} onPressback={() => { this.state.currentStep == 1 ? null : this.handlClick('BACK') }} /> */}
                                    {this.footerView(this.state.currentStep)}
                                    {/* <button className={`mt-0 ${styles.continueBtn}`} onClick={() => { this.handlClick('NEXT') }} currentSTEP={this.state.currentStep} >Next</button>
                                    <button className={`mt-0 ${styles.backBtn}`} onClick={() => { this.onBackAction(this.state.currentStep) }} ><img src="/images/others/left-arrow.svg" class="mr-2"/>Back</button> */}
                                </div>
                                {/* } */}
                            </div>
                        </div>
                    </div>
                </Modal>
            </Fragment>
        )
    }
}
PostAJob.contextType = IdContext
const mapStateToProps = (state) => {

    return {
        jobs: state.job_type.jobtype,
        isLoading: state.job_type.isLoading,
        ModifyPostJob: state.ModifyPostJob.modifyJobData,
        default_postal_code: state.edit_profile.postal_code


    }
}
const actionCreator = {
    job_type: postJobActions.job_type,
    post_job: postJobActions.job_post,
    // modify_job : postJobActions.modify_job
}
export default withRouter(connect(mapStateToProps, actionCreator)(PostAJob));