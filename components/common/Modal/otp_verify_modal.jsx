
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import { verify_otp_service } from "../../helpers/auth_services";
import OtpInput from "./otp_input";

const OtpVerify = ({ isOpen, style, text, setOpen }) => {
    const [otp, setOtp] = useState("")
    const [isDisable, setDisabled] = useState(false)
    const [message, setMessage] = useState("")


    const handleOnchangeOtp = (otp) => {
        setOtp(otp)
        setMessage("")
        if (otp.length == 4) {
            setDisabled(true)
        }
    }
    const verifiedOtp = async (event) => {
        event.preventDefault()
        const response = await verify_otp_service(otp)
        if (response.code == 401) {
            setMessage(response.message)
            setOtp("")
            setDisabled(false)


        }
        if (response.code == 200) {
           setOpen(false)


        }
    }
    return (
        <Fragment>

            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={isOpen}
                // onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={styles.registrationSec}>
                        <form>
                            <div>
                                <h5>Verify Your Phone Number</h5>
                            </div>
                            <h4 style={{ color: "#b2020f", textAlign: "center" }}>{message}</h4>

                            <div id="step3">

                                <div className={`form-group px-0 ${styles.customeFormGroup} ${styles.OtpSec}`}>
                                    <p>Enter your Verification Code Here</p>
                                    <OtpInput
                                        value={otp}
                                        onChange={handleOnchangeOtp}
                                        numInputs={4}
                                        separator={<span>  </span>}
                                        isInputNum={true}
                                        name="otp" />
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button onClick={(e) => { verifiedOtp(e) }} className={styles.sendBtn} disabled={!isDisable}>
                                            VERIFY
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default OtpVerify;