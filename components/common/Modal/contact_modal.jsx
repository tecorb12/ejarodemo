
import { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import { sent_otp_service, verify_contact_service } from "../../helpers/auth_services";
import OtpVerify from "./otp_verify_modal";

const VerifyContact = ({ isOpen, phone, setPhone, countryCode, setCountrycode,setOpenModal, userId, email }) => {
    const [open, setOpen] = useState(false)
    const [isDisable, setDisabled] = useState(false)
    const [message, setMessage] = useState("")
    const[phoneNo, setNo] = useState(countryCode+phone)

    useEffect(() => {
        isDisabledChecked()
    }, [])

    const handleOnchangePhone = (phone, country, e) => {
        console.log(phone)
        let name = "phoneNo"
        let country_code = `+${country.dialCode}`
        let country_name = country.countryCode
        setCountrycode(country_code)
        setNo(phone)

    }
    const isDisabledChecked = () => {
        if (phone != '') {
            setDisabled(true)
        }
        else {
            setDisabled(false)

        }
    }

    const verifyContact = async(e) => {
        e.preventDefault()
        let country_replace = (countryCode).toString().replace("+", "")

        let phone_number = phoneNo
        phone_number = phone_number.toString().replace(country_replace, "").replace("+", "")
        const response = await verify_contact_service(countryCode, email, phone_number, userId)
        if (response.code == 404) {
            const OtpResponse = await sent_otp_service(countryCode, phone_number, userId)
            console.log(OtpResponse)
            if (OtpResponse.code == 200) {
                setOpenModal(false)
                setOpen(true)
            }

        }
        else if (response.code == 200) {
            setMessage(response.message)
        }
    }
    
    return (
        <Fragment>

            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={isOpen}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={styles.registrationSec}>
                        <form>
                            <div>
                                <h5>Verify Your Phone Number</h5>
                                <p>Enter your phone number and we will send you a verification code to confirm</p>
                            </div>
                            <h4 style={{ color: "#b2020f", textAlign: "center" }}>{message}</h4>



                            <div id="step3">

                                <div className={`form-group px-0 ${styles.customeFormGroup} ${styles.phoneInput}`}>
                                    <label>Phone Number</label>
                                    <PhoneInput
                                        inputClass="form-control"
                                        country="sa"
                                        value={phoneNo}
                                        placeholder="phone*"
                                        onChange={(phone, country_name) => { handleOnchangePhone(phone, country_name); isDisabledChecked() }}
                                        prefix="+"
                                        copyNumbersOnly={false}
                                        countryCodeEditable={true}
                                    />
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button onClick={(e) => { verifyContact(e) }} className={styles.sendBtn} disabled={!isDisable}>
                                            VERIFY
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
            <OtpVerify isOpen={open} setOpen={setOpen}/>
        </Fragment>

    )
}

export default VerifyContact;