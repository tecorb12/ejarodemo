
import { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import 'react-phone-input-2/lib/style.css'

const SuccessResponseModal = ({ open,setSuccessresponse,message }) => {
    const closeclick =()=>{
        global.modal=false
        setSuccessresponse(false)
    }
    
    return (
        <Fragment>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={open}
                onRequestClose={closeclick}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={`${styles.registrationSec} ${styles.onlyHeading}`}>
                        {/* <button onClick={closeclick} className="float-right close mr-0 mt-0 ">
                            <span>x</span>
                        </button> */}
                        <form>
                        <div className="row">
                                <div className="col-12">
                                    <button onClick={closeclick} className={`float-right close mr-0 mt-0 ${styles.colseBtn}`}>
                                        <span>x</span>
                                    </button></div>
                            <div className="col-12 text-center">
                                <h5 className="text-center">Ejaro</h5>
                            </div>
                            <div className="col-12" style={{width: "250px"}}>
                            <p style={{color:"#212529"}}> {message} </p>
                            </div>
                            </div>
                               
                            <div class="row">
                                <div class="col-12">

                                    <button onClick={closeclick} style={{ float: "unset" }} className={` mt-0 mr-2 mb-3 `}
                                        className={styles.sendBtn} >
                                        OKAY
                                    </button>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default SuccessResponseModal;