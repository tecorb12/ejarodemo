
import { Fragment, useEffect, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import 'react-phone-input-2/lib/style.css'
import { mycar_action_service } from "../../helpers/myCar_services";

const SharedParkedAction = ({ text,setSuccessresponse,setMessage, carId, car_list, set_car_list, setCount, title,requestFor }) => {
    const [open, setOpen] = useState(false)
    const [isDisable, setDisabled] = useState(false)



    const openClick = () => {
        setOpen(true)
    }
    const closeclick = () => {
        setOpen(false)
    }
    const deleteCar = async (e) => {
        e.preventDefault()
        const response = await mycar_action_service(carId, requestFor,`Unlisting Car ${carId}`)
        console.log(response.code == 200)
        if (response.code == 200) {
            // let temp_array = car_list?.filter(item => item.id != carId)
            // set_car_list([...temp_array])
            // setCount((previous) => previous - 1)
            setOpen(false)
            setSuccessresponse(true)
        }
        else  if (response.code == 422) {
            setOpen(false)
            setSuccessresponse(true)
            setMessage(response.message)
        }
    }
    return (
        <Fragment>
            <button onClick={openClick}>{text}</button>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={open}
                onRequestClose={closeclick}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={`${styles.registrationSec} ${styles.onlyHeading}`}>
                    {/* <button onClick={closeclick} className="float-right close mr-0 mt-0 ">
                            <span>x</span>
                        </button> */}
                        <form>
                        <div className="row">
                                <div className="col-12">
                                <button onClick={closeclick} className={`float-right close mr-0 mt-0 ${styles.colseBtn}`}>
                                       <span>x</span>
                                    </button></div>
                            <div className="col-12 text-center">
                                <h5>{title}</h5>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-6 text-right">

                                    <button style={{ float: "unset" }} className={` mt-0 mr-2 mb-3 `}
                                        onClick={(e) => { deleteCar(e) }} className={styles.sendBtn} >
                                        Yes
                                    </button>
                                    </div>
                                    <div class="col-6 text-right">
                                
                                    <button style={{ float: "unset" }}
                                        onClick={closeclick} className={styles.backBtn} >
                                        No
                                    </button>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default SharedParkedAction;