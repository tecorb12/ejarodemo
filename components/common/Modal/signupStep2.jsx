
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment, { locale } from 'moment';
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import { validateEmail, validatePassword } from "../validation";
import { user_signup_service, verify_contact_service, sent_otp_service } from "../../helpers/auth_services";
import SignUpStepThree from "./signupStep3";
import HijriDatePicker from "../../Auth/demo/app";

const SignUpStepTwo = ({ fname, lname, dob, setDob, isOpen, idNumber, setUser, nationality, id, setOpenModalTwo }) => {
    const [phone, setPhone] = useState("")
    const [countryCode, setCountrycode] = useState("")
    const [email, setEmail] = useState("")
    const [isDisable, setDisabled] = useState(false)
    const [Open, setOpenModalThree] = useState(false)

    const [gender, setGender] = useState("")
    const [message, setMessage] = useState("")
    const [password, setPassword] = useState({
        pass: "",
        confirmPass: ""
    })
    const [isError, setError] = useState({
        email_status: false,
        password_status: false,
        confrm_password_status: false,
    })
    const closeModal = () => {
        setOpenModalTwo(false)
        setDob("")
        setMessage("")
        setDisabled(false)

        setPhone("")
        setUser((prev) => { return { ...prev, fname: "", lname: "" } })
        setEmail("")
        setPassword((prev) => { return { ...prev, confirmPass: "", pass: "" } });

    }
    const validForm = (fieldName, Value) => {
        let isDisabledEmail = isError.email_status
        let isDisabledPassword = isError.password_status
        let confrmPassword_status = isError.confrm_password_status
        switch (fieldName) {
            case "email":
                isDisabledEmail = validateEmail(Value).isDisabled
                break;
            case "pass":
                isDisabledPassword = validatePassword(Value).isDisabled
                break;
            case "confirmPass":
                confrmPassword_status = validatePassword(Value).isDisabled
                break;
            default:
                break;
        }
        setError((prev) => {
            return {
                ...prev,
                email_status: isDisabledEmail,
                password_status: isDisabledPassword,
                confrm_password_status: confrmPassword_status
            }
        })

    }
    const isDisabledChecked = () => {
        if (isError.email_status === true && phone != '' && isError.password_status === true && isError.confrm_password_status && gender != '') {
            setDisabled(true)
        }
        else {
            setDisabled(false)

        }
    }

    const handleNext = async (e) => {
        let now = new Date()
        let dayOfYear = Math.floor((new Date(dob) - new Date(now.getFullYear(), 0, 0)) / (1000 * 60 * 60 * 24))
        let hijriDate = ((now.getFullYear() - 621.5643) * 365.24225 + dayOfYear) / 354.36707
        let hijriYear = Math.floor(hijriDate)
        let hijriMonth = Math.ceil((hijriDate - Math.floor(hijriDate)) * 354.36707 / 29.530589)
        let hijriDay = Math.floor((hijriDate - Math.floor(hijriDate)) * 354.36707 % 29.530589)

        let hijriDob = `${hijriYear}/${hijriMonth}/${hijriDay}`
        e.preventDefault();
        let count = countryCode.length
        let phone_number = phone;
        phone_number = phone_number.substring(count - 1);
        const response = await verify_contact_service(countryCode, email, phone_number)
        if (response.code == 404) {
            const signupResponse = await user_signup_service(countryCode, dob, id = "saudi" ? dob : hijriDob, email, fname, gender, idNumber, lname, phone_number, nationality, password.pass, id)
            if (signupResponse.code == 200) {
                let userId = signupResponse.result.id
                const OtpResponse = await sent_otp_service(countryCode, phone_number, userId)
                console.log(OtpResponse)
                if (OtpResponse.code == 200) {
                    setOpenModalTwo(false)
                    setOpenModalThree(true)
                }

            }
            else if (signupResponse.code == 422) {
                setMessage(signupResponse.message)

            }
        }
        else if (response.code == 200) {
            setMessage(response.message)
        }
    }
    const handleOnchangePhone = (phone, country, e) => {
        let name = "phone"
        let country_code = `+${country.dialCode}`
        let country_name = country.countryCode
        setCountrycode(country_code)
        setPhone(phone)

    }


    return (
        <Fragment>

            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={isOpen}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                    <div className={styles.registrationSec}>
                        <button className="float-right close mr-0 mt-0 ">
                            <span>x</span>
                        </button>
                        <form>
                            <div>
                                <h5>Registration</h5>
                                <p>Please enter your details exactly as it appears on your ID</p>
                            </div>
                            <div class="row">
                                <div className="col-md-12">
                                    <ul className={styles.progressBar}>
                                        <li className={styles.Active}></li>
                                        <li className={styles.Active}></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <h4 style={{ color: "#b2020f" }}>{message}</h4>

                            {/* ------------------Step two---------- */}
                            <div id="step2">
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>First Name</label>
                                    <input type="text" id="fname" name="fname"
                                        placeholder="First Name" value={fname}
                                        onChange={(e) => { setUser((prev) => { return { ...prev, fname: e.target.value } }) }}
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Last Name</label>
                                    <input type="text" id="lname" name="lname"
                                        placeholder="Last Name" value={lname}
                                        onChange={(e) => { setUser((prev) => { return { ...prev, lname: e.target.value } }) }}
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Date Of Birth</label>
                                    {
                                        id == "saudi" ?
                                            <HijriDatePicker
                                                selectedDate={dob}
                                            />

                                            :
                                            <DatePicker
                                                className="form-control"
                                                name="dob"
                                                selected={dob ? new Date(dob) : null}
                                                dateFormat="MM/dd/yyyy"
                                                dropdownMode="scroll"
                                                showYearDropdown={true}
                                                dateFormatCalendar="MMMM"
                                                yearDropdownItemNumber={15}
                                                scrollableYearDropdown={true}
                                                locale={locale.locale}
                                                onChange={date => { setDob(date); isDisabledChecked() }}
                                                autoComplete="off"
                                                maxDate={moment().subtract(16, 'years')._d}

                                            />
                                    }

                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup} ${styles.phoneInput}`}>
                                    <label>Phone Number</label>
                                    <PhoneInput
                                        inputClass="form-control"
                                        country="sa"
                                        value={phone}
                                        placeholder="phone*"
                                        onChange={(phone, country_name) => { handleOnchangePhone(phone, country_name); isDisabledChecked() }}
                                        prefix="+"
                                        copyNumbersOnly={false}
                                        countryCodeEditable={false}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Email</label>
                                    <input type="text" id="email" name="email"
                                        placeholder="email" value={email}
                                        onChange={(e) => {
                                            setEmail(e.target.value); validForm("email", e.target.value);
                                            isDisabledChecked()
                                        }}
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Passward </label>
                                    <input type="password" id="pass" name="pass"
                                        placeholder="Passward" value={password.pass}
                                        onChange={(e) => {
                                            setPassword((prev) => { return { ...prev, pass: e.target.value } });
                                            validForm("pass", e.target.value); isDisabledChecked()
                                        }}
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Confirm Password</label>
                                    <input type="password" id="confirmPass" name="confirmPass"
                                        placeholder="Confirm Password" value={password.confirmPass}
                                        onChange={(e) => {
                                            setPassword((prev) => { return { ...prev, confirmPass: e.target.value } });
                                            validForm("confirmPass", e.target.value); isDisabledChecked()
                                        }}
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                    <span style={{ color: "#b2020f" }}>{password.confirmPass && password.pass !== password.confirmPass ? "password didn't match" : ""}</span>
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Referral Code <span>(Option)</span></label>
                                    <input type="text" id="contact" name="contact"
                                        placeholder="Referral Code"
                                        className={`form-control ${styles.stepbarInput}`}
                                    />
                                </div>
                                <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                    <label>Gender</label>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className={styles.radio}>
                                                <input id="male" name="radio"
                                                    onChange={(e) => {
                                                        setGender(e.target.id);
                                                        isDisabledChecked()
                                                    }} type="radio" checked={gender == "male"} />
                                                <label for="male" className={styles.radioLabel}>Male</label>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className={styles.radio}>
                                                <input id="female" name="radio" checked={gender == "female"}
                                                    onChange={(e) => {
                                                        setGender(e.target.id);
                                                        isDisabledChecked()

                                                    }} type="radio" />
                                                <label for="female" className={styles.radioLabel}>Female</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button onClick={(e) => { handleNext(e); }} className={styles.sendBtn} disabled={!isDisable}>
                                            Next
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
            {Open && <SignUpStepThree isOpen={Open} />}
        </Fragment>

    )
}

export default SignUpStepTwo;