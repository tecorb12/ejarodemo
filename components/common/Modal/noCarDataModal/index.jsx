
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import style from "../../../../styles/customeModal.module.scss"

const NoCar = ({ className, styles, text, text1 }) => {

    const [open, setOpen] = useState(false)
    const [selectedDate, setDate] = useState("")
    const [id, setId] = useState("saudi")
    const openModal = (e, data) => {
        console.log("hit")

        e.preventDefault()
        setDate(data)
        setOpen(true)
    }
    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
    }


    return (
        <Fragment>
            <div className={styles.formGroup} onClick={(e) => { openModal(e, "howItWorks") }}>
                <h6>How it works</h6>
                <span >
                    <img
                        src="/images/others/icon/garage/how_it_works.svg"
                        alt=""
                    />
                    List your vehicle for free on the first license peer-to-peer vehicle
                    sharing platform in the GCC</span
                >
                <span className={`${styles.floatRight} float-right`}>
                    <img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>
            <div className={styles.formGroup} onClick={(e) => { openModal(e, "Insurance") }}>
                <h6>Insurance</h6>
                <span>
                    <img
                        src="/images/others/icon/garage/insurance.svg"
                        alt=""
                    />
                    Ejaro offers the first peer-to-peer insurance policy in the Kingdom to
                    cater to your needs</span
                >
                <span className={`${styles.floatRight} float-right`}>
                    <img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>
            <div className={styles.formGroup} onClick={(e) => { openModal(e, "Safety") }} >
                <h6 >Safety & Security</h6>
                <span>
                    <img
                        src="/images/others/icon/garage/safety_security.svg"
                        alt=""
                    />
                    We vet all users and vehicles within our community for your peace of
                    mind</span
                >
                <span className={`${styles.floatRight} float-right`}>
                    <img
                        src="/images/others/icon/carProfile/red_slider.png"
                        alt=""
                    /></span>
            </div>
            <ReactModal
                className={`${style.customeModal} ${style.carProfileModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            > 
          
                {
                    selectedDate == "howItWorks" &&
                    <div className={style.dialogBox}>
                             { console.log("Safety")}
                        <div className={style.imgArea}>
                            <img src="/images/others/icon/garage/vehicle_hosting.png" />
                        </div>
                        <h2>Vehicle Hosting Criteria</h2>
                        <p>List your vehicle for free on the first licenced peer-to-peer vehicle sharing platform in the GCC</p>
                        <div className={style.instruction}>
                            <ul>
                                <li>You must be a saudi citizen to host a vehicle in the kingdom</li>
                                <li>Your vehicle must not be older then five year</li>
                                <li>Your vehicle odometer mucst not exceed 120,000 KM</li>
                                <li>Your vehicle must have a Comprehensive Insurance</li>
                                <li>Ypu must fully own the vehicle(to be able to authorise the guest)</li>
                                <li>You must have  a valid Abser account without a traffic violations</li>
                            </ul>
                        </div>
                        <h2 >Earnings</h2>
                        <p>Sit back and watch the earnings roll in</p>
                        <div className={style.instruction}>
                            <ul>
                                <li>You will earn 80% of each rental</li>
                                <li>Recive your earnings every 2 weeks</li>
                                <li>Monitor your income</li>
                                <li>Refer a user to Ejaro and earn 25 SR after their first Rental</li>
                            </ul>
                        </div>
                    </div>
                }
                {
                    selectedDate == "Insurance" &&
                    <div className={style.dialogBox}>
                        <div className={style.imgArea}>
                            <img src="/images/others/icon/garage/insurance.png" />
                        </div>
                        <h2>Insurance options</h2>
                        <p>Select your suitable coverage</p>
                        <div className={style.instruction}>
                            <ul>
                                <li>Ejaro's exclusive peer-to-peer insurance policy in partnership with Medgulf</li>
                                <li>Choose your own insurance provider from our list of registerd companies</li>
                            </ul>
                        </div>
                        <h2>Coverage</h2>
                        <p>Ejaro offers the first peer-to-peer  Insurance policy In the Kingdome to cater to you needs</p>
                        <div className={style.instruction}>
                            <ul>
                                <li>Comprehensive Motor Insurance</li>
                                <li>Compensation for loss or damage to the insured car</li>
                                <li>Compensation for loss or damage to a third party vehicle</li>
                                <li>Cover the expenses of treatment, hospital and medicines for the driver , passengers or third party</li>
                                <li>Physical compensation in case of disability or death of the driver, passengers or third party</li>
                                <li>Cover any damage to third party properly</li>
                            </ul>
                        </div>
                    </div>
                }
                {
                    selectedDate == "Safety" &&
                    <div className={style.dialogBox}>
                        <div className={style.imgArea}>
                            <img src="/images/others/icon/garage/safety_icon.png" />
                        </div>
                        <h2 class="title">Safety & Security</h2>
                        <p>We vet all user and vehicles within our community for your peace of mind</p>
                        <div className={style.infoBlock}>
    <label for="">Licensed by</label>
    <div className={style.infoimg}>
        <img src="/images/others/icon/garage/tutorial/transport_general_authority@3x.png" alt="" />
    </div>
    <p>The first licenced peer-to-peer vehicle sharing community in the kingdom of Saudi Arabia</p>
</div> 
<div className={style.infoBlock}>
    <label for="">Governmental Vetting</label>
    <div className={style.infoimg2}>
        <img src="/images/others/icon/garage/tutorial/police_authority@3x.png" alt="" />
        <img src="/images/others/icon/garage/tutorial/elm_icon@3x.png" alt="" />
        <img src="/images/others/icon/garage/tutorial/policy_icon@3x.png" alt="" />
    </div>
    <p>Via integration with our system  pertners, Ejaro proudly has a reliable and secure data infastructure</p>
</div>
<div className={style.infoBlock}>
    <label for="">Roadside Assistance </label>
    <div className={style.infoimg}>
        <img src="/images/others/icon/garage/tutorial/morni_icon@3x.png" style={{maxWidth : "65px"}} alt="" />
    </div>
    <p>We've partnered with Morni for 24/7 roadside assistance to serve you incase of troubles </p>
</div>
<div className={style.infoBlock}>
    <label for="">Secure payment </label>
    <div className={style.infoimg3}>
        <img src="/images/others/icon/garage/tutorial/mastercard@3x.png" alt="" />
        <img src="/images/others/icon/garage/tutorial/visa@3x.png" alt="" />
        <img src="/images/others/icon/garage/tutorial/cada@3x.png" alt="" />
    </div>
    <p>Secure Payment gateway insuring safe & transaction</p>
</div>
                    </div>
                }

            </ReactModal>
        </Fragment>

    )
}

export default NoCar;