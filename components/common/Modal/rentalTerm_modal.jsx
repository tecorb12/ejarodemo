
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"

const RentalTerm = ({ className, style }) => {

    const [open, setOpen] = useState(false)
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)

    }
    return (
        <Fragment>
            {/* <button onClick={openModal} className={className}>
            <img src="/images/others/icon/sidebar/invite_earn.png"  alt=""  style={{width: "14px", marginRight: "15px"}}/>
               
                Invite & Earn</button> */}
            <div onClick={openModal} className={style}>
                <h4> Rental Terms
                                <img src="/images/others/icon/carProfile/red_slider.png" alt="" className="float-right" />
                </h4>
            </div>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeSignUpModal}`}
                isOpen={open}
                style={
                    { overflowY: "scroll" }}
            >


                <div className={styles.dialogBox}>
                    <div className={`${styles.registrationSec} ${styles.onlyHeading}`}>
                        {/* <button onClick={closeclick} className="float-right close mr-0 mt-0 ">
                            <span>x</span>
                        </button> */}
                        <form>
                            <div className="row">
                                <div className="col-12">
                                    <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.colseBtn}`}>
                                        <span>x</span>
                                    </button></div>
                                <div className={`col-12 text-center ${styles.HeadingSection}`}>
                                    <h5 className="text-center mb-0">RENTAL TERMS</h5>
                                    <p>To ensure swift and Easy trip please make sure you follow the rental terms set out by the host.Enjoy your trip </p>
                                      </div>
                               
                            </div>
                            <div className={`row  mt-md-4 ${styles.content}`}>
                                <div className="col-md-12">
                                    <ul>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/arrive_on_time.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p>Arrive on time</p>
                                            </div>
                                        </li>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/bring_your_license.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p>Bring your license</p>
                                            </div>
                                        </li>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/no_smoking.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p className="ng-star-inserted">No smoking</p>
                                            </div>
                                        </li>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/keep_the_vehicle_clean.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p>Keep the vehicle clean</p>
                                            </div>
                                        </li>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/km_icon.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p>850 KM included</p>
                                            </div>
                                        </li>
                                        <li className="row">
                                            <div className="col-2">
                                                <img src="/images/others/icon/modal/refuel_icon.svg" alt="" />
                                            </div>
                                            <div className="col-10">
                                                <p>Refuel with Petrol 91</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                          
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default RentalTerm;