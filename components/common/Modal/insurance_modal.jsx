
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import HijriDatePicker from "../../Auth/demo/app";

const InsuranceModal = ({ className, style, text, text1 }) => {

    const [open, setOpen] = useState(false)
    const [selectedDate, setDate] = useState("")
    const [id, setId] = useState("saudi")
    const openModal = (e) => {
        e.preventDefault()
        setOpen(true)
    }
    const closeModal = (e) => {
        e.preventDefault()
        setOpen(false)
    }


    return (
        <Fragment>
            <button onClick={openModal} className={style.sendBtn1}>
                {text}
                <span className={style.floatRight}> {text1}
                </span></button>
                    {/* <div className={style.formGroup} onClick={openModal}>
                                <span >
                                    <img
                                        src="/images/others/icon/license_plate.svg"
                                        alt=""
                                    />
                                   License Plate Number
                              </span>
                                <span className={`${style.floatRight} float-right`}>
                                    <img
                                        src="/images/others/icon/carProfile/red_slider.png"
                                        alt=""
                                    /></span>
                            </div> */}
            <ReactModal
                className={`${styles.customeModal} ${styles.carProfileModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>
                  <div className={styles.imgArea}>
                      <img src="/images/others/icon/garage/insurance.png" />                      
                  </div>
                  <h2>Insurance options</h2>
            <p>Select your suitable coverage</p>
            <div className={styles.instruction}>
                <ul>
                    <li>Ejaro's exclusive peer-to-peer insurance policy in partnership with Medgulf</li>
                    <li>Choose your own insurance provider from our list of registerd companies</li>
                </ul>
            </div>
            <h2>Coverage</h2>
            <p>Ejaro offers the first peer-to-peer  Insurance policy In the Kingdome to cater to you needs</p>
            <div className={styles.instruction}>
                <ul>
                    <li>Comprehensive Motor Insurance</li>
                    <li>Compensation for loss or damage to the insured car</li>
                    <li>Compensation for loss or damage to a third party vehicle</li>
                    <li>Cover the expenses of treatment, hospital and medicines for the driver , passengers or third party</li>
                    <li>Physical compensation in case of disability or death of the driver, passengers or third party</li>
                    <li>Cover any damage to third party properly</li>
                </ul>
            </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default InsuranceModal;