
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"
import { forget_password_service } from "../../helpers/auth_services";
import { validateEmail } from "../validation";
import Login from "./signin";
const Forgot = ({ className, style, close }) => {
    const [email, setEmail] = useState("")
    const [open, setOpen] = useState(false)
    const [isDisabled, setDisabled] = useState(false)

    const [isError, setError] = useState({
        emailErrorMsg: "", isDisabledEmail: false,
        code: "", msg: "",
    })
    const openModal = () => {
        setOpen(true)
    }

    const closeModal = () => {
        setOpen(false)

    }
    const handleClick = async (e) => {
        e.preventDefault()
        const response = await forget_password_service(email)
        if (response.code == 200) {
            setOpen(false)
            close(false)
            global.forget = true
        }
    }
    const validForm = (fieldName, Value) => {
        let EmailPhoneErrorMsg = isError.emailErrorMsg
        let isDisabledEmail_phone = isError.isDisabledEmail
        switch (fieldName) {
            case "email":
                EmailPhoneErrorMsg = validateEmail(Value).error
                isDisabledEmail_phone = validateEmail(Value).isDisabled
                break;

            default:
                break;
        }
        setError((prev) => { return { ...prev, isDisabledEmail: isDisabledEmail_phone } })
        if (isDisabledEmail_phone) {
            setDisabled(true)
        }
    }

    return (
        <Fragment>
            <a onClick={openModal} className={className} style={{ textAlign: "right" }}>
                <span className={style.floatRight}>
                </span>
                Forgot your Password ?
            </a>

            <ReactModal
                className={`${styles.customeModal} ${styles.customeLoginModal}`}
                isOpen={open}
                onRequestClose={closeModal}
                style={
                    { overflowY: "scroll" }}
            >
                <div className={styles.dialogBox}>

                    <div className={styles.loginSec}  >

                        <div className="row mx-0">
                            <div className={`col-md-12 col-lg-12 ${styles.loginPadding}`} style={{ paddingTop: "3rem" }}>
                                <button onClick={closeModal} className={`float-right close mr-0 mt-0 ${styles.close}`}>
                                    <span>x</span>
                                </button>
                                <h3 className="mb-4" style={{ color: "#b2020f" }}>Forgot Your Password</h3>
                                <h6 className="mb-4">Enter your email address and we will send
                                    you a link to reset your password </h6>

                                <form>
                                    <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                        {/* 
                                        <p style={{ color: "#b2020f" }}>{isError.code == 404 && isError.msg}</p>
                                        <p style={{ color: "#b2020f" }}>{isError.code == 401 && isError.msg}</p> */}

                                    </div>
                                    <div className={`form-group px-0 ${styles.customeFormGroup} mb-3`}>
                                        <label>Email Address</label>
                                        <input type="text" id="contact" name="email" value={email}
                                            placeholder="Enter email address" onChange={(e) => { setEmail(e.target.value); validForm("email", e.target.value) }}
                                            className={`form-control ${styles.stepbarInput}`}
                                        />
                                        <img src="/images/others/icon/email_icon.svg" style={{ width: "10px" }} />
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <button disabled={!isDisabled} className={styles.sendBtn} onClick={handleClick}>
                                                SEND
                                            </button>
                                        </div>
                                        <div className="col-12">
                                            <Login loginstyle={`${style.sendBtn1}`} text="Login" text1="Register Now" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div className={styles.smallHide}>
                            <div className={styles.bannerSec}>
                                <div className={styles.detailText}>
                                    <h6> Share your car and earn
                                        <br></br>
                                        an aditional income with ejaro
                                    </h6>
                                </div>
                                <div className={styles.detailTextFooter}>
                                    <h6>A Trustworthy Community</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ReactModal>
        </Fragment>

    )
}

export default Forgot;