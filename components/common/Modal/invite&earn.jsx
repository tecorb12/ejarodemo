
import { Fragment, useState } from "react";
import ReactModal from "react-modal";
import styles from "../../../styles/customeModal.module.scss"

const InviteAndEarn = ({  className,style }) => {

    const[open, setOpen] = useState(false)
const openModal=()=>{
  setOpen(true)
}

const closeModal=()=>{
  setOpen(false)

}
    return (
        <Fragment>
            <button onClick={openModal} className={className}>
            <img src="/images/others/icon/sidebar/invite_earn.png"  alt=""  style={{width: "14px", marginRight: "15px"}}/>
               
                Invite & Earn</button>
            <ReactModal
                className={`${styles.customeModal} ${styles.customeMoneyModal}  ${styles.customeAccountModal}`}
                isOpen={open}
                style={
                    { overflowY: "scroll" }}
            >
               
                <div className={styles.genrateLink}>
                    <div className={styles.genrateLinkSec}>
                        <form>

                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <h3>Template Type</h3>
                                <select
                                    class="form-control"
                                    name="maritalStatus"
                                    size="0">
                                    <option value="01">Mr.</option>
                                    <option value="02">Mrs.</option>
                                </select>
                            </div>
                            <div className={`form-group px-0 ${styles.customeFormGroup}`}>
                                <h3>Template Title</h3>
                                <input type="text" id="contact" name="contact"
                                    placeholder="Plase enter Mobile Number"
                                    className={`form-control ${styles.stepbarInput}`}
                                />
                            </div>
                            <div class="row"><div class="col-12">
                                <button className={styles.sendBtn}>
                                    Save
                                </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </ReactModal>
        </Fragment>

    )
}

export default InviteAndEarn;