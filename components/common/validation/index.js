import React from "react";

const style = ({
    paddingTop: "6px",
    paddingBottom: "6px",
    fontSize: "12px"
})
const style_newsletter = ({
    paddingTop: "3px",
    paddingBottom: "3px",
    fontSize: "12px"
})
const alert = "alert alert-login"

export const validateName = (name) => {
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name?.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Name.NameError };
    }
    else if (!nameRegex.test(name)) {
        return { isDisabled: false, error: "" };
    }
    else if (name.length < 2) {
        return { isDisabled: false, error: ErrorMessage.Auth.Name.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateAccountNo = (name) => {
    let nameRegex = /^\w{1,17}$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.AccountNumber.NameError };
    }
    else if (!nameRegex.test(name)) {
        return { isDisabled: false, error: "" };
    }
    else if (name.length < 2) {
        return { isDisabled: false, error: ErrorMessage.Auth.AccountNumber.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateSuiteName = (name) => {
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Suite.NameError };
    }
    else if (!nameRegex.test(name)) {
        return { isDisabled: false, error: "" };
    }
    else if (name.length < 2) {
        return { isDisabled: false, error: ErrorMessage.Auth.Suite.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateLastName = (name) => {
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.LastName.NameError };
    }
    else if (!nameRegex.test(name)) {
        return { isDisabled: false, error: "" };
    }
    else if (name.length < 2) {
        return { isDisabled: false, error: ErrorMessage.Auth.Name.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateLocation = (name) => {
    let nameRegex = /^[-_ a-zA-Z0-9]+$/;
    name = name.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Location.NameError };
    }
    else if (!nameRegex.test(name)) {
        return { isDisabled: false, error: "" };
    }
    else if (name.length < 2) {
        return { isDisabled: false, error: ErrorMessage.Auth.Location.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}

export const validatetaskdescription = (name) => {
    let nameRegex = /(?=.*[0-9])*[A-Za-z\\s][A-Za-z0-9\\]$/;
    let notonlynumber_regex = /^[-_ a-zA-Z0-9]+$/
    //  /^[ A-Za-z0-9_@./#&+-]*$/;
    name = name?.trim();
    if (name === "" || name === undefined || name === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Name.NameError };
    }
    // else if (!nameRegex.test(name)) {
    //     return { isDisabled: false, error: "please enter valid description" };
    // }
    else if (name.length < 25) {
        return { isDisabled: false, error: ErrorMessage.Auth.Name.FirstNameError }
    }
    else {
        return { isDisabled: true, error: '' };
    }
}

export const validateEmail = (email) => {
    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    email = email.trim();
    if (email === "" || email === undefined || email === null) {
        return { isDisabled: false, error: "" };
    }
    else if (!emailRegex.test(email)) {
        return { isDisabled: false, error: "" };
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validatePassword = (password) => {
    let passwordRegex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!@^#$%&*? "])[a-zA-Z0-9^!@#$*%&?]{6,20}$/

    password = password.trim();
    if (password === "" || password === undefined || password === null) {
        return { isDisabled: false, error: ""};
    }
  
    else if (password.length < 7) {
        return { isDisabled: false, error: "Error" };
    }
    else {
        return { isDisabled: true, error: '' }
    }
}

export const validateAddress = (address) => {
    address = address.trim();
    if (address === "" || address === undefined || address === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Addresss.NameError };
    }

    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateEmailPhone = (email_phone) => {

    // let emailPhoneRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let emailPhoneRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/;

    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let PhoneNumberRegex = /^([0-9]{10})+$/;
    email_phone = email_phone.trim();
    if (email_phone === "" || email_phone === undefined || email_phone === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
    }
    else if (email_phone.length <= 10) {

        if (!PhoneNumberRegex.test(email_phone)) {


            return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneNumberError };
        }
        else if (!emailPhoneRegex.test(email_phone)) {


            return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError };
        }
        else {


            return { isDisabled: true, error: "" };

        }

    }
    else if ((email_phone.length >= 11) && !emailPhoneRegex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     console.log("Valuu >=  10 check Email")

        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError };
        // }
        // else {


        return { isDisabled: false, error: "Please Enter Valid Phone Number...." };

        // }
    }
    else if (!emailPhoneRegex.test(email_phone)) {

        return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError };
    }
    // else if (!PhoneNumberRegex.test(email_phone)) {
    //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneValidError };
    // }

    else {

        return { isDisabled: true, error: '' };
    }
}
export const validateOtp = (otp) => {
    otp = email_phone.trim();
    if (otp === "" || otp === undefined || otp === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
    }
    // else if (!emailPhoneRegex.test(email_phone)) {
    //     return { isDisabled: false, error: "Please enter valid Email ID." };
    // }
    else if (password.length === 8) {
        return { isDisabled: false, error: ErrorMessage.Auth.Password.PasswordLengthError };
    }
    else {
        return { isDisabled: true, error: '' };
    }
}
export const validateQuessionierField = (input) => {
    input = input.trim();
    if (input === "" || input === undefined || input === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.QuessionierField.inputField };
    }
    else {
        return { isDisabled: true, error: '' };
    }
}

// export const validateNumber = (number) => {
//     // let numberRegex = /\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?/;

//     // let numberRegex = /^[1-9][0-9]{9,12}$/;
//     number = number.trim();
//     if (number === "" || number === undefined || number === null) {
//         return { isDisabled: false, error: Please enter Phone number ID. };
//     }
//     // else if (!numberRegex.test(number)) {
//     //     return {  isDisabled: false, error: Please enter valid Phone Number. };
//     // }
//     else {
//         return { isDisabled: true, error: '' };
//     }
// }

// export const UnAuthorizedAccess = (code) => {
//     // code = license.trim();
//     if (code === 404) {
//         return {
//             error:                 User does not access

//         }
//     }
//     else {
//         return { error: '' }
//     }
// }


export const validateDatepicker = (date) => {
    // let passwordRegex = /^ (?=^.{8,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    // date = date.trim();

    if (date === "" || date === undefined || date === null) {
        return { status: false, isDisabled: false, error: <div className={alert} style={style}>Please enter valid date.</div> }
    }

    else {
        return { status: true, isDisabled: true, error: '' }
    }
}
export const validateLocationAndFlat = (value) => {
    // let passwordRegex = /^ (?=^.{8,16}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    // date = date.trim();

    if (value < 4 || value === undefined || value === null) {
        return { status: false, isDisabled: false, error: <div className={alert} style={style}>Please enter this field</div> }
    }

    else {
        return { status: true, isDisabled: true, error: '' }
    }
}
export const validatePhone = (phone,coutry_code) => {
    // date = date.trim();
    // let phone_regex = /^[789]\d{9}$/;
    let phone_regex= /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/

    if (phone === "" || phone === undefined || phone === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneError }
    }
    else if (!phone_regex.test(phone)) {
        return { isDisabled: false, error: "Please enter valid number." };
    }
    else if (phone.length < 10) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneInvalidError };
    }
    else {
        return { isDisabled: true, error: '' }
    }
}
export const validateAccountNumber = (phone) => {
    // date = date.trim();
    let phone_regex = /^(\d{3}|\d{4})[-](\d{5})$/;

    if (phone === "" || phone === undefined || phone === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneError }
    }
    else if (!phone_regex.test(phone)) {
        return { isDisabled: false, error: "Please enter valid Account." };
    }
    else if (phone.length < 10) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneInvalidError };
    }
    else {
        return { isDisabled: true, error: '' }
    }
}
export const validateComment = (comment) => {
    comment = comment?.trim();
  let  comments = comment?.split(" ")
    let swear = [
        'arse',
        'ass',
        'asshole',
        'bastard',
        'bitch',
        'bollocks',
        'bugger',
        'bullshit',
        'crap',
        'damn',
        'frigger',
        'fuck','Fuck',
        'suck','Suck'
    ]
    let phone_regex = /^[0-9\b]+$/;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    var Regex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    let symbol = /.+@.+/
    // console.log(comments,"swear")
    const foundSwears = swear.filter(word => comment.includes(word));
    const filterComment = comments.filter(word => comments.includes("9999999999"))
    if (foundSwears.length) {
        return { isDisabled: false, error: "Please enter valid comment." }
    }
    else if(Regex.test(comment)){
        return { isDisabled: false, error: "Please enter valid comment." }
    }

   else if (comment === "" || comment === undefined || comment === null) {
        return { isDisabled: false, error: "Please enter  comment."}
    }
    else if (phone_regex.test(comment)) {
        return { isDisabled: false, error: "Please enter valid comment." };
    }
    else if (email.test(comment)) {
        return { isDisabled: false, error: "Please enter valid comment." };
    }
    else if(comment?.match(symbol)){
        return { isDisabled: false, error:  "Please enter valid comment."};
    }
    else if(comment?.match(phone_regex)){
        return { isDisabled: false, error:  "Please enter valid comment."};
    }
    else {
        return { isDisabled: true, error: '' } 
    }
} 
export const validateProfileDescription = (comment) => {
    comment = comment?.trim();
  let  comments = comment?.split(" ")
    let swear = [
        'arse',
        'ass',
        'asshole',
        'bastard',
        'bitch',
        'bollocks',
        'bugger',
        'bullshit',
        'crap',
        'damn',
        'frigger',
        'fuck','Fuck',
        'suck','Suck'
    ]
    let phone_regex = /^[0-9\b]+$/;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    var Regex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    let symbol = /.+@.+/
    const foundSwears = swear.filter(word => comment?.includes(word));
    if (foundSwears.length) {
        return { isDisabled: false, error: "Please enter valid data" }
    }
    else if(Regex.test(comment)){
        return { isDisabled: false, error: "Please enter valid data" }
    }

//    else if (comment === "" || comment === undefined || comment === null) {
//         return { isDisabled: false, error: "Please enter data"}
//     }
    else if (phone_regex.test(comment)) {
        return { isDisabled: false, error: "Please enter valid data" };
    }
    else if (email.test(comment)) {
        return { isDisabled: false, error: "Please enter valid data" };
    }
    else if(comment?.match(symbol)){
        return { isDisabled: false, error:  "Please enter valid data"};
    }
    else if(comment?.match(phone_regex)){
        return { isDisabled: false, error:  "Please enter valid data"};
    }
    else {
        return { isDisabled: true, error: '' } 
    }
} 
export const validateNumber = (phone) => {
    // date = date.trim();

    if (phone === "" || phone === undefined || phone === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneError }
    }
    else if (phone.length > 4) {
        return { isDisabled: false, error: ErrorMessage.Auth.Phone.PhoneInvalidError };
    }
    else {
        return { isDisabled: true, error: '' }
    }
}
export const validateBudget = (phone) => {
    // date = date.trim();

    if (phone === "" || phone === undefined || phone === null) {
        return { isDisabled: false, error: "please enter value" }
    }
    else if (phone.length > 4) {
        return { isDisabled: false, error: "not null" };
    }
    else {
        return { isDisabled: true, error: '' }
    }
}

export const validateNumberofSeeker = (phone) => {
    // date = date.trim();

    if (phone === "" || phone === undefined || phone === null) {
        return { isDisabled: false, error: "please Select the value" }
    }

    else {
        return { isDisabled: true, error: '' }
    }
}
export const validateEmailNewsletter = (email) => {
    let emailRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;

    // let emailRegex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    email = email.trim();
    if (email === "" || email === undefined || email === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.Email.EmailEmptyError };
    }
    else if (!emailRegex.test(email)) {
        return { isDisabled: false, error: ErrorMessage.Auth.Email.EmailInvalidErrors };
    }
    // else if (!/\b[\t\b]/.test(email)) {
    //     return {  isDisabled: false, error: Please enter valid Email Address. };
    // }

    else {
        return { isDisabled: true, error: '' };
    }
}


export const validateEmailPhoneDemo = (email_phone) => {
    // let emailPhoneRegex = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let email = /^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i;
    let phone_regex = /^[0-9\b]+$/;
    let emailPhoneRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/;
    let regex = "@.*[a-zA-Z]";
    let regexNumber = "^[0-9]{10}$";
    email_phone = email_phone.trim();
    let access
    if (email_phone.length === 10) {
        access = Number(email_phone)
    }



    if (email_phone === "" || email_phone === undefined || email_phone === null) {
        return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
    }

    else if (access !== NaN && access !== undefined && !phone_regex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
        // }


        return { isDisabled: false, error: "Please Enter valid number" };
    }

    else if (access !== NaN && email_phone === undefined && !phone_regex.test(email_phone)) {
        // if (!emailPhoneRegex.test(email_phone)) {
        //     return { isDisabled: false, error: ErrorMessage.Auth.EmailPhone.EmailPhoneError };
        // }


        return { isDisabled: false, error: "Please Enter valid numbersaccasc" };
    }
    
    else if (access === undefined && !email.test(email_phone)) {
        if (email_phone > 10) {
            return { isDisabled: false, error: "Please Enter valid mobile number" };
        }

        return { isDisabled: false, error: "Please Enter Valid email" };
    }

    else {
        return { isDisabled: true, error: '' };
    }
}
