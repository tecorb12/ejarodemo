import { Fragment } from "react"
import style from "../../../styles/search.module.scss"
import DatePicker from 'react-datepicker';
import { useState } from "react";

const SearchSec = ({ }) => {
    const [open, setOpen] = useState(false)
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;
    const [isStatus, setStatus] = useState(false)

    const showAddressList = () => {
        console.log(isStatus)
        setStatus(!isStatus)
    }

    return (
        <Fragment>
            <div className={`${style.searchSec}`}  >
                <form  >
                    <div className={`col-lg-12 ${style.inputArea}`}>
                        <div className={`row no-gutters ${style.single}`}>
                            <div className={`col-6 ${style.small}`}>
                                <div className={style.inputWithIcon} >
                                    <img src="/images/others/send_icon.png" />
                                    <input type="text"
                                        placeholder="Enter city, address, airport or hotel"
                                        onClick={showAddressList}
                                        formControlName="place" />

                                </div>
                              
                                    {isStatus &&
                                      <div className={style.searchSection}>
                                     <ul >
                                        <li>
                                            <img src="/images/others/icon/sidebar/send_icon.png" />
                                            Current Location</li>
                                        <li >
                                            <img src="/images/others/icon/sidebar/recent_icon.png" />
                                            Recent Search</li>

                                    </ul>
                                    </div>
                                    }
                               

                            </div>
                            <div className={`col-5 ${style.smallHide}`}>
                                <div className={style.inputWithIcon} >
                                    <img src="/images/others/calender_icon.png" />
                                    {/* <p  className=""> &amp; time</p> */}
                                    <DatePicker

                                        selectsRange={true}
                                        startDate={startDate}
                                        endDate={endDate}
                                        monthsShown={2}
                                        minDate={new Date()}
                                        placeholderText="Select dates"
                                        onChange={(update) => {
                                            setDateRange(update);
                                        }}
                                        withPortal
                                    />
                                </div>
                            </div>
                            <div className="col-1">
                                <button className={style.searchBtn} >
                                    <img
                                        src="/images/others/icon/search_icon.svg"
                                        alt=""
                                        className={style.white}
                                    />
                                    <img
                                        src="/images/others/icon/search_icon_red.png"
                                        alt=""
                                        className={style.red}
                                    />
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </Fragment>
    )
}
export default SearchSec