import {  useContext, useEffect, useState } from "react"
import styles from "../../styles/header.module.scss"
import Link from 'next/link';
import Routes from "../common/route";
import InviteAndEarn from "../common/Modal/invite&earn";
import Login from "../common/Modal/signin";
import SignUp from "../common/Modal/signup";
import { logout_service } from "../helpers/auth_services";
import AddNewCar from "../common/Modal/AddCarModal";
import { IdContext } from "../MyContext/IdDetailsContext";
import { notification_count_service } from "../helpers/service";
import SearchSec from "../common/Search";
import style from "../../styles/search.module.scss";

const Logout = ({ logout }) => {
    return (
        <li role="menuitem" onClick={logout}>
            <a className={` ${styles.dropDownItem} dropdown-item`}>
                <img className={styles.menuImg} src="/images/others/icon/sidebar/sign_out.png" alt="" />
                Sign Out
                <span className={styles.floatRight}>
                </span>
            </a>
        </li>
    )
}
const AuthenticationHeader = () => {
    return (
        <ul className={`dropdown-menu ${styles.dropdownMenu} `} aria-labelledby="exampleModal1">
            <li role="menuitem">
                <SignUp className={`${styles.dropDownItem} dropdown-item`} text="Sign up" style={styles} />
            </li>
            <li role="menuitem">
                <Login className={`${styles.dropDownItem} dropdown-item`} style={styles} text='Log in' />
            </li>
            <li role="menuitem">
                <Link href={Routes.Help.href}
                    as={Routes.Help.href}>
                    <a className={`${styles.dropDownItem}  dropdown-item`}>
                        Help
                    </a>
                </Link>
            </li>

        </ul>
    )

}

const AllPageHeader = () => {

    let token = typeof window !== "undefined" && localStorage.token
    const{userData} = useContext(IdContext)
    const[count,setCount] = useState("")
    const[currentBooking, setCurrentBooking] = useState("")

    useEffect(()=>{
        token && notificationCount()
    },[])

    const notificationCount =async()=>{
        const response = await notification_count_service()
        console.log(response)
        if(response.code==200){
            setCount(response.notificationCount)
            setCurrentBooking(response.bookingRequest)
        }
    }
    const handleLogout = () => {
        logout_service()
    }
    return (
        <div className={styles.header}>
            {/* <!-- Navigation--> */}
            <nav className={`navbar navbar-expand-lg navbar-dark fixed-top ${styles.mainNav} ${styles.mainNavFixedTop} `} >
                <a className={`navbar-brand  ${styles.navbarBrand}`} href="">
                    <img src="/images/others/ejaro_logo_text.png" alt="Ejaro" />
                </a>
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
              
                <div className={`collapse navbar-collapse ${styles.navCollapse}`} id="navbarResponsive">
                <div className={style.SeachFix} >
                        <SearchSec  style={style}/>
                    </div>
                    <ul className={`navbar-nav ml-auto ${styles.navbarNav}`}>
                        <li className={`nav-item ${styles.navItem} `}>
                           { token == undefined || token=='' ?
                            <Login className={styles} text='Share Your Vehicle' />
                            :
                            <AddNewCar text="Share Your Vehicle" style={`nav-link scroll ${styles.navLink}`} />}
                            {/* <a className={}>
                            </a> */}
                        </li>
                    </ul>
                    <div className={` ${styles.HeaderDiv} mt-2 mt-md-0 form-inline`}>
                        <div className={`${styles.navDropdown}`}>
                            <a className={`nav-link dropdown-toggle ${styles.navLink}`} id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/images/others/network_icon.png" alt="" style={{ width: "16px" }} />
                                <img src="/images/others/icon/arrow_down.png" alt="" style={{ width: "15px" }} />

                            </a>
                            <div className={`dropdown-menu ${styles.dropdownMenu}`} aria-labelledby="messagesDropdown">
                                <a className={`dropdown-item ${styles.dropdownItem}`} href="#">
                                    English
                                </a>
                                <a className={`dropdown-item ${styles.dropdownItem}`} href="#">
                                    عربى
                                </a>
                            </div>
                        </div>
                        <div className={`${styles.navProfileDropdown}`}>
                            <a className={`nav-link dropdown-toggle ${styles.navLink}`} id={token ? "exampleModal" : "exampleModal1"} href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/images/others/menu_icon.png" style={{ width: "15px", marginTop: "17px", float: "left", display: "inline-block" }} alt="" />

                                <div className={styles.profileImg}>
                                    <img alt="" src={userData?.image ? userData?.image :"/images/others/icon/userProfile.png"} />
                                </div>
                                <span className={styles.count}>1</span>

                            </a>
                            {
                                token ?


                                    <ul className={`dropdown-menu ${styles.dropdownMenu}`} aria-labelledby="exampleModal">
                                        <li role="menuitem">
                                            <Link href={Routes.myCar.SharedCar.href}
                                                as={Routes.myCar.SharedCar.as}>
                                                <a className={`${styles.bold} ${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/my_cars.png" alt="" />
                                                    My Cars
                                                    <span className={styles.floatRight}> 1<span></span>
                                                        <img src="/images/others/icon/carProfile/red_slider.png" alt="" /></span>
                                                </a>
                                            </Link>
                                        </li>
                                        <li role="menuitem">
                                            <Link href={Routes.Booking.RequestedBooking.as}
                                                as={Routes.Booking.RequestedBooking.as}>
                                                <a className={`${styles.bold} ${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/booking_icon.png" alt="" />
                                                    Bookings
                                                    <span className={styles.floatRight}> {currentBooking}<span></span>
                                                        <img src="/images/others/icon/carProfile/red_slider.png" alt="" /></span>
                                                </a>
                                            </Link>
                                        </li>
                                        <li role="menuitem">
                                            <Link href={Routes.Notification.href}
                                                as={Routes.Notification.as}>
                                                <a className={`${styles.bold} ${styles.dropDownItem}  dropdown-item`} style={{ fontFamily: "NeoSansBold !important" }}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/my_cars.png" alt="" />
                                                    Message
                                                    <span className={styles.floatRight}> {count}<span></span>
                                                        <img src="/images/others/icon/carProfile/red_slider.png" alt="" /></span>
                                                </a>
                                            </Link>
                                        </li>
                                        <li role="menuitem">
                                            <Link href={Routes.TrasctionList.href}
                                                as={Routes.TrasctionList.as}>
                                                <a className={`${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/wallet_icon.png" alt="" />
                                                    Wallet
                                                    <span className={styles.floatRight}> 21 <span>SAR</span>
                                                        {/* <img src="/images/others/icon/carProfile/red_slider.png" alt="" /> */}
                                                    </span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li role="menuitem">
                                            <Link href={Routes.FavouriteList.href}
                                                as={Routes.FavouriteList.as}>
                                                <a className={` ${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/favourite_icon.png" alt="" />
                                                    Favorites
                                                    <span className={styles.floatRight}>
                                                        {/* 21<span>SAR</span> */}
                                                        {/* <img src="/images/others/icon/carProfile/red_slider.png" alt="" /> */}
                                                    </span>
                                                </a>
                                            </Link>
                                        </li>

                                        <li role="menuitem">
                                            <InviteAndEarn className={`${styles.dropDownItem} dropdown-item`} style={styles} />
                                        </li>
                                        <li role="menuitem">
                                            <Link href={Routes.FavouriteList.href}
                                                as={Routes.FavouriteList.as}>
                                                <a className={`${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/setting.png" alt="" />
                                                    Settings

                                                </a>
                                            </Link>
                                        </li>
                                        <li role="menuitem">
                                            <Link href={Routes.Help.href}
                                                as={Routes.Help.as}>
                                                <a className={`$ ${styles.dropDownItem}  dropdown-item`}>
                                                    <img className={styles.menuImg} src="/images/others/icon/sidebar/help.png" alt="" />
                                                    Help


                                                </a>
                                            </Link>
                                        </li>

                                        <Logout logout={handleLogout} />


                                    </ul> :
                                    <AuthenticationHeader />
                            }
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}
export default AllPageHeader