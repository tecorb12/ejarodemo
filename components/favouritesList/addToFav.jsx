import React, { useEffect } from 'react'
import { useState } from 'react/cjs/react.development'
import Login from '../common/Modal/signin'
import { add_favourite_service } from '../helpers/service'

const AddToFav = ({ styles, isChecked, image_not_filled, image_filled, setIsChecked, isFav, carId }) => {
    const [token, settoken] = useState("")
    const [isFavorite, setFav] = useState(isFav)
    useEffect(() => {
        if (typeof window !== "undefined") {
            settoken(localStorage.token)
        }
    }, [])

    const handleFavClick = () => {
        add_to_fav()
    }

    const add_to_fav = async () => {
        setIsChecked(true)
        const response = await add_favourite_service(carId)
        if (response.inFav == true) {
            setIsChecked(false)
            setFav(true)
        }
        else if (response.inFav == false) {
            setIsChecked(false)
            setFav(false)
        }
    }
    return (
        <div className={styles.fav}>
            {
                token == undefined || token=='' ?
                    <Login className={styles} src="/images/others/fav.png" />
                    :
                    <button onClick={() => handleFavClick()} title="Wishlist" style={{ color: "#fff", cursor: "pointer" }}>
                        <img
                            alt=""
                            src={
                                isChecked === true ? '/images/others/spinnerloader.gif' :
                                    isFavorite === false ? image_not_filled : image_filled
                            }
                        />
                    </button>
            }
        </div>
    )
}

export default AddToFav;