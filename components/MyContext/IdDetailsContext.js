import React, { useRef, useState } from "react";
const IdContext = React.createContext();

const IdProvider = (props) => {
    const [carId, setCarId] = useState()
    const [userId, setUserId] = useState()
    const [uploadRegistration, setData] = useState()
    const [newCarData, setNewCarData] = useState()
    const [userData, setUserData] = useState()




    return (
        <IdContext.Provider
            value={{
                carId, setCarId,
                userId, setUserId,
                newCarData, setNewCarData,
                uploadRegistration, setData,
                userData, setUserData
            }}
        >
            {props.children}
        </IdContext.Provider>
    );
};


export { IdContext, IdProvider };
