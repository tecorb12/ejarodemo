import { Fragment, useContext, useEffect, useState } from "react"
import CardSideNav from "../../global_component/myCar/carSideNav"
import { myCar_details_service } from "../helpers/myCar_services"
import { IdContext } from "../MyContext/IdDetailsContext"
import styles from "../../styles/carProfile.module.scss"
import { useRouter } from "next/router"
import PickUpLocationModal from "../common/Modal/ModifyCarDetailModal/pickUpLocationModal"
import EditPriceModal from "../common/Modal/ModifyCarDetailModal/editPriceModal"
import PriceCalendar from "../common/Modal/availviltyCalendar"


const SharedCarDetails = () => {
    const router = useRouter()
    const { carId, userId } = useContext(IdContext)
    const [carDetails, setCarDetails] = useState("")


    useEffect(() => {
        vehicleDetails()
    },[router.query.id])

    const vehicleDetails = async () => {
        let car_id = router.query.id
        const response = await myCar_details_service(car_id, userId)
        console.log(response)
        setCarDetails(response.result)
    }
    let car_model= carDetails?.make?.title + " " + carDetails?.model?.title
    let car_year = carDetails?.year?.title
    let hostedName = carDetails?.user?.fname + " " + carDetails?.user?.lname
    let reviewCount= carDetails?.carReviewCount
    let carRating = carDetails?.avgCarRating
    let rental = carDetails?.totalTrips
    let plateNo = carDetails?.lcPlateNumber
    let car_id = carDetails?.id
    return (
        <Fragment>
            <section className={`${styles.carProfileSec} user`}>

                <div className={styles.containerMd}>
                    <CardSideNav shareCount={global.share} parkedCount={global.parkedCount}/>
                    <div className={`col ${styles.smallCol}`}>
                        <div className={styles.vehicleDetail}>

                            <div className={`${styles.vehicleImg}  mb-2`} style={{backgroundImage : `url(${carDetails?.image})`}}>  

                                <div className={`${styles.profileHeader} text-center`} >
                                    <h2 >
                                        <img src="/images/others/icon/carProfile/back_icon.svg" alt="" className="float-left" />
                                        <img src="/images/others/icon/carProfile/share_icon.svg" alt="" className="float-right" />
                                    </h2>
                                </div>
                                <div className={`${styles.editImg}`} onClick={() => { router.push(`/sharedCar/vehiclePhoto?id=${router.query.id}`) }}>
                                    <label for="editImg">
                                        <img src="/images/others/icon/edit_camera.png" alt="" style={{ marginTop: "-1px", marginRight: "5px" }} />Edit photos
                                 </label>
                                </div>
                            </div>
                            <div className={styles.carInfo}>
                                <div className="row">
                                    <div className="col-md-12 clearfix">
                                        <h4 > {car_model} <span > {car_year} </span>
                                            <p > {carRating} <img src="/images/others/superhost_star.png" alt="" />
                                            </p>
                                        </h4>
                                        <p className="d-inline">
                                            HOSTED BY <span >{hostedName}</span>
                                        </p>
                                        <div className={`${styles.reviewSec} float-right`}>
                                            <ul>
                                                <li>
                                                    <span >{rental} Rentals </span>
                                                </li>
                                                <li>
                                                    <span >{reviewCount} Reviews</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.formGroup}>
                                <span >
                                    <img
                                        src="/images/others/icon/license_plate.svg"
                                        alt=""
                                    />
                                   License Plate Number
                              </span>
                                <span className={`${styles.floatRight} float-right`}>
                                   <b>{plateNo}</b> 
                                    </span>
                            </div>
                            <PickUpLocationModal style={styles} type="carDetail"/>
                           {router.query.id && <PriceCalendar  style={styles} carId={router.query.id}/>}
                            <EditPriceModal style={styles} carId={router.query.id}/>
                            <div className={styles.formGroup} onClick={() => { router.push(`/sharedCar/extraVehicleDetails?id=${router.query.id}`) }}>
                                <span>
                                    <img src="/images/others/icon/extra_details.svg" alt="" />
                                    Extra details
                                 </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    <img
                                        src="/images/others/icon/carProfile/red_slider.png"
                                        alt=""
                                    /></span>
                            </div>
                            <div className={styles.formGroup} onClick={() => { router.push(`/sharedCar/vehicleIsurance?id=$${router.query.id}`) }}>
                                <span>
                                    <img src="/images/others/icon/vehicle_insurance.svg" alt="" />
                                    Vehicle insurance
                                 </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    <img
                                        src="/images/others/icon/carProfile/red_slider.png"
                                        alt=""
                                    /></span>
                            </div>
                            <div className={styles.formGroup} onClick={() => { router.push(`/sharedCar/vehicleEarning?id=${router.query.id}`) }}>
                                <span>
                                    <img src="/images/others/icon/vehicle_earnings.svg" alt="" />
                                    Vehicle earnings
                                 </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    <img
                                        src="/images/others/icon/carProfile/red_slider.png"
                                        alt=""
                                    /></span>
                            </div>
                            <div className={styles.formGroup} onClick={() => { router.push(`/sharedCar/vehicleReview?id=${router.query.id}`) }}>
                                <span>
                                    <img src="/images/others/icon/reviews_icon.svg" alt="" />
                                    Reviews
                                 </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    <img
                                        src="/images/others/icon/carProfile/red_slider.png"
                                        alt=""
                                    /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default SharedCarDetails