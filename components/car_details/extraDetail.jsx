import Router from "next/router"
import { Fragment, useContext, useEffect, useState } from "react"
import CardSideNav from "../../global_component/myCar/carSideNav"
import styles from "../../styles/carProfile.module.scss"
import ExtradeatilModal from "../common/Modal/ExtradeatilModal"
import { useRouter } from 'next/router';
import { car_extra_details_service, update_extra_details_service } from "../helpers/modify_car_details_service"
import {  finanace_list_service, preload_service } from "../helpers/new_vehicle_service"

const ExtraCarDetails = () => {
    const { query,push } = useRouter()
    const [year, setYear] = useState("")
    const [makingCompany, setMaking] = useState("")
    const [model, setModal] = useState("")
    const [transmission, setTransmission] = useState("")
    const [fuelType, setFuelType] = useState({
        id:"",
        label:""
    })
    const [mileage, setMileage] = useState("")
    const [doors, setDoors] = useState("")
    const [seat, setSeats] = useState("")
    const [ownerShip, setOwnership] = useState("")
    const[financeName, setFinance] =useState("")
    const [companyList, setCompanyList] = useState([])
    const [isStatus, setStaus] = useState(false)
const[description, setDescription] = useState("")
const [preloadData, setPreloadData]= useState({
    fuel:[],
    odometer:[]
})
    const [TripDays, setTripDays] = useState({
        min: "",
        max: "",
        maxNoticeHours: ""
    })


    useEffect(() => {
        extraDeatils()
        car_preloads()
    }, [query])

    const car_preloads = async () => {
        const response = await preload_service()
        if (response.code === 200 ) {
            setPreloadData((prev)=>{return{...prev,fuel:response.fuelTypes,odometer:response.odometerOptions}})
           let res = await finanace_list_service()
           if(res.code==200){
               setCompanyList(res.company_list)
           }
           
        }
    }
    const toggleChecked = () => {
        setStaus((prev) => {
            return (!prev)
        })


    }

  

    const extraDeatils = async () => {
        let car_id = query.id
        const response = await car_extra_details_service(car_id)
        if (response.code == 200) {
            setYear(response.car.year.title)
            setMaking(response.car.make.title)
            setModal(response.car.model.title)
            setTransmission(response.car.transmission)
            setFuelType((prev)=>{return{...prev,label:response.car.withFuelType, id:response.car.fuelTypeId}})
            setMileage(response.car.odometer)
            setDoors(response.car.doors)
            setSeats(response.car.seats)
            setOwnership(response.car.ownership)
            setFinance(response.car.finance)
            setTripDays((prev) => { return { ...prev, min: response.car.minTripDays, max: response.car.maxTripDays, maxNoticeHours: response.car.maxNoticeHours } })
        }
    }
    const updateCarData=async(e)=>{
        e.preventDefault()
        let car_id = query.id
        const response = await update_extra_details_service(car_id,seat,doors,ownerShip,financeName,"",fuelType.id,mileage,TripDays.maxNoticeHours,transmission,TripDays.min,
        TripDays.max,isStatus,description)
        if(response.code==200){
            push(`/sharedCar/vehicleDetails?id=${car_id}`)

        }
    }
    return (
        <Fragment>
            <section className={`${styles.carProfileSec} user`}>

                <div className={styles.containerMd}>
                    <CardSideNav shareCount={global.share} parkedCount={global.parkedCount} />
                    <div className={`col ${styles.smallCol}`}>
                        <div className={styles.vehicleDetail}>

                            <div className={styles.detailHeader}>
                                <h2 >
                                    <img src="/images/others/icon/carProfile/back_icon.svg" alt="" className="float-left" onClick={() => { Router.push("/sharedCar/vehicleDetails") }} style={{ cursor: "pointer" }} />EXTRA DETAILS
                                </h2>
                            </div>
                            <div className={`${styles.formGroup} ${styles.formGroup2}`}>
                                <span >Vehicle Details
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                </span>
                            </div>
                            <div className={styles.formGroup}>
                                <span className={`pl-0`} >
                                    Year
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    {year}</span>
                            </div>
                            {/* <PickUpLocationModal style={styles} /> */}

                            <div className={styles.formGroup}>
                                <span className={`pl-0`} >
                                    Make
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    {makingCompany}</span>
                            </div>

                            {/* <EditPriceModal style={styles} /> */}
                            <div className={styles.formGroup} >
                                <span className={`pl-0`} >
                                    Model
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    {model}</span>
                            </div>

                            <ExtradeatilModal style={styles}
                                text="Transmission" headingText="Transmission" carId={query.id}
                                label="Transmission" setTransmission={setTransmission} isStatus={isStatus}
                                selectedText={transmission == "automatic" ? "Automatic" : "Manual"} />

                            <ExtradeatilModal style={styles} text="Fuel Type"
                             headingText="Fuel" label="Fuel Type" setFuelType={setFuelType}
                             fuel={preloadData.fuel} selectedText={fuelType.label} id={fuelType.id} />

                            <ExtradeatilModal style={styles} text="Mileage" setMileage={setMileage}
                             headingText="Mileage" label="Mileage" odometer={preloadData.odometer}
                              selectedText={mileage} />

                            <ExtradeatilModal style={styles} text="Doors" setDoors={setDoors}
                             headingText="Doors" label="Doors" selectedText={doors} />

                            <ExtradeatilModal style={styles} text="Seats" setSeats={setSeats}
                             headingText="Seats" label="Seats" selectedText={`${seat}`} />
                            <ExtradeatilModal style={styles} text="Ownership"
                             setOwnership={setOwnership} companyList={companyList}
                             headingText="Ownership" label="Ownership" setFinance={setFinance}
                             financeCom={financeName} selectedText={ownerShip} />
                            
                            <div className={`${styles.formGroup} ${styles.formGroup2}`}>
                                <span >Notices
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                </span>
                            </div>
                            <ExtradeatilModal style={styles}
                             text="Advance notice before rental ?" headingText="Advance notice before rental"
                              label="Advance Notice Hour" setTripDays={setTripDays}
                              selectedText={TripDays.maxNoticeHours} />

                            <div className={styles.formGroup}>
                                <span className={`pl-0`} >
                                    Allow smoking
                                </span>
                                <span className={`${styles.floatRight} float-right`}>
                                    <div className={`${styles.toggle}`}>
                                        <input type="checkbox" id="switch" onChange={(e) => {
                                            toggleChecked(e); }} />
                                        <label for="switch"></label>
                                    </div></span>
                            </div>
                            <div className={`${styles.formGroup} ${styles.formGroup2}`}>
                                <span >Rental length  </span>
                                <span className={`${styles.floatRight} float-right`}>
                                </span>
                            </div>
                            <ExtradeatilModal style={styles} text="Shortest rental time" description={description}
                            headingText="Shortest time"  label="Shortest rental time" selectedText={TripDays.min}/>


                            <ExtradeatilModal style={styles} text="Longest rental time"
                             headingText="Longest rental time" label="Longest rental time" selectedText={TripDays.max} />

                            <div className={styles.formGroup} >
                                <span className={`pl-0`} >Vehicle description (optional)</span>
                                <textarea type="text" id="" name="description" value={description}
                                onChange={(e)=>setDescription(e.target.value)}
                                    placeholder="Renters will be charged for every additional KM"
                                    className={`form-control `}
                                ></textarea>

                            </div>
                            <div className={`${styles.formGroup} text-right`} >
                                <button className={`${styles.sendBtn}`} onClick={updateCarData}  styles={{ minWidth: "fit-content" }}>
                                    SAVE
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default ExtraCarDetails