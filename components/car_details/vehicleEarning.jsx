import { Bar as ChartJS } from 'chart.js/auto'
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2'
import { earning_service } from '../helpers/modify_car_details_service';
import styles from "../../styles/carProfile.module.scss"
import CardSideNav from '../../global_component/myCar/carSideNav';

const Earning = () => {
	const { query } = useRouter([])
	const [earnData, setEarnData] = useState([])
	const [totalEarning, setEarning] = useState("")
	const [missedEarning, setMissedEarning] = useState("")
	useEffect(() => {
		EarningData()
	}, [])

	const EarningData = async () => {
		let carId = query.id
		const response = await earning_service(carId)
		console.log(response)
		if (response.code == 200) {
			setEarnData(response.result)
			setMissedEarning(response.totalMissedEarning)
			setEarning(response.totalEarning)
		}
	}


	return (
		<section className={`${styles.carProfileSec} user`}>

			<div className={styles.containerMd}>
				<CardSideNav shareCount={global.share} parkedCount={global.parkedCount} />
				<div className={`col ${styles.smallCol}`}>

					<div style={{ maxWidth: "650px" }}>
						<Bar
							data={{
								// Name of the variables on x-axies for each bar
								labels: earnData.map(item => { return item.month }),
								datasets: [
									{
										// Label for bars
										label: "Earning",
										// data: [1552, 1319, 613, 1400],

										// Color of each bar 
										data: earnData.map(item => { return item.earning }),
										backgroundColor: ["#009145"],
										// Border color of each bar
										borderColor: [ "#009145"],
										
										borderWidth: 1,
									},
									{
										// Label for bars
										label: "Mis Earning",
										// data: [1552, 1319, 613, 1400],

										// Color of each bar 
										data: earnData.map(item => { return item.missedEarning }),
										backgroundColor: ["#b2020f"],
										// Border color of each bar
										borderColor: ["#b2020f"],
										
										borderWidth: 1,
									},
								],
								
							}}
							// Height of graph
							height={400}
							options={{
								maintainAspectRatio: false,
								scales: {
									yAxes: [
										{
											ticks: {
												// The y-axis value will start from zero
												beginAtZero: true,
											},
										},
									],
								},
								legend: {
									labels: {
										fontSize: 14,
									},
								},
							}}
						/>


					</div>
					<div className='row'>
						<div className='col-md-1'>
							<img src='/images/others/icon/circle_green.svg' />
						</div>
						<div className='col-md-9'>
							Total Earned

						</div>
						<div className='col-md-2'>
							{totalEarning} SAR
						</div>
					</div>
					<div className='row'>
						<div className='col-md-1'>
							<img src='/images/others/icon/circle_red.svg' />
						</div>
						<div className='col-md-9'>
							Missed Earnings


						</div>
						<div className='col-md-2'>
							{(missedEarning)} SAR

						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default Earning;
