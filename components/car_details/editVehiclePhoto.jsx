import { useEffect, useState } from "react"
import CardSideNav from "../../global_component/myCar/carSideNav"
import styles from "../../styles/carProfile.module.scss"
import { car_all_images_service } from "../helpers/myCar_services"
import Router, { useRouter } from "next/router"

const EditVehiclePhotos = () => {
    const { query, push } = useRouter()
    const[images, setImages] = useState([])

    useEffect(()=>{
        allImages()
    },[])

    const allImages =async()=>{
        let carId = query.id

        const response = await car_all_images_service(carId)
        console.log(response)
        if(response.code==200){
            setImages(response.images)
        }
    }

    return (
        <section className={`${styles.carProfileSec} user`}>

            <div className={styles.containerMd}>
                <CardSideNav />
                <div className={`col ${styles.smallCol}`}>
                    <div className={styles.vehicleDetail}>

                        <div className={styles.detailHeader}>
                            <h2 >
                                <img src="/images/others/icon/carProfile/back_icon.svg" alt="" className="float-left" onClick={() => { Router.push("/sharedCar/vehicleDetails") }} style={{ cursor: "pointer" }} />VEHICLE PHOTOS
                            </h2>
                        </div>
                        <form>
                        <div className="row">
                            <div className="col-12">
                            <div className={styles.addCarSec}>
                                            <div className={styles.insuranceSec}>
                                        <h6>Add some attractive photos of your vehicle</h6>
                                        <p> The better photos you have the more likely it will catch the eyes of renters! Make sure you add some beautiful photos, you can always add more later on if necessary. </p>
                                        <ul className={styles.photoUl}>
                                            <li className="row">
                                                <div className="col-12">
                                                    <p> <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />Use landscape format</p>
                                                </div>
                                            </li>
                                            <li className="row">
                                                <div className="col-12">
                                                    <p> <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />Follow our angle guidelines</p>
                                                </div>
                                            </li>
                                            <li className="row">
                                                <div className="col-12">
                                                    <p> <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />Keep the background clear and natural</p>
                                                </div>
                                            </li>
                                            <li className="row">
                                                <div className="col-12">
                                                    <p> <img src="/images/others/icon/sucess_icon_sel.svg" alt="" />Use only natural daylight</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className={styles.photoSection}>
                                        <div className="row ">
                                            <div className="col-12 mb-4">
                                            <div className={`form-group  ${styles.formGroup} pt-0 pb-0`}>
                                                    <span className="pl-0"> Add main photos </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row no-gutters">
                                        {
                                                        images.map((item,index)=>{
                                                            return(
                                                                <div className={`col-md-6 ${styles.ImgArea}`}>

                                                                <div className={`${styles.imageBox} ${styles.front}`} style={{backgroundImage:`url(${item.link})`}}>
                                                                   
                                                                    <label htmlFor={`photo`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                                       
                                                                        {/* <img alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" /> */}
                                                                    </label>
                                                                    <img src="/images/others/icon/help/drag_and_drop.png" className={styles.dragBtn} alt="" />
                                                                    <div className={styles.counter}><label>{index+1}</label></div>
                                                                </div>
                                                                {
                                                                    item?.imageType==null
                                                                    ?
                                                                    <p></p>
                                                                    :
                                                                <p>A <b>front photo</b>  that stands out,It is the first one renters see</p>


                                                                }
                                                            </div>
                                                                
                                                            )
                                                        })
                                                    }
                                           
                                          
                                            <div className={`col-md-6 ${styles.ImgArea} ml-0`}>
                                                <div className={`${styles.imageBox} ${styles.addNew}`}>
                                                    <label htmlFor={`photo`} className="custom-file-upload fas" style={{ width: "100%" }}>
                                                        {/* <div className={`${styles.photoSec}`}   >
                                                <img htmlFor={`photo`} src={src} />
                                            </div> */}
                                                        <img alt="" className={styles.addBtn} src="images/others/icon/edit_images.svg" />
                                                        <input id={`photo`} type="file" style={{ display: "none" }} accept=".pdf, .jpeg , .jpg ,.png" />
                                                    </label>
                                                    <p className={styles.AddText}>Add more photos</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default EditVehiclePhotos