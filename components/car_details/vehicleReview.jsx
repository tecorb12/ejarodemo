import Router from "next/router"
import { Fragment, useContext, useEffect, useState } from "react"
import CardSideNav from "../../global_component/myCar/carSideNav"
import styles from "../../styles/carProfile.module.scss"
import { review_list_service } from "../helpers/modify_car_details_service"
import { useRouter } from 'next/router';


const CarReviewDetails = () => {
    const { query } = useRouter([])
    const [review, setReview] = useState({
        accuracy: "",
        vehicleCondition: "",
        valueRating: "",
        avgCleanRatings: "",
        avgCommunicationRatings: "",
        avgCarRating: "",
        count: ""
    })
    const [allReviews, setAllReview] = useState([])

    useEffect(() => {
        review_list()
    }, [])

    const review_list = async () => {
        let carId = query.id

        const response = await review_list_service(carId)
        if (response.code == 200) {
            setAllReview(response.carReviews)
            setReview((prev) => {
                return {
                    ...prev,
                    accuracy: response.avgAccuracyRatings,
                    vehicleCondition: response.avgConditionRatings,
                    valueRating: response.avgValueRatings,
                    avgCleanRatings: response.avgCleanRatings,
                    avgCommunicationRatings: response.avgCommunicationRatings,
                    avgCarRating: response.avgCarRating,
                    count: response.carReviewsCount
                }
            })
        }
    }
    let CleanRatings = (review.avgCleanRatings * 100 / 5)
    let accuracyPer = (review.accuracy * 100 / 5)
    let CommuRatingsPer = (review.avgCommunicationRatings * 100 / 5)
    let valuePer = (review.valueRating * 100 / 5)
    let conditionPer = (review.vehicleCondition * 100 / 5)
    let reviews = allReviews.map((item) => {
        return (
            <>
                <div className="col-12 clearfix">
                    <div className={styles.imageSec}>
                        <img alt="" src={item.image ? item.image :"/images/others/icon/userProfile.png"} />
                    </div>
                    <div className={styles.name}>
                        <h6 >{item.fname + " " + item.lname} </h6>
                        <span>{item.reviewed_at}</span>
                    </div>
                </div>
                <div className="col-12">
                    <p> {item.review} </p>
                </div>
            </>
        )
    })
    return (
        <Fragment>
            <section className={`${styles.carProfileSec} user`}>

                <div className={styles.containerMd}>
                    <CardSideNav shareCount={global.share} parkedCount={global.parkedCount}/>
                    <div className={`col ${styles.smallCol}`}>
                        <div className={styles.vehicleDetail}>

                            <div className={styles.detailHeader}>
                                <h2 >
                                    <img src="/images/others/icon/carProfile/back_icon.svg" alt="" 
                                    className="float-left" onClick={() => { Router.push(`/sharedCar/vehicleDetails?id=${query.id}`) }}
                                     style={{ cursor: "pointer" }} />VEHICLE REVIEWS
                                </h2>
                            </div>
                            <div className={`${styles.reviewPageSec} mt-3`}>
                                <div className={`${styles.formGroup} `}>
                                    <ul className={styles.review}>
                                        <li className={styles.stars}>
                                            <img src="/images/others/review_star_small.png" alt="" style={{ width: "14px", marginTop: "-3px" }} /> {review.avgCarRating} </li>
                                        <li>{review.count} Reviews</li>
                                    </ul>
                                </div>
                                <div className={`${styles.formGroup} `}>
                                    <div className={styles.Rating}>
                                        <ul>
                                            <li>
                                                <span >Listing Accuracy</span>
                                                <span className="float-right">
                                                    <progressbar max="5" type="danger" className={`progress ${styles.progress}`}>
                                                        <bar role="progressbar" aria-valuemin="0"
                                                            className={`progress-bar-danger bg-danger progress-bar ${styles.dangerBar}`}
                                                            aria-valuenow={review.accuracy} aria-valuetext="80%" style={{ height: "100%", width: `${accuracyPer}%` }} >
                                                        </bar>
                                                    </progressbar>{review.accuracy} </span>
                                            </li>
                                            <li >
                                                <span >Vehicle Condition</span>
                                                <span className="float-right">
                                                    <progressbar max="5" type="danger" className={`progress ${styles.progress}`}>
                                                        <bar role="progressbar" aria-valuemin="0"
                                                            className={`progress-bar-danger bg-danger progress-bar ${styles.dangerBar}`}
                                                            aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: `${conditionPer}%` }} >
                                                        </bar>
                                                    </progressbar>{review.vehicleCondition}</span>
                                            </li>
                                            <li >
                                                <span >Value For Money</span>
                                                <span className="float-right">
                                                    <progressbar max="5" type="danger" className={`progress ${styles.progress}`}>
                                                        <bar role="progressbar" aria-valuemin="0"
                                                            className={`progress-bar-danger bg-danger progress-bar ${styles.dangerBar}`}
                                                            aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: `${valuePer}%` }} >
                                                        </bar>
                                                    </progressbar>{review.valueRating}</span>
                                            </li>
                                            <li >
                                                <span >Cleanliness</span>
                                                <span className="float-right">
                                                    <progressbar max="5" type="danger" className={`progress ${styles.progress}`}>
                                                        <bar role="progressbar" aria-valuemin="0"
                                                            className={`progress-bar-danger bg-danger progress-bar ${styles.dangerBar}`}
                                                            aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: `${CleanRatings}%` }} >
                                                        </bar>
                                                    </progressbar>{review.avgCleanRatings}</span>
                                            </li>
                                            <li >
                                                <span >Communication</span>
                                                <span className="float-right">
                                                    <progressbar max="5" type="danger" className={`progress ${styles.progress}`}>
                                                        <bar role="progressbar" aria-valuemin="0"
                                                            className={`progress-bar-danger bg-danger progress-bar ${styles.dangerBar}`}
                                                            aria-valuenow={review.avgCommunicationRatings}
                                                            aria-valuetext="80%" style={{ height: "100%", width: `${CommuRatingsPer}%` }} >
                                                        </bar>
                                                    </progressbar>{review.avgCommunicationRatings}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className={`${styles.formGroup} `}>
                                    <div className={styles.reviewList}>
                                        <h3 >Reviews</h3>
                                        <div className={styles.indivisualReview}>
                                            <div className="row">
                                                {reviews}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}
export default CarReviewDetails