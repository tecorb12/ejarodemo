import { Fragment } from 'react'
import SimpleSlider from '../common/slider'
import styles from "../../styles/home.module.scss"
const DestinationSec = ({city}) => {

  let cityData = city.map((item,index)=>{
    return(
      <div className={styles.carInfo} >
                  <div className={styles.carImg}>
                    <img src={item.image ? item.image : "/images/others/home/dammam.png"} />
                  </div>
                  <h4 >{item.title}</h4>
                </div>
    )
  })
  return (
    <Fragment>
      <section className={styles.popularPlace}>
        <div className="container-md">
          <div className="row">
            <div className="col-md-12">
              <h2>Popular Destinations</h2>
              <p>Find the perfect car anywhere you go</p>
            </div>
            <div className="col-md-12">
              <SimpleSlider >
                {cityData}
                
              </SimpleSlider>
            </div>
          </div>
        </div>
      </section>
    </Fragment>

  )
}
export default DestinationSec