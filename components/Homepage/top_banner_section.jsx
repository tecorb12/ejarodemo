import { Fragment } from "react"
import styles from "../../styles/home.module.scss"
import SearchSec from "../common/Search"
const AppBanner = () => {
    return (
        <Fragment>
             <section id={styles.bannerSection}>
        <div className="container-fluid">
            <div className={styles.innerSection}>
                <div className="container">
                    <div className={styles.search}  id="bannerSearch"> 
                   {/* { global.search !=="true" &&  */}
                   <SearchSec/> 
                   {/* } */}
                    </div>
                </div>
            </div>
            <div className={`${styles.textSection} text-left`}>
                <h2>
                    The First Licensed Peer-to-Peer <br /> Vehicle Sharing Community in Saudi Arabia
                </h2>
                <img offset="200" src="/images/others/transport_authority_logo.png" alt="" />
            </div>
        </div>
    </section>
        </Fragment>
    )
}
export default AppBanner