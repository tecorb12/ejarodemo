import { Fragment, useEffect, useState } from "react"
import styles from "../../styles/home.module.scss"
const WelcomeSec = () => {
    const [showMe, setShowMe] = useState(false);

    const playVideo = () => {
        setShowMe(!showMe);
    }

    return (
        <Fragment>
            <section id={styles.welcome}>
                <div className="">
                    <div className={styles.holder}>
                        <div className={styles.welcomeText}>
                            <h2>Welcome to Ejaro</h2>
                            <h4>Trusted Sharing</h4>
                            <p className="mt-4 mb-4">
                                Ejaro is the first licensed peer-to-peer vehicle sharing platfrom in the GCC. Founded in Saudi Arabia, we connect local vehicle owners with individuals who are looking to rent vehicles whenever and wherever they want. We empower owners to earn extra income
                                from their depreciating private vehicles (assets) by sharing it with ejaro’s trusted community.
                            </p>
                            <p>
                                Our mission is to create a trustworthy community that is both safe and reliable by providing an easy to use platform that saves our users time, money and effort and streamlining the processes of renting vehicles in today’s fast-paced market.
                            </p>
                        </div>

                        <div className={`${styles.embedResponsive} ${styles.embedResponsiveBY}`} id="player">
                            {
                                !showMe ?
                                    <div className={styles.imgSec} onClick={playVideo}>
                                        <img src="/images/others/welcome_to_ejaro_image.png" />
                                    </div>
                                    :
                                    null
                            }

                            <div className={styles.videoSection} style={{ display: showMe ? "block" : "none" }}>
                                <iframe
                                    className="embed-responsive embed-responsive-16by9"
                                    src="https://www.youtube.com/embed/TWKC3LIw4dE?autoplay=1&rel=0&showinfo=0&loop=1"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                    height="500px"
                                ></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}
export default WelcomeSec