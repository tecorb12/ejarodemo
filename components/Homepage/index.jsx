import React, { useEffect, useState, Fragment, useContext } from "react";
import styles from "../../styles/home.module.scss"
import BrandLogoSec from "./brand_logo_section";
import DestinationSec from "./destination_section";
import DownloadSec from "./download_section";
import ExtraIncomeSec from "./extra_income_section";
import VehicleSec from "./location_car_section";
import SharingSec from "./sharing_section";
import AppBanner from "./top_banner_section";
import Welcome from "./welcome_section";
import { homepage_service, session_details_service } from '../helpers/service'
import VerifyContact from "../common/Modal/contact_modal";
import { IdContext } from "../MyContext/IdDetailsContext";

const Homepage = () => {
  const {setUserData} = useContext(IdContext)
  const [data, setData] = useState([])
  const [city, setCity] = useState([])
  const [Open, setOpenModal] = useState(false)
  const [isContact, setContact] = useState("")
  const [countryCode, setCode] = useState("")
  const [userId, setUserId] = useState("")
  const [email, setEmail] = useState("")
  const [isLocation, setLocation] = useState({
    lat: "", lng: ""
  })


  useEffect(() => {
    if (typeof window !== "undefined") {
      localStorage.token && sessionDetails()
    }
    permission()
  }, [])

  const permission = async term => {

    const location = window.navigator && window.navigator.geolocation

    if (location) {
      location.getCurrentPosition((position) => {
        setLocation((prev) => { return { ...prev, lat: position.coords.latitude, lng: position.coords.longitude } })
        homePageApi(position.coords.latitude, position.coords.longitude)


      }, (error) => {
        setLocation((prev) => { return { ...prev, lat: '28.6195712', lng: '77.4209536' } })

      })
    }

  }
  const sessionDetails = async () => {
    const response = await session_details_service()
    if (response.code == 200 && response.result.contactVerified == false) {
      setOpenModal(true)
      setContact(response.result.mobile)
      setCode(response.result.countryCode)
      setUserId(response.result.id)
      setEmail(response.result.email)
    }
    else if (response.code == 200 && response.result.contactVerified == true) {
      localStorage.user = response.result.id
      setUserData(response.result)

    }
    else if (response.code == 345) {
      localStorage.clear()
      window.location.href = "/"

    }
  }

  const homePageApi = async (lat, lng) => {
    const response = await homepage_service("28.6195712", "77.4209536")
    if (response.code == 200) {
      setData(response.result.cars)
      setCity(response.result.cities)
    }
  }
  return (
    <Fragment>
      <div className={styles.HomePage}>
        <AppBanner />
        {data.length > 0 && <VehicleSec data={data} location={isLocation} />}
        <Welcome />
        <SharingSec />
        <DestinationSec city={city} />
        <ExtraIncomeSec />
        <BrandLogoSec />
        <DownloadSec />

      </div>
      <VerifyContact isOpen={Open} setOpenModal={setOpenModal} phone={isContact}
        setPhone={setContact} setCountrycode={setCode}
        countryCode={countryCode} email={email} userId={userId} />

        
    </Fragment>
  )
}
export default Homepage;