import { Fragment } from "react"
import styles from "../../styles/home.module.scss"
import { home_image } from "../common/image_urls"
const BrandLogoSec = () => {
    return(
<Fragment>
<div className={styles.featured}>
        <div className="container-fluid">
            <div className="row no-gutters text-center">
                <div className={`col-lg-3 col-xs-12 ${styles.featureHeading}`} >
                    <div className="row">
                        <div className="col-md-12 text-left">
                            <h5>Featured In</h5>
                        </div>
                    </div>
                </div>
                <div className="col-lg-8 ml-auto">
                    <div className="row">
                        <div className={`col-4  ${styles.featureImage}`}>
                        <div className={styles.img}>
                                <img offset="200" src={home_image.BrandLogo.logo1} alt="" />
                            </div>
                        </div>
                        <div className={`col-4  ${styles.featureImage}`}>
                        <div className={styles.img}>
                                <img offset="200" src={home_image.BrandLogo.logo2}  alt="" />
                            </div>
                        </div>
                        <div className={`col-4  ${styles.featureImage}`}>
                            <div className={styles.img}>
                                <img offset="200" src={home_image.BrandLogo.logo3}  alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</Fragment>
    )
}
export default BrandLogoSec