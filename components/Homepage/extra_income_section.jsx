import { Fragment } from "react"
import styles from "../../styles/home.module.scss"

const ExtraIncomeSec = () => {
    return (
        <Fragment>
            <section id={styles.earnSec} >
                <div className="container-fluid">
                    <div className="row">
                        <div className={`col-lg-6 col-sm-12 ${styles.text}`} >
                            <h2>Earn an extra income</h2>
                            <h2>from your vehicle now!</h2>
                            <div className={styles.mini}>
                                <h3>Earn an extra income from your vehicle now!</h3>
                            </div>
                            <p>
                                Make money out of your depreciating assets by <br className="small" /> sharing it with Ejaro’s trusted community.
                            </p>
                            <button className={`mx-auto ${styles.earningBtn}`} >
                                Share Your Vehicle
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}
export default ExtraIncomeSec