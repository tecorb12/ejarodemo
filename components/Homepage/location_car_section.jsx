import  Router from 'next/router'
import { Fragment, useState } from 'react'
import { useEffect } from 'react/cjs/react.development'
import styles from "../../styles/home.module.scss"
import CarSlider from '../common/slider/carLocation'
import AddToFav from '../favouritesList/addToFav'
import moment from 'moment';

const VehicleSec = ({ data,location }) => {
  console.log(data,location)
  const[tripStartDate, setTripStartDate] = useState("")
  const[tripEndDate, setTripEndDate] = useState("")
 

  let modifiedIndex;
  let timeArray = [
    '00:00',
    '00:30',
    '01:00',
    '01:30',
    '02:00',
    '02:30',
    '03:00',
    '03:30',
    '04:00',
    '04:30',
    '05:00',
    '05:30',
    '06:00',
    '06:30',
    '07:00',
    '07:30',
    '08:00',
    '08:30',
    '09:00',
    '09:30',
    '10:00',
    '10:30',
    '11:00',
    '11:30',
    '12:00',
    '12:30',
    '13:00',
    '13:30',
    '14:00',
    '14:30',
    '15:00',
    '15:30',
    '16:00',
    '16:30',
    '17:00',
    '17:30',
    '18:00',
    '18:30',
    '19:00',
    '19:30',
    '20:00',
    '20:30',
    '21:00',
    '21:30',
    '22:00',
    '22:30',
    '23:00',
    '23:30',
  ];
  const getIndexOfNow = () => {
    let date = new Date();
    let hour = date.getHours('HH');
    let minutes = date.getMinutes('mm');
    let hh = hour;
    let mm = minutes;
    if (Number(minutes) >= 0 && Number(minutes) < 30 && Number(hour) <= 22) {
      mm = 30;
      hh = Number(hh) + 1;
      modifiedIndex = false;
    } else {
      mm = 0;
      hh = Number(hh) + 2;
      if (Number(hour) >= 22 && Number(hour) < 23 && Number(minutes) > 30) {
        hh = 0;
        mm = 0;
        modifiedIndex = 'demand';
      } else if (Number(hour) >= 23 && Number(minutes) < 30) {
        hh = 0;
        mm = 30;
        modifiedIndex = 'demand';
      } else if (Number(hour) >= 23 && Number(minutes) > 30) {
        modifiedIndex = 'demand';
        hh = 1;
        mm = 0;
      }
    }

    let finalHH = Number(hh) < 10 ? '0' + hh.toString() : hh.toString();
    let finalmm = Number(mm) < 10 ? '0' + mm.toString() : mm.toString();
    let timeStr = finalHH.toString() + ':' + finalmm.toString();
    let index = timeArray.indexOf(timeStr);
    if (index != -1) {
      if (index <= 47 && modifiedIndex != 'demand') {
        return index;
      } else if (modifiedIndex == 'demand') {
        if (
          Number(this.dp.transform(this.minDate, 'yyyyMMdd')) -
          Number(this.dp.transform(date, 'yyyyMMdd')) !=
          1
        ) {
          let temp = new Date(date);
          temp.setDate(temp.getDate() + 1);
          this.minDate = temp;
          this.bsInlineRangeValue[0] = temp;
        }

        return index;
      }
    }
  }

  let startTime = timeArray[getIndexOfNow()]
  let endTime =  timeArray[timeArray.length - 1]
  let maxDate = new Date();



  const getminDate = () => {
    let date = new Date();
    let hour = date.getHours();
    let minute = date.getMinutes();
    if (hour < 22) {
      date = new Date();
    } else if (hour == 22 && minute < 30) {
      date = new Date();
    } else if (hour == 22 && minute >= 30) {
      date.setDate(date.getDate() + 1);
    } else if (hour > 22) {
      date.setDate(date.getDate() + 1);
    }
    return date;
  }
  let minDate = getminDate();

  let fromdate = minDate;
  let toDate = new Date();
  toDate.setDate(fromdate.getDate() + 2);
  maxDate.setFullYear(maxDate.getFullYear() + 1)

  const formateDate = (startDate,endDate) => {
    var dd1 = startDate.getDate();
    var mm1 = startDate.getMonth() + 1;
    var yyyy1 = startDate.getFullYear();
    if (dd1 < 10) {
      dd1 = '0' + dd1;
    }

    if (mm1 < 10) {
      mm1 = '0' + mm1;
    }
    var dd2 = endDate.getDate();

    var mm2 = endDate.getMonth() + 1;
    var yyyy2 = endDate.getFullYear();
    if (dd2 < 10) {
      dd2 = '0' + dd2;
    }

    if (mm2 < 10) {
      mm2 = '0' + mm2;
    }
   
   setTripStartDate(yyyy1 + '/' + mm1 + '/' + dd1)
   setTripEndDate(yyyy2 + '/' + mm2 + '/' + dd2)
   

  }
  useEffect(()=>{
      formateDate(fromdate,toDate)
  },[])
  const [isChecked, setIsChecked] = useState(false)
 

  let renderData = data.map((item, index) => {
    let tripStartDateTime = tripStartDate + " " + timeArray[getIndexOfNow()]
    let tripEndDateTime =  tripEndDate + " " + timeArray[timeArray.length - 1]
    let showStartDate= moment(new Date(tripStartDate))?.format("MMMM DD")
    let showEndDate= moment(new Date(tripEndDate))?.format("MMMM DD")

  
    return (
      <div className="col-md-12">

        <div className={styles.carInfo} >
          <AddToFav
            isFav={item.inFav}
            image_not_filled='/images/others/fav.png'
            image_filled='/images/others/fav_sel.png'
            carId={item.id}
            styles={styles}
            isChecked={isChecked}
            setIsChecked={setIsChecked} />
          <div className={styles.pricetag}>
            <p >{item.avgCarRating}
              <img src='/images/others/superhost_star.png' className="mx-1" />
              {item.starTag}
            </p>

          </div>
          <div className={styles.carImg} >

            <img src={item.image ? item.image : "/images/others/car_image_one.png"} />
          </div>
          {/* </div> */}
          <div className={styles.carDesc} onClick={()=>{Router.push(`/booking_carProfile?id=${item.id}&lat=${location.lat}&lng=${location.lng}&fromDate=${tripStartDateTime}
          &toDate=${tripEndDateTime}&showStartDate=${showStartDate}&showEndDate=${showEndDate}&startTime=${startTime}&endTime=${endTime}`)}}>
            <div className='row'>
              <div className='col-7'>
                <h5>{item.make.title} {item.model.title}
                  <span>{item.year.title}</span>  </h5>
                <ul>
                  <li>{item.totalTrips} | {item.distance?.toFixed(1)} | {item.guestLocationDeliveryPrice == 0 ? "Avl. for Delivery" : "Free Delivery"} </li>
                </ul>

              </div>
              <div className={`col-5 pl-0 ${styles.price}`}>
                <h5><span>SAR {item.priceMinDaily} </span>/ Day</h5>
                <h6>SAR 180<span>/ Week </span><br></br>SAR 200<span>/ Month </span></h6>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  })

  return (
    <Fragment>

      <section className={styles.relatedVehicle}>
        <div className="container-md">
          <div className="row">
            <div className="col-md-12">
              <p>Vehicles Around You</p>
            </div>
            <CarSlider>
              {renderData}
            </CarSlider>
          </div>
        </div>
      </section>
    </Fragment>

  )
}
export default VehicleSec