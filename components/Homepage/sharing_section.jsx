import { Fragment, useEffect, useState } from "react"
import styles from "../../styles/home.module.scss"
import { home_image } from "../common/image_urls"
const SharingSec = () => {
    return(
        <Fragment>
        <section className={styles.marketplace}>
        <div className="container-fluid">
            <div className={`row ${styles.functionalityGroup}`}>
                <div className={`col-lg-3 ${styles.functionality}`}>
                    <div className="row no-gutters">
                        <div className="col-md-12 text-lg-left">
                            <h5>Explore Saudi Arabia’s leading</h5>
                            <h3>Vehicle sharing marketplace</h3>
                        </div>
                    </div>
                </div>
                <div className="col-lg-9">
                    <div className="row" style={{padding: "0% 0% 0% 6%"}}>
                        <div className="col-md-12 ml-auto">
                            <div className="row">
                                <div className={`col-lg-4 col-md-6 ${styles.functionality}`}>
                                    <div className="row no-gutters">
                                        <div className="col-2 text-md-center">
                                            <img offset="200" src={home_image.comptative_price} alt="" />
                                        </div>
                                        <div className="col-10">
                                            <div className={styles.marketplaceSingle}>
                                                <h4>COMPETITIVE PRICES</h4>
                                                <p>Up to 40% cheaper</p>
                                                <p>then a rental company</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`col-lg-4 col-md-6 ${styles.functionality}`}>
                                    <div className="row no-gutters">
                                        <div className="col-2 text-md-center">
                                            <img offset="200" src={home_image.more_option}  alt="" style={{marginLeft: "-15px"}} />
                                        </div>
                                        <div className="col-10">
                                        <div className={styles.marketplaceSingle}>
                                                <h4>MORE OPTIONS</h4>
                                                <p>Choose from a greater selection</p>
                                                <p>of cars ranging from economic to luxury</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`col-lg-4 col-md-6 ${styles.functionality}`}>
                                    <div className="row no-gutters">
                                        <div className="col-2 text-md-center">
                                            <img offset="200" src={home_image.quick_easy} alt="" />
                                        </div>
                                        <div className="col-10">
                                        <div className={styles.marketplaceSingle}>
                                                <h4>QUICK & EASY TO USE</h4>
                                                <p>Skip the counter, no paperwork</p>
                                                <p>& pay easily</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </Fragment>
    )
}
export default SharingSec