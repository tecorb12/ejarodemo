import { Fragment } from "react"
import { home_image } from "../common/image_urls"
import styles from "../../styles/home.module.scss"

const DownloadSec = () => {
    return(
        <Fragment>
            <section className={styles.download}>
        <div className="container-md">
            <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12">
                    <div className={styles.heading}>
                        <h2>
                            The First Licensed Peer-to-Peer <br className={styles.medium} /> Vehicle Sharing Community in Saudi Arabia
                        </h2>
                    </div>
                    <div className={styles.downloadAddress}>
                        <h3>Download Now</h3>
                        <a href="https://apps.apple.com/in/app/ejaro/id1482484869" target="_blank">
                            <img offset="200" src={home_image.Download.app_store} width="120px" style={{marginRight: "9px"}} />
                            </a>
                        <a href="https://play.google.com/store/apps/details?id=com.ejaro" target="_blank">
                            <img offset="200" src={home_image.Download.google_store}  width="120px" />
                            </a>
                    </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12 text-center">
                    <div className={styles.mobileImg}>
                     
                            <img offset="500"  src={home_image.mobile_img} />
                      
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 m-auto">
                    <ul className={`mt-5 ${styles.logos}`} >
                        <li className="">
                            <img offset="200" src={home_image.CardLogo.logo1} />
                        </li>

                        <li className="col">
                            <img offset="200" src={home_image.CardLogo.logo2} />
                        </li>
                        <li className="col">
                            <img offset="200" src={home_image.CardLogo.logo3}  />
                        </li>
                        <li className="">
                            <img offset="200" src={home_image.CardLogo.logo4} />
                        </li>
                        <li className="">
                            <img offset="200" src={home_image.CardLogo.logo5}  />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
        </Fragment>
    )
}
export default DownloadSec