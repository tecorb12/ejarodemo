import FavouriteList from '../../components/favouritesList'
import Footer from '../../components/Footer'

const Fav = () => {

  return (
    <>
      <FavouriteList />
      <Footer />
    </>
  )
}
export default Fav