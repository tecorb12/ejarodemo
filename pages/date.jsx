import React, { useState } from "react";
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import DatePicker from 'react-datepicker';
const MyComponent = () => {
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;
    return (
        <DatePicker
            selectsRange={true}
            startDate={startDate}
            endDate={endDate}
            monthsShown={2}
            inline
            onChange={(update) => {
                setDateRange(update);
            }}
            withPortal
        />
    );
}

export default MyComponent

