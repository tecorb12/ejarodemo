import Footer from '../../components/Footer'
import BookingProfile from '../../global_component/booking_carprofile'

const BookingCarProfile = () => {

  return (
    <>
      <BookingProfile />
      <Footer />
    </>
  )
}
export default BookingCarProfile
