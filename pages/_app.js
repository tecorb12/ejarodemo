import '../styles/globals.css'
import { IdProvider } from '../components/MyContext/IdDetailsContext'
import AllPageHeader from '../components/Hearder/allPageHeader'
import { useRouter } from 'next/router'
import Routes from '../components/common/route'
function MyApp({ Component, pageProps }) {

  let router = useRouter().asPath;
  console.log(router == Routes.Auth.landing_page.as)

  return (
    <IdProvider>
      {/* {router !== Routes.Auth.landing_page.as && <AllPageHeader />} */}
      <Component {...pageProps} />
    </IdProvider>

  )
}

export default MyApp
