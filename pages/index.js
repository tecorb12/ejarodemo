import { useEffect, useState } from 'react'
import SuccessResponseModal from '../components/common/Modal/succefully_modal'
import Footer from '../components/Footer'
import Header from '../components/Hearder'
import Homepage from '../components/Homepage'
import { useRouter } from 'next/router'
import Routes from '../components/common/route'
import styles from '../styles/header.module.scss'

const Home = () => {
  const [forgetModal, setModal] = useState(global.forget)
  let router = useRouter().asPath;

  const LandingScroll = () => {
    if (typeof window !== "undefined") {
      $(window).scroll(() => {
        if ($(window).scrollTop() > 50) {
          if ($(window).scrollTop() > 320) {
            $('#fixSearch').css("display", "block");
            $('#bannerSearch').css("display", "none");

          }
          else {
            $('#fixSearch').css("display", "none");
            $('#bannerSearch').css("display", "block");
          }
          global.search = "true"
          $('#landingHeader').addClass(`${styles.mainNavFixedTop}`, { duration: 500 });
        }
        else {
          global.search = "false"
          $('#landingHeader').removeClass(`${styles.mainNavFixedTop}`, { duration: 500 });
        }
      })
    }

  }
  useEffect(() => {
    if (router === Routes.Auth.landing_page.as) {
      console.log(router === Routes.Auth.landing_page.as)
      LandingScroll();
    }
    else {
      $('#landingHeader').addClass(`${styles.mainNavFixedTop}`, { duration: 500 });
    }
  }, [])
  return (
    <>
      <Header />
      <Homepage />
      <Footer />
      <SuccessResponseModal open={forgetModal} setSuccessresponse={setModal} message="Please check your email for password reset" />
    </>
  )
}
export default Home
