import { Fragment, useContext, useEffect, useState } from "react"
import styles from "../../../styles/bookingList.module.scss"
import { useRouter } from 'next/router';
import BookingSideNav from "../bookingSideNav"
import { booking_list_service } from "../../../components/helpers/booking_services.jsx";

const RequestedBooking = () => {
    const router = useRouter()

    const [requestCount, setRequestCount] = useState(0)
    const [currentCount, setCurrentCount] = useState(0)
    const [historyCount, setHistoryCount] = useState(0)
    const [bookingList, setBookingData] = useState([])

    useEffect(() => {
        PendingRequestedList()
    }, [])

    const PendingRequestedList = async () => {
        const response = await booking_list_service("")
        if (response.code == 200) {
            setBookingData(response.bookings)
            setRequestCount(response.tripsRequest)
            setCurrentCount(response.tripsCurrent)
            setHistoryCount(response.tripsHistory)
        }

    }

    let pendingBookingList = bookingList.map((item, index) => {
        let carMaking = item?.car.make.title
        let carModel = item?.car.model.title
        let carYear = item?.car.year.title
        let isOwner = localStorage.user == item?.car.user.id ? "HOSTED BY " : "RENTED BY"
        let userName = isOwner == "HOSTED BY " ? item.hostedBy : item.rentedBy

        return (


            <div className={`${styles.containers} mb-4`}>
                <div className={`row  no-gutters ${styles.carDetail}`}>
                    <div className={`col  pl-0 ${styles.carImg}`}
                        style={{ backgroundImage: `url(${item.car.image ? item.car.image : "/images/others/how-works/ejaro_image.png"})` }}>

                    </div>

                    <div className={`col ${styles.carInfo}`}>
                        <div className={styles.carInfoUpper}>
                            <div className={styles.carStatus}>
                                <span className={`${styles.pendingStatus} ${styles.status}`}>
                                    {item.currentStatus}
                                </span>
                                <span className={`float-right text-right ${styles.floatRight}`}>
                                    Get Help
                                    <img src="/images/others/header/contact_support.svg"
                                        alt="" />
                                </span>
                            </div>
                            <h4>
                                {carMaking + " " + carModel + " " + carYear}
                            </h4>
                            <h5>{isOwner} <span>{userName}</span> </h5>
                            <h6>Booking Ref : #CRN{item.id}</h6>
                            <div className={styles.tripinfo}>
                                <p>
                                    <label for="">Start date</label>
                                    <span>{item.tripStartFrom} at {item.tripStartFromTime}</span>
                                </p>
                                <p>
                                    <label for=""> End date</label>
                                    <span>{item.tripEndAt} at {item.tripEndAtTime}</span>
                                </p>
                            </div>
                            <div className={styles.viewbtn}>
                                <p>View / Edit booking</p>
                            </div>
                        </div>
                        <div style={{ padding: "0 1vw" }} className={styles.smallHide}>
                            <div className="row no-gutters">
                                <div className="col-md-12">
                                    <div className={`${styles.editSec} text-center`}>
                                        <ul>
                                            <li >
                                                <div className={styles.action}>
                                                    <button>CANCEL REQUEST</button>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ padding: "0 1vw" }} className={styles.smallShow}>
                    <div className="row no-gutters">
                        <div className="col-md-12">
                            <div className={`${styles.editSec} text-center`}>
                                <ul>
                                    <li >
                                        <div className={styles.action}>
                                            <button>CANCEL BOOKING</button>
                                        </div>
                                    </li>
                                    <li >
                                        <div className={styles.action}>
                                            <button>CHECK IN</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    })
    return (
        <Fragment>

            <section className={`${styles.BookingProfileSec} user`}>

                <div className={styles.containerMd}>
                    <BookingSideNav requestCount={requestCount} currentCount={currentCount} historyCount={historyCount} />
                    <div className={`col ${styles.smallCol}`}>
                    <div className="tab-content">

                        <div className="tab-pane active" id="tabs-1">

                            <div className={styles.BookingSec}>
                             

                                    {pendingBookingList}


                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>

        </Fragment >
    )
}

export default RequestedBooking