import { Fragment } from "react"
import AddNewCar from "../../components/common/Modal/AddCarModal"
import styles from "../../styles/bookingList.module.scss"
import { useRouter } from "next/router";
import Routes from "../../components/common/route";
import Link from 'next/link';

const LinkTag = ({ activeClass, as, href, iconPath, name, count }) => {
    return (
        <Fragment>
            <Link as={as} href={href} >
                <li className={activeClass}>

                    <a href="#tabs-1" role="tab" data-toggle="pill">
                        {name}

                        <span>{count}
                            <img src={iconPath} style={{ marginLeft: " 8px" }} />
                        </span></a>
                </li>
            </Link>

        </Fragment>
    )
}
const BookingSideNav = ({ requestCount, historyCount, currentCount }) => {
    let { pathname } = useRouter();
    let requested_booking = pathname === Routes.Booking.RequestedBooking.as;
    let booking_history = pathname === Routes.Booking.BookingHistory.as;
    let current_booking = pathname === Routes.Booking.CurrentBooking.as;
    return (
        <Fragment>
            <div className={styles.showStatus}>
                <div className={styles.leftMenu}>
                    <h2 >Bookings</h2>
                    <ul className={`nav nav-pills nav-stacked ${styles.NavSec}`} role="tablist">
                        <LinkTag
                            activeClass={requested_booking ? styles.active : ""}
                            as={Routes.Booking.RequestedBooking.as}
                            href={Routes.Booking.RequestedBooking.href}
                            iconPath={requested_booking ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png"}
                            iconAlt="Requests"
                            count={requestCount}
                            name="Requests" />                 
                          <LinkTag
                            activeClass={current_booking ? styles.active : ""}
                            as={Routes.Booking.CurrentBooking.as}
                            href={Routes.Booking.CurrentBooking.href}
                            count={currentCount}
                            iconPath={current_booking ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png"}
                            iconAlt="Current"
                            name="Current" />
                              <LinkTag
                            activeClass={booking_history ? styles.active : ""}
                            as={Routes.Booking.BookingHistory.as}
                            href={Routes.Booking.BookingHistory.href}
                            count={historyCount}
                            iconPath={booking_history ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png"}
                            iconAlt="History"
                            name="History" />
                    </ul>
                    {/* <AddNewCar text="Add a new car" /> */}
                </div>
            </div>
        </Fragment>
    )
}
export default BookingSideNav