
import AddNewCar from "../../components/common/Modal/AddCarModal"
import NoCar from "../../components/common/Modal/noCarDataModal"
import styles from "../../styles/bookingList.module.scss"


const BookingCar = () => {
  return (
    <div className={`col ${styles.smallCol}`}>
      <div className={styles.NoVehicle}>
      <div className={styles.NodataSec}>
        <div className={styles.imageSec}>
          <img
            src="/images/others/icon/inuse/no_upcoming_rentals.svg"
            alt=""
          />
          <p>No rentals requests</p>
        </div>
        </div>
  {/* ------------------------button----------------- */}
        <div className={styles.buttons}>
          <div className="row">
            <div className="col-md-6">
              <div className={styles.formGroup}>
                <button type="submit" className={styles.bookVehicle}>BOOK A VEHICLE</button>
              </div>
            </div>
            <div className="col-md-6">
              <div className={styles.formGroup}>
                {/* <button type="submit" className={styles.shareVehicle}>SHARE A VEHICLE</button> */}
                <AddNewCar text="SHARE YOUR CAR" style={styles.shareVehicle} />
              </div>
            </div>
          </div>
        </div>

        <div className={styles.formGroup}>
          {/* <AddNewCar text="SHARE YOUR CAR" className={styles.button} /> */}
        </div>
     
      </div>
    </div>
  )
}

export default BookingCar