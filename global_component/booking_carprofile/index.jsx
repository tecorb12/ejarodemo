import React from 'react'
import { useEffect, useState } from 'react'
import RentalTerm from '../../components/common/Modal/rentalTerm_modal'
import { booking_car_profile_service, car_estimate_price_service } from '../../components/helpers/booking_services.jsx'
import styles from "../../styles/bookingCarProfile.module.scss"
import { useRouter } from 'next/router';
import { myCar_details_service } from '../../components/helpers/myCar_services'
import PickUpLocationModal from '../../components/common/Modal/ModifyCarDetailModal/pickUpLocationModal'
import { Fragment } from 'react'
import SuccessResponseModal from '../../components/common/Modal/succefully_modal'
import PriceCalendar from '../../components/common/Modal/range_datePicker'
import PromocodeModal from '../../components/common/Modal/promocode_modal'

const BookingProfile = () => {
    const { query } = useRouter()
    const [isCarProfile, setCarProfile] = useState("")
    const [reviews, setReview] = useState([])
    const [nearCar, setNearCar] = useState([])
    const [message, setMessage] = useState("")
    const [code, setCode] = useState("")
    const [estimatePrice, setEstimatePrice] = useState("")
    const [promocode, setPromoCode] = useState("")
    const [tripStartShowDate, setShowStartDate] = useState(query.showStartDate)
    const [tripEndShowDate, setShowEndDate] = useState(query.showEndDate)
    const [startTime, setStartTime] = useState(query.startTime)
    const [endTime, setEndTime] = useState(query.endTime)
    const [isOpen, setOpen] = useState(false)
    const [dateRange, setDateRange] = useState([null, null]);

    let car_Id = query.id
    let latitude = query.lat
    let longitude = query.lng
    let tripStartAt = query.fromDate
    let tripEndAt = query.toDate
    let userId = typeof window !== "undefined" && localStorage.user

    const car_booking_profile_data = async (CarId) => {

        const response = await myCar_details_service(CarId, userId, latitude, longitude, tripStartAt, tripEndAt)
        if (response.code == 200) {
            setCarProfile(response.result)
            setReview(response.reviews)
            setNearCar(response.nearAbout)

        }
        else if (response.code == 404) {
            setOpen(false)
        }
    }
    const car_Estimate_price = async (promoCode) => {
        const response = await car_estimate_price_service(car_Id, latitude, longitude, tripStartAt, tripEndAt, userId, "", promoCode)
        if (response.code == 422) {
            setMessage(response.message)
            setCode(response.code)
            setOpen(true)
        }
        else if (response.code == 200) {
            setEstimatePrice(response.result)
        }
    }
    useEffect(() => {
        query && car_booking_profile_data(car_Id)
        userId && car_Estimate_price()
    }, [query])

    const nearLocationCarDetails=(e,carid)=>{
        e.preventDefault()
        window.scrollTo(0, 0)
        car_booking_profile_data(carid)
        userId && car_Estimate_price()

    }

    let carModel = isCarProfile?.make?.title + " " + isCarProfile?.model?.title + " " + isCarProfile?.year?.title
    let place = isCarProfile?.pickupLocation?.city + " " + isCarProfile?.pickupLocation?.state + " " + isCarProfile?.pickupLocation?.zip
    let hostName = isCarProfile?.user?.fname + " " + isCarProfile?.user?.lname
    let hostRating = isCarProfile?.user?.rating
    let hostTotalTrip = isCarProfile?.user?.totalTrips
    let hostJoinDate = isCarProfile?.user?.joinedAt
    let starTag = isCarProfile?.user?.starTag
    let hostImage = isCarProfile?.user?.image
    return (
        <Fragment>
            <div className={styles.CarProfile}>
                {/* ----------Image----------- */}
                <div className={styles.slider}>
                    <div className={styles.silderCover}>
                        <div className={styles.customSlider} >
                            <div className={styles.main} >
                                {isCarProfile?.images?.length > 4 || isCarProfile?.images?.length == 5 && <>
                                    <div className={styles.imgGrid} >
                                        <div className={`item ${styles.item}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[0]})` }} >
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemOne ${styles.itemOne}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[1]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemTwo ${styles.itemTwo}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[2]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemThree ${styles.itemThree}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[3]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemFour ${styles.itemFour}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[4]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>

                                    </div> </>}
                                { isCarProfile?.images?.length >= 3 && <>
                                    <div className={styles.imgGridThree} >
                                        <div className={`item ${styles.item}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[0]})` }} >
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemOne ${styles.itemOne}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[1]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>
                                        <div className={`itemTwo ${styles.itemTwo}`}
                                            style={{ backgroundImage: `url(${isCarProfile?.images[2]})` }}>
                                            <div className={styles.overlay}></div>
                                        </div>
                                    </div>
                                </>}
                            </div>
                            <div className={styles.showMoreImg}>
                                <span>
                                    <img src="images/others/home/category.png" alt="" />
                                    Show More</span>
                            </div>
                        </div>
                    </div>
                </div>
                {/* --------------ContentSec----------------- */}
                <div className={styles.contentSec}>
                    <div className={`container-fluid ${styles.contentGrid}`}>
                        <div className={styles.tripInfo}>
                            <div className={styles.carInfo}>
                                <h3 > {carModel}
                                    {/* <span >2017 </span> */}
                                    <label > {isCarProfile.avgCarRating}
                                        <img src="/images/others/superhost_star.png" alt="" />
                                    </label>
                                </h3>
                                <ul >
                                    <li >{isCarProfile?.totalTrips} Rentals</li>
                                    <li className="ng-star-inserted">
                                        <img src="/images/others/icon/carProfile/rentals_small_pin.svg" alt="" className={styles.pin} />
                                        {isCarProfile?.distance} Km </li>
                                    <li >{isCarProfile?.guestLocationDeliveryPrice == 0 ? "Free Delivery" : "Avl. for Delivery"}  </li>
                                </ul>
                            </div>
                            {/* ---------------TripDate----------- */}
                            <div className={styles.tripDate}>
                                <h4 >Trip Dates</h4>
                                <div className="row">
                                    <div className="col-10">
                                        <ul style={{ display: "block" }}
                                        ><li >
                                                <img src="/images/others/icon/carProfile/trip_dates.svg" alt="" />
                                            </li>
                                            <li > {tripStartShowDate} - {startTime} </li>
                                            <li >
                                                <img src="/images/others/icon/carProfile/next_grey.svg" alt="" />
                                            </li>
                                            <li > {tripEndShowDate} - {endTime} </li>
                                        </ul>
                                    </div>
                                    <div className="col-2 text-right">
                                        <button className={`${styles.edit} text-right`}>
                                            <span className={styles.smallHide}>

                                                <PriceCalendar dateRange={dateRange}
                                                    setDateRange={setDateRange}
                                                    setShowStartDate={setShowStartDate}
                                                    setShowEndDate={setShowEndDate}
                                                    carId={isCarProfile?.id} style={styles} />
                                            </span>
                                            <span className={styles.small}>
                                                <i aria-hidden="true" className="fa fa-pencil-square-o"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {/* ------------pickup--------- */}
                            <div className={styles.pickupAndReturn}>
                                <h4>Pick &amp; Return Location</h4>
                                <div className="row">
                                    <div className="col-10">
                                        <p className="">
                                            <img src="/images/others/icon/carProfile/city_address_icon.svg" alt="" />
                                            {place} </p>
                                    </div>
                                    <div className="col-2 text-right ">
                                        <button className={styles.edit}>

                                            <PickUpLocationModal carId={isCarProfile?.id}
                                                place={place}
                                                estimatePrice={estimatePrice} setEstimatePrice={setEstimatePrice}
                                                style={styles} type="bookingDetail" />
                                            <span className={styles.small}>
                                                <i aria-hidden="true" className="fa fa-pencil-square-o">
                                                </i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {/* --------------Insurance----------- */}
                            <div className={styles.insurance}>
                                <h4 >Insurance Provider</h4>
                                <p >
                                    <img src="/images/others/icon/carProfile/insurance_provider.svg" alt="" />
                                    {isCarProfile?.carInsurances}
                                </p>
                            </div>
                            <div className={styles.include}>
                                <h4 >Km Included</h4>
                                <p >
                                    <img src="/images/others/icon/carProfile/km_icon-1.svg" alt="" />
                                    <span >{isCarProfile?.rentalTerms?.kmIncluded} </span>
                                    Km included - <span >{isCarProfile?.pricePerExtraMile}</span>
                                    SAR Per Extra Km </p>
                            </div>
                            <div className={styles.features}>
                                <ul >
                                    <li >
                                        <img src="/images/others/icon/carProfile/seat_icon.svg" alt="" />
                                        <p >{isCarProfile?.seats} Seats</p>
                                    </li>
                                    <li >
                                        <img src="/images/others/icon/carProfile/door_icon.svg" alt="" />
                                        <p >{isCarProfile?.doors} Doors</p>
                                    </li>
                                    <li >
                                        <img src="/images/others/icon/carProfile/automatic_icon.svg" alt="" />
                                        <p >{isCarProfile?.transmission}</p>
                                    </li>
                                    <li >
                                        <img src="/images/others/icon/carProfile/petrol_icon.svg" alt="" />
                                        <p >{isCarProfile?.rentalTerms?.fuel}</p>
                                    </li>
                                </ul>
                            </div>
                            {/* ----------Host------------ */}
                            <div className={styles.host}>
                                <h4 >Hosted By</h4>
                                <div className={styles.profileSec}>
                                    <div className={styles.userImg}>
                                        <div className={styles.profileImg} style={{ backgroundImage: `url(${hostImage ? hostImage : "/images/others/icon/userProfile.png"})` }}>
                                        </div>
                                        <p > {hostRating}
                                            <img src="/images/others/review_star_small.png" alt="" />
                                        </p>
                                    </div>
                                    <div className={`${styles.paddingLeft} ${styles.hostInfo}`}>
                                        <h4 >{hostName}</h4>
                                        <span >{starTag}</span>
                                        <ul >
                                            <li >{hostTotalTrip} Rentals</li>
                                            <li >Joined {hostJoinDate}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* -------Description------ */}
                            <div className={styles.description}>
                                <h4>Vehicle Description</h4>
                                <div className="row">
                                    <div className="col-10">
                                        <img src="/images/others/icon/carProfile/review.svg" alt="" />
                                        <p > {isCarProfile?.description} </p>
                                    </div>
                                    <div className="col-2 text-right">
                                        <a className={styles.viewAll}>View All</a>
                                    </div>
                                </div>
                            </div>
                            {/* ------------Rental-Block------------- */}
                            {/* <div className={styles.rentalBlock}>
                            <h4> Rental Terms
                                <img src="/images/others/icon/carProfile/red_slider.png" alt="" className="float-right" />
                            </h4>
                        </div> */}
                            <RentalTerm style={styles.rentalBlock} />
                            {/* ------------------Rental-Block----------- */}
                            <div className={styles.rentalBlock}>
                                <ul>
                                    <li>
                                        <h4>
                                            <img src="/images/others/review_star_small.png" alt="" className={styles.star} /> {isCarProfile?.avgCarRating}
                                        </h4>
                                    </li>
                                    <li >
                                        <img src="/images/others/icon/carProfile/rentals_line.svg" alt="" style={{ width: "3px", height: "22px" }} />
                                    </li>
                                    <li>
                                        <h4>{isCarProfile?.carReviewCount} reviews</h4>
                                    </li>
                                </ul>
                            </div>
                            {/* ---------------ProgressBar---------------- */}
                            <div className={styles.ratings}>
                                <ul>
                                    <li>
                                        <span>
                                            Listing accuracy
                                        </span>
                                        <span className={`float-right ${styles.floatRight}`}>
                                            <progressbar max="5" type="danger" className={styles.progress}>
                                                <bar role="progressbar" aria-valuemin="0" className={`progress-bar-danger  progress-bar-animated progress-bar ${styles.progressBar} ${styles.bgDanger}`} aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: "80%" }}>
                                                </bar>
                                            </progressbar>{isCarProfile?.rating?.accuracy} </span>
                                    </li>
                                    <li >
                                        <span>Vehicle condition</span>
                                        <span className={`float-right ${styles.floatRight}`}>
                                            <progressbar max="5" type="danger" className={styles.progress}>
                                                <bar role="progressbar" aria-valuemin="0" className={`progress-bar-danger  progress-bar-animated progress-bar ${styles.progressBar} ${styles.bgDanger}`} aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: "80%" }}>
                                                </bar>
                                            </progressbar>{isCarProfile?.rating?.condition}</span>
                                    </li>
                                    <li >
                                        <span >Value for money</span>
                                        <span className={`float-right ${styles.floatRight}`}>
                                            <progressbar max="5" type="danger" className={styles.progress}>
                                                <bar role="progressbar" aria-valuemin="0" className={`progress-bar-danger  progress-bar-animated progress-bar ${styles.progressBar} ${styles.bgDanger}`} aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: "80%" }}>
                                                </bar>
                                            </progressbar>{isCarProfile?.rating?.value}</span>
                                    </li>
                                    <li >
                                        <span >Cleanliness</span>
                                        <span className={`float-right ${styles.floatRight}`}>
                                            <progressbar max="5" type="danger" className={styles.progress}>
                                                <bar role="progressbar" aria-valuemin="0" className={`progress-bar-danger  progress-bar-animated progress-bar ${styles.progressBar} ${styles.bgDanger}`} aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: "80%" }}>
                                                </bar>
                                            </progressbar>{isCarProfile?.rating?.clean}</span>
                                    </li>
                                    <li >
                                        <span >Communication</span>
                                        <span className={`float-right ${styles.floatRight}`}>
                                            <progressbar max="5" type="danger" className={styles.progress}>
                                                <bar role="progressbar" aria-valuemin="0" className={`progress-bar-danger  progress-bar-animated progress-bar ${styles.progressBar} ${styles.bgDanger}`} aria-valuenow="4" aria-valuetext="80%" style={{ height: "100%", width: "80%" }}>
                                                </bar>
                                            </progressbar>{isCarProfile?.rating?.communication}</span>
                                    </li>
                                </ul>
                            </div>
                            <div className={styles.reviewList}>
                                <h3 >Reviews</h3>
                                <div className={styles.indivisualReview}>
                                    <div className="row">
                                        {
                                            reviews.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="col-12 clearfix">
                                                            <div className={styles.imageSec}>
                                                                <img alt="" src={item.image ? item.image : "/images/others/icon/userProfile.png"} />
                                                            </div>
                                                            <div className={styles.name}>
                                                                <h6 >{item?.fname + " " + item?.lname}</h6>
                                                                <span>{item?.reviewed_at}</span>
                                                            </div>
                                                        </div>
                                                        <div className="col-12">
                                                            <p> {item?.review} </p>
                                                        </div>
                                                    </>
                                                )
                                            })

                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* --------without login----------- */}
                        {!userId &&
                            <div className={styles.box}>
                                <div id="form" className={`${styles.bookingForm} ${styles.bookingloginForm}`}>
                                    <div className={styles.price}>
                                        <h2>109 SAR/DAY</h2>
                                    </div>
                                    <div className={styles.timeSlot} style={{ paddingTop: "0.5rem" }}>
                                        <div className={styles.summary}>
                                            <h5>Rental Summary</h5>
                                        </div>
                                        <div className={`${styles.formGroup2} clearfix`}>
                                            <span className={styles.start}>January 04 - 22:30</span>
                                            <span className={`${styles.floatRight} float-right`} style={{ marginTop: "2px" }}>January 06 - 23:30</span>
                                        </div>
                                        <div className={styles.summary}>
                                            <h5>To Calculate the summary</h5>
                                        </div>

                                        <div className={`${styles.bookBtn} mt-3`}>
                                            <button className="mx-auto"> LOGIN TO PROCEED </button>
                                        </div>
                                        <div className={styles.bookBtn}>
                                            <button className="mx-auto">  SINUP TO PROCEED  </button>
                                        </div>

                                    </div>
                                </div>
                            </div>}
                        {/* --------------end without login--------- */}
                        {/* --------------with login----------- */}
                        <div className={styles.box}>
                            <div id="form" className={styles.bookingForm}>
                                <div className={styles.price}>
                                    <h2>{isCarProfile?.variablePrice} SAR/DAY</h2>
                                </div>
                                <div className={styles.timeSlot} style={{ paddingTop: "0.5rem" }}>
                                    <div className={styles.summary}>
                                        <h5>Rental Summary</h5>
                                    </div>
                                    <div className={`${styles.formGroup2} clearfix`}>
                                        <span className={styles.start}>{tripStartShowDate} - {startTime}</span>
                                        <span className={`${styles.floatRight} float-right`} style={{ marginTop: "2px" }}>{tripEndShowDate} - {endTime}</span>
                                    </div>
                                    {/* <span>
                                    <p className={styles.busycar}>You can not book your own vehicle</p>
                                </span> */}

                                    {code == 422 && message != "Offer does not exists" ?
                                        <span>
                                            <p className={styles.busycar}>  {message}</p>
                                        </span> :

                                        <div>
                                            <div className={`${styles.formGroup} mt-2`}>
                                                <span >{estimatePrice?.totalDays} Days Rental</span>
                                                <span className={`${styles.floatRight} float-right`}>{estimatePrice?.variablePrice} SAR/ Day</span>
                                            </div>
                                            <div className={`${styles.formGroup}`}>
                                                <span >{estimatePrice?.maxKms} KM Total</span>
                                                <span className="float-right red">Included</span>
                                            </div>
                                            <div className={`${styles.formGroup}`}>
                                                <span >Service Fee</span>
                                                <span className={`${styles.floatRight} float-right`}>{estimatePrice?.serviceFee}.00 SAR</span>
                                            </div>
                                            <div className={`${styles.formGroup}`}>
                                                <span >Delivery fee</span>
                                                <span className={`${styles.floatRight} float-right`}>{estimatePrice?.deliveryFee}.00 SAR</span>
                                            </div>
                                            <div className={`${styles.formGroup}`}>
                                                <span >Discount</span>
                                                <span className={`${styles.floatRight} float-right`}>-{estimatePrice?.discount}.00 SAR</span>
                                            </div>
                                            <div className={`${styles.formGroupBold}`}>
                                                <span >Final Price</span>
                                                <span className={`${styles.floatRight} float-right`}>{estimatePrice?.finalPrice}.0 SAR</span>
                                            </div>
                                            {/* <div className={`${styles.formGroupBold} mb-2`}>

                                                <span >Promo Code</span>
                                                <span className={`${styles.floatRight} float-right`} style={{ cursor: "pointer" }}>Promo Code? 
                                                <img src="/images/others/icon/carProfile/apply_code.svg" alt="" /></span>
                                            </div> */}
                                            <PromocodeModal classStyle={styles}
                                                car_Estimate_price={() => { car_Estimate_price(promocode) }}
                                                promocode={promocode} setPromoCode={setPromoCode}
                                            />
                                            <div className={styles.summary}>
                                                <h5 >Payment Method</h5>
                                                <p > You will not be charged until <span >{hostName}</span> accepts your booking request. </p>
                                            </div>
                                            <div className={`${styles.formGroup}`}>
                                                <span >Introduce yourself to <span >{hostName}</span></span>
                                                <textarea cols="30" rows="7" aria-placeholder="Enter here" className={`${styles.formControl} form-control`}></textarea>
                                            </div>
                                            <div className={styles.bookBtn}>
                                                <button className="mx-auto"> BOOK NOW </button>
                                            </div>
                                        </div>}
                                </div>
                            </div>
                        </div>

                    </div>
                    {/* --------NEAR LOCATION OTHER CAR-------------- */}

                    <div className={`${styles.relatedVehicle} mt-5 mb-3`}>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className={styles.heading}> <p>You might also like</p></div>
                                </div>
                                {
                                    nearCar.map((item, index) => {
                                        return (
                                            <div className="col-md-4 col-sm-6">
                                                <div className={styles.carInfo} >
                                                    <div className={styles.fav}>

                                                        <button title="Wishlist" style={{ color: "#fff", cursor: "pointer" }}>
                                                            <img
                                                                alt=""
                                                                src="/images/others/fav.png"
                                                            />
                                                        </button>

                                                    </div>
                                                    <div className={styles.pricetag}>
                                                        <p > {item?.avgCarRating}
                                                            <img src='/images/others/superhost_star.png' className="mx-1" />
                                                            {item?.user?.starTag}
                                                        </p>
                                                    </div>
                                                    <div className={styles.carImg} onClick={(e)=>{nearLocationCarDetails(e,item.id)}}>
                                                        <img src={item.image ? item.image : "/images/others/car_image_one.png"} />
                                                    </div>
                                                    {/* </div> */}
                                                    <div className={styles.carDesc}>
                                                        <div className='row'>
                                                            <div className='col-9'>
                                                                <h5> {item?.make?.title + " " + item.model.title}
                                                                    <span>{item?.year?.title}</span>
                                                                </h5>
                                                                <ul>
                                                                    <li>{item?.totalTrips} Rentals </li>
                                                                    <li><img src="/images/others/icon/rentals_line.svg" alt="" className={styles.saprate} />  </li>
                                                                    <li><img src="/images/others/location_icon.png" alt="" />{item?.distance} KM </li>
                                                                </ul>
                                                            </div>
                                                            <div className={`col-3 pl-0 ${styles.price}`}>
                                                                <h5>{item?.variablePrice}</h5>
                                                                <h6>SAR / Day</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        )
                                    })


                                }


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <SuccessResponseModal open={isOpen} message={message} setSuccessresponse={setOpen} />
        </Fragment>
    )
}

export default BookingProfile
