
import AddNewCar from "../../components/common/Modal/AddCarModal"
import NoCar from "../../components/common/Modal/noCarDataModal"
import styles from "../../styles/carProfile.module.scss"


const MyCar = () => {
  return (
        <div className={`col ${styles.smallCol}`}>
          <div className={styles.NodataSec}>
            <div className={styles.heading}>
              <h3>Welcome to your Garage</h3>
              <p>
                Sharing your vehicle takes only a few minutes and you’ll be ready to
                start earning!
              </p>
            </div>
            <div className={styles.imageSec}>
              <img
                src="/images/others/icon/help/image_one.png"
                alt=""
              />
            </div>
           
            

            <NoCar styles={styles}/>
            
            <div className={styles.formGroup}>
              {/* <AddNewCar text="SHARE YOUR CAR" className={styles.button} /> */}
            </div>
          </div>

        </div>
  )
}

export default MyCar