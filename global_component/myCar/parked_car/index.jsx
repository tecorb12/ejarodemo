import { Fragment, useContext, useEffect, useState } from "react"
import AddNewCar from "../../../components/common/Modal/AddCarModal"
import DeleteModal from "../../../components/common/Modal/delete_modal"
import SharedParkedAction from "../../../components/common/Modal/shared_parked_car_modal"
import SuccessResponseModal from "../../../components/common/Modal/succefully_modal"
import { myCar_service } from "../../../components/helpers/myCar_services"
import styles from "../../../styles/carProfile.module.scss"
import CardSideNav from "../carSideNav"
import { useRouter } from 'next/router';
import { IdContext } from "../../../components/MyContext/IdDetailsContext"


const ParkedCar = () => {
    const [sharedCar, setCarData] = useState([])
    const [shareCount, setSahredCount] = useState(0)
    const [parkedCount, setParkedCount] = useState(0)
    const [successModal, setSuccessresponse] = useState(global.modal? global.modal:false)
    const [message, setMessage] = useState(global.modal ? "Your Car Will Be Published Shortly" : "")
    const router = useRouter()
    const { setCarId, setUserId } = useContext(IdContext)

    useEffect(() => {
        SharedCar()
    }, [])

    const SharedCar = async () => {
        const response = await myCar_service("parked")
        if (response.code == 200) {
            setCarData(response.result)
            setSahredCount(response.shared)
            setParkedCount(response.parked)
        }

    }

    let carData = sharedCar.map((item, index) => {
        let car_title = item.make.title + " " + item.model.title + " " + item.year?.title
        return (

            <div className={`${styles.containers} mb-4`}>
                <div className={`row  no-gutters ${styles.carDetail}`}>
                    <div className={`col  pl-0 ${styles.carImg}`}
                        style={{ backgroundImage: `url(${item.image ? item.image : "/images/others/how-works/ejaro_image.png"})` }}>
                    </div>

                    <div className={`col ${styles.carInfo}`}>
                        <div className={styles.carInfoUpper}>
                            <h4>
                                <span >{car_title}</span >
                                <span className={`float-right text-right ${styles.floatRight}`}
                                >Get Help
                                    <img
                                        src="/images/others/header/contact_support.svg"
                                        alt=""
                                    /></span>
                            </h4>
                            {item.lcPlateNumber && <h5>{item.lcPlateNumber}</h5>}
                            <div className={styles.rating}>
                                <p> {item.avgCarRating}
                                    <img
                                        src="/images/others/superhost_star.png"
                                        alt=""
                                    />
                                </p>
                                <ul>
                                    <li>
                                        <span>{item.totalTrips} Rentals </span>
                                    </li>
                                    <li>
                                        <span>{item.carReviewCount} Reviews</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div style={{ padding: "0 1vw" }} className={styles.smallHide}>
                            <div className="row no-gutters">
                                <div className="col-md-12">
                                    <div className={`${styles.editSec} text-center`}>
                                        <div className="row no-gutters mx-auto">
                                            <div className="col-4" >
                                                <div onClick={() => { setUserId(item.user.id); setCarId(item.id); }}
                                                    className={`${styles.action} ${styles.actionButton} ml-0`}

                                                >
                                                    {item.isInsuranceUploaded && item.docsUploaded ?
                                                        <button onClick={() => { router.push(`/sharedCar/vehicleDetails?id=${item.id}`); }}>EDIT</button>
                                                        :
                                                        <AddNewCar text="Continue" carId={item.id} />
                                                    }

                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div className={`${styles.action} ${styles.actionButton}`}>
                                                    <DeleteModal
                                                        car_list={sharedCar}
                                                        set_car_list={setCarData}
                                                        carId={item.id}
                                                        total_count={parkedCount}
                                                        setCount={setSahredCount}
                                                        text="DELETE" />
                                                </div>

                                            </div>
                                            <div className="col-4">

                                                <div className={`${styles.action} ${styles.actionButton}  ${styles.shareBtn} mr-0`}>
                                                    <SharedParkedAction
                                                        text="SHARE"
                                                        carId={item.id}
                                                        requestFor="L"
                                                        setMessage={setMessage}
                                                        setSuccessresponse={setSuccessresponse}
                                                        title="Do you want to share this car?" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ padding: "0 1vw" }} className={styles.smallShow}>
                    <div className="row no-gutters">
                        <div className="col-md-12">
                            <div className={`${styles.editSec} text-center`}>
                                <div className="row no-gutters mx-auto">
                                    <div className="col-4">
                                        <div className={`${styles.action} ml-0`}>
                                        <div onClick={() => { setUserId(item.user.id); setCarId(item.id);}}
                                                    className={`${styles.action} ${styles.actionButton} ml-0`}

                                                >
                                                   {item.isInsuranceUploaded==false ? 
                                                  <AddNewCar text="Continue" carId={item.id}/>
                                                    :                                                   
                                                      <button onClick={() => { router.push(`/sharedCar/vehicleDetails?id=${item.id}`); }}>EDIT</button>}
                                                
                                                </div>
                                        </div>
                                    </div>
                                    <div className="col-4">
                                        <div className={`${styles.action} ${styles.actionButton}`}>
                                            <p>DELETE</p>
                                        </div>

                                    </div>
                                    <div className="col-4">
                                        <div className={`${styles.action} ${styles.actionButton}  ${styles.shareBtn} mr-0`}>

                                            PARK

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    })
    return (
        <Fragment>
            <section className={`${styles.carProfileSec} user`}>

                <div className={styles.containerMd}>
                    <CardSideNav shareCount={shareCount} parkedCount={parkedCount} />
                    <div className={`col ${styles.smallCol}`}>

                        <div className="tab-content">
                            <div className="tab-pane active" id="tabs-1">
                                <div className={styles.ShareSec}>
                                    {carData}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <SuccessResponseModal message={message} open={successModal} setSuccessresponse={setSuccessresponse} />

        </Fragment>
    )

}

export default ParkedCar