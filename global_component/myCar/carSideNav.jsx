import { Fragment } from "react"
import AddNewCar from "../../components/common/Modal/AddCarModal"
import styles from "../../styles/carProfile.module.scss"
import { useRouter } from "next/router";
import Routes from "../../components/common/route";
import Link from 'next/link';

const LinkTag = ({ activeClass, as, href, iconPath, name, count }) => {
    return (
        <Fragment>
            <Link as={as} href={href} >
                <li className={activeClass}>

                    <a href="#tabs-1" role="tab" data-toggle="pill">
                        {name}

                        <span>{count}
                            <img src={iconPath} style={{ marginLeft: " 8px" }} />
                        </span></a>
                </li>
            </Link>

        </Fragment>
    )
}
const CardSideNav = ({ shareCount, parkedCount }) => {
    let { pathname } = useRouter();
    let shared_car = pathname === Routes.myCar.SharedCar.as;
    let parked_car = pathname === Routes.myCar.ParkedCar.as;

    return (
        <Fragment>
            <div className={styles.showStatus}>
                <div className={styles.leftMenu}>
                    <h2 >My Cars</h2>
                    <ul className={`nav nav-pills nav-stacked ${styles.NavSec}`} role="tablist">
                        <LinkTag
                            activeClass={shared_car ? styles.active : ""}
                            as={Routes.myCar.SharedCar.as}
                            href={Routes.myCar.SharedCar.href}
                            iconPath={shared_car ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png"}
                            iconAlt="Shared"
                            count={parkedCount}
                            name="Shared" />
                        <LinkTag
                            activeClass={parked_car ? styles.active : ""}
                            as={Routes.myCar.ParkedCar.as}
                            href={Routes.myCar.ParkedCar.href}
                            count={shareCount}
                            iconPath={parked_car ? "/images/others/icon/carProfile/red_slider.png" : "/images/others/icon/carProfile/grey_next.png"}
                            iconAlt="Parked"
                            name="Parked" />

                       
                    </ul>
                    <AddNewCar text="Add a new car" />
                </div>
            </div>
        </Fragment>
    )
}
export default CardSideNav